<?php

namespace Terminalbd\ProcurementBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Terminalbd\ProcurementBundle\Entity\Requisition;

class RequisitionListener
{
    public function prePersist(LifecycleEventArgs $args)
    {
        $this->createCode($args);
    }

    public function createCode(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        // perhaps you only want to act on some "Sales" entity
        if ($entity instanceof Requisition) {

            $datetime = new \DateTime("now");
            $lastCode = $this->getLastCode($args, $datetime, $entity);
            $entity->setCode($lastCode+1);
            if($entity->getRequisitionType()){
                $type = $entity->getRequisitionType()->getCode();
                $entity->setRequisitionNo(sprintf("%s%s%s","{$type}-", $datetime->format('my'), str_pad($entity->getCode(),4, '0', STR_PAD_LEFT)));
            }else{
                if($entity->getRequisitionMode() == "CAPEX") {
                    $entity->setRequisitionNo(sprintf("%s%s%s", "CPX-", $datetime->format('my'), str_pad($entity->getCode(), 4, '0', STR_PAD_LEFT)));
                }elseif($entity->getRequisitionMode() == "Expense"){
                    $entity->setRequisitionNo(sprintf("%s%s%s","EXP-", $datetime->format('my'), str_pad($entity->getCode(),4, '0', STR_PAD_LEFT)));
                }else{
                    $entity->setRequisitionNo(sprintf("%s%s%s","OPX-", $datetime->format('my'), str_pad($entity->getCode(),4, '0', STR_PAD_LEFT)));
                }
            }
        }
    }

    /**
     * @param LifecycleEventArgs $args
     * @param $datetime
     * @param $entity
     * @return int|mixed
     */
    public function getLastCode(LifecycleEventArgs $args, $datetime, $entity)
    {
        $today_startdatetime = $datetime->format('Y-m-01 00:00:00');
        $today_enddatetime = $datetime->format('Y-m-t 23:59:59');

        $entityManager = $args->getEntityManager();
        $qb = $entityManager->getRepository(Requisition::class)->createQueryBuilder('s');

        $qb
            ->select('MAX(s.code)')
            ->where('s.config = :config')
            ->andWhere('s.updated >= :today_startdatetime')
            ->andWhere('s.updated <= :today_enddatetime')
            ->setParameter('config', $entity->getConfig())
            ->setParameter('today_startdatetime', $today_startdatetime)
            ->setParameter('today_enddatetime', $today_enddatetime);
        $lastCode = $qb->getQuery()->getSingleScalarResult();

        if (empty($lastCode)) {
            return 0;
        }

        return $lastCode;
    }
}