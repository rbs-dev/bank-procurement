<?php

namespace Terminalbd\ProcurementBundle\Entity;
use App\Entity\Application\Procurement;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\ProcurementBundle\Repository\ProcurementProcessRepository")
 * @ORM\Table(name="procu_process_level")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ProcurementProcessLevel
{

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;


    /**
     * @var Procurement
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\Procurement")
     */
    private $config;


    /**
     * @var Particular
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\Particular", inversedBy="requisition")
     */
    private $procurementProcess;

    /**
     * @var Particular
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\Particular", inversedBy="requisition")
     */
    private $processDesignation;

    /**
     * @var Particular
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\Particular", inversedBy="requisition")
     */
    private $releverDesignation;


    /**
     * @var integer
     *
     * @ORM\Column(type="integer",nullable=true)
     */
    private $ordering;



    /**
     * @var bool
     *
     * @ORM\Column(type="boolean",nullable=true)
     */
    private $isMandatory;


    /**
     * @var bool
     *
     * @ORM\Column(type="boolean",nullable=true)
     */
    private $isRejected;


    /**
     * @var bool
     *
     * @ORM\Column(type="boolean",nullable=true)
     */
    private $isAcknowledge;


     /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $process;


     /**
     * @var string
     *
     * @ORM\Column(type="text",nullable=true)
     */
    private $comment;


    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }



    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
    }


    /**
     * @return Procurement
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param Procurement $config
     */
    public function setConfig(Procurement $config)
    {
        $this->config = $config;
    }

    /**
     * @return Requisition
     */
    public function getRequisition()
    {
        return $this->requisition;
    }

    /**
     * @param Requisition $requisition
     */
    public function setRequisition(Requisition $requisition)
    {
        $this->requisition = $requisition;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return string
     */
    public function getProcess()
    {
        return $this->process;
    }

    /**
     * @param string $process
     */
    public function setProcess(string $process)
    {
        $this->process = $process;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return Particular
     */
    public function getProcurementProcess(): Particular
    {
        return $this->procurementProcess;
    }

    /**
     * @param Particular $procurementProcess
     */
    public function setProcurementProcess(Particular $procurementProcess): void
    {
        $this->procurementProcess = $procurementProcess;
    }

    /**
     * @return Particular
     */
    public function getProcessDesignation(): Particular
    {
        return $this->processDesignation;
    }

    /**
     * @param Particular $processDesignation
     */
    public function setProcessDesignation(Particular $processDesignation): void
    {
        $this->processDesignation = $processDesignation;
    }

    /**
     * @return int
     */
    public function getOrdering(): int
    {
        return $this->ordering;
    }

    /**
     * @param int $ordering
     */
    public function setOrdering(int $ordering): void
    {
        $this->ordering = $ordering;
    }

    /**
     * @return bool
     */
    public function isMandatory(): bool
    {
        return $this->isMandatory;
    }

    /**
     * @param bool $isMandatory
     */
    public function setIsMandatory(bool $isMandatory): void
    {
        $this->isMandatory = $isMandatory;
    }

    /**
     * @return bool
     */
    public function isRejected(): bool
    {
        return $this->isRejected;
    }

    /**
     * @param bool $isRejected
     */
    public function setIsRejected(bool $isRejected): void
    {
        $this->isRejected = $isRejected;
    }

    /**
     * @return bool
     */
    public function isAcknowledge(): bool
    {
        return $this->isAcknowledge;
    }

    /**
     * @param bool $isAcknowledge
     */
    public function setIsAcknowledge(bool $isAcknowledge): void
    {
        $this->isAcknowledge = $isAcknowledge;
    }

    /**
     * @return Particular
     */
    public function getReleverDesignation(): Particular
    {
        return $this->releverDesignation;
    }

    /**
     * @param Particular $releverDesignation
     */
    public function setReleverDesignation(Particular $releverDesignation): void
    {
        $this->releverDesignation = $releverDesignation;
    }



}
