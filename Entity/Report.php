<?php

namespace Terminalbd\ProcurementBundle\Entity;

use App\Entity\Application\GenericMaster;
use App\Entity\Application\Procurement;
use App\Entity\Application\SecurityBilling;
use App\Entity\Core\Customer;
use App\Entity\Admin\Location;
use Terminalbd\ProcurementBundleBundle\Repository\ParticularRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\ProcurementBundle\Repository\ReportRepository")
 * @ORM\Table(name="procu_report")
 * @UniqueEntity(fields={"slug","config"}, message="This particular slug must be unique")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class Report
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;


}
