<?php

namespace Terminalbd\ProcurementBundle\Entity;

use App\Entity\Application\SecurityBilling;
use App\Entity\Core\Customer;
use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Terminalbd\GenericBundle\Entity\Item;
use Terminalbd\GenericBundle\Entity\ItemBrand;
use Terminalbd\GenericBundle\Entity\ItemColor;
use Terminalbd\GenericBundle\Entity\ItemSize;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\InventoryBundle\Entity\StockBook;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\ProcurementBundle\Repository\RequisitionIssueItemRepository")
 * @ORM\Table(name="procu_requisition_issue_item")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class RequisitionIssueItem
{

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;



    /**
     * @var RequisitionIssue
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\RequisitionIssue",inversedBy="requisitionItems")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $requisition;


    /**
     * @var RequisitionItemHistory
     *
     * @ORM\OneToMany(targetEntity="Terminalbd\ProcurementBundle\Entity\RequisitionItemHistory",mappedBy="requisitionItem")
     * @ORM\OrderBy({"id" = "DESC"})
     */
    private $requisitionItemHistory;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $issueUser;


    /**
     * @var Item
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\GenericBundle\Entity\Item")
     */
    private $item;

    /**
     * @var Particular
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\Particular")
     */
    private $line;

    /**
     * @var Particular
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\Particular")
     */
    private $section;


     /**
     * @var Particular
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\Particular")
     */
    private $machineType;


    /**
     * @var Stock
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Stock")
     */
    private $stock;

    /**
     * @var StockBook
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\StockBook")
     */
    private $stockBook;



    /**
     * @var ItemBrand
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\GenericBundle\Entity\ItemBrand")
     */
    private $brand;


    /**
     * @var ItemSize
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\GenericBundle\Entity\ItemSize")
     */
    private $size;


    /**
     * @var ItemColor
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\GenericBundle\Entity\ItemColor")
     */
    private $color;


    /**
     * @var RequisitionItem
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\RequisitionItem")
     */
    private $lastRequisitionItem;


    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $name;

     /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $issueType;


    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $unit;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $mpNo;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $description;


    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $actualQuantity=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $returnQuantity;

     /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $quantity;

     /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $issueQuantity;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $stockIn = 0;


    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $subTotal=0;


     /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $price=0;


    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return string
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @param string $unit
     */
    public function setUnit( $unit)
    {
        $this->unit = $unit;
    }

    /**
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param string $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return SecurityBilling
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param SecurityBilling $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @return float
     */
    public function getSubTotal()
    {
        return $this->subTotal;
    }

    /**
     * @param float $subTotal
     */
    public function setSubTotal( $subTotal)
    {
        $this->subTotal = $subTotal;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param float $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }


    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }


    /**
     * @return Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param Item $item
     */
    public function setItem($item)
    {
        $this->item = $item;
    }

    /**
     * @return Stock
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param Stock $stock
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return float
     */
    public function getActualQuantity()
    {
        return $this->actualQuantity;
    }

    /**
     * @param float $actualQuantity
     */
    public function setActualQuantity($actualQuantity)
    {
        $this->actualQuantity = $actualQuantity;
    }

    /**
     * @return float
     */
    public function getStockIn()
    {
        return $this->stockIn;
    }

    /**
     * @param float $stockIn
     */
    public function setStockIn($stockIn)
    {
        $this->stockIn = $stockIn;
    }

    /**
     * @return RequisitionItem
     */
    public function getLastRequisitionItem()
    {
        return $this->lastRequisitionItem;
    }

    /**
     * @param RequisitionItem $lastRequisitionItem
     */
    public function setLastRequisitionItem($lastRequisitionItem)
    {
        $this->lastRequisitionItem = $lastRequisitionItem;
    }

    /**
     * @return Requisition
     */
    public function getRequisitionItemHistory(): Requisition
    {
        return $this->requisitionItemHistory;
    }


    /**
     * @return float
     */
    public function getReturnQuantity()
    {
        return $this->returnQuantity;
    }

    /**
     * @param float $returnQuantity
     */
    public function setReturnQuantity($returnQuantity)
    {
        $this->returnQuantity = $returnQuantity;
    }

    /**
     * @return ItemBrand
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param ItemBrand $brand
     */
    public function setBrand(ItemBrand $brand)
    {
        $this->brand = $brand;
    }

    /**
     * @return ItemSize
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param ItemSize $size
     */
    public function setSize(ItemSize $size)
    {
        $this->size = $size;
    }

    /**
     * @return ItemColor
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param ItemColor $color
     */
    public function setColor( $color)
    {
        $this->color = $color;
    }

    /**
     * @return Particular
     */
    public function getLine()
    {
        return $this->line;
    }

    /**
     * @param Particular $line
     */
    public function setLine($line)
    {
        $this->line = $line;
    }

    /**
     * @return Particular
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * @param Particular $section
     */
    public function setSection( $section)
    {
        $this->section = $section;
    }

    /**
     * @return RequisitionIssue
     */
    public function getRequisition()
    {
        return $this->requisition;
    }

    /**
     * @param RequisitionIssue $requisition
     */
    public function setRequisition(RequisitionIssue $requisition)
    {
        $this->requisition = $requisition;
    }

    /**
     * @return string
     */
    public function getMpNo()
    {
        return $this->mpNo;
    }

    /**
     * @param string $mpNo
     */
    public function setMpNo($mpNo)
    {
        $this->mpNo = $mpNo;
    }

    /**
     * @return int
     */
    public function getLastPurchaseQuantity(): int
    {
        return $this->lastPurchaseQuantity;
    }

    /**
     * @param int $lastPurchaseQuantity
     */
    public function setLastPurchaseQuantity(int $lastPurchaseQuantity)
    {
        $this->lastPurchaseQuantity = $lastPurchaseQuantity;
    }

    /**
     * @return float
     */
    public function getLastPurchasePrice()
    {
        return $this->lastPurchasePrice;
    }

    /**
     * @param float $lastPurchasePrice
     */
    public function setLastPurchasePrice(float $lastPurchasePrice)
    {
        $this->lastPurchasePrice = $lastPurchasePrice;
    }

    /**
     * @return \DateTime
     */
    public function getLastPurchaseDate()
    {
        return $this->lastPurchaseDate;
    }

    /**
     * @param \DateTime $lastPurchaseDate
     */
    public function setLastPurchaseDate($lastPurchaseDate)
    {
        $this->lastPurchaseDate = $lastPurchaseDate;
    }

    /**
     * @return Particular
     */
    public function getMachineType()
    {
        return $this->machineType;
    }

    /**
     * @param Particular $machineType
     */
    public function setMachineType( $machineType)
    {
        $this->machineType = $machineType;
    }

    /**
     * @return float
     */
    public function getIssueQuantity()
    {
        return $this->issueQuantity;
    }

    /**
     * @param float $issueQuantity
     */
    public function setIssueQuantity( $issueQuantity)
    {
        $this->issueQuantity = $issueQuantity;
    }



    /**
     * @return string
     */
    public function getIssueType()
    {
        return $this->issueType;
    }

    /**
     * @param string $issueType
     */
    public function setIssueType( $issueType)
    {
        $this->issueType = $issueType;
    }

    /**
     * @return User
     */
    public function getIssueUser()
    {
        return $this->issueUser;
    }

    /**
     * @param User $issueUser
     */
    public function setIssueUser(User $issueUser)
    {
        $this->issueUser = $issueUser;
    }

    /**
     * @return StockBook
     */
    public function getStockBook()
    {
        return $this->stockBook;
    }

    /**
     * @param StockBook $stockBook
     */
    public function setStockBook($stockBook)
    {
        $this->stockBook = $stockBook;
    }



}
