<?php

namespace Terminalbd\ProcurementBundle\Entity;

use App\Entity\Application\Procurement;
use App\Entity\Core\Setting;
use App\Entity\Domain\Branch;
use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\ModuleProcessItem;
use App\Entity\Domain\Vendor;
use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Xiidea\EasyAuditBundle\Annotation\SubscribeDoctrineEvents;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\ProcurementBundle\Repository\TenderMemoRepository")
 * @ORM\Table(name="procu_tender_memo")
 * @UniqueEntity(fields={"tenderComparative"}, message="This invoice already used")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class TenderMemo
{

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @var Procurement
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\Procurement")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $config;

     /**
     * @var ModuleProcess
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain\ModuleProcess")
     */
    private $moduleProcess;

    /**
     * @var ProcurementCondition
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\ProcurementCondition")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $condition;

    /**
     * @var TenderConditionItem
     *
     * @ORM\OneToMany(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderConditionItem", mappedBy="tenderMemo")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $conditionItems;


    /**
     * @var ProcurementProcess
     *
     * @ORM\OneToOne(targetEntity="ProcurementProcess")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="approveProcess_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     * })
     */
    private $approveProcess;


    /**
     * @var TenderComparative
     *
     * @ORM\OneToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderComparative")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tenderComparative_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     * })
     */
    private $tenderComparative;

    /**
     * @ORM\OneToOne(targetEntity="TenderMemo", inversedBy="children")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     * })
     */
    private $parent;

    /**
     * @var TenderVendor
     * @ORM\ManyToMany(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderVendor",inversedBy="tenderMemo")
     **/
    private $approvedVendor;

    /**
     * @var TenderWorkorder
     * @ORM\OneToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderWorkorder", mappedBy="managementMemo")
     **/
    private $workorder;

    /**
     * @var TenderMemoComment
     *
     * @ORM\OneToMany(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderMemoComment", cascade={"persist", "remove"}, mappedBy="memo")
     */
    private $comments;



    /**
     * @ORM\OneToOne(targetEntity="TenderMemo" , mappedBy="parent")
     **/
    private $children;


    /**
     * @var Tender
     * @ORM\OneToOne(targetEntity="Tender")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tender_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     * })
     */
    private $tender;


     /**
     * @var Setting
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Setting")
     **/
    private $vendorType;


    /**
     * @var TenderMemoItem
     *
     * @ORM\OneToMany(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderMemoItem", mappedBy="memo")
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $memoItems;

    /**
     * @var TenderMemoUser
     *
     * @ORM\OneToMany(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderMemoUser", mappedBy="memo")
     * @ORM\OrderBy({"ordering" = "ASC"})
     */
    private $memoUsers;

     /**
     * @var TenderCommittee
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderCommittee" , inversedBy="memos")
     * @ORM\JoinColumn(name="tenderCommittee_id", referencedColumnName="id", onDelete="SET NULL")
     * @ORM\OrderBy({"ordering" = "ASC"})
     */
    private $tenderCommittee;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $tenderAmount = 0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $tenderBudget = 0;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $createdBy;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $reportTo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $approveTo;

    /**
     * @var integer
     * @ORM\Column(type="integer",nullable=true)
     */
    private $code = 0;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $invoice;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $subject;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $financialDelegation;

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $content;

     /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $footerContent;

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $recommendationt;

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $customContentOne;

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $customContentTwo;

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $customContentThree;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $process='New';

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $module ="purchase-memo";

    /**
     * @var integer
     * @ORM\Column(type="integer",nullable=true)
     */
    private $processOrdering = 0;


    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $waitingProcess;


     /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $comment;


     /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status = true;

      /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $showItemList = false;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isDelete = false;


    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Procurement
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param Procurement $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @return ModuleProcess
     */
    public function getModuleProcess()
    {
        return $this->moduleProcess;
    }

    /**
     * @param ModuleProcess $moduleProcess
     */
    public function setModuleProcess($moduleProcess)
    {
        $this->moduleProcess = $moduleProcess;
    }

    /**
     * @return TenderMemoItem
     */
    public function getMemoItems()
    {
        return $this->memoItems;
    }

    /**
     * @return TenderMemoUser
     */
    public function getMemoUsers()
    {
        return $this->memoUsers;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return mixed
     */
    public function getReportTo()
    {
        return $this->reportTo;
    }

    /**
     * @param mixed $reportTo
     */
    public function setReportTo($reportTo)
    {
        $this->reportTo = $reportTo;
    }

    /**
     * @return mixed
     */
    public function getApproveTo()
    {
        return $this->approveTo;
    }

    /**
     * @param mixed $approveTo
     */
    public function setApproveTo($approveTo)
    {
        $this->approveTo = $approveTo;
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @param int $code
     */
    public function setCode(int $code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * @param string $invoice
     */
    public function setInvoice($invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return string
     */
    public function getFinancialDelegation()
    {
        return $this->financialDelegation;
    }

    /**
     * @param string $financialDelegation
     */
    public function setFinancialDelegation($financialDelegation)
    {
        $this->financialDelegation = $financialDelegation;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getProcess()
    {
        return $this->process;
    }

    /**
     * @param string $process
     */
    public function setProcess($process)
    {
        $this->process = $process;
    }

    /**
     * @return string
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @param string $module
     */
    public function setModule($module)
    {
        $this->module = $module;
    }

    /**
     * @return string
     */
    public function getWaitingProcess()
    {
        return $this->waitingProcess;
    }

    /**
     * @param string $waitingProcess
     */
    public function setWaitingProcess($waitingProcess)
    {
        $this->waitingProcess = $waitingProcess;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return TenderComparative
     */
    public function getTenderComparative()
    {
        return $this->tenderComparative;
    }

    /**
     * @param TenderComparative $tenderComparative
     */
    public function setTenderComparative($tenderComparative)
    {
        $this->tenderComparative = $tenderComparative;
    }

    /**
     * @return string
     */
    public function getRecommendationt()
    {
        return $this->recommendationt;
    }

    /**
     * @param string $recommendationt
     */
    public function setRecommendationt($recommendationt)
    {
        $this->recommendationt = $recommendationt;
    }

    /**
     * @return string
     */
    public function getCustomContentOne()
    {
        return $this->customContentOne;
    }

    /**
     * @param string $customContentOne
     */
    public function setCustomContentOne($customContentOne)
    {
        $this->customContentOne = $customContentOne;
    }

    /**
     * @return string
     */
    public function getCustomContentTwo()
    {
        return $this->customContentTwo;
    }

    /**
     * @param string $customContentTwo
     */
    public function setCustomContentTwo($customContentTwo)
    {
        $this->customContentTwo = $customContentTwo;
    }

    /**
     * @return string
     */
    public function getCustomContentThree()
    {
        return $this->customContentThree;
    }

    /**
     * @param string $customContentThree
     */
    public function setCustomContentThree($customContentThree)
    {
        $this->customContentThree = $customContentThree;
    }

    /**
     * @return TenderCommittee
     */
    public function getTenderCommittee()
    {
        return $this->tenderCommittee;
    }

    /**
     * @param TenderCommittee $tenderCommittee
     */
    public function setTenderCommittee($tenderCommittee)
    {
        $this->tenderCommittee = $tenderCommittee;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param mixed $children
     */
    public function setChildren($children)
    {
        $this->children = $children;
    }

    /**
     * @return ProcurementProcess
     */
    public function getApproveProcess()
    {
        return $this->approveProcess;
    }

    /**
     * @param ProcurementProcess $approveProcess
     */
    public function setApproveProcess($approveProcess)
    {
        $this->approveProcess = $approveProcess;
    }

    /**
     * @return int
     */
    public function getProcessOrdering()
    {
        return $this->processOrdering;
    }

    /**
     * @param int $processOrdering
     */
    public function setProcessOrdering($processOrdering)
    {
        $this->processOrdering = $processOrdering;
    }

    /**
     * @return TenderVendor
     */
    public function getApprovedVendor()
    {
        return $this->approvedVendor;
    }

    /**
     * @param TenderVendor $approvedVendor
     */
    public function setApprovedVendor($approvedVendor)
    {
        $this->approvedVendor = $approvedVendor;
    }

    /**
     * @return float
     */
    public function getTenderAmount()
    {
        return $this->tenderAmount;
    }

    /**
     * @param float $tenderAmount
     */
    public function setTenderAmount($tenderAmount)
    {
        $this->tenderAmount = $tenderAmount;
    }

    /**
     * @return float
     */
    public function getTenderBudget()
    {
        return $this->tenderBudget;
    }

    /**
     * @param float $tenderBudget
     */
    public function setTenderBudget($tenderBudget)
    {
        $this->tenderBudget = $tenderBudget;
    }

    /**
     * @return TenderWorkorder
     */
    public function getWorkorder()
    {
        return $this->workorder;
    }

    /**
     * @return mixed
     */
    public function getTender()
    {
        return $this->tender;
    }

    /**
     * @param mixed $tender
     */
    public function setTender($tender)
    {
        $this->tender = $tender;
    }

    /**
     * @return Setting
     */
    public function getVendorType()
    {
        return $this->vendorType;
    }

    /**
     * @param Setting $vendorType
     */
    public function setVendorType($vendorType)
    {
        $this->vendorType = $vendorType;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return ProcurementCondition
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * @param ProcurementCondition $condition
     */
    public function setCondition($condition)
    {
        $this->condition = $condition;
    }

    /**
     * @return TenderConditionItem
     */
    public function getConditionItems()
    {
        return $this->conditionItems;
    }

    /**
     * @return TenderMemoComment
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @return string
     */
    public function getFooterContent()
    {
        return $this->footerContent;
    }

    /**
     * @param string $footerContent
     */
    public function setFooterContent($footerContent)
    {
        $this->footerContent = $footerContent;
    }

    /**
     * @return bool
     */
    public function isShowItemList()
    {
        return $this->showItemList;
    }

    /**
     * @param bool $showItemList
     */
    public function setShowItemList( $showItemList)
    {
        $this->showItemList = $showItemList;
    }











}
