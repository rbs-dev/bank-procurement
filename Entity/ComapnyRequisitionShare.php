<?php

namespace Terminalbd\ProcurementBundle\Entity;

use App\Entity\Application\Procurement;
use App\Entity\Domain\Branch;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints\Date;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\ProcurementBundle\Repository\ComapnyRequisitionShareRepository")
 * @ORM\Table(name="procu_company_requisition_share")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ComapnyRequisitionShare
{

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @var Procurement
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\Procurement")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $config;


    /**
     * @var Requisition
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\Requisition", inversedBy="comapnyRequisitionShares")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $requisition;

     /**
     * @var JobRequisition
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\JobRequisition",inversedBy="comapnyRequisitionShares")
      * @ORM\JoinColumn(onDelete="CASCADE")
      */
    private $jobRequisition;

    /**
     * @var Branch
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain\Branch")
     */
    private $branch;


    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $branchShare = 0;


    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $amount = 0;


    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Branch
     */
    public function getBranch()
    {
        return $this->branch;
    }

    /**
     * @param Branch $branch
     */
    public function setBranch($branch)
    {
        $this->branch = $branch;
    }

    /**
     * @return Procurement
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param Procurement $config
     */
    public function setConfig(Procurement $config)
    {
        $this->config = $config;
    }

    /**
     * @return Requisition
     */
    public function getRequisition()
    {
        return $this->requisition;
    }

    /**
     * @param Requisition $requisition
     */
    public function setRequisition(Requisition $requisition)
    {
        $this->requisition = $requisition;
    }

    /**
     * @return float
     */
    public function getBranchShare()
    {
        return $this->branchShare;
    }

    /**
     * @param float $branchShare
     */
    public function setBranchShare($branchShare)
    {
        $this->branchShare = $branchShare;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated(\DateTime $updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return JobRequisition
     */
    public function getJobRequisition(): JobRequisition
    {
        return $this->jobRequisition;
    }

    /**
     * @param JobRequisition $jobRequisition
     */
    public function setJobRequisition(JobRequisition $jobRequisition): void
    {
        $this->jobRequisition = $jobRequisition;
    }


}
