<?php

namespace Terminalbd\ProcurementBundle\Entity;

use App\Entity\Application\Procurement;
use App\Entity\Domain\Branch;
use App\Entity\Domain\ModuleProcess;
use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\ProcurementBundle\Repository\TenderWorkorderRepository")
 * @ORM\Table(name="procu_tender_workorder")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class TenderWorkorder
{

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @var Procurement
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\Procurement")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $config;


    /**
     * @var ProcurementProcess
     *
     * @ORM\OneToOne(targetEntity="ProcurementProcess")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="approveProcess_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     * })
     */
    private $approveProcess;


    /**
     * @var TenderComparative
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderComparative", inversedBy="workorder")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $tenderComparative;

      /**
     * @var Requisition
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\Requisition", inversedBy="workorders")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $requisition;

     /**
     * @var TenderMemo
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderMemo", inversedBy="workorder")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $managementMemo;

    /**
     * @var ProcurementCondition
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\ProcurementCondition")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $condition;

    /**
     * @var TenderConditionItem
     * @ORM\OneToMany(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderConditionItem", mappedBy="workorder")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $conditionItems;

    /**
     * @var TenderWorkorderItem
     * @ORM\OneToMany(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderWorkorderItem", mappedBy="tenderWorkorder")
     */
    private $workOrderItems;


    /**
     * @var TenderVendor
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderVendor")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $tenderVendor;

    /**
     * @var Branch
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain\Branch")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $shipTo;

    /**
     * @var Branch
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain\Branch")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $wearhouse;

    /**
     * @var Branch
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain\Branch")
     */
    private $branch;

    /**
     * @var ModuleProcess
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain\ModuleProcess")
     */
    private $moduleProcess;


    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $createdBy;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $reportTo;


     /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $approveTo;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $transferTo;


    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $subTotal=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $total=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $discount=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $vat=0;


    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $tax=0;


    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $ait=0;


    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $vendorCommissionPercent=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $vendorCommission=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $shippingCharge=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $grandTotal=0;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $attendTo;

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $subject;

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $comment;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $workorderMode = "PO-01";


    /**
     * @var integer
     * @ORM\Column(type="integer",nullable=true)
     */
    private $code=0;

    /**
     * @var integer
     * @ORM\Column(type="integer",nullable=true)
     */
    private $processOrdering = 0;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $invoice;


    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $businessGroup;


    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $process='New';

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $module ="work-order";


    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $waitingProcess;

    /**
     * @var Particular
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\Particular")
     */
    private $paymentMode;

    /**
     * @var EnlistedVendor
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\EnlistedVendor")
     */
    private $enlistedVendor;

    /**
     * @var Particular
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\Particular")
     */
    private $deliveryMode;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $workorderDate;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $validateDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deliveryDate;

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $content;

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $footerContent;

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $terms;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status = true;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $recurring = false;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isDelete = false;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $partialPayment = true;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $partialDelivery = true;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Procurement
     */
    public function getConfig(): Procurement
    {
        return $this->config;
    }

    /**
     * @param Procurement $config
     */
    public function setConfig(Procurement $config)
    {
        $this->config = $config;
    }

    /**
     * @return TenderComparative
     */
    public function getTenderComparative()
    {
        return $this->tenderComparative;
    }

    /**
     * @param TenderComparative $tenderComparative
     */
    public function setTenderComparative(TenderComparative $tenderComparative)
    {
        $this->tenderComparative = $tenderComparative;
    }


    /**
     * @return TenderVendor
     */
    public function getTenderVendor()
    {
        return $this->tenderVendor;
    }

    /**
     * @param TenderVendor $tenderVendor
     */
    public function setTenderVendor($tenderVendor)
    {
        $this->tenderVendor = $tenderVendor;
    }


    /**
     * @return ModuleProcess
     */
    public function getModuleProcess()
    {
        return $this->moduleProcess;
    }

    /**
     * @param ModuleProcess $moduleProcess
     */
    public function setModuleProcess($moduleProcess)
    {
        $this->moduleProcess = $moduleProcess;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }


    /**
     * @return mixed
     */
    public function getReportTo()
    {
        return $this->reportTo;
    }

    /**
     * @param mixed $reportTo
     */
    public function setReportTo($reportTo)
    {
        $this->reportTo = $reportTo;
    }

    /**
     * @return mixed
     */
    public function getTransferTo()
    {
        return $this->transferTo;
    }

    /**
     * @param mixed $transferTo
     */
    public function setTransferTo($transferTo)
    {
        $this->transferTo = $transferTo;
    }

    /**
     * @return float
     */
    public function getSubTotal()
    {
        return $this->subTotal;
    }

    /**
     * @param float $subTotal
     */
    public function setSubTotal($subTotal)
    {
        $this->subTotal = $subTotal;
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @param int $code
     */
    public function setCode(int $code)
    {
        $this->code = $code;
    }

    /**
     * @return int
     */
    public function getProcessOrdering(): int
    {
        return $this->processOrdering;
    }

    /**
     * @param int $processOrdering
     */
    public function setProcessOrdering(int $processOrdering)
    {
        $this->processOrdering = $processOrdering;
    }

    /**
     * @return string
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * @param string $invoice
     */
    public function setInvoice($invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * @return string
     */
    public function getBusinessGroup()
    {
        return $this->businessGroup;
    }

    /**
     * @param string $businessGroup
     */
    public function setBusinessGroup($businessGroup)
    {
        $this->businessGroup = $businessGroup;
    }

    /**
     * @return string
     */
    public function getProcess()
    {
        return $this->process;
    }

    /**
     * @param string $process
     */
    public function setProcess($process)
    {
        $this->process = $process;
    }

    /**
     * @return string
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @param string $module
     */
    public function setModule($module)
    {
        $this->module = $module;
    }

    /**
     * @return string
     */
    public function getWaitingProcess()
    {
        return $this->waitingProcess;
    }

    /**
     * @param string $waitingProcess
     */
    public function setWaitingProcess($waitingProcess)
    {
        $this->waitingProcess = $waitingProcess;
    }

    /**
     * @return Particular
     */
    public function getPaymentMode()
    {
        return $this->paymentMode;
    }

    /**
     * @param Particular $paymentMode
     */
    public function setPaymentMode( $paymentMode)
    {
        $this->paymentMode = $paymentMode;
    }

    /**
     * @return Particular
     */
    public function getDeliveryMode()
    {
        return $this->deliveryMode;
    }

    /**
     * @param Particular $deliveryMode
     */
    public function setDeliveryMode( $deliveryMode)
    {
        $this->deliveryMode = $deliveryMode;
    }



    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated( $updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return TenderWorkorderItem
     */
    public function getWorkOrderItems()
    {
        return $this->workOrderItems;
    }

    /**
     * @return EnlistedVendor
     */
    public function getEnlistedVendor()
    {
        return $this->enlistedVendor;
    }

    /**
     * @param EnlistedVendor $enlistedVendor
     */
    public function setEnlistedVendor(EnlistedVendor $enlistedVendor)
    {
        $this->enlistedVendor = $enlistedVendor;
    }

    /**
     * @return bool
     */
    public function isRecurring()
    {
        return $this->recurring;
    }

    /**
     * @param bool $recurring
     */
    public function setRecurring($recurring)
    {
        $this->recurring = $recurring;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param float $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return float
     */
    public function getVendorCommissionPercent()
    {
        return $this->vendorCommissionPercent;
    }

    /**
     * @param float $vendorCommissionPercent
     */
    public function setVendorCommissionPercent($vendorCommissionPercent)
    {
        $this->vendorCommissionPercent = $vendorCommissionPercent;
    }

    /**
     * @return float
     */
    public function getVendorCommission()
    {
        return $this->vendorCommission;
    }

    /**
     * @param float $vendorCommission
     */
    public function setVendorCommission($vendorCommission)
    {
        $this->vendorCommission = $vendorCommission;
    }

    /**
     * @return float
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * @param float $vat
     */
    public function setVat($vat)
    {
        $this->vat = $vat;
    }

    /**
     * @return ProcurementCondition
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * @param ProcurementCondition $condition
     */
    public function setCondition($condition)
    {
        $this->condition = $condition;
    }

    /**
     * @return string
     */
    public function getTerms()
    {
        return $this->terms;
    }

    /**
     * @param string $terms
     */
    public function setTerms($terms)
    {
        $this->terms = $terms;
    }

    /**
     * @return TenderConditionItem
     */
    public function getConditionItems()
    {
        return $this->conditionItems;
    }

    /**
     * @return Branch
     */
    public function getShipTo()
    {
        return $this->shipTo;
    }

    /**
     * @param Branch $shipTo
     */
    public function setShipTo($shipTo)
    {
        $this->shipTo = $shipTo;
    }

    /**
     * @return bool
     */
    public function isPartialPayment()
    {
        return $this->partialPayment;
    }

    /**
     * @param bool $partialPayment
     */
    public function setPartialPayment( $partialPayment)
    {
        $this->partialPayment = $partialPayment;
    }

    /**
     * @return bool
     */
    public function isPartialDelivery()
    {
        return $this->partialDelivery;
    }

    /**
     * @param bool $partialDelivery
     */
    public function setPartialDelivery( $partialDelivery)
    {
        $this->partialDelivery = $partialDelivery;
    }

    /**
     * @return float
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param float $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * @return ProcurementProcess
     */
    public function getApproveProcess()
    {
        return $this->approveProcess;
    }

    /**
     * @param ProcurementProcess $approveProcess
     */
    public function setApproveProcess($approveProcess)
    {
        $this->approveProcess = $approveProcess;
    }

    /**
     * @return Branch
     */
    public function getWearhouse()
    {
        return $this->wearhouse;
    }

    /**
     * @param Branch $wearhouse
     */
    public function setWearhouse($wearhouse)
    {
        $this->wearhouse = $wearhouse;
    }

    /**
     * @return Branch
     */
    public function getBranch()
    {
        return $this->branch;
    }

    /**
     * @param Branch $branch
     */
    public function setBranch($branch)
    {
        $this->branch = $branch;
    }

    /**
     * @return TenderMemo
     */
    public function getManagementMemo()
    {
        return $this->managementMemo;
    }

    /**
     * @param TenderMemo $managementMemo
     */
    public function setManagementMemo($managementMemo)
    {
        $this->managementMemo = $managementMemo;
    }



    /**
     * @return float
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * @param float $tax
     */
    public function setTax($tax)
    {
        $this->tax = $tax;
    }

    /**
     * @return float
     */
    public function getAit()
    {
        return $this->ait;
    }

    /**
     * @param float $ait
     */
    public function setAit($ait)
    {
        $this->ait = $ait;
    }

    /**
     * @return Requisition
     */
    public function getRequisition()
    {
        return $this->requisition;
    }

    /**
     * @param Requisition $requisition
     */
    public function setRequisition($requisition)
    {
        $this->requisition = $requisition;
    }

    /**
     * @return string
     */
    public function getAttendTo()
    {
        return $this->attendTo;
    }

    /**
     * @param string $attendTo
     */
    public function setAttendTo(string $attendTo)
    {
        $this->attendTo = $attendTo;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return string
     */
    public function getFooterContent()
    {
        return $this->footerContent;
    }

    /**
     * @param string $footerContent
     */
    public function setFooterContent($footerContent)
    {
        $this->footerContent = $footerContent;
    }

    /**
     * @return string
     */
    public function getWorkorderMode()
    {
        return $this->workorderMode;
    }

    /**
     * @param string $workorderMode
     */
    public function setWorkorderMode($workorderMode)
    {
        $this->workorderMode = $workorderMode;
    }

    /**
     * @return float
     */
    public function getShippingCharge()
    {
        return $this->shippingCharge;
    }

    /**
     * @param float $shippingCharge
     */
    public function setShippingCharge($shippingCharge)
    {
        $this->shippingCharge = $shippingCharge;
    }

    /**
     * @return float
     */
    public function getGrandTotal()
    {
        return $this->grandTotal;
    }

    /**
     * @param float $grandTotal
     */
    public function setGrandTotal($grandTotal)
    {
        $this->grandTotal = $grandTotal;
    }

    /**
     * @return DateTime
     */
    public function getWorkorderDate()
    {
        return $this->workorderDate;
    }

    /**
     * @param DateTime $workorderDate
     */
    public function setWorkorderDate($workorderDate)
    {
        $this->workorderDate = $workorderDate;
    }

    /**
     * @return DateTime
     */
    public function getValidateDate()
    {
        return $this->validateDate;
    }

    /**
     * @param DateTime $validateDate
     */
    public function setValidateDate($validateDate)
    {
        $this->validateDate = $validateDate;
    }

    /**
     * @return \DateTime
     */
    public function getDeliveryDate()
    {
        return $this->deliveryDate;
    }

    /**
     * @param \DateTime $deliveryDate
     */
    public function setDeliveryDate($deliveryDate)
    {
        $this->deliveryDate = $deliveryDate;
    }

    /**
     * @return mixed
     */
    public function getApproveTo()
    {
        return $this->approveTo;
    }

    /**
     * @param mixed $approveTo
     */
    public function setApproveTo($approveTo)
    {
        $this->approveTo = $approveTo;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return bool
     */
    public function isDelete()
    {
        return $this->isDelete;
    }

    /**
     * @param bool $isDelete
     */
    public function setIsDelete($isDelete)
    {
        $this->isDelete = $isDelete;
    }




}
