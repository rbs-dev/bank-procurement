<?php

namespace Terminalbd\ProcurementBundle\Entity;

use App\Entity\Application\SecurityBilling;
use App\Entity\Core\Customer;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Terminalbd\GenericBundle\Entity\Item;
use Terminalbd\GenericBundle\Entity\ItemBrand;
use Terminalbd\GenericBundle\Entity\ItemColor;
use Terminalbd\GenericBundle\Entity\ItemSize;
use Terminalbd\InventoryBundle\Entity\Purchase;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\InventoryBundle\Entity\StockBook;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\ProcurementBundle\Repository\RequisitionItemRepository")
 * @ORM\Table(name="procu_requisition_item")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class RequisitionItem
{

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;


    /**
     * @var Requisition
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\Requisition",inversedBy="requisitionItems")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $requisition;


    /**
     * @var JobRequisition
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\JobRequisition",inversedBy="requisitionItems")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $jobRequisition;


    /**
     * @var RequisitionItemHistory
     *
     * @ORM\OneToMany(targetEntity="Terminalbd\ProcurementBundle\Entity\RequisitionItemHistory",mappedBy="requisitionItem")
     * @ORM\OrderBy({"id" = "DESC"})
     */
    private $requisitionItemHistories;

    /**
     * @var TenderItem
     *
     * @ORM\OneToMany(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderItem",mappedBy="requisitionItem")
     * @ORM\OrderBy({"id" = "DESC"})
     */
    private $tenderItems;


    /**
     * @var TenderBatchItem
     *
     * @ORM\OneToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderBatchItem",mappedBy="requisitionItem")
     */
    private $batchItem;


    /**
     * @var Item
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\GenericBundle\Entity\Item")
     */
    private $item;

    /**
     * @var Stock
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Stock")
     */
    private $stock;

    /**
     * @var StockBook
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\StockBook")
     */
    private $stockBook;


    /**
     * @var RequisitionItem
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\RequisitionItem")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $lastRequisitionItem;

    /**
     * @var ItemBrand
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\GenericBundle\Entity\ItemBrand")
     */
    private $brand;


    /**
     * @var ItemSize
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\GenericBundle\Entity\ItemSize")
     */
    private $size;


    /**
     * @var ItemColor
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\GenericBundle\Entity\ItemColor")
     */
    private $color;


    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $unit;


    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $description;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $previousPurchasePrice;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $presentBookValue;

     /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $warranty;

    /**
     * @var \DateTime
     * @ORM\Column(type="date",nullable=true)
     */
    private $previousPurchaseDate;

    /**
     * @var \DateTime
     * @ORM\Column(type="date",nullable=true)
     */
    private $lastServiceDate;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $repairCost;


    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $actualQuantity=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $approveQuantity;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $quantity;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $remainigQuantity;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $issueQuantity;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $workorderQuantity;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $receiveQuantity;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $stockIn;


    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $subTotal=0;


     /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $price;

     /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $currentPrice;

     /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $cmp;

    /**
     * @var integer
     * @ORM\Column(type="integer",nullable=true)
     */
    private $lastPurchaseQuantity=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $lastPurchasePrice=0;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastPurchaseDate;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $cmpDate;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isClose = true;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $path;

    /**
     * @Assert\File(maxSize="1M")
     */
    protected $file;



    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return string
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @param string $unit
     */
    public function setUnit( $unit)
    {
        $this->unit = $unit;
    }

    /**
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param string $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return SecurityBilling
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param SecurityBilling $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @return float
     */
    public function getSubTotal()
    {
        return $this->subTotal;
    }

    /**
     * @param float $subTotal
     */
    public function setSubTotal( $subTotal)
    {
        $this->subTotal = $subTotal;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param float $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }


    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return Requisition
     */
    public function getRequisition()
    {
        return $this->requisition;
    }

    /**
     * @param Requisition $requisition
     */
    public function setRequisition($requisition)
    {
        $this->requisition = $requisition;
    }

    /**
     * @return Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param Item $item
     */
    public function setItem($item)
    {
        $this->item = $item;
    }

    /**
     * @return Stock
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param Stock $stock
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return float
     */
    public function getActualQuantity()
    {
        return $this->actualQuantity;
    }

    /**
     * @param float $actualQuantity
     */
    public function setActualQuantity($actualQuantity)
    {
        $this->actualQuantity = $actualQuantity;
    }

    /**
     * @return float
     */
    public function getStockIn()
    {
        return $this->stockIn;
    }

    /**
     * @param float $stockIn
     */
    public function setStockIn($stockIn)
    {
        $this->stockIn = $stockIn;
    }

    /**
     * @return RequisitionItem
     */
    public function getLastRequisitionItem()
    {
        return $this->lastRequisitionItem;
    }

    /**
     * @param RequisitionItem $lastRequisitionItem
     */
    public function setLastRequisitionItem($lastRequisitionItem)
    {
        $this->lastRequisitionItem = $lastRequisitionItem;
    }

    /**
     * @return Requisition
     */
    public function getRequisitionItemHistory(): Requisition
    {
        return $this->requisitionItemHistory;
    }

    /**
     * @return JobRequisition
     */
    public function getJobRequisition()
    {
        return $this->jobRequisition;
    }

    /**
     * @param JobRequisition $jobRequisition
     */
    public function setJobRequisition($jobRequisition)
    {
        $this->jobRequisition = $jobRequisition;
    }

    /**
     * @return ItemBrand
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param ItemBrand $brand
     */
    public function setBrand(ItemBrand $brand)
    {
        $this->brand = $brand;
    }

    /**
     * @return ItemSize
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param ItemSize $size
     */
    public function setSize(ItemSize $size)
    {
        $this->size = $size;
    }

    /**
     * @return ItemColor
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param ItemColor $color
     */
    public function setColor(ItemColor $color)
    {
        $this->color = $color;
    }

    /**
     * @return int
     */
    public function getLastPurchaseQuantity()
    {
        return $this->lastPurchaseQuantity;
    }

    /**
     * @param int $lastPurchaseQuantity
     */
    public function setLastPurchaseQuantity(int $lastPurchaseQuantity)
    {
        $this->lastPurchaseQuantity = $lastPurchaseQuantity;
    }

    /**
     * @return float
     */
    public function getLastPurchasePrice()
    {
        return $this->lastPurchasePrice;
    }

    /**
     * @param float $lastPurchasePrice
     */
    public function setLastPurchasePrice($lastPurchasePrice)
    {
        $this->lastPurchasePrice = $lastPurchasePrice;
    }

    /**
     * @return \DateTime
     */
    public function getLastPurchaseDate()
    {
        return $this->lastPurchaseDate;
    }

    /**
     * @param \DateTime $lastPurchaseDate
     */
    public function setLastPurchaseDate($lastPurchaseDate)
    {
        $this->lastPurchaseDate = $lastPurchaseDate;
    }

    /**
     * @return float
     */
    public function getCurrentPrice()
    {
        return $this->currentPrice;
    }

    /**
     * @param float $currentPrice
     */
    public function setCurrentPrice( $currentPrice)
    {
        $this->currentPrice = $currentPrice;
    }

    /**
     * @return StockBook
     */
    public function getStockBook()
    {
        return $this->stockBook;
    }

    /**
     * @param StockBook $stockBook
     */
    public function setStockBook($stockBook)
    {
        $this->stockBook = $stockBook;
    }

    /**
     * @return \DateTime
     */
    public function getCmpDate()
    {
        return $this->cmpDate;
    }

    /**
     * @param \DateTime $cmpDate
     */
    public function setCmpDate($cmpDate)
    {
        $this->cmpDate = $cmpDate;
    }

    /**
     * @return float
     */
    public function getRemainigQuantity()
    {
        return $this->remainigQuantity;
    }

    /**
     * @param float $remainigQuantity
     */
    public function setRemainigQuantity($remainigQuantity)
    {
        $this->remainigQuantity = $remainigQuantity;
    }

    /**
     * @return float
     */
    public function getIssueQuantity()
    {
        return $this->issueQuantity;
    }

    /**
     * @param float $issueQuantity
     */
    public function setIssueQuantity($issueQuantity)
    {
        $this->issueQuantity = $issueQuantity;
    }

    /**
     * @return bool
     */
    public function isClose()
    {
        return $this->isClose;
    }

    /**
     * @param bool $isClose
     */
    public function setIsClose(bool $isClose)
    {
        $this->isClose = $isClose;
    }

    /**
     * @return float
     */
    public function getCmp()
    {
        return $this->cmp;
    }

    /**
     * @param float $cmp
     */
    public function setCmp($cmp)
    {
        $this->cmp = $cmp;
    }

    /**
     * @return float
     */
    public function getApproveQuantity()
    {
        return $this->approveQuantity;
    }

    /**
     * @param float $approveQuantity
     */
    public function setApproveQuantity($approveQuantity)
    {
        $this->approveQuantity = $approveQuantity;
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @param string $filename
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
    }

    /**
     * Sets file.
     *
     * @param Purchase $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return Purchase
     */
    public function getFile()
    {
        return $this->file;
    }


    public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->path;
    }


    protected function getUploadRootDir()
    {
        if(!file_exists( $this->getUploadDir())){
            mkdir( $this->getUploadDir(), 0777, true);
        }
        return __DIR__ . '/../../../../public/' . $this->getUploadDir();
    }

    public function getImagePath()
    {
        return '/uploads/procurement/requisition-item/'.$this->getpath();
    }

    protected function getUploadDir()
    {
        return 'uploads/procurement/requisition-item/';
    }

    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
            $this->path = null ;
        }
    }

    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getFile()) {
            return;
        }
        // use the original file name here but you should
        // sanitize it at least to avoid any security issues

        // move takes the target directory and then the
        // target filename to move to
        $filename = date('YmdHmi') . "_" . $this->getFile()->getClientOriginalName();
        $this->getFile()->move(
            $this->getUploadRootDir(),
            $filename
        );

        // set the path property to the filename where you've saved the file
        $this->path = $filename ;

        // clean up the file property as you won't need it anymore
        $this->file = null;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return float
     */
    public function getPreviousPurchasePrice()
    {
        return $this->previousPurchasePrice;
    }

    /**
     * @param float $previousPurchasePrice
     */
    public function setPreviousPurchasePrice($previousPurchasePrice)
    {
        $this->previousPurchasePrice = $previousPurchasePrice;
    }

    /**
     * @return float
     */
    public function getPresentBookValue()
    {
        return $this->presentBookValue;
    }

    /**
     * @param float $presentBookValue
     */
    public function setPresentBookValue($presentBookValue)
    {
        $this->presentBookValue = $presentBookValue;
    }

    /**
     * @return string
     */
    public function getWarranty()
    {
        return $this->warranty;
    }

    /**
     * @param string $warranty
     */
    public function setWarranty($warranty)
    {
        $this->warranty = $warranty;
    }

    /**
     * @return \DateTime
     */
    public function getPreviousPurchaseDate()
    {
        return $this->previousPurchaseDate;
    }

    /**
     * @param \DateTime $previousPurchaseDate
     */
    public function setPreviousPurchaseDate($previousPurchaseDate)
    {
        $this->previousPurchaseDate = $previousPurchaseDate;
    }

    /**
     * @return \DateTime
     */
    public function getLastServiceDate()
    {
        return $this->lastServiceDate;
    }

    /**
     * @param \DateTime $lastServiceDate
     */
    public function setLastServiceDate($lastServiceDate)
    {
        $this->lastServiceDate = $lastServiceDate;
    }

    /**
     * @return float
     */
    public function getRepairCost()
    {
        return $this->repairCost;
    }

    /**
     * @param float $repairCost
     */
    public function setRepairCost($repairCost)
    {
        $this->repairCost = $repairCost;
    }

    /**
     * @return RequisitionItemHistory
     */
    public function getRequisitionItemHistories()
    {
        return $this->requisitionItemHistories;
    }

    /**
     * @return float
     */
    public function getWorkorderQuantity()
    {
        return $this->workorderQuantity;
    }

    /**
     * @param float $workorderQuantity
     */
    public function setWorkorderQuantity($workorderQuantity)
    {
        $this->workorderQuantity = $workorderQuantity;
    }

    /**
     * @return float
     */
    public function getReceiveQuantity()
    {
        return $this->receiveQuantity;
    }

    /**
     * @param float $receiveQuantity
     */
    public function setReceiveQuantity($receiveQuantity)
    {
        $this->receiveQuantity = $receiveQuantity;
    }



}
