<?php

namespace Terminalbd\ProcurementBundle\Entity;

use App\Entity\Application\SecurityBilling;
use App\Entity\Core\Customer;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Terminalbd\GenericBundle\Entity\Item;
use Terminalbd\GenericBundle\Entity\ItemBrand;
use Terminalbd\GenericBundle\Entity\ItemColor;
use Terminalbd\GenericBundle\Entity\ItemSize;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\InventoryBundle\Entity\StockBook;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\ProcurementBundle\Repository\TenderBatchItemRepository")
 * @ORM\Table(name="procu_tender_batch_item")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class TenderBatchItem
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;


    /**
     * @var TenderBatch
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderBatch",inversedBy="tenderItems")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $tenderBatch;

    /**
     * @var RequisitionItem
     *
     * @ORM\OneToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\RequisitionItem",inversedBy="batchItem")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $requisitionItem;

     /**
     * @var RequisitionItem
     *
     * @ORM\OneToMany(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderItemDetails",mappedBy="tenderBatchItem")
     */
    private $tenderItemDetails;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $assignTo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $approvedBy;


    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isClose;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $quantity=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $issueQuantity=0;

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $comment;



    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated(\DateTime $updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return RequisitionItem
     */
    public function getRequisitionItem()
    {
        return $this->requisitionItem;
    }

    /**
     * @param RequisitionItem $requisitionItem
     */
    public function setRequisitionItem( $requisitionItem)
    {
        $this->requisitionItem = $requisitionItem;
    }


    /**
     * @return TenderBatch
     */
    public function getTenderBatch()
    {
        return $this->tenderBatch;
    }

    /**
     * @param TenderBatch $tenderBatch
     */
    public function setTenderBatch($tenderBatch)
    {
        $this->tenderBatch = $tenderBatch;
    }

    /**
     * @return bool
     */
    public function isClose()
    {
        return $this->isClose;
    }

    /**
     * @param bool $isClose
     */
    public function setIsClose($isClose)
    {
        $this->isClose = $isClose;
    }

    /**
     * @return float
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param float $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return float
     */
    public function getIssueQuantity()
    {
        return $this->issueQuantity;
    }

    /**
     * @param float $issueQuantity
     */
    public function setIssueQuantity($issueQuantity)
    {
        $this->issueQuantity = $issueQuantity;
    }

    /**
     * @return mixed
     */
    public function getAssignTo()
    {
        return $this->assignTo;
    }

    /**
     * @param mixed $assignTo
     */
    public function setAssignTo($assignTo)
    {
        $this->assignTo = $assignTo;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return mixed
     */
    public function getApprovedBy()
    {
        return $this->approvedBy;
    }

    /**
     * @param mixed $approvedBy
     */
    public function setApprovedBy($approvedBy)
    {
        $this->approvedBy = $approvedBy;
    }





}
