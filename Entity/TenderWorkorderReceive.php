<?php

namespace Terminalbd\ProcurementBundle\Entity;

use App\Entity\Application\Procurement;
use App\Entity\Domain\ModuleProcess;
use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Terminalbd\InventoryBundle\Entity\Purchase;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\ProcurementBundle\Repository\TenderWorkorderReceiveRepository")
 * @ORM\Table(name="procu_tender_workorder_receive")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class TenderWorkorderReceive
{

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @var Procurement
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\Procurement")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $config;

    /**
     * @var ProcurementProcess
     *
     * @ORM\OneToOne(targetEntity="ProcurementProcess")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="approveProcess_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     * })
     */
    private $approveProcess;


    /**
     * @var TenderWorkorder
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderWorkorder", inversedBy="workorderReceive",cascade={"persist"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $workorder;


    /**
     * @var TenderWorkorderReceiveItem
     * @ORM\OneToMany(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderWorkorderReceiveItem", mappedBy="workorderReceive")
     */
    private $receiveItems;

    /**
     * @var ModuleProcess
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain\ModuleProcess")
     */
    private $moduleProcess;


    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $createdBy;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $reportTo;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $transferTo;


    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $subTotal=0;
    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $total=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $vendorCommissionPercent=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $vendorCommission=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $discount=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $vat=0;


    /**
     * @var integer
     * @ORM\Column(type="integer",nullable=true)
     */
    private $code=0;

    /**
     * @var integer
     * @ORM\Column(type="integer",nullable=true)
     */
    private $processOrdering = 0;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $invoice;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $gatePassNo;


    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $businessGroup;


    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $process='New';

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $module ="goods-receive";

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $waitingProcess;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $challanNo;

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $content;

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $acceptence;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status = true;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isDelete = false;

    /**
     * @var Date
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $receiveDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $path;

    /**
     * @Assert\File(maxSize="1M")
     */
    protected $file;


    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Procurement
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param Procurement $config
     */
    public function setConfig(Procurement $config)
    {
        $this->config = $config;
    }

    /**
     * @return TenderWorkorder
     */
    public function getWorkorder(): TenderWorkorder
    {
        return $this->workorder;
    }

    /**
     * @param TenderWorkorder $workorder
     */
    public function setWorkorder(TenderWorkorder $workorder)
    {
        $this->workorder = $workorder;
    }


    /**
     * @return TenderWorkorderReceiveItem
     */
    public function getReceiveItems()
    {
        return $this->receiveItems;
    }



    /**
     * @return ModuleProcess
     */
    public function getModuleProcess(): ModuleProcess
    {
        return $this->moduleProcess;
    }

    /**
     * @param ModuleProcess $moduleProcess
     */
    public function setModuleProcess(ModuleProcess $moduleProcess)
    {
        $this->moduleProcess = $moduleProcess;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return mixed
     */
    public function getReportTo()
    {
        return $this->reportTo;
    }

    /**
     * @param mixed $reportTo
     */
    public function setReportTo($reportTo)
    {
        $this->reportTo = $reportTo;
    }

    /**
     * @return mixed
     */
    public function getTransferTo()
    {
        return $this->transferTo;
    }

    /**
     * @param mixed $transferTo
     */
    public function setTransferTo($transferTo)
    {
        $this->transferTo = $transferTo;
    }

    /**
     * @return float
     */
    public function getSubTotal()
    {
        return $this->subTotal;
    }

    /**
     * @param float $subTotal
     */
    public function setSubTotal( $subTotal)
    {
        $this->subTotal = $subTotal;
    }

    /**
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param int $code
     */
    public function setCode(int $code)
    {
        $this->code = $code;
    }

    /**
     * @return int
     */
    public function getProcessOrdering()
    {
        return $this->processOrdering;
    }

    /**
     * @param int $processOrdering
     */
    public function setProcessOrdering(int $processOrdering)
    {
        $this->processOrdering = $processOrdering;
    }

    /**
     * @return string
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * @param string $invoice
     */
    public function setInvoice($invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * @return string
     */
    public function getBusinessGroup()
    {
        return $this->businessGroup;
    }

    /**
     * @param string $businessGroup
     */
    public function setBusinessGroup($businessGroup)
    {
        $this->businessGroup = $businessGroup;
    }

    /**
     * @return string
     */
    public function getProcess()
    {
        return $this->process;
    }

    /**
     * @param string $process
     */
    public function setProcess($process)
    {
        $this->process = $process;
    }

    /**
     * @return string
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @param string $module
     */
    public function setModule($module)
    {
        $this->module = $module;
    }

    /**
     * @return string
     */
    public function getWaitingProcess()
    {
        return $this->waitingProcess;
    }

    /**
     * @param string $waitingProcess
     */
    public function setWaitingProcess($waitingProcess)
    {
        $this->waitingProcess = $waitingProcess;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent( $content)
    {
        $this->content = $content;
    }

    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus( $status)
    {
        $this->status = $status;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return string
     */
    public function getChallanNo()
    {
        return $this->challanNo;
    }

    /**
     * @param string $challanNo
     */
    public function setChallanNo(string $challanNo)
    {
        $this->challanNo = $challanNo;
    }

    /**
     * @return Date
     */
    public function getReceiveDate()
    {
        return $this->receiveDate;
    }

    /**
     * @param Date $receiveDate
     */
    public function setReceiveDate( $receiveDate)
    {
        $this->receiveDate = $receiveDate;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param float $total
     */
    public function setTotal( $total)
    {
        $this->total = $total;
    }

    /**
     * @return float
     */
    public function getVendorCommissionPercent()
    {
        return $this->vendorCommissionPercent;
    }

    /**
     * @param float $vendorCommissionPercent
     */
    public function setVendorCommissionPercent( $vendorCommissionPercent)
    {
        $this->vendorCommissionPercent = $vendorCommissionPercent;
    }

    /**
     * @return float
     */
    public function getVendorCommission()
    {
        return $this->vendorCommission;
    }

    /**
     * @param float $vendorCommission
     */
    public function setVendorCommission( $vendorCommission)
    {
        $this->vendorCommission = $vendorCommission;
    }

    /**
     * @return float
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * @param float $vat
     */
    public function setVat( $vat)
    {
        $this->vat = $vat;
    }



    /**
     * @return float
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param float $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * @return ProcurementProcess
     */
    public function getApproveProcess()
    {
        return $this->approveProcess;
    }

    /**
     * @param ProcurementProcess $approveProcess
     */
    public function setApproveProcess($approveProcess)
    {
        $this->approveProcess = $approveProcess;
    }

    /**
     * @return string
     */
    public function getGatePassNo()
    {
        return $this->gatePassNo;
    }

    /**
     * @param string $gatePassNo
     */
    public function setGatePassNo($gatePassNo)
    {
        $this->gatePassNo = $gatePassNo;
    }

    /**
     * @return string
     */
    public function getAcceptence()
    {
        return $this->acceptence;
    }

    /**
     * @param string $acceptence
     */
    public function setAcceptence($acceptence)
    {
        $this->acceptence = $acceptence;
    }

    /**
     * Sets file.
     *
     * @param Purchase $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return Purchase
     */
    public function getFile()
    {
        return $this->file;
    }


    public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->path;
    }


    protected function getUploadRootDir()
    {
        if(!file_exists( $this->getUploadDir())){
            mkdir( $this->getUploadDir(), 0777, true);
        }
        return __DIR__ . '/../../../../public/' . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'uploads/procurement/';
    }

    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
            $this->path = null ;
        }
    }

    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getFile()) {
            return;
        }
        // use the original file name here but you should
        // sanitize it at least to avoid any security issues

        // move takes the target directory and then the
        // target filename to move to
        $filename = date('YmdHmi') . "_" . $this->getFile()->getClientOriginalName();
        $this->getFile()->move(
            $this->getUploadRootDir(),
            $filename
        );

        // set the path property to the filename where you've saved     the file
        $this->path = $filename ;

        // clean up the file property as you won't need it anymore
        $this->file = null;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return bool
     */
    public function isDelete()
    {
        return $this->isDelete;
    }

    /**
     * @param bool $isDelete
     */
    public function setIsDelete($isDelete)
    {
        $this->isDelete = $isDelete;
    }


    
}
