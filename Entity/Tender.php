<?php

namespace Terminalbd\ProcurementBundle\Entity;

use App\Entity\Application\Procurement;
use App\Entity\Core\Setting;
use App\Entity\Domain\Branch;
use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\ModuleProcessItem;
use App\Entity\Domain\Vendor;
use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints\Date;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\ProcurementBundle\Repository\TenderRepository")
 * @ORM\Table(name="procu_tender")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class Tender
{

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @var Procurement
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\Procurement")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $config;


    /**
     * @var ProcurementProcess
     *
     * @ORM\OneToOne(targetEntity="ProcurementProcess")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="approveProcess_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     * })
     */
    private $approveProcess;


    /**
     * @var TenderCommittee
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderCommittee",inversedBy="tenders")
     * @ORM\JoinColumn(name="tenderCommittee_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $tenderCommittee;

    /**
     * @var TenderMemoComment
     *
     * @ORM\OneToMany(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderMemoComment", cascade={"persist", "remove"}, mappedBy="tender")
     */
    private $comments;


    /**
     * @var TenderComparative
     *
     * @ORM\OneToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderComparative",mappedBy="tender")
     */
    private $tenderComparative;

     /**
     * @var TenderMemo
     *
     * @ORM\OneToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderMemo",mappedBy="tender")
     */
    private $tenderMemo;

    /**
     * @var Requisition
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\Requisition",inversedBy="tender")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $requisition;

    /**
     * @var ProcurementCondition
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\ProcurementCondition")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $condition;

     /**
     * @var TenderConditionItem
     *
     * @ORM\OneToMany(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderConditionItem", mappedBy="tender")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $conditionItems;

    /**
     * @var TenderConditionItem
     *
     * @ORM\OneToMany(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderMemoComment", mappedBy="tender")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $meetingComments;

     /**
     * @var TenderItemDetails
     *
     * @ORM\OneToMany(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderItemDetails", mappedBy="tender", cascade={"persist", "remove"})
     */
    private $tenderItemDetails;

     /**
     * @var ModuleProcess
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain\ModuleProcess")
     */
    private $moduleProcess;

     /**
     * @var TenderWorkorder
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderWorkorder")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $workOrder;


    /**
     * @var TenderItem
     *
     * @ORM\OneToMany(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderItem", mappedBy="tender")
     * @ORM\OrderBy({"created" = "DESC"})
     */
    private $tenderItems;

    /**
     * @var Setting
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Setting")
     */
    private $department;

    /**
     * @var Setting
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Setting")
     */
    private $processDepartment;

    /**
     * @var TenderVendor
     *
     * @ORM\OneToMany(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderVendor", mappedBy="tender")
     * @ORM\OrderBy({"subTotal" = "ASC"})
     */
    private $tenderVendors;

    /**
     * @var Branch
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain\Branch")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $shipTo;


    /**
     * @var Vendor
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain\Vendor")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $directVendor;

    /**
     * @var Particular
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\Particular")
     */
    private $paymentMode;


    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $tenderReplyDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deliveryDate;


    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $createdBy;


     /**
     * @var Vendor
     * @ORM\ManyToMany(targetEntity="App\Entity\Domain\Vendor")
     **/
    private  $vendors;

    /**
     * @var Setting
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Setting")
     **/
    private $vendorType;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $reportTo;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $transferTo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $assignTo;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $approveTo;


    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $subTotal=0;


    /**
     * @var integer
     * @ORM\Column(type="integer",nullable=true)
     */
    private $code=0;

    /**
     * @var integer
     * @ORM\Column(type="integer",nullable=true)
     */
    private $processOrdering = 0;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $invoice;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $processMode ="quotation";


    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $businessGroup;


    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $process='New';

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $module ="tender";

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    private $isDelete = false;


    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $waitingProcess;


    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $subject;

     /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $email;

     /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $terms;

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $content;

     /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $footerContent;

     /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $comment;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isTender = true;


    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status = true;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return string
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * @param string $invoice
     */
    public function setInvoice( $invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * @return TenderItem
     */
    public function getTenderItems()
    {
        return $this->tenderItems;
    }

    /**
     * @return Procurement
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param Procurement $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }


    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return mixed
     */
    public function getReportTo()
    {
        return $this->reportTo;
    }

    /**
     * @param mixed $reportTo
     */
    public function setReportTo($reportTo)
    {
        $this->reportTo = $reportTo;
    }

    /**
     * @return mixed
     */
    public function getTransferTo()
    {
        return $this->transferTo;
    }

    /**
     * @param mixed $transferTo
     */
    public function setTransferTo($transferTo)
    {
        $this->transferTo = $transferTo;
    }

    /**
     * @return float
     */
    public function getSubTotal()
    {
        return $this->subTotal;
    }

    /**
     * @param float $subTotal
     */
    public function setSubTotal(float $subTotal)
    {
        $this->subTotal = $subTotal;
    }

    /**
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param int $code
     */
    public function setCode(int $code)
    {
        $this->code = $code;
    }

    /**
     * @return int
     */
    public function getProcessOrdering()
    {
        return $this->processOrdering;
    }

    /**
     * @param int $processOrdering
     */
    public function setProcessOrdering(int $processOrdering)
    {
        $this->processOrdering = $processOrdering;
    }

    /**
     * @return string
     */
    public function getProcess()
    {
        return $this->process;
    }

    /**
     * @param string $process
     */
    public function setProcess($process)
    {
        $this->process = $process;
    }

    /**
     * @return string
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @param string $module
     */
    public function setModule($module)
    {
        $this->module = $module;
    }

    /**
     * @return string
     */
    public function getWaitingProcess()
    {
        return $this->waitingProcess;
    }

    /**
     * @param string $waitingProcess
     */
    public function setWaitingProcess($waitingProcess)
    {
        $this->waitingProcess = $waitingProcess;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated(): \DateTime
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated(\DateTime $updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getTerms()
    {
        return $this->terms;
    }

    /**
     * @param string $terms
     */
    public function setTerms($terms)
    {
        $this->terms = $terms;
    }

    /**
     * @return Vendor
     */
    public function getVendors()
    {
        return $this->vendors;
    }

    /**
     * @param Vendor $vendors
     */
    public function setVendors($vendors)
    {
        $this->vendors = $vendors;
    }

    /**
     * @return TenderVendor
     */
    public function getTenderVendors()
    {
        return $this->tenderVendors;
    }

    /**
     * @return string
     */
    public function getBusinessGroup()
    {
        return $this->businessGroup;
    }

    /**
     * @param string $businessGroup
     */
    public function setBusinessGroup($businessGroup)
    {
        $this->businessGroup = $businessGroup;
    }

    /**
     * @return Setting
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @param Setting $department
     */
    public function setDepartment( $department)
    {
        $this->department = $department;
    }

    /**
     * @return ModuleProcess
     */
    public function getModuleProcess()
    {
        return $this->moduleProcess;
    }

    /**
     * @param ModuleProcess $moduleProcess
     */
    public function setModuleProcess($moduleProcess)
    {
        $this->moduleProcess = $moduleProcess;
    }


    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return TenderComparative
     */
    public function getTenderComparative()
    {
        return $this->tenderComparative;
    }

    /**
     * @return ProcurementCondition
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * @param ProcurementCondition $condition
     */
    public function setCondition($condition)
    {
        $this->condition = $condition;
    }

    /**
     * @return TenderConditionItem
     */
    public function getConditionItems()
    {
        return $this->conditionItems;
    }

    /**
     * @return ProcurementProcess
     */
    public function getApproveProcess()
    {
        return $this->approveProcess;
    }

    /**
     * @param ProcurementProcess $approveProcess
     */
    public function setApproveProcess($approveProcess)
    {
        $this->approveProcess = $approveProcess;
    }

    /**
     * @return bool
     */
    public function isTender()
    {
        return $this->isTender;
    }

    /**
     * @param bool $isTender
     */
    public function setIsTender($isTender)
    {
        $this->isTender = $isTender;
    }

    /**
     * @return TenderConditionItem
     */
    public function getTenderItemDetails()
    {
        return $this->tenderItemDetails;
    }

    /**
     * @return Requisition
     */
    public function getRequisition()
    {
        return $this->requisition;
    }

    /**
     * @param Requisition $requisition
     */
    public function setRequisition($requisition)
    {
        $this->requisition = $requisition;
    }

    /**
     * @return Vendor
     */
    public function getDirectVendor()
    {
        return $this->directVendor;
    }

    /**
     * @param Vendor $directVendor
     */
    public function setDirectVendor($directVendor)
    {
        $this->directVendor = $directVendor;
    }

    /**
     * @return Setting
     */
    public function getProcessDepartment()
    {
        return $this->processDepartment;
    }

    /**
     * @param Setting $processDepartment
     */
    public function setProcessDepartment($processDepartment)
    {
        $this->processDepartment = $processDepartment;
    }

    /**
     * @return User
     */
    public function getAssignTo()
    {
        return $this->assignTo;
    }

    /**
     * @param User $assignTo
     */
    public function setAssignTo($assignTo)
    {
        $this->assignTo = $assignTo;
    }

    /**
     * @return TenderCommittee
     */
    public function getTenderCommittee()
    {
        return $this->tenderCommittee;
    }

    /**
     * @param TenderCommittee $tenderCommittee
     */
    public function setTenderCommittee($tenderCommittee)
    {
        $this->tenderCommittee = $tenderCommittee;
    }

    /**
     * @return mixed
     */
    public function getApproveTo()
    {
        return $this->approveTo;
    }

    /**
     * @param mixed $approveTo
     */
    public function setApproveTo($approveTo)
    {
        $this->approveTo = $approveTo;
    }


    /**
     * @return TenderMemo
     */
    public function getTenderMemo()
    {
        return $this->tenderMemo;
    }

    /**
     * @return string
     */
    public function getProcessMode()
    {
        return $this->processMode;
    }

    /**
     * @param string $processMode
     */
    public function setProcessMode($processMode)
    {
        $this->processMode = $processMode;
    }

    /**
     * @return TenderWorkorder
     */
    public function getWorkOrder()
    {
        return $this->workOrder;
    }

    /**
     * @param TenderWorkorder $workOrder
     */
    public function setWorkOrder($workOrder)
    {
        $this->workOrder = $workOrder;
    }

    /**
     * @return Setting
     */
    public function getVendorType()
    {
        return $this->vendorType;
    }

    /**
     * @param Setting $vendorType
     */
    public function setVendorType($vendorType)
    {
        $this->vendorType = $vendorType;
    }

    /**
     * @return TenderMemoComment
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @return TenderConditionItem
     */
    public function getMeetingComments()
    {
        return $this->meetingComments;
    }

    /**
     * @return string
     */
    public function getFooterContent()
    {
        return $this->footerContent;
    }

    /**
     * @param string $footerContent
     */
    public function setFooterContent($footerContent)
    {
        $this->footerContent = $footerContent;
    }


    /**
     * @return Particular
     */
    public function getPaymentMode()
    {
        return $this->paymentMode;
    }

    /**
     * @param Particular $paymentMode
     */
    public function setPaymentMode($paymentMode)
    {
        $this->paymentMode = $paymentMode;
    }


    /**
     * @return Branch
     */
    public function getShipTo()
    {
        return $this->shipTo;
    }

    /**
     * @param Branch $shipTo
     */
    public function setShipTo(Branch $shipTo)
    {
        $this->shipTo = $shipTo;
    }

    /**
     * @return DateTime
     */
    public function getTenderReplyDate()
    {
        return $this->tenderReplyDate;
    }

    /**
     * @param DateTime $tenderReplyDate
     */
    public function setTenderReplyDate($tenderReplyDate)
    {
        $this->tenderReplyDate = $tenderReplyDate;
    }

    /**
     * @return \DateTime
     */
    public function getDeliveryDate()
    {
        return $this->deliveryDate;
    }

    /**
     * @param \DateTime $deliveryDate
     */
    public function setDeliveryDate($deliveryDate)
    {
        $this->deliveryDate = $deliveryDate;
    }

    /**
     * @return bool
     */
    public function isDelete()
    {
        return $this->isDelete;
    }

    /**
     * @param bool $isDelete
     */
    public function setIsDelete($isDelete)
    {
        $this->isDelete = $isDelete;
    }

    /**
     * @return Vendor
     */
    public function getVendor()
    {
        return $this->vendor;
    }

    /**
     * @param Vendor $vendor
     */
    public function setVendor(Vendor $vendor)
    {
        $this->vendor = $vendor;
    }





}
