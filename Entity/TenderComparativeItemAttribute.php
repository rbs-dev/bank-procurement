<?php

namespace Terminalbd\ProcurementBundle\Entity;

use App\Entity\Application\Procurement;
use App\Entity\Core\Setting;
use App\Entity\Domain\Branch;
use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\ModuleProcessItem;
use App\Entity\Domain\Vendor;
use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints\Date;
use Terminalbd\GenericBundle\Entity\CategoryMeta;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\InventoryBundle\Entity\StockBook;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\ProcurementBundle\Repository\TenderComparativeItemAttributeRepository")
 * @ORM\Table(name="procu_tender_comparative_itemattribute")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class TenderComparativeItemAttribute
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var TenderComparative
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderComparative", inversedBy="tenderComparativeAttributeItems")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $tenderComparative;


     /**
     * @var TenderVendor
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderVendor" , inversedBy="tenderComparativeAttributeItems")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $tenderVendor;

     /**
     * @var TenderItem
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderItem" , inversedBy="tenderComparativeAttributeItems")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $tenderItem;

    /**
     * @var StockBook
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\StockBook")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $stockBook;

    /**
     * @var Stock
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Stock")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $stock;


    /**
     * @var TenderComparativeItem
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderComparativeItem" , inversedBy="tenderComparativeAttributeItems")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $tenderComparativeItem;

    /**
     * @var CategoryMeta
     * @ORM\ManyToOne(targetEntity="Terminalbd\GenericBundle\Entity\CategoryMeta", inversedBy="tenderComparativeAttributeItems")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $categoryMeta;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $metaKey;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $metaValue;


    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status = true;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return TenderVendor
     */
    public function getTenderVendor()
    {
        return $this->tenderVendor;
    }

    /**
     * @param TenderVendor $tenderVendor
     */
    public function setTenderVendor($tenderVendor)
    {
        $this->tenderVendor = $tenderVendor;
    }

    /**
     * @return TenderItem
     */
    public function getTenderItem()
    {
        return $this->tenderItem;
    }

    /**
     * @param TenderItem $tenderItem
     */
    public function setTenderItem($tenderItem)
    {
        $this->tenderItem = $tenderItem;
    }

    /**
     * @return float
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param float $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return float
     */
    public function getUnitPrice()
    {
        return $this->unitPrice;
    }

    /**
     * @param float $unitPrice
     */
    public function setUnitPrice($unitPrice)
    {
        $this->unitPrice = $unitPrice;
    }

    /**
     * @return float
     */
    public function getSubTotal()
    {
        return $this->subTotal;
    }

    /**
     * @param float $subTotal
     */
    public function setSubTotal($subTotal)
    {
        $this->subTotal = $subTotal;
    }

    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return TenderComparative
     */
    public function getTenderComparative()
    {
        return $this->tenderComparative;
    }

    /**
     * @param TenderComparative $tenderComparative
     */
    public function setTenderComparative($tenderComparative)
    {
        $this->tenderComparative = $tenderComparative;
    }

    /**
     * @return CategoryMeta
     */
    public function getCategoryMeta()
    {
        return $this->categoryMeta;
    }

    /**
     * @param CategoryMeta $categoryMeta
     */
    public function setCategoryMeta($categoryMeta)
    {
        $this->categoryMeta = $categoryMeta;
    }

    /**
     * @return string
     */
    public function getMetaKey()
    {
        return $this->metaKey;
    }

    /**
     * @param string $metaKey
     */
    public function setMetaKey($metaKey)
    {
        $this->metaKey = $metaKey;
    }

    /**
     * @return string
     */
    public function getMetaValue()
    {
        return $this->metaValue;
    }

    /**
     * @param string $metaValue
     */
    public function setMetaValue($metaValue)
    {
        $this->metaValue = $metaValue;
    }

    /**
     * @return TenderComparativeItem
     */
    public function getTenderComparativeItem()
    {
        return $this->tenderComparativeItem;
    }

    /**
     * @param TenderComparativeItem $tenderComparativeItem
     */
    public function setTenderComparativeItem($tenderComparativeItem)
    {
        $this->tenderComparativeItem = $tenderComparativeItem;
    }

    /**
     * @return StockBook
     */
    public function getStockBook()
    {
        return $this->stockBook;
    }

    /**
     * @param StockBook $stockBook
     */
    public function setStockBook($stockBook)
    {
        $this->stockBook = $stockBook;
    }

    /**
     * @return Stock
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param Stock $stock
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    }








}
