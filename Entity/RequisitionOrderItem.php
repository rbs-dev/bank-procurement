<?php

namespace Terminalbd\ProcurementBundle\Entity;

use App\Entity\Application\SecurityBilling;
use App\Entity\Core\Customer;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Terminalbd\GenericBundle\Entity\Item;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\InventoryBundle\Entity\StockBook;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\ProcurementBundle\Repository\RequisitionOrderItemRepository")
 * @ORM\Table(name="procu_requisition_order_item")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class RequisitionOrderItem
{

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;


    /**
     * @var RequisitionOrder
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\RequisitionOrder",inversedBy="requisitionOrderItems", cascade={"persist"}))
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $requisitionOrder;


     /**
     * @var OrderDelivery
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\OrderDelivery",inversedBy="requisitionOrderItems", cascade={"persist"}))
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $orderDelivery;


     /**
     * @var RequisitionItem
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\RequisitionItem")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $requisitionItem;

    /**
     * @var RequisitionIssueItem
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\RequisitionIssueItem")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $requisitionIssueItem;


    /**
     * @var Requisition
     *
     * @ORM\OneToMany(targetEntity="Terminalbd\ProcurementBundle\Entity\RequisitionItemHistory",mappedBy="requisitionItem")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @ORM\OrderBy({"id" = "DESC"})
     */
    private $requisitionItemHistory;


    /**
     * @var Particular
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\Particular")
     */
    private $deliveryMethod;

    /**
     * @var Stock
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Stock")
     */
    private $stock;

    /**
     * @var StockBook
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\StockBook")
     */
    private $stockBook;


    /**
     * @var RequisitionItem
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\RequisitionItem")
     */
    private $lastRequisitionItem;


    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $unit;


    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $description;


    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $actualQuantity=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $quantity=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $stockIn = 0;


    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $subTotal=0;


     /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $price=0;


    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return string
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @param string $unit
     */
    public function setUnit( $unit)
    {
        $this->unit = $unit;
    }

    /**
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param string $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return SecurityBilling
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param SecurityBilling $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @return float
     */
    public function getSubTotal()
    {
        return $this->subTotal;
    }

    /**
     * @param float $subTotal
     */
    public function setSubTotal( $subTotal)
    {
        $this->subTotal = $subTotal;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param float $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }


    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return Requisition
     */
    public function getRequisition()
    {
        return $this->requisition;
    }

    /**
     * @param Requisition $requisition
     */
    public function setRequisition($requisition)
    {
        $this->requisition = $requisition;
    }

    /**
     * @return Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param Item $item
     */
    public function setItem($item)
    {
        $this->item = $item;
    }

    /**
     * @return Stock
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param Stock $stock
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return float
     */
    public function getActualQuantity()
    {
        return $this->actualQuantity;
    }

    /**
     * @param float $actualQuantity
     */
    public function setActualQuantity($actualQuantity)
    {
        $this->actualQuantity = $actualQuantity;
    }

    /**
     * @return float
     */
    public function getStockIn()
    {
        return $this->stockIn;
    }

    /**
     * @param float $stockIn
     */
    public function setStockIn($stockIn)
    {
        $this->stockIn = $stockIn;
    }

    /**
     * @return RequisitionItem
     */
    public function getLastRequisitionItem()
    {
        return $this->lastRequisitionItem;
    }

    /**
     * @param RequisitionItem $lastRequisitionItem
     */
    public function setLastRequisitionItem($lastRequisitionItem)
    {
        $this->lastRequisitionItem = $lastRequisitionItem;
    }

    /**
     * @return Requisition
     */
    public function getRequisitionItemHistory(): Requisition
    {
        return $this->requisitionItemHistory;
    }

    /**
     * @return RequisitionOrder
     */
    public function getRequisitionOrder()
    {
        return $this->requisitionOrder;
    }

    /**
     * @param RequisitionOrder $requisitionOrder
     */
    public function setRequisitionOrder(RequisitionOrder $requisitionOrder)
    {
        $this->requisitionOrder = $requisitionOrder;
    }

    /**
     * @return RequisitionItem
     */
    public function getRequisitionItem()
    {
        return $this->requisitionItem;
    }

    /**
     * @param RequisitionItem $requisitionItem
     */
    public function setRequisitionItem($requisitionItem)
    {
        $this->requisitionItem = $requisitionItem;
    }

    /**
     * @return Particular
     */
    public function getDeliveryMethod()
    {
        return $this->deliveryMethod;
    }

    /**
     * @param Particular $deliveryMethod
     */
    public function setDeliveryMethod($deliveryMethod)
    {
        $this->deliveryMethod = $deliveryMethod;
    }

    /**
     * @return OrderDelivery
     */
    public function getOrderDelivery()
    {
        return $this->orderDelivery;
    }

    /**
     * @param OrderDelivery $orderDelivery
     */
    public function setOrderDelivery($orderDelivery)
    {
        $this->orderDelivery = $orderDelivery;
    }

    /**
     * @return RequisitionIssueItem
     */
    public function getRequisitionIssueItem()
    {
        return $this->requisitionIssueItem;
    }

    /**
     * @param RequisitionIssueItem $requisitionIssueItem
     */
    public function setRequisitionIssueItem( $requisitionIssueItem)
    {
        $this->requisitionIssueItem = $requisitionIssueItem;
    }

    /**
     * @return StockBook
     */
    public function getStockBook()
    {
        return $this->stockBook;
    }

    /**
     * @param StockBook $stockBook
     */
    public function setStockBook($stockBook)
    {
        $this->stockBook = $stockBook;
    }

}
