<?php

namespace Terminalbd\ProcurementBundle\Entity;

use App\Entity\Application\Procurement;
use App\Entity\Core\Setting;
use App\Entity\Domain\Branch;
use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\ModuleProcessItem;
use App\Entity\Domain\Vendor;
use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints\Date;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\ProcurementBundle\Repository\TenderBatchRepository")
 * @ORM\Table(name="procu_tender_batch")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class TenderBatch
{

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @var Procurement
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\Procurement")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $config;


    /**
     * @var ProcurementProcess
     *
     * @ORM\OneToOne(targetEntity="ProcurementProcess")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="approveProcess_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     * })
     */
    private $approveProcess;


     /**
     * @var ModuleProcess
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain\ModuleProcess")
     */
    private $moduleProcess;

    /**
     * @var TenderBatchItem
     *
     * @ORM\OneToMany(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderBatchItem", mappedBy="tenderBatch")
     * @ORM\OrderBy({"created" = "DESC"})
     */
    private $tenderBatchItems;

    /**
     * @var Setting
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Setting")
     */
    private $department;


    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $createdBy;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $reportTo;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $assignTo;

    /**
     * @var integer
     * @ORM\Column(type="integer",nullable=true)
     */
    private $code=0;

    /**
     * @var integer
     * @ORM\Column(type="integer",nullable=true)
     */
    private $processOrdering = 0;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $invoice;

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $content;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $process='New';

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $module ="tender-batch";


    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $waitingProcess;


     /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $comment;


     /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status = true;


    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isDelete = false;


    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Procurement
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param Procurement $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @return ProcurementProcess
     */
    public function getApproveProcess()
    {
        return $this->approveProcess;
    }

    /**
     * @param ProcurementProcess $approveProcess
     */
    public function setApproveProcess($approveProcess)
    {
        $this->approveProcess = $approveProcess;
    }

    /**
     * @return ModuleProcess
     */
    public function getModuleProcess()
    {
        return $this->moduleProcess;
    }

    /**
     * @param ModuleProcess $moduleProcess
     */
    public function setModuleProcess($moduleProcess)
    {
        $this->moduleProcess = $moduleProcess;
    }

    /**
     * @return TenderBatchItem
     */
    public function getTenderBatchItems()
    {
        return $this->tenderBatchItems;
    }


    /**
     * @return Setting
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @param Setting $department
     */
    public function setDepartment($department)
    {
        $this->department = $department;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return mixed
     */
    public function getReportTo()
    {
        return $this->reportTo;
    }

    /**
     * @param mixed $reportTo
     */
    public function setReportTo($reportTo)
    {
        $this->reportTo = $reportTo;
    }

    /**
     * @return mixed
     */
    public function getAssignTo()
    {
        return $this->assignTo;
    }

    /**
     * @param mixed $assignTo
     */
    public function setAssignTo($assignTo)
    {
        $this->assignTo = $assignTo;
    }

    /**
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param int $code
     */
    public function setCode( $code)
    {
        $this->code = $code;
    }

    /**
     * @return int
     */
    public function getProcessOrdering()
    {
        return $this->processOrdering;
    }

    /**
     * @param int $processOrdering
     */
    public function setProcessOrdering( $processOrdering)
    {
        $this->processOrdering = $processOrdering;
    }

    /**
     * @return string
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * @param string $invoice
     */
    public function setInvoice($invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * @return string
     */
    public function getProcess()
    {
        return $this->process;
    }

    /**
     * @param string $process
     */
    public function setProcess($process)
    {
        $this->process = $process;
    }

    /**
     * @return string
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @param string $module
     */
    public function setModule($module)
    {
        $this->module = $module;
    }

    /**
     * @return string
     */
    public function getWaitingProcess()
    {
        return $this->waitingProcess;
    }

    /**
     * @param string $waitingProcess
     */
    public function setWaitingProcess($waitingProcess)
    {
        $this->waitingProcess = $waitingProcess;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return bool
     */
    public function isDelete()
    {
        return $this->isDelete;
    }

    /**
     * @param bool $isDelete
     */
    public function setIsDelete($isDelete)
    {
        $this->isDelete = $isDelete;
    }



}
