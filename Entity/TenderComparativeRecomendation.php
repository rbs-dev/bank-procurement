<?php

namespace Terminalbd\ProcurementBundle\Entity;

use App\Entity\Application\Procurement;
use App\Entity\Domain\ModuleProcess;
use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\ProcurementBundle\Repository\TenderComparativeRepository")
 * @ORM\Table(name="procu_tender_comparative_recomendation")
 * @UniqueEntity(fields="tender",message="Tender & Tender Comparative one to one relationship")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class TenderComparativeRecomendation
{

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @var Procurement
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\Procurement")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $config;

    /**
     * @var ProcurementProcess
     *
     * @ORM\OneToOne(targetEntity="ProcurementProcess")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="approveProcess_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     * })
     */
    private $approveProcess;



    /**
     * @var TenderComparative
     * @ORM\OneToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderComparative", inversedBy="tenderComparative")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $tenderComparative;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $reportTo;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $approveTo;


    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $content;

     /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $comment;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status = true;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return string
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * @param string $invoice
     */
    public function setInvoice( $invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * @return TenderItem
     */
    public function getTenderItems()
    {
        return $this->tenderItems;
    }

    /**
     * @return Procurement
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param Procurement $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }


    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return mixed
     */
    public function getReportTo()
    {
        return $this->reportTo;
    }

    /**
     * @param mixed $reportTo
     */
    public function setReportTo($reportTo)
    {
        $this->reportTo = $reportTo;
    }

    /**
     * @return mixed
     */
    public function getTransferTo()
    {
        return $this->transferTo;
    }

    /**
     * @param mixed $transferTo
     */
    public function setTransferTo($transferTo)
    {
        $this->transferTo = $transferTo;
    }

    /**
     * @return float
     */
    public function getSubTotal()
    {
        return $this->subTotal;
    }

    /**
     * @param float $subTotal
     */
    public function setSubTotal(float $subTotal)
    {
        $this->subTotal = $subTotal;
    }

    /**
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param int $code
     */
    public function setCode(int $code)
    {
        $this->code = $code;
    }

    /**
     * @return int
     */
    public function getProcessOrdering()
    {
        return $this->processOrdering;
    }

    /**
     * @param int $processOrdering
     */
    public function setProcessOrdering(int $processOrdering)
    {
        $this->processOrdering = $processOrdering;
    }

    /**
     * @return string
     */
    public function getProcess()
    {
        return $this->process;
    }

    /**
     * @param string $process
     */
    public function setProcess(string $process)
    {
        $this->process = $process;
    }

    /**
     * @return string
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @param string $module
     */
    public function setModule(string $module)
    {
        $this->module = $module;
    }

    /**
     * @return string
     */
    public function getWaitingProcess()
    {
        return $this->waitingProcess;
    }

    /**
     * @param string $waitingProcess
     */
    public function setWaitingProcess(string $waitingProcess)
    {
        $this->waitingProcess = $waitingProcess;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content)
    {
        $this->content = $content;
    }

    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated(): \DateTime
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated(\DateTime $updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject(string $subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getTerms()
    {
        return $this->terms;
    }

    /**
     * @param string $terms
     */
    public function setTerms(string $terms)
    {
        $this->terms = $terms;
    }

    /**
     * @return Vendor
     */
    public function getVendors()
    {
        return $this->vendors;
    }

    /**
     * @param Vendor $vendors
     */
    public function setVendors($vendors)
    {
        $this->vendors = $vendors;
    }

    /**
     * @return TenderVendor
     */
    public function getTenderVendors()
    {
        return $this->tenderVendors;
    }

    /**
     * @return string
     */
    public function getBusinessGroup()
    {
        return $this->businessGroup;
    }

    /**
     * @param string $businessGroup
     */
    public function setBusinessGroup($businessGroup)
    {
        $this->businessGroup = $businessGroup;
    }

    /**
     * @return Setting
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @param Setting $department
     */
    public function setDepartment( $department)
    {
        $this->department = $department;
    }

    /**
     * @return Tender
     */
    public function getTender()
    {
        return $this->tender;
    }

    /**
     * @param Tender $tender
     */
    public function setTender($tender)
    {
        $this->tender = $tender;
    }

    /**
     * @return ModuleProcess
     */
    public function getModuleProcess()
    {
        return $this->moduleProcess;
    }

    /**
     * @param ModuleProcess $moduleProcess
     */
    public function setModuleProcess($moduleProcess)
    {
        $this->moduleProcess = $moduleProcess;
    }

    /**
     * @return TenderComparativeItem
     */
    public function getTenderComparativeItems()
    {
        return $this->tenderComparativeItems;
    }

    /**
     * @return ProcurementProcess
     */
    public function getApproveProcess()
    {
        return $this->approveProcess;
    }

    /**
     * @param ProcurementProcess $approveProcess
     */
    public function setApproveProcess($approveProcess)
    {
        $this->approveProcess = $approveProcess;
    }

    /**
     * @return TenderMemo
     */
    public function getTenderMemo()
    {
        return $this->tenderMemo;
    }

    /**
     * @return mixed
     */
    public function getApproveTo()
    {
        return $this->approveTo;
    }

    /**
     * @param mixed $approveTo
     */
    public function setApproveTo($approveTo)
    {
        $this->approveTo = $approveTo;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }




}
