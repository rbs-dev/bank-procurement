<?php

namespace Terminalbd\ProcurementBundle\Entity;
use App\Entity\Application\Procurement;
use App\Entity\Domain\ModuleProcessItem;
use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\ProcurementBundle\Repository\ProcurementProcessRepository")
     * @ORM\Table(name="procu_process")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ProcurementProcess
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var Procurement
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\Procurement")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
      private $config;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $createdBy;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $assignTo;

    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $assignToRole;

     /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $assignToRoleName;

    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $process;

    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $module;

     /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $entityId;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer",nullable=true)
     */
    private $ordering = 0;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean",nullable=true)
     */
    private $isRejected;

     /**
     * @var bool
     *
     * @ORM\Column(type="boolean",nullable=true)
     */
    private $isMandatory;

     /**
     * @var bool
     *
     * @ORM\Column(type="boolean",nullable=true)
     */
    private $custom;

     /**
     * @var bool
     *
     * @ORM\Column(type="boolean",nullable=true)
     */
    private $status = true;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean",nullable=true)
     */
    private $close = false;


    /**
     * @var string
     *
     * @ORM\Column(type="text",nullable=true)
     */
    private $comment;


    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime", nullable=true)
     */
    private $updated;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }


    /**
     * @return Procurement
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param Procurement $config
     */
    public function setConfig(Procurement $config)
    {
        $this->config = $config;
    }


    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return string
     */
    public function getProcess()
    {
        return $this->process;
    }

    /**
     * @param string $process
     */
    public function setProcess($process)
    {
        $this->process = $process;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return bool
     */
    public function isRejected()
    {
        return $this->isRejected;
    }

    /**
     * @param bool $isRejected
     */
    public function setIsRejected($isRejected)
    {
        $this->isRejected = $isRejected;
    }

    /**
     * @return ModuleProcessItem
     */
    public function getModuleProcessItem()
    {
        return $this->moduleProcessItem;
    }

    /**
     * @param ModuleProcessItem $moduleProcessItem
     */
    public function setModuleProcessItem(ModuleProcessItem $moduleProcessItem)
    {
        $this->moduleProcessItem = $moduleProcessItem;
    }

    /**
     * @return User
     */
    public function getAssignTo()
    {
        return $this->assignTo;
    }

    /**
     * @param User $assignTo
     */
    public function setAssignTo($assignTo)
    {
        $this->assignTo = $assignTo;
    }



    /**
     * @return int
     */
    public function getOrdering()
    {
        return $this->ordering;
    }

    /**
     * @param int $ordering
     */
    public function setOrdering(int $ordering)
    {
        $this->ordering = $ordering;
    }

    /**
     * @return bool
     */
    public function isMandatory()
    {
        return $this->isMandatory;
    }

    /**
     * @param bool $isMandatory
     */
    public function setIsMandatory($isMandatory)
    {
        $this->isMandatory = $isMandatory;
    }

    /**
     * @return bool
     */
    public function isCustom()
    {
        return $this->custom;
    }

    /**
     * @param bool $custom
     */
    public function setCustom($custom)
    {
        $this->custom = $custom;
    }

    /**
     * @return bool
     */
    public function isClose(): bool
    {
        return $this->close;
    }

    /**
     * @param bool $close
     */
    public function setClose(bool $close): void
    {
        $this->close = $close;
    }



    /**
     * @return string
     */
    public function getEntityId()
    {
        return $this->entityId;
    }

    /**
     * @param string $entityId
     */
    public function setEntityId($entityId)
    {
        $this->entityId = $entityId;
    }

    /**
     * @return string
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @param string $module
     */
    public function setModule($module)
    {
        $this->module = $module;
    }

    /**
     * @return Tender
     */
    public function getTender()
    {
        return $this->tender;
    }

    /**
     * @param Tender $tender
     */
    public function setTender($tender)
    {
        $this->tender = $tender;
    }

    /**
     * @return string
     */
    public function getAssignToRole()
    {
        return $this->assignToRole;
    }

    /**
     * @param string $assignToRole
     */
    public function setAssignToRole($assignToRole)
    {
        $this->assignToRole = $assignToRole;
    }

    /**
     * @return string
     */
    public function getAssignToRoleName()
    {
        return $this->assignToRoleName;
    }

    /**
     * @param string $assignToRoleName
     */
    public function setAssignToRoleName($assignToRoleName)
    {
        $this->assignToRoleName = $assignToRoleName;
    }


    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated(\DateTime $updated)
    {
        $this->updated = $updated;
    }






}
