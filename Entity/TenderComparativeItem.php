<?php

namespace Terminalbd\ProcurementBundle\Entity;

use App\Entity\Application\Procurement;
use App\Entity\Core\Setting;
use App\Entity\Domain\Branch;
use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\ModuleProcessItem;
use App\Entity\Domain\Vendor;
use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints\Date;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\InventoryBundle\Entity\StockBook;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\ProcurementBundle\Repository\TenderComparativeItemRepository")
 * @ORM\Table(name="procu_tender_comparative_item")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class TenderComparativeItem
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @var TenderComparative
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderComparative", inversedBy="tenderComparativeItems",cascade={"persist"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $tenderComparative;


     /**
     * @var TenderVendor
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderVendor", inversedBy="tenderComparativeItems")
      * @ORM\JoinColumn(onDelete="CASCADE")
      **/
    private  $tenderVendor;

     /**
     * @var TenderItem
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderItem", inversedBy="tenderComparativeItems")
      * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $tenderItem;

    /**
     * @var StockBook
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\StockBook")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $stockBook;

    /**
     * @var Stock
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Stock")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $stock;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $quantity=0;

     /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $issueQuantity=0;

     /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $remainingQuantity = 0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $unitPrice=0;

     /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $price=0;

     /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $revisedUnitPrice=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $subTotal=0;


     /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $revisedSubTotal=0;


    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status = true;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return TenderVendor
     */
    public function getTenderVendor()
    {
        return $this->tenderVendor;
    }

    /**
     * @param TenderVendor $tenderVendor
     */
    public function setTenderVendor($tenderVendor)
    {
        $this->tenderVendor = $tenderVendor;
    }

    /**
     * @return TenderItem
     */
    public function getTenderItem()
    {
        return $this->tenderItem;
    }

    /**
     * @param TenderItem $tenderItem
     */
    public function setTenderItem($tenderItem)
    {
        $this->tenderItem = $tenderItem;
    }

    /**
     * @return float
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param float $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return float
     */
    public function getUnitPrice()
    {
        return $this->unitPrice;
    }

    /**
     * @param float $unitPrice
     */
    public function setUnitPrice($unitPrice)
    {
        $this->unitPrice = $unitPrice;
    }

    /**
     * @return float
     */
    public function getSubTotal()
    {
        return $this->subTotal;
    }

    /**
     * @param float $subTotal
     */
    public function setSubTotal($subTotal)
    {
        $this->subTotal = $subTotal;
    }

    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return TenderComparative
     */
    public function getTenderComparative()
    {
        return $this->tenderComparative;
    }

    /**
     * @param TenderComparative $tenderComparative
     */
    public function setTenderComparative($tenderComparative)
    {
        $this->tenderComparative = $tenderComparative;
    }

    /**
     * @return StockBook
     */
    public function getStockBook()
    {
        return $this->stockBook;
    }

    /**
     * @param StockBook $stockBook
     */
    public function setStockBook($stockBook)
    {
        $this->stockBook = $stockBook;
    }

    /**
     * @return Stock
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param Stock $stock
     */
    public function setStock(Stock $stock)
    {
        $this->stock = $stock;
    }

    /**
     * @return float
     */
    public function getRevisedUnitPrice()
    {
        return $this->revisedUnitPrice;
    }

    /**
     * @param float $revisedUnitPrice
     */
    public function setRevisedUnitPrice( $revisedUnitPrice)
    {
        $this->revisedUnitPrice = $revisedUnitPrice;
    }

    /**
     * @return float
     */
    public function getRevisedSubTotal()
    {
        return $this->revisedSubTotal;
    }

    /**
     * @param float $revisedSubTotal
     */
    public function setRevisedSubTotal($revisedSubTotal)
    {
        $this->revisedSubTotal = $revisedSubTotal;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return float
     */
    public function getIssueQuantity()
    {
        return $this->issueQuantity;
    }

    /**
     * @param float $issueQuantity
     */
    public function setIssueQuantity(float $issueQuantity)
    {
        $this->issueQuantity = $issueQuantity;
    }

    /**
     * @return float
     */
    public function getRemainingQuantity()
    {
        return $this->remainingQuantity;
    }

    /**
     * @param float $remainingQuantity
     */
    public function setRemainingQuantity($remainingQuantity)
    {
        $this->remainingQuantity = $remainingQuantity;
    }




}
