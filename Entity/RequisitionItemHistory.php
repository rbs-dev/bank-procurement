<?php

namespace Terminalbd\ProcurementBundle\Entity;

use App\Entity\Application\SecurityBilling;
use App\Entity\Core\Customer;
use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Terminalbd\GenericBundle\Entity\Item;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\ProcurementBundle\Repository\RequisitionItemHistoryRepository")
 * @ORM\Table(name="procu_requisition_item_history")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class RequisitionItemHistory
{

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;


    /**
     * @var RequisitionItem
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\RequisitionItem",inversedBy="requisitionItemHistories")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $requisitionItem;

    /**
     * @var RequisitionIssueItem
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\RequisitionIssueItem",inversedBy="requisitionItemHistory")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $requisitionIssueItem;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $createdBy;


    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $actualQuantity=0;


    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $quantity=0;



    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return RequisitionItem
     */
    public function getRequisitionItem(): RequisitionItem
    {
        return $this->requisitionItem;
    }

    /**
     * @param RequisitionItem $requisitionItem
     */
    public function setRequisitionItem(RequisitionItem $requisitionItem)
    {
        $this->requisitionItem = $requisitionItem;
    }

    /**
     * @return float
     */
    public function getActualQuantity()
    {
        return $this->actualQuantity;
    }

    /**
     * @param float $actualQuantity
     */
    public function setActualQuantity(float $actualQuantity)
    {
        $this->actualQuantity = $actualQuantity;
    }

    /**
     * @return float
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param float $quantity
     */
    public function setQuantity(float $quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated(): \DateTime
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated(\DateTime $updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return RequisitionIssueItem
     */
    public function getRequisitionIssueItem(): RequisitionIssueItem
    {
        return $this->requisitionIssueItem;
    }

    /**
     * @param RequisitionIssueItem $requisitionIssueItem
     */
    public function setRequisitionIssueItem( $requisitionIssueItem)
    {
        $this->requisitionIssueItem = $requisitionIssueItem;
    }

    /**
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     */
    public function setCreatedBy(User $createdBy)
    {
        $this->createdBy = $createdBy;
    }







}
