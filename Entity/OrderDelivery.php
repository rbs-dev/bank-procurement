<?php

namespace Terminalbd\ProcurementBundle\Entity;

use App\Entity\Application\Procurement;
use App\Entity\Core\Setting;
use App\Entity\Domain\Branch;
use App\Entity\Domain\ModuleProcessItem;
use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints\Date;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\ProcurementBundle\Repository\OrderDeliveryRepository")
 * @ORM\Table(name="procu_order_delivery")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class OrderDelivery
{

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @var Procurement
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\Procurement")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $config;

    /**
     * @var ProcurementProcess
     *
     * @ORM\OneToOne(targetEntity="ProcurementProcess")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="approveProcess_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     * })
     */
    private $approveProcess;


    /**
     * @var RequisitionOrder
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\RequisitionOrder", inversedBy="orderDeliveries")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @ORM\OrderBy({"id" = "DESC"})
     */
    private $requisitionOrder;

    /**
     * @var DeliveryBatch
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\DeliveryBatch")
     */
    private $deliveryBatch;

    /**
     * @var RequisitionOrderItem
     *
     * @ORM\OneToMany(targetEntity="Terminalbd\ProcurementBundle\Entity\RequisitionOrderItem", mappedBy="orderDelivery")
     * @ORM\OrderBy({"id" = "DESC"})
     */
    private $requisitionOrderItems;


     /**
     * @var Branch
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain\Branch")
     */
     private $branch;

     /**
     * @var Particular
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\Particular")
     */
     private $deliveryMethod;

     /**
     * @var Particular
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\Particular")
     */
     private $courier;


    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $comment;


    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
     private $receiveUser;


    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
     private $approvedBy;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $createdBy;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $deliveryNo;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $cnNo;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $comments;


    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $receiverName;


    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $receiverDesignation;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $receiverAddress;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $receiverMobile;


    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $subTotal=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $vat=0;


    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $total=0;


    /**
     * @var integer
     * @ORM\Column(type="integer",nullable=true)
     */
    private $code = 0;



    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $process='New';


    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $filename;


    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status = true;


    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $approved = false;


    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Procurement
     */
    public function getConfig(): Procurement
    {
        return $this->config;
    }

    /**
     * @param Procurement $config
     */
    public function setConfig(Procurement $config)
    {
        $this->config = $config;
    }

    /**
     * @return RequisitionOrder
     */
    public function getRequisitionOrder(): RequisitionOrder
    {
        return $this->requisitionOrder;
    }

    /**
     * @param RequisitionOrder $requisitionOrder
     */
    public function setRequisitionOrder(RequisitionOrder $requisitionOrder)
    {
        $this->requisitionOrder = $requisitionOrder;
    }

    /**
     * @return Branch
     */
    public function getBranch()
    {
        return $this->branch;
    }

    /**
     * @param Branch $branch
     */
    public function setBranch($branch)
    {
        $this->branch = $branch;
    }

    /**
     * @return Particular
     */
    public function getDeliveryMethod()
    {
        return $this->deliveryMethod;
    }

    /**
     * @param Particular $deliveryMethod
     */
    public function setDeliveryMethod($deliveryMethod)
    {
        $this->deliveryMethod = $deliveryMethod;
    }

    /**
     * @return User
     */
    public function getReceiveUser(): User
    {
        return $this->receiveUser;
    }

    /**
     * @param User $receiveUser
     */
    public function setReceiveUser(User $receiveUser)
    {
        $this->receiveUser = $receiveUser;
    }

    /**
     * @return RequisitionOrderItem
     */
    public function getRequisitionOrderItems()
    {
        return $this->requisitionOrderItems;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return string
     */
    public function getDeliveryNo()
    {
        return $this->deliveryNo;
    }

    /**
     * @param string $deliveryNo
     */
    public function setDeliveryNo($deliveryNo)
    {
        $this->deliveryNo = $deliveryNo;
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @param int $code
     */
    public function setCode(int $code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return float
     */
    public function getSubTotal(): float
    {
        return $this->subTotal;
    }

    /**
     * @param float $subTotal
     */
    public function setSubTotal(float $subTotal)
    {
        $this->subTotal = $subTotal;
    }

    /**
     * @return float
     */
    public function getVat(): float
    {
        return $this->vat;
    }

    /**
     * @param float $vat
     */
    public function setVat(float $vat)
    {
        $this->vat = $vat;
    }

    /**
     * @return float
     */
    public function getTotal(): float
    {
        return $this->total;
    }

    /**
     * @param float $total
     */
    public function setTotal(float $total)
    {
        $this->total = $total;
    }

    /**
     * @return string
     */
    public function getProcess()
    {
        return $this->process;
    }

    /**
     * @param string $process
     */
    public function setProcess($process)
    {
        $this->process = $process;
    }

    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @param string $filename
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated(): \DateTime
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated(\DateTime $updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return Particular
     */
    public function getCourier()
    {
        return $this->courier;
    }

    /**
     * @param Particular $courier
     */
    public function setCourier($courier)
    {
        $this->courier = $courier;
    }

    /**
     * @return string
     */
    public function getCnNo()
    {
        return $this->cnNo;
    }

    /**
     * @param string $cnNo
     */
    public function setCnNo($cnNo)
    {
        $this->cnNo = $cnNo;
    }

    /**
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param string $comments
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
    }

    /**
     * @return string
     */
    public function getReceiverName()
    {
        return $this->receiverName;
    }

    /**
     * @param string $receiverName
     */
    public function setReceiverName($receiverName)
    {
        $this->receiverName = $receiverName;
    }

    /**
     * @return string
     */
    public function getReceiverDesignation()
    {
        return $this->receiverDesignation;
    }

    /**
     * @param string $receiverDesignation
     */
    public function setReceiverDesignation($receiverDesignation)
    {
        $this->receiverDesignation = $receiverDesignation;
    }

    /**
     * @return string
     */
    public function getReceiverAddress()
    {
        return $this->receiverAddress;
    }

    /**
     * @param string $receiverAddress
     */
    public function setReceiverAddress($receiverAddress)
    {
        $this->receiverAddress = $receiverAddress;
    }

    /**
     * @return string
     */
    public function getReceiverMobile()
    {
        return $this->receiverMobile;
    }

    /**
     * @param string $receiverMobile
     */
    public function setReceiverMobile($receiverMobile)
    {
        $this->receiverMobile = $receiverMobile;
    }

    /**
     * @return bool
     */
    public function isApproved()
    {
        return $this->approved;
    }

    /**
     * @param bool $approved
     */
    public function setApproved($approved)
    {
        $this->approved = $approved;
    }

    /**
     * @return User
     */
    public function getApprovedBy()
    {
        return $this->approvedBy;
    }

    /**
     * @param User $approvedBy
     */
    public function setApprovedBy(User $approvedBy)
    {
        $this->approvedBy = $approvedBy;
    }

    /**
     * @return ProcurementProcess
     */
    public function getApproveProcess()
    {
        return $this->approveProcess;
    }

    /**
     * @param ProcurementProcess $approveProcess
     */
    public function setApproveProcess($approveProcess)
    {
        $this->approveProcess = $approveProcess;
    }

    /**
     * @return DeliveryBatch
     */
    public function getDeliveryBatch()
    {
        return $this->deliveryBatch;
    }

    /**
     * @param DeliveryBatch $deliveryBatch
     */
    public function setDeliveryBatch($deliveryBatch)
    {
        $this->deliveryBatch = $deliveryBatch;
    }







}
