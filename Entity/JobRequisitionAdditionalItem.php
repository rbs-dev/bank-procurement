<?php

namespace Terminalbd\ProcurementBundle\Entity;

use App\Entity\Application\SecurityBilling;
use App\Entity\Core\Customer;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Terminalbd\GenericBundle\Entity\Item;
use Terminalbd\GenericBundle\Entity\ItemUnit;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\ProcurementBundle\Repository\JobRequisitionAdditionalItemRepository")
 * @ORM\Table(name="procu_job_requisition_additional_item")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class JobRequisitionAdditionalItem
{

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;


    /**
     * @var JobRequisition
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\JobRequisition",inversedBy="additionalItems")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $jobRequisition;


     /**
     * @var ItemUnit
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\GenericBundle\Entity\ItemUnit")
     */
    private $unit;


    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $description;


    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $price;


    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $quantity;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $subTotal;



    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return JobRequisition
     */
    public function getJobRequisition(): JobRequisition
    {
        return $this->jobRequisition;
    }

    /**
     * @param JobRequisition $jobRequisition
     */
    public function setJobRequisition(JobRequisition $jobRequisition)
    {
        $this->jobRequisition = $jobRequisition;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return float
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param float $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return float
     */
    public function getSubTotal()
    {
        return $this->subTotal;
    }

    /**
     * @param float $subTotal
     */
    public function setSubTotal($subTotal)
    {
        $this->subTotal = $subTotal;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated( $created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated( $updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return ItemUnit
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @param ItemUnit $unit
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;
    }





}
