<?php

namespace Terminalbd\ProcurementBundle\Entity;

use App\Entity\Application\Procurement;
use App\Entity\Core\Setting;
use App\Entity\Domain\Branch;
use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\ModuleProcessItem;
use App\Entity\Domain\Vendor;
use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints\Date;
use Terminalbd\BudgetBundle\Entity\Head;
use Terminalbd\InventoryBundle\Entity\StockBook;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\ProcurementBundle\Repository\TenderWorkorderItemRepository")
 * @ORM\Table(name="procu_tender_workorder_item")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class TenderWorkorderItem
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @var TenderWorkorder
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderWorkorder", inversedBy="workOrderItems",cascade={"persist"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $tenderWorkorder;


     /**
     * @var TenderComparativeItem
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderComparativeItem", inversedBy="tenderComparativeItems")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $tenderComparativeItem;

    /**
     * @var TenderItemDetails
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderItemDetails", inversedBy="workorderItems")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $tenderItemDetail;

      /**
     * @var TenderItem
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderItem")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $tenderItem;

     /**
     * @var StockBook
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\StockBook", inversedBy="workorderItem")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $stockBook;

    /**
     * @var Branch
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain\Branch", inversedBy="workorderItem")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $wearhouse;

    /**
     * @var Stock
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Stock", inversedBy="workorderItem")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $stock;

     /**
     * @var Requisition
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\Requisition")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="requisition_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     * })
     **/
    private  $requisition;

    /**
     * @var RequisitionItem
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\RequisitionItem")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="requisition_item_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     * })
     **/
    private  $requisitionItem;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $name;


    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $description;


    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $quantity=0;

     /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $remaining=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $receive=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $unitPrice=0;

     /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $price=0;

     /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $discount=0;

     /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $total=0;

     /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $grandTotal=0;

     /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $vendorCommission=0;

     /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $vendorCommissionPercent=0;

     /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $vat=0;

     /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $shippingCharge=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $subTotal=0;


    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status = true;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return TenderVendor
     */
    public function getTenderVendor()
    {
        return $this->tenderVendor;
    }

    /**
     * @param TenderVendor $tenderVendor
     */
    public function setTenderVendor($tenderVendor)
    {
        $this->tenderVendor = $tenderVendor;
    }

    /**
     * @return TenderItem
     */
    public function getTenderItem()
    {
        return $this->tenderItem;
    }

    /**
     * @param TenderItem $tenderItem
     */
    public function setTenderItem($tenderItem)
    {
        $this->tenderItem = $tenderItem;
    }

    /**
     * @return float
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param float $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return float
     */
    public function getUnitPrice()
    {
        return $this->unitPrice;
    }

    /**
     * @param float $unitPrice
     */
    public function setUnitPrice($unitPrice)
    {
        $this->unitPrice = $unitPrice;
    }

    /**
     * @return float
     */
    public function getSubTotal()
    {
        return $this->subTotal;
    }

    /**
     * @param float $subTotal
     */
    public function setSubTotal($subTotal)
    {
        $this->subTotal = $subTotal;
    }

    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return TenderComparativeItem
     */
    public function getTenderComparativeItem()
    {
        return $this->tenderComparativeItem;
    }

    /**
     * @param TenderComparativeItem $tenderComparativeItem
     */
    public function setTenderComparativeItem( $tenderComparativeItem)
    {
        $this->tenderComparativeItem = $tenderComparativeItem;
    }

    /**
     * @return TenderWorkorder
     */
    public function getTenderWorkorder()
    {
        return $this->tenderWorkorder;
    }

    /**
     * @param TenderWorkorder $tenderWorkorder
     */
    public function setTenderWorkorder($tenderWorkorder)
    {
        $this->tenderWorkorder = $tenderWorkorder;
    }

    /**
     * @return StockBook
     */
    public function getStockBook()
    {
        return $this->stockBook;
    }

    /**
     * @param StockBook $stockBook
     */
    public function setStockBook($stockBook)
    {
        $this->stockBook = $stockBook;
    }

    /**
     * @return float
     */
    public function getRemaining()
    {
        return $this->remaining;
    }

    /**
     * @param float $remaining
     */
    public function setRemaining($remaining)
    {
        $this->remaining = $remaining;
    }

    /**
     * @return float
     */
    public function getReceive()
    {
        return $this->receive;
    }

    /**
     * @param float $receive
     */
    public function setReceive($receive)
    {
        $this->receive = $receive;
    }

    /**
     * @return Stock
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param Stock $stock
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return Head
     */
    public function getGl()
    {
        return $this->gl;
    }

    /**
     * @param Head $gl
     */
    public function setGl($gl)
    {
        $this->gl = $gl;
    }

    /**
     * @return Requisition
     */
    public function getRequisition()
    {
        return $this->requisition;
    }

    /**
     * @param Requisition $requisition
     */
    public function setRequisition($requisition)
    {
        $this->requisition = $requisition;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return TenderItemDetails
     */
    public function getTenderItemDetail()
    {
        return $this->tenderItemDetail;
    }

    /**
     * @param TenderItemDetails $tenderItemDetail
     */
    public function setTenderItemDetail($tenderItemDetail)
    {
        $this->tenderItemDetail = $tenderItemDetail;
    }

    /**
     * @return RequisitionItem
     */
    public function getRequisitionItem()
    {
        return $this->requisitionItem;
    }

    /**
     * @param RequisitionItem $requisitionItem
     */
    public function setRequisitionItem($requisitionItem)
    {
        $this->requisitionItem = $requisitionItem;
    }

    /**
     * @return Branch
     */
    public function getWearhouse()
    {
        return $this->wearhouse;
    }

    /**
     * @param Branch $wearhouse
     */
    public function setWearhouse($wearhouse)
    {
        $this->wearhouse = $wearhouse;
    }

    /**
     * @return float
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param float $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * @return float
     */
    public function getVendorCommission()
    {
        return $this->vendorCommission;
    }

    /**
     * @param float $vendorCommission
     */
    public function setVendorCommission($vendorCommission)
    {
        $this->vendorCommission = $vendorCommission;
    }

    /**
     * @return float
     */
    public function getVendorCommissionPercent()
    {
        return $this->vendorCommissionPercent;
    }

    /**
     * @param float $vendorCommissionPercent
     */
    public function setVendorCommissionPercent($vendorCommissionPercent)
    {
        $this->vendorCommissionPercent = $vendorCommissionPercent;
    }

    /**
     * @return float
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * @param float $vat
     */
    public function setVat($vat)
    {
        $this->vat = $vat;
    }

    /**
     * @return float
     */
    public function getShippingCharge()
    {
        return $this->shippingCharge;
    }

    /**
     * @param float $shippingCharge
     */
    public function setShippingCharge($shippingCharge)
    {
        $this->shippingCharge = $shippingCharge;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param float $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return float
     */
    public function getGrandTotal()
    {
        return $this->grandTotal;
    }

    /**
     * @param float $grandTotal
     */
    public function setGrandTotal($grandTotal)
    {
        $this->grandTotal = $grandTotal;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }



}
