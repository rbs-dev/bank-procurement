<?php

namespace Terminalbd\ProcurementBundle\Entity;

use App\Entity\Application\Procurement;
use App\Entity\Core\Setting;
use App\Entity\Domain\Branch;
use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\ModuleProcessItem;
use App\Entity\Domain\Vendor;
use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints\Date;
use Terminalbd\InventoryBundle\Entity\StockBook;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\ProcurementBundle\Repository\TenderWorkorderReceiveItemRepository")
 * @ORM\Table(name="procu_tender_workorder_receive_item")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class TenderWorkorderReceiveItem
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @var TenderWorkorderReceive
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderWorkorderReceive", inversedBy="receiveItems")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $workorderReceive;

    /**
     * @var TenderWorkorderItem
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderWorkorderItem", inversedBy="receiveItems")
     * @ORM\JoinColumn(onDelete="SET NULL")
     **/
    private  $tenderWorkorderItem;

     /**
     * @var StockBook
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\StockBook", inversedBy="receiveItems")
     **/
    private  $stockBook;

    /**
     * @var Stock
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Stock", inversedBy="workorderItem")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $stock;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $name;


    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $quantity=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $returnQuantity=0;


    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $unitPrice=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $price=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $subTotal=0;


    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status = true;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return TenderWorkorderReceive
     */
    public function getWorkorderReceive()
    {
        return $this->workorderReceive;
    }

    /**
     * @param TenderWorkorderReceive $workorderReceive
     */
    public function setWorkorderReceive($workorderReceive)
    {
        $this->workorderReceive = $workorderReceive;
    }

    /**
     * @return TenderWorkorderItem
     */
    public function getTenderWorkorderItem()
    {
        return $this->tenderWorkorderItem;
    }

    /**
     * @param TenderWorkorderItem $tenderWorkorderItem
     */
    public function setTenderWorkorderItem($tenderWorkorderItem)
    {
        $this->tenderWorkorderItem = $tenderWorkorderItem;
    }

    /**
     * @return StockBook
     */
    public function getStockBook()
    {
        return $this->stockBook;
    }

    /**
     * @param StockBook $stockBook
     */
    public function setStockBook($stockBook)
    {
        $this->stockBook = $stockBook;
    }

    /**
     * @return float
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param float $quantity
     */
    public function setQuantity(float $quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return float
     */
    public function getUnitPrice()
    {
        return $this->unitPrice;
    }

    /**
     * @param float $unitPrice
     */
    public function setUnitPrice(float $unitPrice)
    {
        $this->unitPrice = $unitPrice;
    }

    /**
     * @return float
     */
    public function getSubTotal()
    {
        return $this->subTotal;
    }

    /**
     * @param float $subTotal
     */
    public function setSubTotal(float $subTotal)
    {
        $this->subTotal = $subTotal;
    }

    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus( $status)
    {
        $this->status = $status;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return Stock
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param Stock $stock
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return float
     */
    public function getReturnQuantity()
    {
        return $this->returnQuantity;
    }

    /**
     * @param float $returnQuantity
     */
    public function setReturnQuantity($returnQuantity)
    {
        $this->returnQuantity = $returnQuantity;
    }






}
