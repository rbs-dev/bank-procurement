<?php

namespace Terminalbd\ProcurementBundle\Entity;

use App\Entity\Application\Procurement;
use App\Entity\Core\Setting;
use App\Entity\Domain\Branch;
use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\ModuleProcessItem;
use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints\Date;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\ProcurementBundle\Repository\RequisitionOrderRepository")
 * @ORM\Table(name="procu_requisition_order")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class RequisitionOrder
{

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @var Procurement
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\Procurement")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $config;

    /**
     * @var ProcurementProcess
     *
     * @ORM\OneToOne(targetEntity="ProcurementProcess")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="approveProcess_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     * })
     */
    private $approveProcess;


    /**
     * @var Requisition
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\Requisition", inversedBy="requisitionOrders")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @ORM\OrderBy({"id" = "DESC"})
     */
    private $requisition;

    /**
     * @var ModuleProcess
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain\ModuleProcess")
     */
    private $moduleProcess;


    /**
     * @var RequisitionIssue
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\RequisitionIssue", inversedBy="requisitionOrder")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @ORM\OrderBy({"id" = "DESC"})
     */
    private $requisitionIssue;


    /**
     * @var RequisitionOrderItem
     *
     * @ORM\OneToMany(targetEntity="Terminalbd\ProcurementBundle\Entity\RequisitionOrderItem", mappedBy="requisitionOrder")
     * @ORM\OrderBy({"id" = "DESC"})
     */
    private $requisitionOrderItems;


    /**
     * @var OrderDelivery
     *
     * @ORM\OneToMany(targetEntity="Terminalbd\ProcurementBundle\Entity\OrderDelivery", mappedBy="requisitionOrder")
     * @ORM\OrderBy({"id" = "DESC"})
     */
    private $orderDeliveries;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $reportTo;

    /**
     * @var integer
     * @ORM\Column(type="integer",nullable=true)
     */
    private $processOrdering = 0;


    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $process='New';

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $module;


    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $waitingProcess="New";

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $businessGroup;

    /**
     * @var Setting
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Setting")
     */
    private $userDepartment;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isDelete = false;


    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $createdBy;


    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $subTotal=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $vat=0;


    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $total=0;


    /**
     * @var integer
     * @ORM\Column(type="integer",nullable=true)
     */
    private $code=0;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $orderNo;


    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $requisitionNote;


    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $filename;

     /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $content;

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $comment;

     /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $acceptence;

    /**
     * @var Date
     *
     * @ORM\Column(name="expectedDate", type="date", nullable=true)
     */
    private $expectedDate;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status = true;


    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Branch
     */
    public function getBranch()
    {
        return $this->branch;
    }

    /**
     * @param Branch $branch
     */
    public function setBranch($branch)
    {
        $this->branch = $branch;
    }

    /**
     * @return Procurement
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param Procurement $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @return RequisitionItem
     */
    public function getRequisitionItems()
    {
        return $this->requisitionItems;
    }

    /**
     * @return Particular
     */
    public function getRequisitionType()
    {
        return $this->requisitionType;
    }

    /**
     * @param Particular $requisitionType
     */
    public function setRequisitionType(Particular $requisitionType)
    {
        $this->requisitionType = $requisitionType;
    }

    /**
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return User
     */
    public function getCheckedBy()
    {
        return $this->checkedBy;
    }

    /**
     * @param User $checkedBy
     */
    public function setCheckedBy(User $checkedBy)
    {
        $this->checkedBy = $checkedBy;
    }

    /**
     * @return User
     */
    public function getApprovedBy()
    {
        return $this->approvedBy;
    }

    /**
     * @param User $approvedBy
     */
    public function setApprovedBy(User $approvedBy)
    {
        $this->approvedBy = $approvedBy;
    }

    /**
     * @return float
     */
    public function getSubTotal()
    {
        return $this->subTotal;
    }

    /**
     * @param float $subTotal
     */
    public function setSubTotal(float $subTotal)
    {
        $this->subTotal = $subTotal;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated(\DateTime $updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return string
     */
    public function getProcess()
    {
        return $this->process;
    }

    /**
     * @param string $process
     */
    public function setProcess(string $process)
    {
        $this->process = $process;
    }

    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param float $total
     */
    public function setTotal(float $total)
    {
        $this->total = $total;
    }

    /**
     * @return Date
     */
    public function getExpectedDate()
    {
        return $this->expectedDate;
    }

    /**
     * @param Date $expectedDate
     */
    public function setExpectedDate($expectedDate)
    {
        $this->expectedDate = $expectedDate;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content)
    {
        $this->content = $content;
    }

    /**
     * @return Setting
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @param Setting $department
     */
    public function setDepartment(Setting $department)
    {
        $this->department = $department;
    }

    /**
     * @return Particular
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param Particular $priority
     */
    public function setPriority(Particular $priority)
    {
        $this->priority = $priority;
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @param int $code
     */
    public function setCode(int $code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getRequisitionNo()
    {
        return $this->requisitionNo;
    }

    /**
     * @param string $requisitionNo
     */
    public function setRequisitionNo(string $requisitionNo)
    {
        $this->requisitionNo = $requisitionNo;
    }

    /**
     * @return float
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * @param float $vat
     */
    public function setVat(float $vat)
    {
        $this->vat = $vat;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return mixed
     */
    public function getReportTo()
    {
        return $this->reportTo;
    }

    /**
     * @param mixed $reportTo
     */
    public function setReportTo($reportTo)
    {
        $this->reportTo = $reportTo;
    }

    /**
     * @return mixed
     */
    public function getTransferTo()
    {
        return $this->transferTo;
    }

    /**
     * @param mixed $transferTo
     */
    public function setTransferTo($transferTo)
    {
        $this->transferTo = $transferTo;
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @param string $filename
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
    }

    /**
     * @return ModuleProcessItem
     */
    public function getCurrentRole()
    {
        return $this->currentRole;
    }

    /**
     * @param ModuleProcessItem $currentRole
     */
    public function setCurrentRole($currentRole)
    {
        $this->currentRole = $currentRole;
    }

    /**
     * @return ModuleProcessItem
     */
    public function getWaitingRole()
    {
        return $this->waitingRole;
    }

    /**
     * @param ModuleProcessItem $waitingRole
     */
    public function setWaitingRole($waitingRole)
    {
        $this->waitingRole = $waitingRole;
    }

    /**
     * @return string
     */
    public function getWaitingProcess()
    {
        return $this->waitingProcess;
    }

    /**
     * @param string $waitingProcess
     */
    public function setWaitingProcess(string $waitingProcess)
    {
        $this->waitingProcess = $waitingProcess;
    }


    /**
     * @return Requisition
     */
    public function getRequisition()
    {
        return $this->requisition;
    }

    /**
     * @param Requisition $requisition
     */
    public function setRequisition(Requisition $requisition)
    {
        $this->requisition = $requisition;
    }


    /**
     * @return string
     */
    public function getRequisitionNote()
    {
        return $this->requisitionNote;
    }

    /**
     * @param string $requisitionNote
     */
    public function setRequisitionNote(string $requisitionNote)
    {
        $this->requisitionNote = $requisitionNote;
    }

    /**
     * @return string
     */
    public function getOrderNo()
    {
        return $this->orderNo;
    }

    /**
     * @param string $orderNo
     */
    public function setOrderNo(string $orderNo)
    {
        $this->orderNo = $orderNo;
    }

    /**
     * @return RequisitionOrderItem
     */
    public function getRequisitionOrderItems()
    {
        return $this->requisitionOrderItems;
    }

    /**
     * @return OrderDelivery
     */
    public function getOrderDeliveries()
    {
        return $this->orderDeliveries;
    }

    /**
     * @return RequisitionIssue
     */
    public function getRequisitionIssue()
    {
        return $this->requisitionIssue;
    }

    /**
     * @param RequisitionIssue $requisitionIssue
     */
    public function setRequisitionIssue($requisitionIssue)
    {
        $this->requisitionIssue = $requisitionIssue;
    }

    /**
     * @return int
     */
    public function getProcessOrdering(): int
    {
        return $this->processOrdering;
    }

    /**
     * @param int $processOrdering
     */
    public function setProcessOrdering(int $processOrdering)
    {
        $this->processOrdering = $processOrdering;
    }

    /**
     * @return string
     */
    public function getBusinessGroup()
    {
        return $this->businessGroup;
    }

    /**
     * @param string $businessGroup
     */
    public function setBusinessGroup(string $businessGroup)
    {
        $this->businessGroup = $businessGroup;
    }

    /**
     * @return string
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @param string $module
     */
    public function setModule($module)
    {
        $this->module = $module;
    }

    /**
     * @return ModuleProcess
     */
    public function getModuleProcess()
    {
        return $this->moduleProcess;
    }

    /**
     * @param ModuleProcess $moduleProcess
     */
    public function setModuleProcess($moduleProcess)
    {
        $this->moduleProcess = $moduleProcess;
    }


    /**
     * @return Setting
     */
    public function getUserDepartment()
    {
        return $this->userDepartment;
    }

    /**
     * @param Setting $userDepartment
     */
    public function setUserDepartment($userDepartment)
    {
        $this->userDepartment = $userDepartment;
    }

    /**
     * @return ProcurementProcess
     */
    public function getApproveProcess()
    {
        return $this->approveProcess;
    }

    /**
     * @param ProcurementProcess $approveProcess
     */
    public function setApproveProcess($approveProcess)
    {
        $this->approveProcess = $approveProcess;
    }

    /**
     * @return string
     */
    public function getAcceptence()
    {
        return $this->acceptence;
    }

    /**
     * @param string $acceptence
     */
    public function setAcceptence(string $acceptence)
    {
        $this->acceptence = $acceptence;
    }

    /**
     * @return bool
     */
    public function isDelete()
    {
        return $this->isDelete;
    }

    /**
     * @param bool $isDelete
     */
    public function setIsDelete($isDelete)
    {
        $this->isDelete = $isDelete;
    }



}
