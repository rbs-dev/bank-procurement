<?php

namespace Terminalbd\ProcurementBundle\Entity;

use App\Entity\Application\GenericMaster;
use App\Entity\Application\Procurement;
use App\Entity\Application\SecurityBilling;
use App\Entity\Core\Customer;
use App\Entity\Admin\Location;
use Terminalbd\ProcurementBundleBundle\Repository\ParticularRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\ProcurementBundle\Repository\EnlistedVendorRepository")
 * @ORM\Table(name="procu_enlisted_vendor")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class EnlistedVendor
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var Procurement
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\Procurement")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $config;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $name;

     /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $phone;

     /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $email;

     /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $binNo;

     /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $commissionPercent;

     /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $code;

     /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $address;


    /**
     * @Gedmo\Slug(fields={"name"})
     * @Doctrine\ORM\Mapping\Column(length=255,unique=false, nullable=true)
     */
    private $slug;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status = true;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Procurement
     */
    public function getConfig(): Procurement
    {
        return $this->config;
    }

    /**
     * @param Procurement $config
     */
    public function setConfig(Procurement $config)
    {
        $this->config = $config;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getBinNo()
    {
        return $this->binNo;
    }

    /**
     * @param string $binNo
     */
    public function setBinNo($binNo)
    {
        $this->binNo = $binNo;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
    }

    /**
     * @return float
     */
    public function getCommissionPercent()
    {
        return $this->commissionPercent;
    }

    /**
     * @param float $commissionPercent
     */
    public function setCommissionPercent($commissionPercent)
    {
        $this->commissionPercent = $commissionPercent;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail( $email)
    {
        $this->email = $email;
    }






}
