<?php

namespace Terminalbd\ProcurementBundle\Entity;

use App\Entity\Application\Procurement;
use App\Entity\Domain\Vendor;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use NumberFormatter;
/**
 * @ORM\Entity(repositoryClass="Terminalbd\ProcurementBundle\Repository\TenderVendorRepository")
 * @ORM\Table(name="procu_tender_vendors")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class TenderVendor
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;


    /**
     * @var Procurement
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\Procurement")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $config;


    /**
     * @var Tender
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\Tender",inversedBy="tenderVendors")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $tender;

    /**
     * @var TenderMemo
     *
     * @ORM\ManyToMany(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderMemo",mappedBy="approvedVendor")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $tenderMemo;

    /**
     * @var TenderComparativeItem
     * @ORM\OneToMany(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderComparativeItem", mappedBy="tenderVendor")
     **/
    private  $tenderComparativeItems;


    /**
     * @var TenderVendor
     * @ORM\OneToMany(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderComparativeItemAttribute", mappedBy="tenderVendor")
     **/
    private  $tenderComparativeAttributeItems;

    /**
     * @var Vendor
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain\Vendor")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $vendor;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $name;

     /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $email;

     /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $contactPerson;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $phone;

     /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $address;

     /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $remark;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $subTotal;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $revisedTotal;

    /**
     * @var boolean
     * @ORM\Column(type="boolean",nullable=true)
     */
    private $status;

    /**
     * @var boolean
     * @ORM\Column(type="boolean",nullable=true)
     */
    private $isDirect = false;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $path;

    /**
     * @Assert\File(maxSize="5M")
     */
    protected $file;

    
    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;

    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return RequisitionItem
     */
    public function getRequisitionItem()
    {
        return $this->requisitionItem;
    }

    /**
     * @param RequisitionItem $requisitionItem
     */
    public function setRequisitionItem( $requisitionItem)
    {
        $this->requisitionItem = $requisitionItem;
    }

    /**
     * @return Tender
     */
    public function getTender()
    {
        return $this->tender;
    }

    /**
     * @param Tender $tender
     */
    public function setTender($tender)
    {
        $this->tender = $tender;
    }

    /**
     * @return Vendor
     */
    public function getVendor()
    {
        return $this->vendor;
    }

    /**
     * @param Vendor $vendor
     */
    public function setVendor(Vendor $vendor)
    {
        $this->vendor = $vendor;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }

    /**
     * @param string $contactPerson
     */
    public function setContactPerson($contactPerson)
    {
        $this->contactPerson = $contactPerson;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * Sets file.
     *
     * @param TenderVendor $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return TenderVendor
     */
    public function getFile()
    {
        return $this->file;
    }


    public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->path;
    }


    protected function getUploadRootDir()
    {
        if(!file_exists( $this->getUploadDir())){
            mkdir( $this->getUploadDir(), 0777, true);
        }
        return __DIR__ . '/../../../../public/' . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'uploads/procurement/';
    }

    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
            $this->path = null ;
        }
    }

    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getFile()) {
            return;
        }
        // use the original file name here but you should
        // sanitize it at least to avoid any security issues

        // move takes the target directory and then the
        // target filename to move to
        $filename = date('YmdHmi') . "_" . $this->getFile()->getClientOriginalName();
        $this->getFile()->move(
            $this->getUploadRootDir(),
            $filename
        );

        // set the path property to the filename where you've saved the file
        $this->path = $filename ;

        // clean up the file property as you won't need it anymore
        $this->file = null;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return TenderComparativeItem
     */
    public function getTenderComparativeItems()
    {
        return $this->tenderComparativeItems;
    }

    /**
     * @return TenderVendor
     */
    public function getTenderComparativeAttributeItems()
    {
        return $this->tenderComparativeAttributeItems;
    }

    /**
     * @return bool
     */
    public function isDirect()
    {
        return $this->isDirect;
    }

    /**
     * @param bool $isDirect
     */
    public function setIsDirect($isDirect)
    {
        $this->isDirect = $isDirect;
    }

    public function getTenderPosition()
    {
        $locale = 'en_US';
        $nf = new NumberFormatter($locale, NumberFormatter::ORDINAL);
        return $nf->format($this->subTotal).' Lowest';
    }

    /**
     * @return float
     */
    public function getSubTotal()
    {
        return $this->subTotal;
    }

    /**
     * @param float $subTotal
     */
    public function setSubTotal($subTotal)
    {
        $this->subTotal = $subTotal;
    }

    /**
     * @return float
     */
    public function getRevisedTotal()
    {
        return $this->revisedTotal;
    }

    /**
     * @param float $revisedTotal
     */
    public function setRevisedTotal($revisedTotal)
    {
        $this->revisedTotal = $revisedTotal;
    }

    public function ordinal($number)
    {
        $ends = array('th','st','nd','rd','th','th','th','th','th','th');

        if ((($number % 100) >= 11) && (($number % 100) <= 13))
        {
            return $number.'th';
        }
        else
        {
            return $number.$ends[$number % 10];
        }
    }

    /**
     * @return string
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * @param string $remark
     */
    public function setRemark($remark)
    {
        $this->remark = $remark;
    }

    /**
     * @return Procurement
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param Procurement $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }





}
