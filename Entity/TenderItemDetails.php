<?php

namespace Terminalbd\ProcurementBundle\Entity;

use App\Entity\Application\SecurityBilling;
use App\Entity\Core\Customer;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Terminalbd\GenericBundle\Entity\Item;
use Terminalbd\GenericBundle\Entity\ItemBrand;
use Terminalbd\GenericBundle\Entity\ItemColor;
use Terminalbd\GenericBundle\Entity\ItemSize;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\InventoryBundle\Entity\StockBook;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\ProcurementBundle\Repository\TenderItemDetailsRepository")
 * @ORM\Table(name="procu_tender_item_details")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class TenderItemDetails
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;


    /**
     * @var Tender
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\Tender",inversedBy="tenderItemDetails")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $tender;

    /**
     * @var RequisitionItem
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\RequisitionItem",inversedBy="tenderItemDetails")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $requisitionItem;

    /**
     * @var TenderItem
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderItem",inversedBy="tenderItemDetails")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $tenderItem;

    /**
     * @var TenderBatchItem
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderBatchItem",inversedBy="tenderItemDetails")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $tenderBatchItem;


    /**
     * @var StockBook
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\StockBook")
     */
    private $stockBook;

    /**
     * @var Stock
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Stock")
     */
    private $stock;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $issueQuantity = 0;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $itemName;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $poQuantity;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $remainigQuantity;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $price;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $subTotal;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status = false;


    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * @return StockBook
     */
    public function getStockBook()
    {
        return $this->stockBook;
    }

    /**
     * @param StockBook $stockBook
     */
    public function setStockBook($stockBook)
    {
        $this->stockBook = $stockBook;
    }

    /**
     * @return float
     */
    public function getIssueQuantity()
    {
        return $this->issueQuantity;
    }

    /**
     * @param float $issueQuantity
     */
    public function setIssueQuantity($issueQuantity)
    {
        $this->issueQuantity = $issueQuantity;
    }

    /**
     * @return float
     */
    public function getPoQuantity()
    {
        return $this->poQuantity;
    }

    /**
     * @param float $poQuantity
     */
    public function setPoQuantity($poQuantity)
    {
        $this->poQuantity = $poQuantity;
    }

    /**
     * @return float
     */
    public function getRemainigQuantity()
    {
        return $this->remainigQuantity;
    }

    /**
     * @param float $remainigQuantity
     */
    public function setRemainigQuantity($remainigQuantity)
    {
        $this->remainigQuantity = $remainigQuantity;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated(\DateTime $updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return RequisitionItem
     */
    public function getRequisitionItem()
    {
        return $this->requisitionItem;
    }

    /**
     * @param RequisitionItem $requisitionItem
     */
    public function setRequisitionItem( $requisitionItem)
    {
        $this->requisitionItem = $requisitionItem;
    }

    /**
     * @return Tender
     */
    public function getTender()
    {
        return $this->tender;
    }

    /**
     * @param Tender $tender
     */
    public function setTender($tender)
    {
        $this->tender = $tender;
    }

    /**
     * @return string
     */
    public function getItemName()
    {
        return $this->itemName;
    }

    /**
     * @param string $itemName
     */
    public function setItemName($itemName)
    {
        $this->itemName = $itemName;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return float
     */
    public function getSubTotal()
    {
        return $this->subTotal;
    }

    /**
     * @param float $subTotal
     */
    public function setSubTotal($subTotal)
    {
        $this->subTotal = $subTotal;
    }

    /**
     * @return Stock
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param Stock $stock
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    }

    /**
     * @return TenderItem
     */
    public function getTenderItem()
    {
        return $this->tenderItem;
    }

    /**
     * @param TenderItem $tenderItem
     */
    public function setTenderItem($tenderItem)
    {
        $this->tenderItem = $tenderItem;
    }

    /**
     * @return TenderBatchItem
     */
    public function getTenderBatchItem()
    {
        return $this->tenderBatchItem;
    }

    /**
     * @param TenderBatchItem $tenderBatchItem
     */
    public function setTenderBatchItem($tenderBatchItem)
    {
        $this->tenderBatchItem = $tenderBatchItem;
    }

    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }




}
