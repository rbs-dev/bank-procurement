<?php

namespace Terminalbd\ProcurementBundle\Entity;
use App\Entity\Application\Procurement;
use App\Entity\Core\Setting;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\ProcurementBundle\Repository\ProcurementConditionRepository")
 * @ORM\Table(name="procu_condition")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ProcurementCondition
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var Procurement
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\Procurement")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $config;

    /**
     * @var Particular
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\Particular")
     */
    private $conditionType;

    /**
     * @var ProcurementConditionItem
     * @ORM\OneToMany(targetEntity="Terminalbd\ProcurementBundle\Entity\ProcurementConditionItem",mappedBy="condition")
     */
    private $conditionItems;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $description;

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $subject;

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $body;

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $ccEmail;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status = true;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }



    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }


    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return Procurement
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param Procurement $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription( $description)
    {
        $this->description = $description;
    }

    /**
     * @return Setting
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @param Setting $department
     */
    public function setDepartment($department)
    {
        $this->department = $department;
    }

    /**
     * @return ProcurementConditionItem
     */
    public function getConditionItems()
    {
        return $this->conditionItems;
    }

    /**
     * @return Particular
     */
    public function getConditionType()
    {
        return $this->conditionType;
    }

    /**
     * @param Particular $conditionType
     */
    public function setConditionType($conditionType)
    {
        $this->conditionType = $conditionType;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param string $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * @return string
     */
    public function getCcEmail()
    {
        return $this->ccEmail;
    }

    /**
     * @param string $ccEmail
     */
    public function setCcEmail($ccEmail)
    {
        $this->ccEmail = $ccEmail;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated(\DateTime $updated)
    {
        $this->updated = $updated;
    }

}
