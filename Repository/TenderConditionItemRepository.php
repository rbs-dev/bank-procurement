<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\ProcurementBundle\Entity\ProcurementCondition;
use Terminalbd\ProcurementBundle\Entity\ProcurementConditionItem;
use Terminalbd\ProcurementBundle\Entity\Tender;
use Terminalbd\ProcurementBundle\Entity\TenderCommittee;
use Terminalbd\ProcurementBundle\Entity\TenderConditionItem;
use Terminalbd\ProcurementBundle\Entity\TenderMemo;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorder;


/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class TenderConditionItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TenderConditionItem::class);
    }


    public function insertTenderConditionItem(Tender $tender,$data)
    {
        $em = $this->_em;
        if ($tender and isset($data['conditionItem']) and  !empty($data['conditionItem'])) {
            foreach ($data['conditionItem'] as $i => $value) {
                if($value){
                    $metaKey = isset($data["conditionKey"][$i]) ? $data["conditionKey"][$i] :'';
                    $metaValue = isset($data["conditionValue"][$i]) ? $data["conditionValue"][$i] :'';
                    $itemKeyValue = $this->findOneBy(array('tender' => $tender,'metaId' => $value));
                    if($itemKeyValue){
                        $this->updateMetaAttribute($itemKeyValue,$metaKey,$metaValue);
                    }else{
                        $entity = new TenderConditionItem();
                        $entity->setConfig($tender->getConfig());
                        $entity->setMetaId($value);
                        $entity->setMetaKey($metaKey);
                        $entity->setMetaValue($metaValue);
                        $entity->setTender($tender);
                        $em->persist($entity);
                        $em->flush();
                    }
                }
            }

        }
    }


    public function insertTenderUpdateConditionItem(Tender $tender,$data)
    {

        $em = $this->_em;
        if ($tender->getConditionItems() and isset($data['conditionItem']) and  !empty($data['conditionItem'])) {
            foreach ($data['conditionItem'] as $i => $value) {
                if ($value) {
                    $metaKey = isset($data["conditionKey"][$value]) ? $data["conditionKey"][$value] : '';
                    $metaValue = isset($data["conditionValue"][$value]) ? $data["conditionValue"][$value] : '';
                    $metaStatus = isset($data["conditionStatus"][$value]) ? 1 : 0;
                    $itemKeyValue = $this->findOneBy(array('tender' => $tender, 'id' => $value));
                    if ($itemKeyValue) {
                        $itemKeyValue->setMetaKey($metaKey);
                        $itemKeyValue->setMetaValue($metaValue);
                        $itemKeyValue->setStatus($metaStatus);
                        $em->persist($itemKeyValue);
                        $em->flush();
                    }
                }
            }
        }
    }


    public function initiatTenderPurchaseMemoConditionItem(TenderMemo $tender)
    {
        $em = $this->_em;
        if($tender->getCondition()){
            $template = $tender->getCondition();
            $qb = $em->createQueryBuilder();
            $remove = $qb->delete(TenderConditionItem::class, 'e')->where('e.tenderMemo = ?1')->setParameter(1, $tender->getId())->getQuery();
            if($remove){ $remove->execute();}

            /* @var ProcurementConditionItem $item */

            foreach ($template->getConditionItems() as $item):

                $entity = new TenderConditionItem();
                $entity->setConfig($tender->getConfig());
                $entity->setTenderMemo($tender);
                $entity->setMetaId($item->getId());
                $entity->setMetaKey($item->getMetaKey());
                $entity->setMetaValue($item->getMetaValue());
                $em->persist($entity);
                $em->flush();

            endforeach;
        }
    }

    public function insertTenderPurchaseMemoConditionItem(TenderMemo $tender,$data)
    {
        $em = $this->_em;
        if ($tender->getConditionItems() and isset($data['conditionItem']) and  !empty($data['conditionItem'])) {
            foreach ($data['conditionItem'] as $i => $value) {
                if ($value) {
                    $metaKey = isset($data["conditionKey"][$value]) ? $data["conditionKey"][$value] : '';
                    $metaValue = isset($data["conditionValue"][$value]) ? $data["conditionValue"][$value] : '';
                    $metaStatus = isset($data["conditionStatus"][$value]) ? 1 : 0;
                    $itemKeyValue = $this->findOneBy(array('tenderMemo' => $tender, 'id' => $value));
                    if ($itemKeyValue) {
                        $itemKeyValue->setMetaKey($metaKey);
                        $itemKeyValue->setMetaValue($metaValue);
                        $itemKeyValue->setStatus($metaStatus);
                        $em->persist($itemKeyValue);
                        $em->flush();
                    }
                }
            }
        }
    }

    public function insertWorkorderConditionItem(TenderWorkorder $tender,$data)
    {
        $em = $this->_em;

        if ($tender and isset($data['conditionItem']) and  !empty($data['conditionItem'])) {
            foreach ($data['conditionItem'] as $i =>  $value) {
                if($value){
                    $metaKey = isset($data["conditionKey"][$i]) ? $data["conditionKey"][$i] :'';
                    $metaValue = isset($data["conditionValue"][$i]) ? $data["conditionValue"][$i] :'';
                    $itemKeyValue = $this->findOneBy(array('workorder' => $tender,'metaId' => $value));
                    if($itemKeyValue){
                        $this->updateMetaAttribute($itemKeyValue,$metaKey,$metaValue);
                    }else{
                        $entity = new TenderConditionItem();
                        $entity->setConfig($tender->getConfig());
                        $entity->setMetaId($value);
                        $entity->setMetaKey($metaKey);
                        $entity->setMetaValue($metaValue);
                        $entity->setWorkorder($tender);
                        $em->persist($entity);
                        $em->flush();
                    }
                }

            }
            $em->flush();

        }

    }

    public function updateMetaAttribute(TenderConditionItem $itemKeyValue , $key , $value ='')
    {
        $em = $this->_em;
        $itemKeyValue->setMetaKey($key);
        $itemKeyValue->setMetaValue($value);
        $em->flush();
    }

    public function initialBankTenderConditionItem(Tender $tender)
    {
        $em = $this->_em;
        if($tender->getCondition()){
            $template = $tender->getCondition();
            $qb = $em->createQueryBuilder();
            $remove = $qb->delete(TenderConditionItem::class, 'e')->where('e.tender = ?1')->setParameter(1, $tender->getId())->getQuery();
            if($remove){ $remove->execute();}

            /* @var ProcurementConditionItem $item */

            foreach ($template->getConditionItems() as $item):

                $entity = new TenderConditionItem();
                $entity->setConfig($tender->getConfig());
                $entity->setTender($tender);
                $entity->setMetaId($item->getId());
                $entity->setMetaKey($item->getMetaKey());
                $entity->setMetaValue($item->getMetaValue());
                $em->persist($entity);
                $em->flush();

            endforeach;
         }
    }

    public function insertBankTenderConditionItem(Tender $tender,$data)
    {
        $em = $this->_em;
        if ($tender->getConditionItems() and isset($data['conditionItem']) and  !empty($data['conditionItem'])) {
            foreach ($data['conditionItem'] as $i => $value) {
                if ($value) {
                    $metaKey = isset($data["conditionKey"][$value]) ? $data["conditionKey"][$value] : '';
                    $metaValue = isset($data["conditionValue"][$value]) ? $data["conditionValue"][$value] : '';
                    $metaStatus = isset($data["conditionStatus"][$value]) ? 1 : 0;
                    $itemKeyValue = $this->findOneBy(array('tender' => $tender, 'id' => $value));
                    if ($itemKeyValue) {
                        $itemKeyValue->setMetaKey($metaKey);
                        $itemKeyValue->setMetaValue($metaValue);
                        $itemKeyValue->setStatus($metaStatus);
                        $em->persist($itemKeyValue);
                        $em->flush();
                    }
                }
            }
        }
    }

    public function initialBankTenderCommitteeConditionItem(TenderCommittee $tender)
    {
        $em = $this->_em;
        if($tender->getCondition()){
            $template = $tender->getCondition();
            $qb = $em->createQueryBuilder();
            $remove = $qb->delete(TenderConditionItem::class, 'e')->where('e.tenderCommittee = ?1')->setParameter(1, $tender->getId())->getQuery();
            if($remove){ $remove->execute();}

            /* @var ProcurementConditionItem $item */

            foreach ($template->getConditionItems() as $item):

                $entity = new TenderConditionItem();
                $entity->setConfig($tender->getConfig());
                $entity->setTenderCommittee($tender);
                $entity->setMetaId($item->getId());
                $entity->setMetaKey($item->getMetaKey());
                $entity->setMetaValue($item->getMetaValue());
                $em->persist($entity);
                $em->flush();

            endforeach;
         }
    }

    public function insertBankCommitteeConditionItem(TenderCommittee $tender,$data)
    {
        $em = $this->_em;
        if ($tender->getConditionItems() and isset($data['conditionItem']) and  !empty($data['conditionItem'])) {
            foreach ($data['conditionItem'] as $i => $value) {
                if ($value) {
                    $metaKey = isset($data["conditionKey"][$value]) ? $data["conditionKey"][$value] : '';
                    $metaValue = isset($data["conditionValue"][$value]) ? $data["conditionValue"][$value] : '';
                    $metaStatus = isset($data["conditionStatus"][$value]) ? 1 : 0;
                    $itemKeyValue = $this->findOneBy(array('tenderCommittee' => $tender, 'id' => $value));
                    if ($itemKeyValue) {
                        $itemKeyValue->setMetaKey($metaKey);
                        $itemKeyValue->setMetaValue($metaValue);
                        $itemKeyValue->setStatus($metaStatus);
                        $em->persist($itemKeyValue);
                        $em->flush();
                    }
                }
            }
        }
    }

    public function initialBankWorkorderConditionItem(TenderWorkorder $tender)
    {
        $em = $this->_em;
        if($tender->getCondition()){
            $template = $tender->getCondition();
            $qb = $em->createQueryBuilder();
            $remove = $qb->delete(TenderConditionItem::class, 'e')->where('e.workorder = ?1')->setParameter(1, $tender->getId())->getQuery();
            if($remove){ $remove->execute();}

            /* @var ProcurementConditionItem $item */

            foreach ($template->getConditionItems() as $item):

                $entity = new TenderConditionItem();
                $entity->setConfig($tender->getConfig());
                $entity->setWorkorder($tender);
                $entity->setMetaId($item->getId());
                $entity->setMetaKey($item->getMetaKey());
                $entity->setMetaValue($item->getMetaValue());
                $entity->setStatus(1);
                $em->persist($entity);
                $em->flush();

            endforeach;
        }
    }

    public function initialGarmentsTenderConditionItem(Tender $tender)
    {
        $em = $this->_em;
        if($tender->getCondition()){
            $template = $tender->getCondition();
            $qb = $em->createQueryBuilder();
            $remove = $qb->delete(TenderConditionItem::class, 'e')->where('e.tender = ?1')->setParameter(1, $tender->getId())->getQuery();
            if($remove){ $remove->execute();}

            /* @var ProcurementConditionItem $item */

            foreach ($template->getConditionItems() as $item):
                $entity = new TenderConditionItem();
                $entity->setConfig($tender->getConfig());
                $entity->setTender($tender);
                $entity->setMetaId($item->getId());
                $entity->setMetaKey($item->getMetaKey());
                $entity->setMetaValue($item->getMetaValue());
                $em->persist($entity);
                $em->flush();
            endforeach;
        }
    }

    public function insertBankWorkorderConditionItem(TenderWorkorder $tender,$data)
    {
        $em = $this->_em;
        if ($tender->getConditionItems() and isset($data['conditionItem']) and  !empty($data['conditionItem'])) {
            foreach ($data['conditionItem'] as $i => $value) {
                if ($value) {
                    $metaKey = isset($data["conditionKey"][$value]) ? $data["conditionKey"][$value] : '';
                    $metaValue = isset($data["conditionValue"][$value]) ? $data["conditionValue"][$value] : '';
                    $metaStatus = isset($data["conditionStatus"][$value]) ? 1 : 0;
                    $itemKeyValue = $this->findOneBy(array('workorder' => $tender, 'id' => $value));
                    if ($itemKeyValue) {
                        $itemKeyValue->setMetaKey($metaKey);
                        $itemKeyValue->setMetaValue($metaValue);
                        $itemKeyValue->setStatus($metaStatus);
                        $em->persist($itemKeyValue);
                        $em->flush();
                    }
                }
            }
        }
    }



}
