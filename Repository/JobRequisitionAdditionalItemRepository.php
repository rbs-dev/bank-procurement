<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\GenericBundle\Entity\ItemUnit;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\ProcurementBundle\Entity\JobRequisition;
use Terminalbd\ProcurementBundle\Entity\JobRequisitionAdditionalItem;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionIssue;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItemHistory;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class JobRequisitionAdditionalItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, JobRequisitionAdditionalItem::class);
    }

    public function getItemSummary(JobRequisition $invoice)
    {
        $id = $invoice->getId();
        $em = $this->_em;
        $qb = $this->createQueryBuilder('e');
        $qb->select('SUM(e.subTotal) as subTotal');
        $qb->join('e.jobRequisition','i');
        $qb->where("i.id = '{$id}'");
        $result = $qb->getQuery()->getOneOrNullResult();
        if(!empty($result) and !empty($result['subTotal'])){
            $subTotal = $result['subTotal'];
            $invoice->setAdditionalSubTotal($subTotal);
        }else{
            $invoice->setAdditionalSubTotal(0);
        }
        $em->persist($invoice);
        $em->flush();

    }

    public function insertJobRequisitionItem(JobRequisition $requisition, $data)
    {

        $em = $this->_em;

        if(!empty($data['name']) and $data['quantity'] > 0){
            $exist = $this->findOneBy(array('jobRequisition'=> $requisition, 'name' => $data['name']));
            if(empty($exist)){
                $entity = new JobRequisitionAdditionalItem();
                $entity->setJobRequisition($requisition);
                if($data['unit']){
                    $u = (int)$data['unit'];
                    $unit = $this->_em->getRepository(ItemUnit::class)->find($u);
                    $entity->setUnit($unit);
                }
                $entity->setName($data['name']);
                $entity->setQuantity($data['quantity']);
                $entity->setPrice($data['price']);
                $entity->setDescription($data['description']);
                $entity->setSubTotal($entity->getPrice() * $entity->getQuantity());
                $em->persist($entity);
                $em->flush();
            }else{
                $entity = $exist;
                $entity->setQuantity($data['quantity']);
                $entity->setDescription($data['description']);
                $entity->setSubTotal($entity->getPrice() * $entity->getQuantity());
                $em->persist($entity);
                $em->flush();
            }

        }

    }

}
