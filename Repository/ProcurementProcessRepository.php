<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Repository;

use App\Entity\Admin\AppModule;
use App\Entity\Domain\ApprovalUser;
use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\ModuleProcessItem;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\DmsBundle\Entity\DmsFile;
use Terminalbd\InventoryBundle\Entity\StockBook;
use Terminalbd\ProcurementBundle\Entity\JobRequisition;
use Terminalbd\ProcurementBundle\Entity\ParticularType;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionIssue;
use Terminalbd\ProcurementBundle\Entity\RequisitionOrder;
use Terminalbd\ProcurementBundle\Entity\Tender;
use Terminalbd\ProcurementBundle\Entity\TenderCommittee;
use Terminalbd\ProcurementBundle\Entity\TenderComparative;
use Terminalbd\ProcurementBundle\Entity\TenderMemo;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorder;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderReceive;


/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class ProcurementProcessRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProcurementProcess::class);
    }

    public function getEntityCheck($id,$module)
    {
        $em = $this->_em;
        if(in_array($module,["requisition","purchase-requisition","store-requisition","replacement-requisition","repair-requisition"])){
            $entity = $em->getRepository(Requisition::class)->find($id);
        }elseif($module == "job-approval"){
            $entity = $em->getRepository(JobRequisition::class)->find($id);
        }elseif($module == "job-requisition"){
            $entity = $em->getRepository(JobRequisition::class)->find($id);
        }elseif(in_array($module ,['requisition-issue','purchase-issue'])){
            $entity = $em->getRepository(RequisitionIssue::class)->find($id);
       }elseif($module == "purchase-order"){
            $entity = $em->getRepository(RequisitionOrder::class)->find($id);
        }elseif($module == "order-issue"){
            $entity = $em->getRepository(RequisitionOrder::class)->find($id);
        }elseif($module == "tender"){
            $entity = $em->getRepository(Tender::class)->find($id);
        }elseif(in_array($module ,["purchase-memo","management-memo","board-memo","floating-memo"])){
            $entity = $em->getRepository(TenderMemo::class)->find($id);
        }elseif(in_array($module ,['invitation-approval','tender-committee','purchase-committee','management-approval','board-approval'])){
            $entity = $em->getRepository(TenderCommittee::class)->find($id);
        }elseif($module == "tender-comparative"){
            $entity = $em->getRepository(TenderComparative::class)->find($id);
        }elseif($module == "work-order"){
            $entity = $em->getRepository(TenderWorkorder::class)->find($id);
        }elseif($module == "gate-pass"){
            $entity = $em->getRepository(TenderWorkorderReceive::class)->find($id);
        }elseif($module == "goods-receive"){
            $entity = $em->getRepository(TenderWorkorderReceive::class)->find($id);
        }
        return $entity;
    }

    public function resetRechecked($entity)
    {
        $queryBuilder = $this->createQueryBuilder('e');
        $query = $queryBuilder ->update()
            ->set('e.close', ':close')
            ->setParameter('close', 0)
            ->where('e.entityId =:entityId')->setParameter('entityId', $entity->getId())
            ->andWhere('e.module =:module')->setParameter('module', $entity->getModule())
            ->getQuery();
        $query ->execute();
    }

    public function findApproveProcessUser($entities,$user): array
    {
        $arrs = array();
        foreach ($entities as $arr){
            $arrs[] = $arr['id'];
        }
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.entityId as entityId');
        $qb->where('e.assignTo =:assign')->setParameter('assign', $user);
        $qb->andWhere('e.entityId IN (:process)')->setParameter('process', $arrs);
        $result = $qb->getQuery()->getArrayResult();
        $approvs = array();
        foreach ($result as $row){
            $approvs[$row['entityId']] = $row['entityId'];
        }
        return $approvs;

    }

    public function checkApproveProcessUser($entity,$user)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.entityId as entityId');
        $qb->where('e.assignTo =:assign')->setParameter('assign', $user);
        $qb->andWhere('e.entityId =:entityId')->setParameter('entityId', $entity);
        $result = $qb->getQuery()->getOneOrNullResult();
        if($result){
            return 'valid';
        }
        return 'invalid';
    }

    /* This function use for  Procurement requisition Process */

    public function insertProcurementRequisitionProcessAssign($requisition,$module,$approveUsers)
    {
        $em = $this->_em;
        $i = 2;
        if(!empty($approveUsers)){
            foreach ($approveUsers as $approve) {
                $user = $em->getRepository(User::class)->find($approve['uid']);
                $exist = $this->findOneBy(array('assignTo' => $user,'module'=> $module,'entityId'=>"{$requisition->getId()}"));
                if(empty($exist)){
                    $entity = new ProcurementProcess();
                    $entity->setConfig($requisition->getConfig());
                    $entity->setEntityId($requisition->getId());
                    $entity->setModule($module);
                    $entity->setAssignTo($user);
                    $entity->setOrdering($i);
                    $entity->setIsMandatory(1);
                    $entity->setProcess($approve['process']);
                    $entity->setStatus(true);
                    $em->persist($entity);
                    $em->flush();
                }
                $i++;
            }
        }
    }

    /* This function use for  Procurement requisition Process */

    public function insertProcurementProcessAssign($requisition,$module,$approveUsers)
    {
        $em = $this->_em;
        $i = 1;
        if(!empty($approveUsers)){
            /* @var $user ApprovalUser */
            foreach ($approveUsers as $user) {
                $exist = $this->findOneBy(array('assignTo' => $user->getUser(),'module'=> $module,'entityId'=>"{$requisition->getId()}"));
                if(empty($exist)){
                    $entity = new ProcurementProcess();
                    $entity->setConfig($requisition->getConfig());
                    $entity->setEntityId($requisition->getId());
                    $entity->setModule($module);
                    $entity->setAssignTo($user->getUser());
                    $entity->setOrdering($i);
                    $entity->setIsMandatory($user->isMandatory());
                    $entity->setProcess($user->getProcess());
                    $entity->setStatus(true);
                    $em->persist($entity);
                    $em->flush();
                    $i++;
                }
            }
        }
    }

    /* This function use for  Procurement requisition Process */

    public function insertRoleProcessAssign($requisition)
    {
        $em = $this->_em;
        if(!empty($requisition)){
            $module = $requisition->getModuleProcess();
            if($module->getModuleProcessItems()){
                $i = 1;
                /** @var $role ModuleProcessItem */
                foreach ($module->getModuleProcessItems() as $role) {
                    if($role->isStatus() == 1 and $role->getBundleRoleGroup()->getRoleGroup() == 'Approve'){
                        $name =  $role->getBundleRoleGroup()->getName();
                        $roleName =  $role->getBundleRoleGroup()->getRoleName();
                        $process =  $role->getBundleRoleGroup()->getProcess();
                        $moduleSlug =   $module->getModule()->getSlug();
                        $exist = $this->findOneBy(array('assignToRole'=> $name,'module'=> $moduleSlug,'entityId'=>"{$requisition->getId()}"));
                        if(empty($exist)){
                            $entity = new ProcurementProcess();
                            $entity->setConfig($requisition->getConfig());
                            $entity->setEntityId($requisition->getId());
                            $entity->setModule($moduleSlug);
                            $entity->setAssignToRoleName($name);
                            $entity->setAssignToRole($roleName);
                            $entity->setOrdering($i);
                            $entity->setProcess($process);
                            $entity->setStatus(true);
                            $em->persist($entity);
                            $em->flush();
                            $i++;
                        }
                    }
                }
            }
        }
    }


    public function getExistApprovalUsers($entity,$module)
    {
        $data = array();
        $em = $this->_em;
        $qb = $this->createQueryBuilder('e');
        $qb->select('u.id as id');
        $qb->leftJoin('e.assignTo', 'u');
        $qb->where('e.entityId =:entity')->setParameter("entity",$entity);
        $ids = $qb->getQuery()->getArrayResult();
        return $ids;
    }

    public function approvalAssign($entity,$ordering = 1)
    {
        $data = '';
        $em = $this->_em;
        $this->approvalUserOrdering($entity);
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.assignTo', 'u');
        $qb->where('e.entityId =:entityId')->setParameter("entityId",$entity->getId());
        $qb->andWhere('e.module =:module')->setParameter("module",$entity->getModule());
        $qb->andWhere("e.ordering = {$ordering}");
        $currentRole = $qb->getQuery()->getOneOrNullResult();
        if($currentRole and !empty($currentRole->getAssignTo())){
            $data = $currentRole;
        }
        $this->processAssignProcurement($entity,$ordering,$data);
    }

    private function processAssignProcurement($entity,$ordering,$operationalRole)
    {
        $em = $this->_em;
        if($operationalRole){
            $entity->setApproveProcess($operationalRole);
            $entity->setReportTo($operationalRole->getAssignTo());
            $entity->setProcess($operationalRole->getProcess());
            $entity->setWaitingProcess("In-progress");
            $entity->setProcessOrdering($ordering);

        }else{

            $entity->setApproveProcess(null);
            $entity->setReportTo(null);
            if(!empty($entity->getModuleProcess()->getEndStatus())){
                $entity->setProcess($entity->getModuleProcess()->getEndStatus());
            }else{
                $entity->setProcess("Approved");
            }
            $entity->setWaitingProcess("Approved");
            $entity->setProcessOrdering(0);
        }
        $em->persist($entity);
        $em->flush();

    }

    public function approvalRoleAssign($entity,$ordering = 1)
    {
        $data ='';
        $em = $this->_em;
        $this->approvalUserOrdering($entity);
        $qb = $this->createQueryBuilder('e');
        $qb->where('e.entityId =:entityId')->setParameter("entityId",$entity->getId());
        $qb->andWhere('e.module =:module')->setParameter("module",$entity->getModule());
        $qb->andWhere("e.ordering = {$ordering}");
        $currentRole = $qb->getQuery()->getOneOrNullResult();
        if($currentRole and !empty($currentRole->getAssignToRole())){
            $data = $currentRole;
        }
        $this->processAssignRoleProcurement($entity,$ordering,$data);
    }

    public function approvalUserOrdering($entity)
    {
        $em = $this->_em;
        $qb = $this->createQueryBuilder('e');
        $qb->where('e.entityId =:entityId')->setParameter("entityId",$entity->getId());
        $qb->andWhere('e.module =:module')->setParameter("module",$entity->getModule());
        $qb->orderBy("e.ordering",'ASC');
        $result = $qb->getQuery()->getResult();
        $i = 1;
        foreach ($result as $row):
            /* @var $row ProcurementProcess */
            $row->setOrdering($i);
            $em->persist($row);
            $em->flush();
            $i++;
        endforeach;
    }

    public function requisitionAssignHod(Requisition $requisition)
    {
        $em = $this->_em;
        $reportTo = $requisition->getCreatedBy()->getReportTo();
        $exist = $this->findOneBy(array('config'=> $requisition->getConfig(),'entityId'=>$requisition->getId(),'assignTo'=>$reportTo));
        if($reportTo and $reportTo->getEnabled() == 1 and empty($exist)){
            $entity = new ProcurementProcess();
            $entity->setConfig($requisition->getConfig());
            $entity->setEntityId($requisition->getId());
            $entity->setModule($requisition->getModule());
            $entity->setAssignTo($reportTo);
            $entity->setOrdering(1);
            $entity->setIsMandatory(true);
            $entity->setProcess('Verify');
            $entity->setStatus(true);
            $em->persist($entity);
            $em->flush();
            $requisition->setProcess('Verifier');
            $requisition->setApproveProcess($entity);
            $requisition->setReportTo($reportTo);
            $requisition->setWaitingProcess("In-progress");
            $requisition->setProcessOrdering(1);
            $em->persist($requisition);
            $em->flush();
        }
    }


    /*
    * This Process for Role base user assign
    *
    **/

    private function processAssignRoleProcurement($entity,$ordering,$operationalRole)
    {
        $em = $this->_em;
        if($operationalRole){
            $entity->setApproveProcess($operationalRole);
            $entity->setProcess($operationalRole->getProcess());
            $entity->setWaitingProcess("In-progress");
            $entity->setProcessOrdering($ordering);
        }else{
            $entity->setReportTo(null);
            $entity->setApproveProcess(null);
            if(!empty($entity->getModuleProcess()->getEndStatus())){
                $entity->setProcess($entity->getModuleProcess()->getEndStatus());
            }else{
                $entity->setProcess("Approved");
            }
            $entity->setWaitingProcess("Approved");
            $entity->setProcessOrdering(0);
        }
        $em->persist($entity);
        $em->flush();

    }

    /*
    * This Process for Approve user comment
    *
    **/

    public function insertApprovalProcessForGenericComment(User $user ,$requisition,$comment)
    {
        $em = $this->_em;
        $module = $requisition->getModuleProcess();
        $exist = '';
        if(!empty($user->getUserGroupRole()) and !empty($requisition->getModuleProcess()) and $module->getApproveType() == 'role'){
            $role = $user->getUserGroupRole()->getRoleName();
            $exist  = $this->findOneBy(array('assignToRole' => $role, 'entityId' => $requisition));
        }else{
            $exist  = $this->findOneBy(array('assignTo' => $user, 'entityId' => $requisition));
        }
        if(!empty($exist)){
            $exist->setComment(trim($comment));
            $exist->setAssignTo($user);
            $exist->setClose(true);
            $em->persist($exist);
            $em->flush();
        }

    }

    public function insertApprovalProcessComment(User $user ,$requisition,$comment)
    {
        $em = $this->_em;
        $exist = '';
        if(!empty($user->getUserGroupRole())){
            $role = $user->getUserGroupRole()->getRoleName();
            $exist  = $this->findOneBy(array('assignToRole' => $role, 'entityId' => $requisition));
        }else{
            $exist  = $this->findOneBy(array('assignTo' => $user, 'entityId' => $requisition));
        }
        if(!empty($exist)){
            $exist->setComment(trim($comment));
            $exist->setAssignTo($user);
            $exist->setClose(true);
            $em->persist($exist);
            $em->flush();
        }

    }

    /*
     * This Process for Requisition Amendment Approve Success
     *
     **/


    public function requisitionBudgetApprovalAssign($entity,$ordering = 1)
    {
        $data = '';
        $em = $this->_em;
        $qb = $this->createQueryBuilder('e');
        $qb->where('e.entityId =:entityId')->setParameter("entityId",$entity);
        $qb->andWhere('e.module =:module')->setParameter("module",'purchase-requisition');
        $qb->andWhere("e.ordering = 1");
        $currentRole = $qb->getQuery()->getOneOrNullResult();
        if($currentRole and !empty($currentRole->getAssignTo())){
            $data = $currentRole;
            return $data;
        }
        return $data;
    }

    public function insertBankProcurementProcessAssign(Requisition $requisition,$module)
    {
        $em = $this->_em;
        $i = 1;
        /* @var $user ApprovalUser */
        $reportTo = $requisition->getCreatedBy()->getReportTo();
        $relieverTo = $requisition->getCreatedBy()->getRelieverTo();
        if($reportTo and $reportTo->getEnabled() == 1){
            $entity = new ProcurementProcess();
            $entity->setConfig($requisition->getConfig());
            $entity->setRequisition($requisition);
            $entity->setEntityId($requisition->getId());
            $entity->setModule($module);
            $entity->setAssignTo($reportTo);
            $entity->setOrdering(1);
            $entity->setIsMandatory(true);
            $entity->setProcess('Approve');
            $entity->setStatus(true);
            $em->persist($entity);
            $em->flush();
        }

        if($relieverTo and $reportTo->getEnabled() == 1){
            $entity1 = new ProcurementProcess();
            $entity1->setConfig($requisition->getConfig());
            $entity1->setRequisition($requisition);
            $entity1->setEntityId($requisition->getId());
            $entity1->setModule($module);
            $entity1->setAssignTo($relieverTo);
            $entity1->setOrdering(2);
            $entity1->setIsMandatory(true);
            $entity1->setProcess('Approve');
            $entity1->setStatus(false);
            $em->persist($entity1);
            $em->flush();
        }

    }

    public function requisitionBankApprovalAssign($requisition,$ordering = 1)
    {
        $data = array();
        $em = $this->_em;
        $qb = $this->createQueryBuilder('e');
        $qb->select('u.id as userId','e.process as process');
        $qb->join('e.assignTo', 'u');
        $qb->where('e.entityId =:entityId')->setParameter("entityId",$requisition);
        $qb->andWhere('e.module =:module')->setParameter("module",$entity->getModule());
        $qb->andWhere("e.status = 1");
        $qb->andWhere("e.ordering = {$ordering}");
        $currentRole = $qb->getQuery()->getOneOrNullResult();
        if($currentRole['userId']){
            $user = $this->_em->getRepository(User::class)->find($currentRole['userId']);
            $data = array('user' => $user,'process'=> $currentRole['process']);
            return $data;
        }
        return $data;
    }

    protected function handleSearchBetween($qb, $form)
    {

        if (isset($form['requisition_filter_form'])) {

            $data = $form['requisition_filter_form'];
            $startDate = isset($data['startDate']) ? $data['startDate'] : '';
            $endDate = isset($data['endDate']) ? $data['endDate'] : '';
            $branch = isset($data['branch']) ? $data['branch'] : '';
            $department = isset($data['department']) ? $data['department'] : '';
            $priroty = isset($data['priroty']) ? $data['priroty'] : '';
            $process = isset($data['process']) ? $data['process'] : '';
            $requisitionType = isset($data['requisitionType']) ? $data['requisitionType'] : '';
            $requisitionNo = !empty($data['requisitionNo']) ? $data['requisitionNo'] : '';

            if (!empty($process) && $process == 'RUNNING') {
//                $qb->andWhere($qb->expr()->like("e.process", "'%$process%'"));
                $qb->andWhere('e.process IN (:processes)')->setParameter('processes', ['New', 'Checked']);
            }
            if (!empty($process) && $process == 'HISTORY') {
//                $qb->andWhere($qb->expr()->like("e.process", "'%$process%'"));
                $qb->andWhere('e.process IN (:processes)')->setParameter('processes', ['Approved']);
            }
            if (!empty($requisitionNo)) {
                $qb->andWhere($qb->expr()->like("e.requisitionNo", "'%$requisitionNo%'"));
            }
            if (!empty($branch)) {
                $qb->andWhere('b.id = :branch')->setParameter('branch', $branch);
            }
            if (!empty($department)) {
                $qb->andWhere('d.id = :department')->setParameter('department', $department);
            }
            if (!empty($priroty)) {
                $qb->andWhere('p.id = :priority')->setParameter('priroty', $priroty);
            }
            if (!empty($requisitionType)) {
                $qb->andWhere('t.id = :requisitionType')->setParameter('requisitionType', $requisitionType);
            }
            if (!empty($startDate)) {
                $datetime = new \DateTime($startDate);
                $startDate = $datetime->format('Y-m-d 00:00:00');
                $qb->andWhere("e.created >= :startDate");
                $qb->setParameter('startDate', $startDate);
            }

            if (!empty($endDate)) {
                $datetime = new \DateTime($endDate);
                $endDate = $datetime->format('Y-m-d 23:59:59');
                $qb->andWhere("e.created <= :endDate");
                $qb->setParameter('endDate', $endDate);
            }

        }
    }

    public function lastProcessId($requisition,$module = "")
    {

        $qb = $this->createQueryBuilder('e');
        $qb->select('MAX(e.ordering) as ordering');
        $qb->where('e.config = :config');
        $qb->setParameter('config', $requisition->getConfig()->getId());
        if($module == "job-requisition"){
            $qb->andWhere('e.jobRequisition = :requisition');
            $qb->setParameter('requisition', $requisition->getId());
        }else if($module == "requisition"){
            $qb->andWhere('e.requisition = :requisition');
            $qb->setParameter('requisition', $requisition->getId());
        }else if($module == "requisition-issue"){
            $qb->andWhere('e.requisitionIssue = :requisition');
            $qb->setParameter('requisition', $requisition->getId());
        }else if($module == "requisition-order"){
            $qb->andWhere('e.requisitionOrder = :requisition');
            $qb->setParameter('requisition', $requisition->getId());
        }
        $lastCode = $qb->getQuery()->getSingleScalarResult();
        if(empty($lastCode)){
            return  1;
        }
        return $lastCode+1;
    }

    public function insertAttachmentFile($requisition, $data, $files)
    {
        $em = $this->_em;
        if (isset($files)) {
            $errors = array();
            if($files['tmp_name']){
                define('FILETYPE', array('jpg', 'png', 'jpeg', 'pdf'));
                define('DESTINATION', 'uploads/procurement/');
                define('RESIZEBY', 'w');
                define('FILESIZE', 10097152);
                define('RESIZETO', 520);
                define('QUALITY', 100);
                foreach ($files['tmp_name'] as $key => $tmp_name) {
                    if($tmp_name){
                        $fileName = strtolower($requisition->getModule()). '-' . time() . "-" . $_FILES['files']['name'][$key];
                        $fileType = $_FILES['files']['type'][$key];
                        $fileSize = $_FILES['files']['size'][$key];

                        if ($fileSize > FILESIZE) {
                            $errors[] = 'File size must be less than 2 MB';
                        }
                        if (empty($errors) == true and in_array($fileType, FILETYPE)) {
                            if (is_dir(DESTINATION) == false) {
                                mkdir(DESTINATION, 0777);        // Create directory if it does not exist
                            }
                            $image = new Image($_FILES['files']['tmp_name'][$key]);
                            $image->destination = DESTINATION . $fileName;
                            $image->constraint = RESIZEBY;
                            $image->size = RESIZETO;
                            $image->quality = QUALITY;
                            $image->render();
                            $entity = new DmsFile();
                            $entity->setSourceId($requisition->getId());
                            $entity->setBundle("Procurement");
                            $entity->setModule($requisition->getModule());
                            if($data['labelName'][$key]){
                                $entity->setName($data['labelName'][$key]);
                            }
                            $entity->setFileName($fileName);
                            $em->persist($entity);
                            $em->flush();
                        } elseif (empty($errors) == true and $fileType = "application/pdf") {
                            $file_tmp = $_FILES['files']['tmp_name'][$key];
                            if (is_dir(DESTINATION) == false) {
                                mkdir(DESTINATION, 0777);        // Create directory if it does not exist
                            }
                            if (is_dir(DESTINATION . $fileName) == false) {
                                move_uploaded_file($file_tmp, DESTINATION . $fileName);
                            }
                            $entity = new DmsFile();
                            $entity->setSourceId($requisition->getId());
                            $entity->setBundle("Procurement");
                            $entity->setModule($requisition->getModule());
                            if($data['labelName'][$key]){
                                $entity->setName($data['labelName'][$key]);
                            }
                            $entity->setFileName($fileName);
                            $em->persist($entity);
                            $em->flush();
                        } else {
                            return $errors;
                        }
                    }

                }
            }
        }
    }

    public function updateOrdering($entityId,$module)
    {
        $entities = $this->findBy(array('entityId'=>$entityId,'module'=>$module),array('ordering'=>'asc'));
        /* @var ProcurementProcess $entity */
        $i = 1;
        $em = $this->_em;
        foreach ($entities as $entity){
            $entity->setOrdering($i);
            $em->persist($entity);
            $em->flush();
            $i++;
        }

    }

    public function getDmsAttchmentFile($entity,$module)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->from(DmsFile::class,'e');
        $qb->select('e.id', 'e.created as created','e.name as name', 'e.fileName as fileName');
        $qb->where('e.sourceId = :sourceId')->setParameter('sourceId',"{$entity}");
        $qb->andWhere('e.bundle = :bundle')->setParameter('bundle',"Procurement");
        $qb->andWhere('e.module = :module')->setParameter('module',$module);
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

    public function getEmailData($entity,$link)
    {
        $module = $entity->getModule();
        if(in_array($module,["requisition","purchase-order","job-approval","job-requisition","requisition-issue","purchase-issue","purchase-requisition","store-requisition","replacement-requisition"])){
            $invoice = $entity->getRequisitionNo();
        }elseif(in_array($module ,["tender","tender-comparative","work-order","goods-receive"])){
            $invoice = $entity->getInvoice();
        }elseif ($module == 'order-issue'){
            $invoice = $entity->getOrderNo();
        }
        $user = $entity->getApproveProcess()->getAssignTo();
        if($entity->getApproveProcess()){
            $fromEmail  = empty($entity->getCreatedBy()) ? '' : $entity->getCreatedBy()->getUsername();
            $fromName = empty($entity->getCreatedBy()) ? '' : $entity->getCreatedBy()->getName();
            $toEmail = empty($user) ? '' :$user->getUsername();
            $toName = empty($user) ? '' : $user->getName();
            $emailData = [
                'link'          => $link ,
                'invoice'       => $invoice,
                'created'       => $entity->getCreated()->format('d-m-Y'),
                'fromEmail'     => $fromEmail,
                'fromName'      => $fromName,
                'toEmail'       => $toEmail,
                'toName'        => $toName
            ];
            return $emailData;
        }
        return false;
    }

    public function getEmailPurchaseData($entity,$link)
    {
        $module = $entity->getModule();
        if(in_array($module,["requisition","purchase-order","job-approval","job-requisition","requisition-issue","purchase-issue","purchase-requisition","store-requisition","replacement-requisition"])){
            $invoice = $entity->getRequisitionNo();
        }elseif(in_array($module ,["tender","tender-comparative","work-order","goods-receive"])){
            $invoice = $entity->getInvoice();
        }elseif ($module == 'order-issue'){
            $invoice = $entity->getOrderNo();
        }
        $fromEmail  = empty($entity->getCreatedBy()) ? '' : $entity->getCreatedBy()->getUsername();
        $fromName = empty($entity->getCreatedBy()) ? '' : $entity->getCreatedBy()->getName();
        $toEmail = 'pr@amanknittings.com';
        $toName = "Purchase Order";
        $emailData = [
            'link'          => $link ,
            'invoice'       => $invoice,
            'created'       => $entity->getCreated()->format('d-m-Y'),
            'fromEmail'     => $fromEmail,
            'fromName'      => $fromName,
            'toEmail'       => $toEmail,
            'toName'        => $toName
        ];
        return $emailData;
    }

    public function meetingApprover(TenderCommittee $committee, $process)
    {
        $em = $this->_em;
        $terminal = $committee->getCreatedBy()->getTerminal()->getId();
        $approveUsers = $this->findBy(array('config' => $committee->getConfig(),'entityId'=>$committee->getId()));
        if($process == "tender"){
            foreach ($committee->getTenders() as $requisition):
                $i = 1;
                foreach ($approveUsers as $user) {
                    $exist = $this->findOneBy(array('assignTo'=> $user->getAssignTo(),'module'=> $requisition->getModule(),'entityId'=>"{$requisition->getId()}"));
                    if(empty($exist)){
                        $entity = new ProcurementProcess();
                        $entity->setConfig($requisition->getConfig());
                        $entity->setEntityId($requisition->getId());
                        $entity->setModule($requisition->getModule());
                        $entity->setAssignTo($user->getAssignTo());
                        $entity->setOrdering($i);
                        $entity->setIsMandatory($user->isMandatory());
                        $entity->setProcess($user->getProcess());
                        $entity->setStatus(true);
                        $em->persist($entity);
                        $em->flush();
                    }
                    $i++;
                }
            endforeach;
        }
        if($process == "purchase-memo"){
            foreach ($committee->getMemos() as $requisition):
                $i = 1;
                foreach ($approveUsers as $user) {
                    $exist = $this->findOneBy(array('assignTo'=> $user->getAssignTo(),'module'=> $requisition->getModule(),'entityId'=>"{$requisition->getId()}"));
                    if(empty($exist)){
                        $entity = new ProcurementProcess();
                        $entity->setConfig($requisition->getConfig());
                        $entity->setEntityId($requisition->getId());
                        $entity->setModule($requisition->getModule());
                        $entity->setAssignTo($user->getAssignTo());
                        $entity->setOrdering($i);
                        $entity->setIsMandatory($user->isMandatory());
                        $entity->setProcess($user->getProcess());
                        $entity->setStatus(true);
                        $em->persist($entity);
                        $em->flush();
                    }
                    $i++;
                }
            endforeach;
        }
    }

    public function revisedProcess($entity)
    {
        $em = $this->_em;
        $results = $this->findBy(array('entityId'=>$entity->getId()));
        foreach ($results as $row){
            $row->setComment(NULL);
            $row->setClose(false);
            $em->persist($row);
            $em->flush();
        }
    }


}
