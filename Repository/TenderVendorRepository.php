<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Repository;

use App\Entity\Application\Procurement;
use App\Entity\Domain\Vendor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\DmsBundle\Entity\DmsFile;
use Terminalbd\InventoryBundle\Entity\StockBook;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\Tender;
use Terminalbd\ProcurementBundle\Entity\TenderItem;
use Terminalbd\ProcurementBundle\Entity\TenderMemo;
use Terminalbd\ProcurementBundle\Entity\TenderVendor;
use Terminalbd\ProcurementBundle\Service\Image;


/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */

class TenderVendorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TenderVendor::class);
    }

    public function insertTendorVendor(Tender $reEntity,$data)
    {
        $em = $this->_em;
        $i=0;
        if(isset($data['name'])){
            foreach ($data['name'] as $value) {
                $metaId = isset($data['metaId'][$i]) ? $data['metaId'][$i] : '' ;
                if(!empty($metaId)){
                    $tendorVendor = $this->findOneBy(array('tender' => $reEntity,'id' => $metaId));
                    if($tendorVendor){
                        $this->updateMetaAttribute($tendorVendor,$data,$i);
                    }
                }elseif(($data['name'][$i] != "" and empty($metaId))){
                    $entity = new TenderVendor();
                    $entity->setConfig($reEntity->getConfig());
                    $entity->setTender($reEntity);
                    $entity->setName($data['name'][$i]);
                    $entity->setContactPerson($data['contactName'][$i]);
                    $entity->setPhone($data['phone'][$i]);
                    $entity->setAddress($data['address'][$i]);
                    $em->persist($entity);
                    $em->flush();
                }
                $i++;
            }
        }

        /* @var $vendor Vendor */

        foreach ($reEntity->getVendors() as $vendor){
          $exist = $this->findOneBy(array('tender'=> $reEntity,'vendor' => $vendor));
          if(empty($exist)){
              $entity = new TenderVendor();
              $entity->setConfig($reEntity->getConfig());
              $entity->setTender($reEntity);
              $entity->setVendor($vendor);
              $entity->setName($vendor->getCompanyName());
              $entity->setContactPerson($vendor->getName());
              $entity->setPhone($vendor->getMobile());
              $entity->setEmail($vendor->getEmail());
              $entity->setAddress($vendor->getAddress());
              $em->persist($entity);
              $em->flush();
          }
        }
        $this->insertSelectVendor($reEntity,$data);
    }

    public function insertQuotationVendor(Tender $reEntity,$data)
    {
        $em = $this->_em;
        $vendor = $data['tender_quotation_form']['approvedVendor'];
        $vendor = $em->getRepository(Vendor::class)->find($vendor);
        $exist = $this->findOneBy(array('tender'=> $reEntity,'vendor' => $vendor));
        if(empty($exist) and $vendor){
            $entity = new TenderVendor();
            $entity->setConfig($reEntity->getConfig());
            $entity->setTender($reEntity);
            $entity->setVendor($vendor);
            $entity->setName($vendor->getCompanyName());
            $entity->setContactPerson($vendor->getName());
            $entity->setPhone($vendor->getMobile());
            $entity->setEmail($vendor->getEmail());
            $entity->setAddress($vendor->getAddress());
            $entity->isDirect(1);
            $em->persist($entity);
            $em->flush();
            return $entity;
        }
        return $exist;

    }

    public function insertSelectVendor($reEntity,$data)
    {
        $em = $this->_em;
        if(isset($data['vendorSelect']) and $data['vendorSelect']){
            foreach ($data['vendorSelect'] as $key => $value):
                $exist = $this->findOneBy(array('tender'=> $reEntity,'vendor' => $value));
                $vendor = $em->getRepository(Vendor::class)->find($value);
                if(empty($exist) and $vendor){
                    $entity = new TenderVendor();
                    $entity->setConfig($reEntity->getConfig());
                    $entity->setTender($reEntity);
                    $entity->setVendor($vendor);
                    $entity->setName($vendor->getCompanyName());
                    $entity->setContactPerson($vendor->getName());
                    $entity->setPhone($vendor->getMobile());
                    $entity->setEmail($vendor->getEmail());
                    $entity->setStatus(1);
                    $entity->setAddress($vendor->getAddress());
                    $em->persist($entity);
                    $em->flush();
                }
            endforeach;
        }

    }

    public function directTenderVendor(Tender $entity, Vendor $vendor)
    {
        $em = $this->_em;
        $exist = $this->findOneBy(array('tender' => $entity,'vendor' => $vendor));
        if(empty($exist)){
            $tenderVendor = new TenderVendor();
            $tenderVendor->setConfig($entity->getConfig());
            $tenderVendor->setTender($entity);
            $tenderVendor->setVendor($vendor);
            $tenderVendor->setName($vendor->getCompanyName());
            $tenderVendor->setContactPerson($vendor->getName());
            $tenderVendor->setPhone($vendor->getMobile());
            $tenderVendor->setAddress($vendor->getAddress());
            $tenderVendor->setEmail($vendor->getEmail());
            $tenderVendor->setStatus(1);
            $tenderVendor->setIsDirect(1);
            $em->persist($tenderVendor);
            $em->flush();
        }
    }

    public function updateMetaAttribute(TenderVendor $entity , $data,$i)
    {
        $em = $this->_em;
        $email = (isset($data['email'][$i]) and $data['email'][$i])? $data['email'][$i]:'';
        $entity->setName($data['name'][$i]);
        $entity->setContactPerson($data['contactName'][$i]);
        $entity->setPhone($data['phone'][$i]);
        $entity->setAddress($data['address'][$i]);
        $entity->setEmail($email);
        $em->flush();
    }

    public function updateSubTotal($cs,TenderVendor $vendor)
    {
        $em = $this->_em;
        $qb = $em->createQueryBuilder();
        $qb->from('TerminalbdProcurementBundle:TenderComparativeItem','e');
        $qb->select('SUM(e.subTotal) as subTotal','SUM(e.revisedSubTotal) as revisedSubTotal');
        $qb->where('e.tenderComparative = :cs')->setParameter('cs', $cs->getId());
        $qb->andWhere('e.tenderVendor = :vendor')->setParameter('vendor', $vendor->getId());
        $result = $qb->getQuery()->getOneOrNullResult();
        $vendor->setSubTotal($result['subTotal']);
        $vendor->setRevisedTotal($result['revisedSubTotal']);
        $em->persist($vendor);
        $em->flush();
        return $result['revisedSubTotal'];
    }


    public function getTenderAverageAmount(TenderMemo $memo)
    {

        $qb = $this->createQueryBuilder('e');
        $qb->join('e.tenderMemo','tendermemo');
        $qb->select('AVG(e.subTotal) as subTotal','AVG(e.revisedTotal) as revisedTotal');
        $qb->where('tendermemo.id = :cs')->setParameter('cs', $memo->getId());
        $result = $qb->getQuery()->getOneOrNullResult();
        return $result;

    }


    public function vendorAttachmentFile($requisition, $data, $files)
    {
        $em = $this->_em;

        if (isset($files)) {
            define('FILETYPE', array('jpg', 'png', 'jpeg', 'pdf'));
            define('DESTINATION', 'uploads/procurement/');
            define('RESIZEBY', 'w');
            define('FILESIZE', 10097152);
            define('RESIZETO', 520);
            define('QUALITY', 100);
            $errors = array();
            foreach ($data['vendorCheck'] as $key => $vendor) {

                if(isset($files['tmp_name'][$key]) and !empty($files['tmp_name'][$key])){

                    $fileName = strtolower($requisition->getInvoice()). '-' . time() . "-" . $_FILES['vendorFiles']['name'][$key];
                    $fileType = $_FILES['vendorFiles']['type'][$key];
                    $fileSize = $_FILES['vendorFiles']['size'][$key];
                    if ($fileSize > FILESIZE) {
                        $errors[] = 'File size must be less than 2 MB';
                    }
                    if (empty($errors) == true and in_array($fileType, FILETYPE)) {
                        if (is_dir(DESTINATION) == false) {
                            mkdir(DESTINATION, 0777);        // Create directory if it does not exist
                        }
                        $image = new Image($_FILES['vendorFiles']['tmp_name'][$key]);
                        $image->destination = DESTINATION . $fileName;
                        $image->constraint = RESIZEBY;
                        $image->size = RESIZETO;
                        $image->quality = QUALITY;
                        $image->render();
                        $entity = $this->find($vendor);
                        if($entity){
                            $entity->setRemark($data['remark'][$key]);
                            $entity->setPath($fileName);
                            $em->persist($entity);
                            $em->flush();
                        }

                    } elseif (empty($errors) == true and $fileType = "application/pdf") {
                        $file_tmp = $_FILES['vendorFiles']['tmp_name'][$key];
                        if (is_dir(DESTINATION) == false) {
                            mkdir(DESTINATION, 0777);        // Create directory if it does not exist
                        }
                        if (is_dir(DESTINATION . $fileName) == false) {
                            move_uploaded_file($file_tmp, DESTINATION . $fileName);
                        }
                        $entity = $this->find($vendor);
                        if($entity){
                            $entity->setRemark($data['remark'][$key]);
                            $entity->setPath($fileName);
                            $em->persist($entity);
                            $em->flush();
                        }
                    } else {
                        return $errors;
                    }
                }
            }
        }
    }

    public function csBankVendorSelection($tender,$vendorCheck){
        $em = $this->_em;
        $queryBuilder = $this->_em->createQueryBuilder();
        $query = $queryBuilder ->update(TenderVendor::class, 'e')
            ->set('e.status', ':status')
            ->setParameter('status', 0)
            ->where('e.tender =:tender')
            ->setParameter('tender', $tender)
            ->getQuery();
        $query ->execute();
        foreach ($vendorCheck as $key => $vendor) {
            $entity = $this->find($vendor);
            $entity->setStatus(1);
            $em->persist($entity);
            $em->flush();
        }
    }

    public function csVendorSelection($data){
        $em = $this->_em;
        foreach ($data['vendorCheck'] as $key => $vendor) {
            $entity = $this->find($vendor);
            $entity->setStatus(1);
            $em->persist($entity);
            $em->flush();
        }
    }

}


