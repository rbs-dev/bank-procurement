<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Repository;

use App\Entity\Admin\AppModule;
use App\Entity\Domain\ModuleProcess;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\BudgetBundle\Entity\BudgetMonth;
use Terminalbd\DmsBundle\Entity\DmsFile;
use Terminalbd\GenericBundle\Entity\ItemBrand;
use Terminalbd\GenericBundle\Entity\ItemColor;
use Terminalbd\GenericBundle\Entity\ItemSize;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\InventoryBundle\Entity\StockBook;
use Terminalbd\InventoryBundle\Entity\StockHistory;
use Terminalbd\ProcurementBundle\Entity\JobRequisition;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionBudgetItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItemHistory;
use Terminalbd\ProcurementBundle\Entity\TenderComparativeItem;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorder;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderItem;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class RequisitionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Requisition::class);
    }


    protected function handleSearchBetween($qb,$form)
    {

        if(isset($form['requisition_filter_form'])){

            $data = $form['requisition_filter_form'];
            $startDate = isset($data['startDate'])? $data['startDate'] :'';
            $endDate = isset($data['endDate'])? $data['endDate'] :'';
            $branch = isset($data['branch'])? $data['branch'] :'';
            $unit = isset($data['unit'])? $data['unit'] :'';
            $department = isset($data['department'])? $data['department'] :'';
            $priroty = isset($data['priroty'])? $data['priroty'] :'';
            $process = isset($data['process'])? $data['process'] :'';
            $requisitionType = isset($data['requisitionType'])? $data['requisitionType'] :'';
            $requisitionNo = !empty($data['requisitionNo'])? trim($data['requisitionNo']) :'';
            $jobRequisition = !empty($data['jobRequisition'])? trim($data['jobRequisition']) :'';
            $budget = !empty($data['budget'])? trim($data['budget']) :'';
            $keyword = !empty($data['keyword'])? trim($data['keyword']) :'';
            if(!empty($requisitionNo)){
                $qb->andWhere($qb->expr()->like("e.requisitionNo", "'%$requisitionNo%'"));
            }
            if(!empty($jobRequisition)){
                $qb->andWhere($qb->expr()->like("jb.requisitionNo", "'%$jobRequisition%'"));
            }
            if(!empty($branch)){
                 $qb->andWhere('b.id = :branch')->setParameter('branch',$branch);
            }
            if(!empty($unit)){
                 $qb->andWhere('e.companyUnit = :unit')->setParameter('unit',$unit);
            }
            if(!empty($department)){
                 $qb->andWhere('d.id = :department')->setParameter('department',$department);
            }
            if(!empty($priroty)){
                 $qb->andWhere('p.id = :priority')->setParameter('priroty',$priroty);
            }
            if(!empty($requisitionType)){
                 $qb->andWhere('t.id = :requisitionType')->setParameter('requisitionType',$requisitionType);
            }
            if(!empty($budget) and $budget == "budget"){
                 $qb->andWhere('br.createdBy IS NOT NULL');
            }
            if (!empty($startDate) ) {
                $datetime = new \DateTime($startDate);
                $startDate = $datetime->format('Y-m-d 00:00:00');
                $qb->andWhere("e.created >= :startDate");
                $qb->setParameter('startDate', $startDate);
            }

            if (!empty($endDate)) {
                $datetime = new \DateTime($endDate);
                $endDate = $datetime->format('Y-m-d 23:59:59');
                $qb->andWhere("e.created <= :endDate");
                $qb->setParameter('endDate', $endDate);
            }
            if (!empty($keyword)) {
                $qb->andWhere('e.requisitionNo LIKE :searchTerm OR jb.requisitionNo LIKE :searchTerm OR cu.name LIKE :searchTerm  OR t.name LIKE :searchTerm OR d.name LIKE :searchTerm');
                $qb->setParameter('searchTerm', '%'.trim($keyword).'%');
            }

        }
    }



    /**
     * @return Requisition[]
     */

    public function processModuleLists( $config, User $user, $data = "")
    {
        $em = $this->_em;
        $terminal = $user->getTerminal()->getId();
        $moduleProcess = $em->getRepository(ModuleProcess::class)->existModuleProcess($terminal,'requisition');
        return $this->findRoleBaseSearchQuery( $config,$user,$data);

    }

    /**
     * @return Requisition[]
     */

    public function requisitionSummary( $config, User $user)
    {

        $group = $user->getTerminal()->getBusinessGroup()->getSlug();
        $start = new \DateTime("now");
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id','e.requisitionNo as requisitionNo','e.content as content','e.created as created','e.expectedDate as expectedDate','e.subTotal as subTotal','e.total as total', 'e.process as process', 'e.processOrdering as ordering','e.waitingProcess as waitingProcess');
        $qb->addSelect('mp.id as moduleId','mp.approveType as moduleApproveType');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('d.name as department');
        $qb->addSelect('t.name as requisitionType');
        $qb->addSelect('p.name as priority');
        $qb->addSelect('approve.id as approveId','approve.assignToRole as assignToRole','approve.assignToRoleName as roleToName');
        $qb->addSelect('rto.id as reportToId','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->join('e.moduleProcess','mp');
        $qb->leftJoin('e.branch','b');
        $qb->leftJoin('e.department','d');
        $qb->leftJoin('e.requisitionType','t');
        $qb->leftJoin('e.priority','p');
        $qb->leftJoin('e.createdBy','u');
        $qb->leftJoin('e.approveProcess', 'approve');
        $qb->leftJoin('approve.assignTo', 'rto');
        $qb->where('e.config = :config')->setParameter('config',$config);
        if($user->getApprovalRole() and in_array($user->getApprovalRole()->getRoleName(),['ROLE_PROCUREMENT_MANAGER','ROLE_PROCUREMENT_DEPARTMENT_HEAD'])){
            $qb->andWhere('approve.assignToRole =:role')->setParameter('role',$user->getApprovalRole()->getRoleName());
        }else{
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }
        if(!empty($user->getProfile()) and $user->getProfile()->getBranch()){
            $branchId = $user->getProfile()->getBranch()->getId();
            $qb->andWhere('b.id = :branchId')->setParameter('branchId',$branchId);
        }elseif(!empty($user->getProfile()) and $user->getProfile()->getDepartment()){
            $dId = $user->getProfile()->getDepartment()->getId();
            $qb->andWhere('d.id = :dId')->setParameter('dId',$dId);
        }
        $start = new \DateTime("now");
        $startDate = $start->format('Y-m-d 00:00:00');
        $qb->andWhere("e.created >= :startDate");
        $qb->setParameter('startDate', $startDate);
        $endDate = $start->format('Y-m-d 23:59:59');
        $qb->andWhere("e.created <= :endDate");
        $qb->setParameter('endDate', $endDate);
        $results = $qb->getQuery()->getArrayResult();

        $em = $this->_em;
        $terminal = $user->getTerminal()->getId();
        $moduleProcess = $em->getRepository(ModuleProcess::class)->existModuleProcess($terminal,'store-requisition');
        $group = $user->getTerminal()->getBusinessGroup()->getSlug();
        $report = isset($data[0]['reportTo']) ? $data[0]['reportTo']:'';
        $sort = isset($data['sort'])? $data['sort'] :'e.created';
        $direction = isset($data['direction'])? $data['direction'] :'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('COUNT(e.requisitionNo) as count');
        $qb->leftJoin('e.branch','b');
        $qb->leftJoin('e.department','d');
        $qb->leftJoin('e.approveProcess', 'approve');
        $qb->leftJoin('approve.assignTo', 'rto');
        $qb->leftJoin('e.createdBy','u');
        $qb->where('e.config = :config')->setParameter('config',$config);
        if($user->getApprovalRole() and  in_array($user->getApprovalRole()->getRoleName(),['ROLE_PROCUREMENT_MANAGER','ROLE_PROCUREMENT_DEPARTMENT_HEAD'])){
            $qb->andWhere('approve.assignToRole =:role')->setParameter('role',$user->getApprovalRole()->getRoleName());
        }else{
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }
        if(!empty($user->getProfile()) and $user->getProfile()->getBranch()){
            $branchId = $user->getProfile()->getBranch()->getId();
            $qb->andWhere('b.id = :branchId')->setParameter('branchId',$branchId);
        }elseif(!empty($user->getProfile()) and $user->getProfile()->getDepartment()){
            $dId = $user->getProfile()->getDepartment()->getId();
            $qb->andWhere('d.id = :dId')->setParameter('dId',$dId);
        }
        $startDate = $start->format('Y-m-d 00:00:00');
        $qb->andWhere("e.created >= :startDate");
        $qb->setParameter('startDate', $startDate);
        $endDate = $start->format('Y-m-d 23:59:59');
        $qb->andWhere("e.created <= :endDate");
        $qb->setParameter('endDate', $endDate);
        $day = $qb->getQuery()->getSingleScalarResult();

        $em = $this->_em;
        $terminal = $user->getTerminal()->getId();
        $moduleProcess = $em->getRepository(ModuleProcess::class)->existModuleProcess($terminal,'store-requisition');
        $group = $user->getTerminal()->getBusinessGroup()->getSlug();
        $report = isset($data[0]['reportTo']) ? $data[0]['reportTo']:'';
        $sort = isset($data['sort'])? $data['sort'] :'e.created';
        $direction = isset($data['direction'])? $data['direction'] :'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('COUNT(e.requisitionNo) as count');
        $qb->leftJoin('e.branch','b');
        $qb->leftJoin('e.department','d');
        $qb->leftJoin('e.approveProcess', 'approve');
        $qb->leftJoin('approve.assignTo', 'rto');
        $qb->leftJoin('e.createdBy','u');
        $qb->where('e.config = :config')->setParameter('config',$config);
        if($user->getApprovalRole() and in_array($user->getApprovalRole()->getRoleName(),['ROLE_PROCUREMENT_MANAGER','ROLE_PROCUREMENT_DEPARTMENT_HEAD'])){
            $qb->andWhere('approve.assignToRole =:role')->setParameter('role',$user->getApprovalRole()->getRoleName());
        }else{
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }
        if(!empty($user->getProfile()) and $user->getProfile()->getBranch()){
            $branchId = $user->getProfile()->getBranch()->getId();
            $qb->andWhere('b.id = :branchId')->setParameter('branchId',$branchId);
        }elseif(!empty($user->getProfile()) and $user->getProfile()->getDepartment()){
            $dId = $user->getProfile()->getDepartment()->getId();
            $qb->andWhere('d.id = :dId')->setParameter('dId',$dId);
        }
        $start = new \DateTime("now");
        $startDate = $start->format('Y-m-01 00:00:00');
        $qb->andWhere("e.created >= :startDate");
        $qb->setParameter('startDate', $startDate);
        $endDate = $start->format('Y-m-t 23:59:59');
        $qb->andWhere("e.created <= :endDate");
        $qb->setParameter('endDate', $endDate);
        $month = $qb->getQuery()->getSingleScalarResult();

        $em = $this->_em;
        $terminal = $user->getTerminal()->getId();
        $moduleProcess = $em->getRepository(ModuleProcess::class)->existModuleProcess($terminal,'store-requisition');
        $group = $user->getTerminal()->getBusinessGroup()->getSlug();
        $report = isset($data[0]['reportTo']) ? $data[0]['reportTo']:'';
        $sort = isset($data['sort'])? $data['sort'] :'e.created';
        $direction = isset($data['direction'])? $data['direction'] :'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('COUNT(e.requisitionNo) as count');
        $qb->leftJoin('e.branch','b');
        $qb->leftJoin('e.department','d');
        $qb->leftJoin('e.approveProcess', 'approve');
        $qb->leftJoin('approve.assignTo', 'rto');
        $qb->leftJoin('e.createdBy','u');
        $qb->where('e.config = :config')->setParameter('config',$config);
        if($user->getApprovalRole() and in_array($user->getApprovalRole()->getRoleName(),['ROLE_PROCUREMENT_MANAGER','ROLE_PROCUREMENT_DEPARTMENT_HEAD'])){
            $qb->andWhere('approve.assignToRole =:role')->setParameter('role',$user->getApprovalRole()->getRoleName());
        }else{
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }
        if(!empty($user->getProfile()) and $user->getProfile()->getBranch()){
            $branchId = $user->getProfile()->getBranch()->getId();
            $qb->andWhere('b.id = :branchId')->setParameter('branchId',$branchId);
        }elseif(!empty($user->getProfile()) and $user->getProfile()->getDepartment()){
            $dId = $user->getProfile()->getDepartment()->getId();
            $qb->andWhere('d.id = :dId')->setParameter('dId',$dId);
        }
        $start = new \DateTime("now");
        $startDate = $start->format('Y-01-01 00:00:00');
        $qb->andWhere("e.created >= :startDate");
        $qb->setParameter('startDate', $startDate);
        $endDate = $start->format('Y-12-t 23:59:59');
        $qb->andWhere("e.created <= :endDate");
        $qb->setParameter('endDate', $endDate);
        $year = $qb->getQuery()->getSingleScalarResult();

        $result = array('entities'=>$results,'day'=>$day,'month'=>$month,'year'=>$year);
        return $result;

    }

    public function findBankBaseSearchQuery( $config, User $user, $data = "" )
    {
        $em = $this->_em;
        $terminal = $user->getTerminal()->getId();
        $moduleProcess = $em->getRepository(ModuleProcess::class)->existModuleProcess($terminal,'store-requisition');
        $group = $user->getTerminal()->getBusinessGroup()->getSlug();
        $report = isset($data[0]['reportTo']) ? $data[0]['reportTo']:'';
        $sort = isset($data['sort'])? $data['sort'] :'e.created';
        $direction = isset($data['direction'])? $data['direction'] :'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id','e.requisitionNo as requisitionNo','e.created as created','e.expectedDate as expectedDate','e.subTotal as subTotal','e.total as total', 'e.process as process', 'e.processOrdering as ordering','e.waitingProcess as waitingProcess');
        $qb->addSelect('mp.id as moduleId','mp.approveType as moduleApproveType');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('d.name as department');
        $qb->addSelect('t.name as requisitionType');
        $qb->addSelect('p.name as priority');
        $qb->addSelect('approve.id as approveId','approve.assignToRole as assignToRole','approve.assignToRoleName as roleToName');
        $qb->addSelect('rto.id as reportToId','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->join('e.moduleProcess','mp');
        $qb->leftJoin('e.branch','b');
        $qb->leftJoin('e.department','d');
        $qb->leftJoin('e.requisitionType','t');
        $qb->leftJoin('e.priority','p');
        $qb->leftJoin('e.createdBy','u');
        $qb->leftJoin('e.approveProcess', 'approve');
        $qb->leftJoin('approve.assignTo', 'rto');
        $qb->where('e.config = :config')->setParameter('config',$config);
        if($data['mode'] == "in-progress") {
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
        }elseif($data['mode'] == "approve" and $moduleProcess->getApproveType() == "role" and !empty($user->getApprovalRole())){
            $qb->andWhere('approve.assignToRole =:role')->setParameter('role',$user->getApprovalRole()->getRoleName());
        }elseif($data['mode'] == "approve" and $moduleProcess->getApproveType() == "user"){
            $qb->andWhere('approve.assignTo =:assignTo')->setParameter('assignTo',"{$user->getId()}");
        }elseif($data['mode'] == "list"){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }elseif($data['mode'] == "archive"){
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "Approved");
        }
        if(!empty($user->getProfile()) and $user->getProfile()->getBranch()){
            $branchId = $user->getProfile()->getBranch()->getId();
            $qb->andWhere('b.id = :branchId')->setParameter('branchId',$branchId);
        }elseif(!empty($user->getProfile()) and $user->getProfile()->getDepartment()){
            $dId = $user->getProfile()->getDepartment()->getId();
            $qb->andWhere('d.id = :dId')->setParameter('dId',$dId);
        }
        $this->handleSearchBetween($qb,$data);
        $qb->orderBy("{$sort}",$direction);
        $result = $qb->getQuery();
        return $result;

    }

    /**
     * @return Requisition[]
     */

    public function findUserBaseSearchQuery( $config, User $user, $data = "" )
    {

        $group = $user->getTerminal()->getBusinessGroup()->getSlug();
        $report = isset($data[0]['reportTo']) ? $data[0]['reportTo']:'';
        $sort = isset($data['sort'])? $data['sort'] :'e.created';
        $direction = isset($data['direction'])? $data['direction'] :'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id','e.requisitionNo as requisitionNo','e.created as created','e.expectedDate as expectedDate','e.subTotal as subTotal','e.total as total', 'e.processOrdering as ordering', 'e.process as process','e.requisitionMode as requisitionMode', 'e.waitingProcess as waitingProcess');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('cu.code as unit');
        $qb->addSelect('d.name as department');
        $qb->addSelect('t.name as requisitionType');
        $qb->addSelect('p.name as priority');
        $qb->addSelect('jb.requisitionNo as jobRequisition');
        $qb->addSelect('rto.id as reportToId','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->leftJoin('e.jobRequisition','jb');
        $qb->leftJoin('e.companyUnit','cu');
        $qb->leftJoin('cu.parent','b');
        $qb->leftJoin('e.department','d');
        $qb->leftJoin('e.requisitionType','t');
        $qb->leftJoin('e.priority','p');
        $qb->leftJoin('e.createdBy','u');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->where('e.config = :config')->setParameter('config',$config);
        if($data['mode'] == "in-progress") {
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
        }
        if($data['mode'] == "in-progress") {
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
        }elseif($data['mode'] == "approve"){
            $qb->andWhere('e.reportTo =:report')->setParameter('report',"{$user->getId()}");
        }elseif($data['mode'] == "list"){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }elseif($data['mode'] == "archive"){
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "Approved");
        }
        if(!empty($user->getProfile()) and $user->getProfile()->getBranch()){
            $branchId = $user->getProfile()->getBranch()->getId();
            $qb->andWhere('b.id = :branchId')->setParameter('branchId',$branchId);
        }elseif(!empty($user->getProfile()) and $user->getProfile()->getDepartment()){
            $dId = $user->getProfile()->getDepartment()->getId();
            $qb->andWhere('d.id = :dId')->setParameter('dId',$dId);
        }
        $this->handleSearchBetween($qb,$data);
        $qb->orderBy("{$sort}",$direction);
        $result = $qb->getQuery();
        return $result;

    }

    /**
     * @return Requisition[]
     */
    public function findRequisitionSearchQuery( $config,User $user, $data = "" )
    {

        $group = $user->getTerminal()->getBusinessGroup()->getSlug();
        $report = isset($data[0]['reportTo']) ? $data[0]['reportTo']:'';
        $sort = isset($data['sort'])? $data['sort'] :'e.created';
        $direction = isset($data['direction'])? $data['direction'] :'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id','e.requisitionNo as requisitionNo','e.created as created','e.expectedDate as expectedDate','e.subTotal as subTotal','e.total as total', 'e.process as process','e.processOrdering as ordering', 'e.waitingProcess as waitingProcess', 'e.requisitionMode as requisitionMode', 'e.isEmergency as isEmergency', 'e.isRevised as isRevised');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('cu.code as unit');
        $qb->addSelect('reverse.id as reverseId','reverse.requisitionNo as reverseRequisitionNo');
        $qb->addSelect('d.name as department');
        $qb->addSelect('sec.name as section');
        $qb->addSelect('t.name as requisitionType');
        $qb->addSelect('p.name as priority');
        $qb->addSelect('jb.requisitionNo as jobRequisitionNo,jb.id as jobRequisition');
        $qb->addSelect('rto.id as reportToId','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->addSelect('brat.name as budgetUser');
        $qb->leftJoin('e.jobRequisition','jb');
        $qb->leftJoin('e.reverse','reverse');
        $qb->leftJoin('e.section','sec');
        $qb->leftJoin('e.processDepartment','d');
        $qb->leftJoin('e.requisitionType','t');
        $qb->leftJoin('e.priority','p');
        $qb->leftJoin('e.createdBy','u');
        $qb->leftJoin('u.profile','profile');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->leftJoin('e.companyUnit','cu');
        $qb->leftJoin('cu.parent','b');
        $qb->leftJoin('e.budgetRequisition', 'br');
        $qb->leftJoin('br.createdBy', 'brat');
        $qb->where('e.config = :config')->setParameter('config',$config);
        if($data['mode'] == "in-progress") {
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
        }elseif($data['mode'] == "approve"){
            $qb->andWhere('e.reportTo =:report')->setParameter('report',"{$user->getId()}");
        }elseif($data['mode'] == "list"){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }elseif($data['mode'] == "archive"){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', array('Approved','Closed'));
        }elseif($data['mode'] == "assaign"){
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
            $qb->andWhere("brat.id != 'NULL'");
        }elseif($data['mode'] == "archived"){
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', array('Approved','Closed'));
        }
        $this->handleSearchBetween($qb,$data);
        $qb->orderBy("{$sort}",$direction);
        $result = $qb->getQuery();
        return $result;

    }


    /**
     * @return Requisition[]
     */
    public function findPurchaseOrderSearchQuery( $config,User $user, $data = "" )
    {

        $group = $user->getTerminal()->getBusinessGroup()->getSlug();
        $report = isset($data[0]['reportTo']) ? $data[0]['reportTo']:'';
        $sort = isset($data['sort'])? $data['sort'] :'e.updated';
        $direction = isset($data['direction'])? $data['direction'] :'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id','e.requisitionNo as requisitionNo','e.created as created','e.updated as updated','e.expectedDate as expectedDate','e.subTotal as subTotal','e.total as total', 'e.process as process','e.processOrdering as ordering', 'e.waitingProcess as waitingProcess', 'e.isEmergency as isEmergency', 'e.isRevised as isRevised');
        $qb->addSelect('cu.code as unit');
        $qb->addSelect('reverse.id as reverseId','reverse.requisitionNo as reverseRequisitionNo');
        $qb->addSelect('d.name as department');
        $qb->addSelect('sec.name as section');
        $qb->addSelect('t.name as requisitionType');
        $qb->addSelect('p.name as priority');
        $qb->addSelect('jb.requisitionNo as jobRequisitionNo,jb.id as jobRequisition');
        $qb->addSelect('rto.id as reportToId','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->addSelect('SUM(ri.quantity) as quantity','SUM(ri.issueQuantity) as issueQuantity','SUM(ri.workorderQuantity) as workorderQuantity','SUM(ri.receiveQuantity) as receiveQuantity');
        $qb->leftJoin('e.jobRequisition','jb');
        $qb->leftJoin('e.requisitionItems','ri');
        $qb->leftJoin('e.reverse','reverse');
        $qb->leftJoin('e.section','sec');
        $qb->leftJoin('e.processDepartment','d');
        $qb->leftJoin('e.requisitionType','t');
        $qb->leftJoin('e.priority','p');
        $qb->leftJoin('e.createdBy','u');
        $qb->leftJoin('u.profile','profile');
        $qb->leftJoin('e.companyUnit','cu');
        $qb->leftJoin('cu.parent','b');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->leftJoin('e.tender', 'tender');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $qb->andWhere('e.waitingProcess =:wProcess')->setParameter('wProcess', "Approved");
        $qb->andWhere('e.requisitionMode IN (:mode)')->setParameter('mode', array('CAPEX','OPEX'));
        if($data['mode'] == "list") {
            $qb->andWhere("e.process !='Closed'");
        }elseif($data['mode'] == "archive") {
            $qb->andWhere("e.process = 'Closed'");
        }
        $qb->groupBy('e.id');
        $this->handleSearchBetween($qb,$data);
        $qb->orderBy("{$sort}",$direction);
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }


    /**
     * @return Requisition[]
     */
    public function findBySearchQuery( $config, User $user, $data = "" )
    {

        $sort = isset($data['sort'])? $data['sort'] :'e.created';
        $direction = isset($data['direction'])? $data['direction'] :'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id','e.requisitionNo as requisitionNo','e.created as created','e.expectedDate as expectedDate','e.subTotal as subTotal','e.total as total','e.process as process','e.processOrdering as ordering','e.filename as filename');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('d.name as department');
        $qb->addSelect('t.name as requisitionType');
        $qb->addSelect('p.name as priority');
        $qb->addSelect('rto.id as reportToId','rto.name as reportToName');
        $qb->leftJoin('e.branch','b');
        $qb->leftJoin('e.processDepartment','d');
        $qb->leftJoin('e.requisitionType','t');
        $qb->leftJoin('e.priority','p');
        $qb->leftJoin('e.createdBy','u');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $qb->andWhere('u.id = :initior')->setParameter('initior',$user->getId());
        if($user->getProfile()->getBranch()){
            $branchId = $user->getProfile()->getBranch()->getId();
            $qb->andWhere('b.id = :branchId')->setParameter('branchId',$branchId);
        }elseif($user->getProfile()->getDepartment()){
            $dId = $user->getProfile()->getDepartment()->getId();
            $qb->andWhere('d.id = :dId')->setParameter('dId',$dId);
        }
        $this->handleSearchBetween($qb,$data);
        $qb->orderBy("{$sort}",$direction);
        $result = $qb->getQuery();
        return $result;

    }

    /**
     * @return Requisition[]
     */
    public function findByApproveQuery( $config, User $user, $data = "" )
    {

        $workingArea = $user->getProfile()->getServiceMode()->getSlug();
        $roleId =  $user->getUserGroupRole()->getId();
        $process = $user->getUserGroupRole()->getProcess();

        $sort = isset($data['sort'])? $data['sort'] :'e.created';
        $direction = isset($data['direction'])? $data['direction'] :'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id','e.requisitionNo as requisitionNo','e.created as created','e.expectedDate as expectedDate','e.subTotal as subTotal','e.total as total','e.processOrdering as ordering','e.process as process','e.filename as filename');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('d.name as department');
        $qb->addSelect('t.name as requisitionType');
        $qb->addSelect('p.name as priority');
        $qb->leftJoin('e.branch','b');
        $qb->leftJoin('e.processDepartment','d');
        $qb->leftJoin('e.requisitionType','t');
        $qb->leftJoin('e.priority','p');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $qb->andWhere('e.waitingProcess = :waitingProcess')->setParameter('waitingProcess',$process);
        $qb->andWhere('e.process != :process')->setParameter('process','Rejected');
        if($workingArea == "branch"){
            $branchId = $user->getProfile()->getBranch()->getId();
            $qb->andWhere('b.id = :branchId')->setParameter('branchId',$branchId);
        }elseif($workingArea == "department"){
            $branchId = $user->getProfile()->getDepartment()->getId();
            $qb->andWhere('d.id = :branchId')->setParameter('branchId',$branchId);
        }
        $this->handleSearchBetween($qb,$data);
        $qb->orderBy("{$sort}",$direction);
        $result = $qb->getQuery();
        return $result;

    }

    /**
     * @return Purchase[]
     */
    public function findByStoreApproveQuery( $config, User $user, $data = "" )
    {

        $sort = isset($data['sort'])? $data['sort'] :'e.created';
        $direction = isset($data['direction'])? $data['direction'] :'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id','e.requisitionNo as requisitionNo','e.created as created','e.expectedDate as expectedDate','e.subTotal as subTotal','e.total as total','e.process as process','e.processOrdering as ordering','e.filename as filename');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('d.name as department');
        $qb->addSelect('t.name as requisitionType');
        $qb->addSelect('p.name as priority');
        $qb->addSelect('u.name as createdBy');
        $qb->addSelect('ro.id as requisitionOrderId');
        $qb->join('e.createdBy','u');
        $qb->leftJoin('e.branch','b');
        $qb->leftJoin('e.processDepartment','d');
        $qb->leftJoin('e.requisitionType','t');
        $qb->leftJoin('e.priority','p');
        $qb->leftJoin('e.requisitionOrder','ro');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $qb->andWhere('e.waitingProcess = :waitingProcess')->setParameter('waitingProcess','Approved');
        $qb->andWhere('t.slug = :type')->setParameter('type','store-requisition');
        $qb->andWhere('ro.id IS NULL');
        $this->handleSearchBetween($qb,$data);
        $qb->orderBy("{$sort}",$direction);
        $result = $qb->getQuery();
        return $result;

    }

    public function generateStoreRequisition(Requisition $requisition , $process = "")
    {
        $em =$this->_em;

        $module = $em->getRepository(AppModule::class)->findOneBy(array('slug' => $process));
        $terminal = $requisition->getCreatedBy()->getTerminal()->getId();

        /* @var $moduleProcess ModuleProcess */
        $moduleProcess = $em->getRepository(ModuleProcess::class)->findOneBy(array('terminal' =>$terminal,'module' => $module));

        foreach ($moduleProcess->getModuleProcessItems() as $row){
            $exist = $em->getRepository(ModuleProcess::class)->findOneBy(array('requisition'=>$requisition,'moduleProcessItem'=>$row));
            if(empty($exist)){
                $entity = new ProcurementProcess();
                $entity->setRequisition($requisition);
                $entity->setModuleProcessItem($row);
                $entity->setProcess($requisition->getProcess());
                $em->persist($entity);
                $em->flush();
            }
        }

    }

    public function requisitionItemHistory(Requisition $requisition)
    {
        $em =$this->_em;
        /* @var $item RequisitionItem */

        if(!empty($requisition->getRequisitionItems())){

            foreach ($requisition->getRequisitionItems() as $item){

                $entity = new RequisitionItemHistory();
                $entity->setRequisitionItem($item);
                $entity->setActualQuantity($item->getActualQuantity());
                $entity->setQuantity($item->getQuantity());
                $em->persist($entity);
                $em->flush();

            }
        }
    }

    public function insertReverseRequsition(Requisition $requisition)
    {
        $em = $this->_em;
        $module = "purchase-requisition";
        $entity = new Requisition();
        $entity->setConfig($requisition->getConfig());
        $moduleProcess = $em->getRepository(ModuleProcess::class)->existModuleProcess($requisition->getCreatedBy()->getTerminal(),$module);
        if(!empty($moduleProcess)) {
            $entity->setModuleProcess($moduleProcess);
            $entity->setModule($module);
        }
        $entity->setBusinessGroup('garment');
        $entity->setCreatedBy($requisition->getCreatedBy());
        $entity->setProcess("New");
        $entity->setContent($requisition->getContent());
        $entity->setWaitingProcess("New");
        $entity->setDepartment($requisition->getDepartment());
        $entity->setProcessDepartment($requisition->getProcessDepartment());
        $em->persist($entity);
        $em->flush();
        if($requisition->getRequisitionItems()){

            /* @var $row RequisitionItem  */

            foreach ($requisition->getRequisitionItems() as $row){

                $requsitionItem = new RequisitionItem();
                $requsitionItem->setRequisition($entity);
                $requsitionItem->setItem($row->getItem());
                $requsitionItem->setStock($row->getStock());
                $requsitionItem->setStockBook($row->getStockBook());
                $requsitionItem->setQuantity($row->getQuantity());
                $requsitionItem->setApproveQuantity($row->getQuantity());
                $requsitionItem->setActualQuantity($row->getQuantity());
                $requsitionItem->setPrice($row->getPrice());
                $stockBook = $row->getStockBook();
                if($stockBook->getPrice()){
                    $requsitionItem->setPrice($stockBook->getPrice());
                }
                $requsitionItem->setCurrentPrice($stockBook->getCmp());
                if($stockBook->getCmp()){
                    $requsitionItem->setCmpDate($stockBook->getUpdated());
                }
                $lastPurchase = $em->getRepository(StockHistory::class)->getLastPurchaseItemInformation($stockBook->getId());
                if($lastPurchase){
                    $requsitionItem->setLastPurchaseQuantity($lastPurchase['purchaseQuantity']);
                    $requsitionItem->setLastPurchasePrice($lastPurchase['price']);
                    $requsitionItem->setLastPurchaseDate($lastPurchase['updated']);
                }
                $requsitionItem->setSubTotal($requsitionItem->getPrice() * $row->getQuantity());
                $em->persist($requsitionItem);
                $em->flush();
            }
        }
        $requisition->setReverse($entity);
        $requisition->setProcess('Reversed');
        $requisition->setWaitingProcess('Reversed');
        $em->flush();
        return $entity;
    }

    public function insertRequsitionFromJobRequisition(JobRequisition $jobRequisition)
    {
        $em = $this->_em;
        $module = "purchase-requisition";
        $entity = new Requisition();
        $entity->setJobRequisition($jobRequisition);
        $entity->setConfig($jobRequisition->getConfig());
        $moduleProcess = $em->getRepository(ModuleProcess::class)->existModuleProcess($jobRequisition->getCreatedBy()->getTerminal(),$module);
        if(!empty($moduleProcess)) {
            $entity->setModuleProcess($moduleProcess);
            $entity->setModule($module);
        }
        $entity->setBusinessGroup('garment');
        $entity->setCreatedBy($jobRequisition->getCreatedBy());
        $entity->setRequisitionMode($jobRequisition->getRequisitionMode());
        $entity->setProcess("New");
        $entity->setContent($jobRequisition->getContent());
        $entity->setWaitingProcess("New");
        $entity->setDepartment($jobRequisition->getDepartment());
        $entity->setCompanyUnit($jobRequisition->getCompanyUnit());
        $entity->setProcessDepartment($jobRequisition->getProcessDepartment());
        $em->persist($entity);
        $em->flush();
        if($jobRequisition->getRequisitionItems()){

            /* @var $row RequisitionItem  */

            foreach ($jobRequisition->getRequisitionItems() as $row){

                $requsitionItem = new RequisitionItem();
                $requsitionItem->setRequisition($entity);
                $requsitionItem->setItem($row->getItem());
                $requsitionItem->setStock($row->getStock());
                $requsitionItem->setStockBook($row->getStockBook());
                $requsitionItem->setQuantity($row->getQuantity());
                $requsitionItem->setApproveQuantity($row->getQuantity());
                $requsitionItem->setActualQuantity($row->getQuantity());
                $requsitionItem->setRemainigQuantity($row->getQuantity());
                $requsitionItem->setPrice($row->getPrice());
                $stockBook = $row->getStockBook();
                if($stockBook->getPrice()){
                    $requsitionItem->setPrice($stockBook->getPrice());
                }
                $requsitionItem->setCurrentPrice($stockBook->getCmp());
                if($stockBook->getCmp()){
                    $requsitionItem->setCmpDate($stockBook->getUpdated());
                }
                $lastPurchase = $em->getRepository(StockHistory::class)->getLastPurchaseItemInformation($stockBook->getId());
                if($lastPurchase){
                    $requsitionItem->setLastPurchaseQuantity($lastPurchase['purchaseQuantity']);
                    $requsitionItem->setLastPurchasePrice($lastPurchase['price']);
                    $requsitionItem->setLastPurchaseDate($lastPurchase['updated']);
                }
                $requsitionItem->setSubTotal($requsitionItem->getPrice() * $row->getQuantity());
                $em->persist($requsitionItem);
                $em->flush();
            }
        }
        return $entity;
    }

    public function inserCanbanItem(Requisition $requisition, $data)
    {
        $em = $this->_em;
            foreach ($data['item'] as $row){
                /* @var $stock StockBook */
                $quantity = $data["quantity-{$row}"];
                if($quantity > 0 ){
                    $stock = $em->getRepository(StockBook::class)->find($row);
                    $requsitionItem = new RequisitionItem();
                    $requsitionItem->setRequisition($requisition);
                    $requsitionItem->setStockBook($stock);
                    $requsitionItem->setItem($stock->getItem());
                    $requsitionItem->setStock($stock->getStock());
                    $requsitionItem->setActualQuantity($quantity);
                    $requsitionItem->setQuantity($quantity);
                    $requsitionItem->setRemainigQuantity($quantity);
                    $requsitionItem->setPrice($stock->getPrice());
                    if($stock->getBrand()){
                        $requsitionItem->setBrand($stock->getBrand());
                    }
                    if($stock->getSize()){
                        $requsitionItem->setSize($stock->getSize());
                    }
                    if($stock->getColor()){
                        $requsitionItem->setColor($stock->getColor());
                    }
                    $requsitionItem->setSubTotal($stock->getPrice() * $requsitionItem->getQuantity());
                    $em->persist($requsitionItem);
                    $em->flush();
                }

            }

    }

    public function insertAttachmentFile(Requisition $requisition, $data, $files)
    {
        $em = $this->_em;

        define('FILETYPE', array('jpg', 'png', 'jpeg', 'pdf'));
        define('DESTINATION', 'uploads/procurement/');
        define('RESIZEBY', 'w');
        define('FILESIZE', 10097152);
        define('RESIZETO', 520);
        define('QUALITY', 100);

        if (isset($files)) {
            $errors = array();
            foreach ($files['tmp_name'] as $key => $tmp_name) {

                if($tmp_name){

                    $fileName = $requisition->getRequisitionNo() . '-' . time() . "-" . $_FILES['files']['name'][$key];
                    $fileType = $_FILES['files']['type'][$key];
                    $fileSize = $_FILES['files']['size'][$key];

                    if ($fileSize > FILESIZE) {
                        $errors[] = 'File size must be less than 2 MB';
                    }
                    if (empty($errors) == true and in_array($fileType, FILETYPE)) {
                        if (is_dir(DESTINATION) == false) {
                            mkdir(DESTINATION, 0777);        // Create directory if it does not exist
                        }
                        $image = new Image($_FILES['files']['tmp_name'][$key]);
                        $image->destination = DESTINATION . $fileName;
                        $image->constraint = RESIZEBY;
                        $image->size = RESIZETO;
                        $image->quality = QUALITY;
                        $image->render();
                        $entity = new DmsFile();
                        $entity->setSourceId($requisition->getId());
                        $entity->setBundle("Procurement");
                        $entity->setModule("Job-Requisition");
                        if($data['labelName'][$key]){
                            $entity->setName($data['labelName'][$key]);
                        }
                        $entity->setFileName($fileName);
                        $em->persist($entity);
                        $em->flush();
                    } elseif (empty($errors) == true and $fileType = "application/pdf") {
                        $file_tmp = $_FILES['files']['tmp_name'][$key];
                        if (is_dir(DESTINATION) == false) {
                            mkdir(DESTINATION, 0777);        // Create directory if it does not exist
                        }
                        if (is_dir(DESTINATION . $fileName) == false) {
                            move_uploaded_file($file_tmp, DESTINATION . $fileName);
                        }
                        $entity = new DmsFile();
                        $entity->setSourceId($requisition->getId());
                        $entity->setBundle("Procurement");
                        $entity->setModule("Requisition");
                        if($data['labelName'][$key]){
                            $entity->setName($data['labelName'][$key]);
                        }
                        $entity->setFileName($fileName);
                        $em->persist($entity);
                        $em->flush();
                    } else {
                        return $errors;
                    }
                }

            }
        }
    }

    public function getDmsAttchmentFile(Requisition $entity)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->from(DmsFile::class,'e');
        $qb->select('e.id', 'e.created as created','e.name as name', 'e.fileName as fileName');
        $qb->where('e.sourceId = :sourceId')->setParameter('sourceId',"{$entity->getId()}");
        $qb->andWhere('e.bundle = :bundle')->setParameter('bundle',"Procurement");
        $qb->andWhere('e.module = :module')->setParameter('module',"requisition");
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }


    public function checkBudgetLowStatus($entity)
    {

        $qb = $this->_em->createQueryBuilder();
        $qb->from(RequisitionBudgetItem::class,'e');
        $qb->select('COUNT(e.id) as lowStatus');
        $qb->where('e.requisition = :requisition')->setParameter('requisition',$entity);
        $qb->andWhere('e.lowStatus = 1');
        $low = $qb->getQuery()->getSingleScalarResult();
        if($low){
            return 1;
        }else{
            return 0;
        }
    }

    public function workorderwithBudgetExpense(TenderWorkorder $workorder)
    {
        $em = $this->_em;
        $qb = $this->_em->createQueryBuilder();
        $qb->from(TenderWorkorderItem::class,'e');
        $qb->join('e.tenderWorkorder','workorder');
        $qb->join('e.gl','gl');
        $qb->join('e.requisition','requisition');
        $qb->join('requisition.comapnyRequisitionShares','share');
        $qb->join('share.branch','b');
        $qb->select('sum(e.subTotal) requisitionAmount');
        $qb->addSelect('requisition.id as requisitionId');
        $qb->addSelect('b.id as branchId');
        $qb->addSelect('gl.id as glId');
        $qb->addSelect('share.branchShare as branchShare');
        $qb->addSelect('((sum(e.subTotal) * share.branchShare)/100) as percentAmount');
        $qb->addSelect('((sum(e.vat) * share.branchShare)/100) as vat');
        $qb->where('workorder.id = :workorderId')->setParameter('workorderId',$workorder->getId());
        $qb->andWhere("workorder.waitingProcess = 'Approved'");
        $qb->andWhere("requisition.requisitionMode != 'CAPEX'");
        $qb->andWhere('share.branchShare > 0');
        $qb->groupBy('gl.id','share.id');
        $result = $qb->getQuery()->getArrayResult();
        foreach ($result as $row){
           $find = $em->getRepository(RequisitionBudgetItem::class)->findOneBy(array('requisition'=>$row['requisitionId'],'company'=>$row['branchId'],'gl'=>$row['glId']));
           if($find){
               $find->setExpenseRequisition($row['percentAmount']);
               $find->setVatExpense($row['vat']);
               $em->flush();
           }
        }
        /* @var $item RequisitionBudgetItem */

        foreach ($workorder->getWorkOrderItems() as $workOrderItem) {

            if ($workOrderItem->getRequisition()->getRequisitionMode() != "CAPEX"){
                foreach ($workOrderItem->getRequisition()->getBudgetItems() as $item):
                    $glMonth = $item->getGlBudgetMonth();
                    $budget = $this->monthlyRequisitionSummary($glMonth);
                    $holdRemain = ((float)$budget['amount'] - (float)$budget['expense']);
                    $glMonth->setHoldAmount($holdRemain);
                    $glMonth->setExpense($budget['expense']);
                    $glMonth->setVatExpense($budget['vat']);
                    $em->flush();
                    $em->getRepository(BudgetMonth::class)->remainingQnt($glMonth);
                endforeach;
            }
        }
    }

    public function monthlyRequisitionExpenseProcess(Requisition $requisition){
        $em = $this->_em;
        /* @var $item RequisitionBudgetItem */
        foreach ($requisition->getBudgetItems() as $item):

            $item->setExpense($item->getRequisitionAmount());
            $em->persist($item);
            $em->flush();
            $em->getRepository(BudgetMonth::class)->remainingQnt($glMonth);

        endforeach;
        $this->monthlyRequisitionExpense($requisition);
    }

    public function monthlyRequisitionExpense(Requisition $requisition)
    {
        $em = $this->_em;
        /* @var $item RequisitionBudgetItem */
        foreach ($requisition->getBudgetItems() as $item):

            $glMonth = $item->getGlBudgetMonth();
            $budget = $this->monthlyRequisitionSummary($glMonth);
            $holdRemain = ((float)$budget['amount'] - (float) $budget['expense']);
            $glMonth->setHoldAmount($holdRemain);
            $glMonth->setExpense($budget['expense']);
            $em->flush();
            $em->getRepository(BudgetMonth::class)->remainingQnt($glMonth);

        endforeach;
    }

    private function monthlyRequisitionSummary($glMonth)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->from(RequisitionBudgetItem::class,'e');
        $qb->select('sum(e.requisitionAmount) as amount','sum(e.expenseRequisition) as expense','sum(e.vatExpense) as vat');
        $qb->where('e.glBudgetMonth = :id')->setParameter('id',$glMonth->getId());
        $result = $qb->getQuery()->getOneOrNullResult();
        return $result;
    }

    public function getUserBaseRequisition($config,$users)
    {

        $qb = $this->createQueryBuilder('e');
        $qb->select('COUNT(e.id) as count');
        $qb->addSelect('u.id as uid');
        $qb->join('e.createdBy','u');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $qb->andWhere('u.id IN(:users)')->setParameter('users',$users);
        $qb->groupBy('u.id');
        $result = $qb->getQuery()->getArrayResult();
        $arrs = array();
        foreach ($result as $row){
            $arrs[$row['uid']] = $row;
        }
        return $arrs;
    }

    public function getUserBaseInprogressRequisition($config,$users)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->select('COUNT(e.id) as count');
        $qb->addSelect('u.id as uid');
        $qb->join('e.createdBy','u');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $qb->andWhere('u.id IN(:users)')->setParameter('users',$users);
        $qb->andWhere('e.waitingProcess =:approve')->setParameter('approve','In-progress');
        $qb->groupBy('u.id');
        $result = $qb->getQuery()->getArrayResult();
        $arrs = array();
        foreach ($result as $row){
            $arrs[$row['uid']] = $row;
        }
        return $arrs;
    }



    public function getUserBaseBudgetRequisition($config,$users)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->select('COUNT(e.id) as count');
        $qb->addSelect('u.id as uid');
        $qb->join('e.createdBy','u');
        $qb->leftJoin('e.budgetRequisition', 'br');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $qb->andWhere('u.id IN(:users)')->setParameter('users',$users);
        $qb->andWhere('br.createdBy IS NOT NULL');
        $qb->groupBy('u.id');
        $result = $qb->getQuery()->getArrayResult();
        $arrs = array();
        foreach ($result as $row){
            $arrs[$row['uid']] = $row;
        }
        return $arrs;

    }

    public function pendingWorkorder($config , $users)
    {
        $qb = $this->createQueryBuilder('r');
        $qb->join('r.createdBy','u');
        $qb->select('COUNT(r.id) as count');
        $qb->addSelect('u.id as userId');
        $qb->where('r.config = :config')->setParameter('config',"{$config}");
        $qb->andWhere('r.waitingProcess = :process')->setParameter('process',"Approved");
        $qb->andWhere('r.requisitionMode IN (:mode)')->setParameter('mode', array('CAPEX','OPEX'));
        $qb->andWhere("r.process !='Closed'");
        $qb->andWhere('u.id IN(:users)')->setParameter('users',$users);
        $qb->groupBy('userId');
        $result = $qb->getQuery()->getArrayResult();
        $data = array();
        foreach ($result as $row)
        {
            $data[$row['userId']] = $row;
        }
        return $data;
    }




}
