<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Repository;

use App\Entity\Application\Procurement;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\ProcurementBundle\Entity\ProcurementCondition;
use Terminalbd\ProcurementBundle\Entity\ProcurementInvoiceConditionItem;
use Terminalbd\ProcurementBundle\Entity\TenderComparativeItem;
use Terminalbd\ProcurementBundle\Entity\TenderItem;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorder;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderItem;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderReceive;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderReceiveItem;


/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */

class TenderWorkorderReceiveRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TenderWorkorderReceive::class);
    }

    protected function handleSearchBetween($qb, $form)
    {

        if (isset($form['requisition_filter_form'])) {

            $data = $form['requisition_filter_form'];
            $startDate = isset($data['startDate']) ? $data['startDate'] : '';
            $endDate = isset($data['endDate']) ? $data['endDate'] : '';
            $department = isset($data['department']) ? $data['department'] : '';
            $process = isset($data['process']) ? $data['process'] : '';
            $invoice = !empty($data['invoice']) ? $data['invoice'] : '';
            $createdBy = !empty($data['createdBy']) ? $data['createdBy'] : '';

            if (!empty($process)) {
                $qb->andWhere('e.process = :process')->setParameter('process',$process);
            }
            if (!empty($invoice)) {
                $qb->andWhere($qb->expr()->like("e.invoice", "'%$invoice%'"));
            }
            if (!empty($createdBy)) {
                $qb->andWhere('u.id = :user')->setParameter('user', $createdBy);
            }
            if (!empty($department)) {
                $qb->andWhere('d.id = :department')->setParameter('department', $department);
            }
            if (!empty($startDate)) {
                $datetime = new \DateTime($startDate);
                $startDate = $datetime->format('Y-m-d 00:00:00');
                $qb->andWhere("e.created >= :startDate");
                $qb->setParameter('startDate', $startDate);
            }

            if (!empty($endDate)) {
                $datetime = new \DateTime($endDate);
                $endDate = $datetime->format('Y-m-d 23:59:59');
                $qb->andWhere("e.created <= :endDate");
                $qb->setParameter('endDate', $endDate);
            }

        }
    }

    /**
     * @return RequisitionIssue[]
     */
    public function findBankSearchQuery($config, User $user, $data = []): array
    {

        $sort = isset($data['sort']) ? $data['sort'] : 'e.created';
        $direction = isset($data['direction']) ? $data['direction'] : 'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id', 'e.invoice as invoice','e.total as total','e.created as created','e.challanNo as challanNo','e.receiveDate as receiveDate', 'e.process as process', 'e.waitingProcess as waitingProcess', 'e.path as path');
        $qb->addSelect('rto.id as reportTo','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->addSelect('wo.id as workorder','wo.invoice as workorderInvoice');
        $qb->addSelect('tv.id as tenderVendorId','tv.name as tenderVendor');
        $qb->addSelect('ev.id as enlistedVendorId','ev.name as enlistedVendor');
        $qb->leftJoin('e.workorder', 'wo');
        $qb->leftJoin('e.createdBy', 'u');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->leftJoin('wo.tenderVendor', 'tv');
        $qb->leftJoin('wo.enlistedVendor', 'ev');
        $qb->where('e.config = :config')->setParameter('config', $config);
        if($data['mode'] == "in-progress") {
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
        }elseif($data['mode'] == "approve"){
            $qb->andWhere('e.reportTo =:report')->setParameter('report',"{$user->getId()}");
        }elseif($data['mode'] == "list"){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }elseif($data['mode'] == "archive"){
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', ["Approved","Closed"]);
        }
        $this->handleSearchBetween($qb, $data);
        $qb->orderBy("{$sort}", $direction);
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }

    public function findSearchQuery($config, User $user, $data = []): array
    {

        $sort = isset($data['sort']) ? $data['sort'] : 'e.created';
        $direction = isset($data['direction']) ? $data['direction'] : 'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id', 'e.invoice as invoice','e.total as total','e.created as created','e.challanNo as challanNo','e.receiveDate as receiveDate', 'e.process as process', 'e.waitingProcess as waitingProcess');
        $qb->addSelect('rto.id as reportTo','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->addSelect('wo.id as workorder','wo.invoice as workorderInvoice');
        $qb->addSelect('tv.id as tenderVendorId','tv.name as tenderVendor');
        $qb->addSelect('ev.id as enlistedVendorId','ev.name as enlistedVendor');
        $qb->leftJoin('e.workorder', 'wo');
        $qb->leftJoin('e.createdBy', 'u');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->leftJoin('wo.tenderVendor', 'tv');
        $qb->leftJoin('wo.enlistedVendor', 'ev');
        $qb->where('e.config = :config')->setParameter('config', $config);
        if($data['mode'] == "in-progress") {
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
        }elseif($data['mode'] == "approve"){
            $qb->andWhere('e.reportTo =:report')->setParameter('report',"{$user->getId()}");
        }elseif($data['mode'] == "list" and !empty($user->getProfile()->getDepartment())){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }elseif($data['mode'] == "archive"){
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', ["Approved","Closed"]);
        }
        $this->handleSearchBetween($qb, $data);
        $qb->orderBy("{$sort}", $direction);
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }

    public function findPerformanceReport($config, User $user, $data = []): array
    {

        $sort = isset($data['sort']) ? $data['sort'] : 'e.created';
        $direction = isset($data['direction']) ? $data['direction'] : 'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id', 'e.invoice as invoice','e.total as total','e.created as created','e.challanNo as challanNo','e.receiveDate as receiveDate', 'e.process as process', 'e.waitingProcess as waitingProcess');
        $qb->addSelect('rto.id as reportTo','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->addSelect('wo.id as workorder','wo.invoice as workorderInvoice');
        $qb->addSelect('tv.id as tenderVendorId','tv.name as tenderVendor');
        $qb->addSelect('ev.id as enlistedVendorId','ev.name as enlistedVendor');
        $qb->leftJoin('e.workorder', 'wo');
        $qb->leftJoin('e.createdBy', 'u');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->leftJoin('wo.tenderVendor', 'tv');
        $qb->leftJoin('wo.enlistedVendor', 'ev');
        $qb->where('e.config = :config')->setParameter('config', $config);
        $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', ["Closed"]);
        $this->handleSearchBetween($qb, $data);
        $qb->orderBy("{$sort}", $direction);
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }


    public function insertWorkOrderReceiveItem(TenderWorkorderReceive $receive, TenderWorkorder $workorder)
    {
        $em = $this->_em;

        foreach ($workorder->getWorkOrderItems() as $item){

            /** @var  $item TenderWorkorderItem */

            if($item->getRemaining() > 0){

                $entity = new TenderWorkorderReceiveItem();
                $entity->setWorkorderReceive($receive);
                $entity->setTenderWorkorderItem($item);
                if(!empty($item)){
                    $entity->setStockBook($item->getStockBook());
                    $entity->setStock($item->getStock());
                }
                $em->persist($entity);
                $em->flush();
            }

        }
    }

    public function insertBankWorkOrderReceiveItem(TenderWorkorderReceive $receive, TenderWorkorder $workorder)
    {
        $em = $this->_em;

        foreach ($workorder->getWorkOrderItems() as $item){

            /** @var  $item TenderWorkorderItem */

            $entity = new TenderWorkorderReceiveItem();
            $entity->setWorkorderReceive($receive);
            $entity->setTenderWorkorderItem($item);
            if(!empty($item)){
                $entity->setStock($item->getStock());
                $entity->setPrice($item->getUnitPrice());
                $entity->setUnitPrice($item->getUnitPrice());
            }
            $em->persist($entity);
            $em->flush();
        }
    }


    public function updateOpenItem(TenderWorkorderReceive $receive)
    {
        $em = $this->_em;

        /* @var $item TenderWorkorderReceiveItem */

        $items = $this->_em->getRepository(TenderWorkorderReceiveItem::class)->findBy(array('workorderReceive'=>$receive));
        if(!empty($items)){
            foreach ($items as $item){
                $workorderItem = $item->getTenderWorkorderItem();
                $issue = $em->getRepository(TenderWorkorderReceiveItem::class)->countTenderQuantity($workorderItem);
                $remin = ($workorderItem->getQuantity() - $issue);
                $workorderItem->setReceive($issue);
                $workorderItem->setRemaining($remin);
                $em->persist($workorderItem);
                $em->flush();
            }
        }
    }
}


