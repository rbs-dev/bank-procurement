<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Repository;

use App\Entity\Admin\AppModule;
use App\Entity\Domain\ModuleProcess;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\DemoBundle\TerminalbdDemoBundle;
use Terminalbd\DmsBundle\Entity\DmsFile;
use Terminalbd\ProcurementBundle\Entity\JobRequisition;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItemHistory;
use Terminalbd\ProcurementBundle\Service\Image;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class JobRequisitionRepository extends ServiceEntityRepository
{


    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, JobRequisition::class);
    }


    protected function handleSearchBetween($qb, $form)
    {

        if (isset($form['requisition_filter_form'])) {

            $data = $form['requisition_filter_form'];
            $startDate = isset($data['startDate']) ? $data['startDate'] : '';
            $endDate = isset($data['endDate']) ? $data['endDate'] : '';
            $branch = isset($data['branch']) ? $data['branch'] : '';
            $department = isset($data['department']) ? $data['department'] : '';
            $priroty = isset($data['priority']) ? $data['priority'] : '';
            $process = isset($data['process']) ? $data['process'] : '';
            $requisitionType = isset($data['requisitionType'])? $data['requisitionType'] :'';
            $requisitionNo = !empty($data['requisitionNo'])? trim($data['requisitionNo']) :'';
            $keyword = !empty($data['keyword'])? trim($data['keyword']) :'';

            if (!empty($process) && $process == 'RUNNING') {
                $qb->andWhere('e.process IN (:processes)')->setParameter('processes', ['New', 'Checked']);
            }
            if (!empty($process) && $process == 'HISTORY') {
                $qb->andWhere('e.process IN (:processes)')->setParameter('processes', ['Approved']);
            }
            if (!empty($requisitionNo)) {
                $qb->andWhere($qb->expr()->like("e.requisitionNo", "'%$requisitionNo%'"));
            }
            if (!empty($branch)) {
                $qb->andWhere('b.id = :branch')->setParameter('branch', $branch);
            }
            if (!empty($department)) {
                $qb->andWhere('d.id = :department')->setParameter('department', $department);
            }
            if (!empty($priroty)) {
                $qb->andWhere('p.id = :priorityId')->setParameter('priorityId', $priroty);
            }
            if (!empty($startDate)) {
                $datetime = new \DateTime($startDate);
                $startDate = $datetime->format('Y-m-d 00:00:00');
                $qb->andWhere("e.created >= :startDate");
                $qb->setParameter('startDate', $startDate);
            }

            if (!empty($endDate)) {
                $datetime = new \DateTime($endDate);
                $endDate = $datetime->format('Y-m-d 23:59:59');
                $qb->andWhere("e.created <= :endDate");
                $qb->setParameter('endDate', $endDate);
            }
            if (!empty($keyword)) {
                $qb->andWhere('e.requisitionNo LIKE :searchTerm  OR b.name LIKE :searchTerm  OR t.name LIKE :searchTerm OR d.name LIKE :searchTerm');
                $qb->setParameter('searchTerm', '%'.trim($keyword).'%');
            }

        }
    }


    /**
     * @return Purchase[]
     */
    public function findBySearchQuery($config, User $user, $data = []): array
    {

        $sort = isset($data['sort']) ? $data['sort'] : 'e.created';
        $direction = isset($data['direction']) ? $data['direction'] : 'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id', 'e.requisitionNo as requisitionNo', 'e.created as created', 'e.processOrdering as ordering','e.expectedDate as expectedDate', 'e.subTotal as subTotal', 'e.total as total', 'e.process as process', 'e.waitingProcess as waitingProcess','e.requisitionMode as requisitionMode');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('d.name as department');
        $qb->addSelect('t.name as requisitionType');
        $qb->addSelect('p.name as priority','p.id as priorityId');
        $qb->addSelect('rto.id as reportToId','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->addSelect('ins.name as instructionFrom');
        $qb->leftJoin('e.companyUnit','cu');
        $qb->leftJoin('cu.parent','b');
        $qb->leftJoin('e.processDepartment', 'd');
        $qb->leftJoin('e.requisitionType', 't');
        $qb->leftJoin('e.priority', 'p');
        $qb->leftJoin('e.createdBy', 'u');
        $qb->leftJoin('e.instructionFrom', 'ins');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->where('e.config = :config')->setParameter('config', $config);
        if($data['mode'] == "in-progress") {
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }elseif($data['mode'] == "approve"){
            $qb->andWhere('e.reportTo =:report')->setParameter('report',"{$user->getId()}");
        }elseif($data['mode'] == "list"){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }elseif($data['mode'] == "archive"){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', array('Approved','Closed'));
        }elseif($data['mode'] == "assaign"){
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
            $qb->andWhere("brat.id != 'NULL'");
        }elseif($data['mode'] == "archived"){
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', array('Approved','Closed'));
        }
        $this->handleSearchBetween($qb, $data);
        $qb->orderBy("{$sort}", $direction);
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }



    public function generateStoreRequisition(Requisition $requisition, $process = "")
    {
        $em = $this->_em;

        $module = $em->getRepository(AppModule::class)->findOneBy(array('slug' => $process));
        $terminal = $requisition->getCreatedBy()->getTerminal()->getId();

        /* @var $moduleProcess ModuleProcess */
        $moduleProcess = $em->getRepository(ModuleProcess::class)->findOneBy(array('terminal' => $terminal, 'module' => $module));

        foreach ($moduleProcess->getModuleProcessItems() as $row) {
            $exist = $em->getRepository(ModuleProcess::class)->findOneBy(array('requisition' => $requisition, 'moduleProcessItem' => $row));
            if (empty($exist)) {
                $entity = new ProcurementProcess();
                $entity->setRequisition($requisition);
                $entity->setModuleProcessItem($row);
                $entity->setProcess($requisition->getProcess());
                $em->persist($entity);
                $em->flush();
            }
        }

    }

    public function requisitionItemHistory(Requisition $requisition)
    {
        $em = $this->_em;
        /* @var $item RequisitionItem */

        if (!empty($requisition->getRequisitionItems())) {

            foreach ($requisition->getRequisitionItems() as $item) {

                $entity = new RequisitionItemHistory();
                $entity->setRequisitionItem($item);
                $entity->setActualQuantity($item->getActualQuantity());
                $entity->setQuantity($item->getQuantity());
                $em->persist($entity);
                $em->flush();

            }
        }
    }

    public function insertAttachmentFile(JobRequisition $requisition, $data, $files)
    {
        $em = $this->_em;

        define('FILETYPE', array('jpg', 'png', 'jpeg', 'pdf'));
        define('DESTINATION', 'uploads/procurement/');
        define('RESIZEBY', 'w');
        define('FILESIZE', 10097152);
        define('RESIZETO', 520);
        define('QUALITY', 100);

        if (isset($files)) {
            $errors = array();
            foreach ($files['tmp_name'] as $key => $tmp_name) {
                if(!empty($tmp_name)){
                    $fileName = $requisition->getRequisitionNo() . '-' . time() . "-" . $_FILES['files']['name'][$key];
                    $fileType = $_FILES['files']['type'][$key];
                    $fileSize = $_FILES['files']['size'][$key];

                    if ($fileSize > FILESIZE) {
                        $errors[] = 'File size must be less than 2 MB';
                    }
                    if (empty($errors) == true and in_array($fileType, FILETYPE)) {
                        if (is_dir(DESTINATION) == false) {
                            mkdir(DESTINATION, 0777);        // Create directory if it does not exist
                        }
                        $image = new Image($_FILES['files']['tmp_name'][$key]);
                        $image->destination = DESTINATION . $fileName;
                        $image->constraint = RESIZEBY;
                        $image->size = RESIZETO;
                        $image->quality = QUALITY;
                        $image->render();
                        $entity = new DmsFile();
                        $entity->setSourceId($requisition->getId());
                        $entity->setBundle("Procurement");
                        $entity->setModule("Job-Requisition");
                        if($data['labelName'][$key]){
                            $entity->setName($data['labelName'][$key]);
                        }
                        $entity->setFileName($fileName);
                        $em->persist($entity);
                        $em->flush();
                    } elseif (empty($errors) == true and $fileType = "application/pdf") {
                        $file_tmp = $_FILES['files']['tmp_name'][$key];
                        if (is_dir(DESTINATION) == false) {
                            mkdir(DESTINATION, 0777);        // Create directory if it does not exist
                        }
                        if (is_dir(DESTINATION . $fileName) == false) {
                            move_uploaded_file($file_tmp, DESTINATION . $fileName);
                        }
                        $entity = new DmsFile();
                        $entity->setSourceId($requisition->getId());
                        $entity->setBundle("Procurement");
                        $entity->setModule("Job-Requisition");
                        if($data['labelName'][$key]){
                            $entity->setName($data['labelName'][$key]);
                        }
                        $entity->setFileName($fileName);
                        $em->persist($entity);
                        $em->flush();
                    } else {
                        return $errors;
                    }
                }
            }
        }
    }

    public function getDmsAttchmentFile(JobRequisition $entity)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->from(DmsFile::class,'e');
        $qb->select('e.id', 'e.created as created','e.name as name', 'e.fileName as fileName');
        $qb->where('e.sourceId = :sourceId')->setParameter('sourceId',"{$entity->getId()}");
        $qb->andWhere('e.bundle = :bundle')->setParameter('bundle',"Procurement");
        $qb->andWhere('e.module = :module')->setParameter('module',"Job-Requisition");
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }


}
