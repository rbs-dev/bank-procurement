<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\GenericBundle\Entity\ItemBrand;
use Terminalbd\GenericBundle\Entity\ItemColor;
use Terminalbd\GenericBundle\Entity\ItemSize;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\InventoryBundle\Entity\StockBook;
use Terminalbd\InventoryBundle\Entity\StockHistory;
use Terminalbd\ProcurementBundle\Entity\JobRequisition;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionIssue;
use Terminalbd\ProcurementBundle\Entity\RequisitionIssueItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class RequisitionIssueItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RequisitionIssueItem::class);
    }

    public function getItemSummary(RequisitionIssue $invoice)
    {
        $id = $invoice->getId();
        $em = $this->_em;
        $qb = $this->createQueryBuilder('e');
        $qb->select('SUM(e.subTotal) as subTotal');
        $qb->join('e.requisition','i');
        $qb->where("i.id = '{$id}'");
        $result = $qb->getQuery()->getOneOrNullResult();
        if(!empty($result) and !empty($result['subTotal'])){
            $subTotal = $result['subTotal'];
            $invoice->setSubTotal($subTotal);
            $invoice->setTotal($subTotal);
        }else{
            $invoice->setSubTotal(0);
            $invoice->setTotal(0);
        }
        $em->persist($invoice);
        $em->flush();

    }

    public function insertRequisitionItem(RequisitionIssue $requisition, $data)
    {

        $em = $this->_em;

        /* @var $stock Stock */

        $stock = $em->getRepository(Stock::class)->findOneBy(array('item' => $data['item']));
        if($stock and $data['quantity'] > 0){

            /* @var $entity RequisitionIssueItem */

            $entity = new RequisitionIssueItem();

            $entity->setRequisition($requisition);
            if($data['quantity']) {
                $entity->setQuantity($data['quantity']);
                $entity->setActualQuantity($data['quantity']);
            }
            if($data['returnQuantity']){
                $entity->setReturnQuantity($data['returnQuantity']);
            }
            if($data['mpNo']){
                $entity->setMpNo($data['mpNo']);
            }
            if($data['issueType']){
                $entity->setIssueType($data['issueType']);
            }
            if($data['description']){
                $entity->setDescription($data['description']);
            }
            if($data['issueUser']){
                $issueUser = $em->getRepository(User::class)->find($data['issueUser']);
                $entity->setIssueUser($issueUser);
            }
            if($data['brand']){
                $brand = $em->getRepository(ItemBrand::class)->find($data['brand']);
                $entity->setBrand($brand);
            }
            if($data['machineType']){
                $machineType = $em->getRepository(Particular::class)->find($data['machineType']);
                $entity->setMachineType($machineType);
            }
            if($data['size']){
                $size = $em->getRepository(ItemSize::class)->find($data['size']);
                $entity->setSize($size);
            }
            if($data['section']){
                $section = $em->getRepository(Particular::class)->find($data['section']);
                $entity->setSection($section);
            }
            if($data['line']){
                $line = $em->getRepository(Particular::class)->find($data['line']);
                $entity->setLine($line);
            }
            if($data['size']){
                $size = $em->getRepository(ItemSize::class)->find($data['size']);
                $entity->setSize($size);
            }
            if($data['color']){
                $color = $em->getRepository(ItemColor::class)->find($data['color']);
                $entity->setColor($color);
            }
            $stockBook = $em->getRepository(StockBook::class)->existStockBookItem($stock, $data);
            if ($stockBook) {
                $entity->setStockIn($stockBook->getStockIn() - $stockBook->getStockOut());
                $entity->setStockBook($stockBook);
                $entity->setPrice($stockBook->getPrice());
            }
            /*$lastPurchase = $em->getRepository(StockHistory::class)->getLastPurchaseItemInformation($stock->getId());
            if($lastPurchase){
                $entity->setLastPurchaseQuantity($lastPurchase['purchaseQuantity']);
                $entity->setLastPurchasePrice($lastPurchase['price']);
                $entity->setLastPurchaseDate($lastPurchase['updated']);
            }*/
            $entity->setStock($stock);
            $entity->setItem($stock->getItem());
            $entity->setName($stock->getItem()->getName());
            $entity->setPrice($stock->getPurchasePrice());
            $entity->setSubTotal($entity->getPrice() * $entity->getQuantity());
            $em->persist($entity);
            $em->flush();

        }

    }


    public function updateItem(RequisitionItem $item,$quantity,$stockIn)
    {
        $em = $this->_em;
        $item->setQuantity($quantity);
        $item->setActualQuantity($quantity);
        $item->setSubTotal($quantity * $item->getPrice());
        $em->persist($item);
        $em->flush();
    }

    public function updateApproveQuantity($data)
    {
        $em = $this->_em;
        /* @var $item RequisitionItem */
        foreach ($data['purchaseItem'] as $key => $value){
            $item = $this->find($value);
            $quantity = (integer)$data['quantity'][$key];
            $item->setActualQuantity($item->getQuantity());
            $item->setQuantity($quantity);
            $item->setSubTotal($quantity * $item->getPrice());
            $em->persist($item);
            $em->flush();
        }
    }


}
