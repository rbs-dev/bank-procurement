<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Repository;

use App\Entity\Application\Procurement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\InventoryBundle\Entity\Purchase;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\InventoryBundle\Entity\StockBook;
use Terminalbd\ProcurementBundle\Entity\ProcurementCondition;
use Terminalbd\ProcurementBundle\Entity\ProcurementInvoiceConditionItem;
use Terminalbd\ProcurementBundle\Entity\TenderComparativeItem;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorder;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderItem;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderReceive;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderReceiveItem;


/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */

class TenderWorkorderReceiveItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TenderWorkorderReceiveItem::class);
    }

    public function wordorderStockBookItemUpdate(StockBook $stockItem)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.workorderReceive', 'mp');
        $qb->select('SUM(e.quantity) AS quantity');
        $qb->where('e.stockBook = :stock')->setParameter('stock', $stockItem->getId());
      //  $qb->andWhere('mp.process = :process')->setParameter('process', 'Approved');
        $qnt = $qb->getQuery()->getOneOrNullResult();
        return $qnt['quantity'];
    }

    public function wordorderStockItemUpdate(Stock $stockItem)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.workorderReceive', 'mp');
        $qb->select('SUM(e.quantity) AS quantity');
        $qb->where('e.stock = :stock')->setParameter('stock', $stockItem->getId());
        $qnt = $qb->getQuery()->getOneOrNullResult();
        return $qnt['quantity'];
    }

    public function countTenderQuantity(TenderWorkorderItem $item)
    {
        $id = $item->getId();
        $qb = $this->createQueryBuilder('e');
        $qb->select('SUM(e.quantity) as quantity');
        $qb->join('e.tenderWorkorderItem','r');
        $qb->join('e.workorderReceive','t');
        $qb->where("r.id = '{$id}'");
        $qb->andWhere('t.waitingProcess =:process')->setParameter('process', "Approved");
        $result = $qb->getQuery()->getSingleScalarResult();
        return $result;
    }

    public function getReceiveItemSummary(TenderWorkorderReceive $invoice)
    {
        $id = $invoice->getId();
        $em = $this->_em;
        $qb = $this->createQueryBuilder('e');
        $qb->select('SUM(e.subTotal) as subTotal');
        $qb->join('e.workorderReceive','i');
        $qb->where("i.id = '{$id}'");
        $result = $qb->getQuery()->getOneOrNullResult();
        if(!empty($result) and !empty($result['subTotal'])){
            $subTotal = $result['subTotal'];
            $invoice->setSubTotal($subTotal);
            $percent = $invoice->getVendorCommissionPercent();
            $sTotal = ($subTotal - $invoice->getDiscount());
            if($percent>0){
                $commission = $this->getEnlistedVendorCommission($percent,$sTotal);
                $invoice->setVendorCommission($commission);
                $commission = $invoice->getVendorCommission();
                $total = $commission + $sTotal;
            }else{
                $total = $sTotal;
            }
            $invoice->setTotal($total);
        }else{
            $invoice->setSubTotal(0);
            $invoice->setVat(0);
            $invoice->setVendorCommission(0);
            $invoice->setTotal(0);
        }
        $em->persist($invoice);
        $em->flush();
        return $invoice;

    }

    public function getEnlistedVendorCommission($percent,$totalAmount)
    {
        if($percent){
            $amount = ( ($totalAmount * floatval($percent))/100);
            return round($amount);
        }
        return  false;

    }


}


