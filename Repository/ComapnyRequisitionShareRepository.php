<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Repository;

use App\Entity\Application\Procurement;
use App\Entity\Domain\Branch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\DmsBundle\Entity\DmsFile;
use Terminalbd\ProcurementBundle\Entity\BudgetRequisition;
use Terminalbd\ProcurementBundle\Entity\ComapnyRequisitionShare;
use Terminalbd\ProcurementBundle\Entity\JobRequisition;
use Terminalbd\ProcurementBundle\Entity\Requisition;


/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */

class ComapnyRequisitionShareRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ComapnyRequisitionShare::class);
    }

    public function insertJobRequisitionShare(JobRequisition $jobRequisition, $data)
    {

        $em = $this->_em;
        foreach ($data['branchId'] as $key => $value):

            $data['branchShare'][$key];
            $exist = $this->findOneBy(array('jobRequisition' => $jobRequisition, 'branch' => $value));
            if (empty($exist)) {
                $entity = new ComapnyRequisitionShare();
                $entity->setConfig($jobRequisition->getConfig());
                $entity->setJobRequisition($jobRequisition);
                $branch = $em->getRepository(Branch::class)->find($value);
                $entity->setBranch($branch);
                if ($data['branchShare'][$key] > 0) {
                    $share = floatval($data['branchShare'][$key]);
                    $amount = $this->percentageAmount($jobRequisition->getTotal(), $share);
                    $entity->setBranchShare(floatval($share));
                    $entity->setAmount($amount);
                } else {
                    $entity->setBranchShare(0);
                    $entity->setAmount(0);
                }
                $em->persist($entity);
                $em->flush();
            } else {
                $entity = $exist;
                if ($data['branchShare'][$key] > 0) {
                    $share = floatval($data['branchShare'][$key]);
                    if($share > 0 ){
                        $amount = $this->percentageAmount($jobRequisition->getTotal(), $share);
                        $entity->setBranchShare(floatval($share));
                        $entity->setAmount($amount);
                    }
                } else {
                    $entity->setBranchShare(0);
                    $entity->setAmount(0);
                }
                $em->persist($entity);
                $em->flush();
            }
        endforeach;
    }

    public function insertJobShareSingle(JobRequisition $jobRequisition)
    {
        $em = $this->_em;
        $exist = $em->getRepository(ComapnyRequisitionShare::class)->findOneBy(array('requisition'=>$jobRequisition));
        if(empty($exist)) {
            $entity = new ComapnyRequisitionShare();
            $entity->setConfig($jobRequisition->getConfig());
            $entity->setJobRequisition($jobRequisition);
            $entity->setBranch($jobRequisition->getCompanyUnit()->getParent());
            $entity->setBranchShare(floatval(100));
            $entity->setAmount($jobRequisition->getTotal());
            $em->persist($entity);
            $em->flush();
        }elseif($exist){
            $entity = $exist;
            $entity->setBranch($jobRequisition->getCompanyUnit()->getParent());
            $entity->setAmount($jobRequisition->getTotal());
            $em->persist($entity);
            $em->flush();
        }

    }

    public function insertKanbanRequisitionShare(Requisition $requisition, $store)
    {


        $em = $this->_em;
        $entity = new ComapnyRequisitionShare();
        $entity->setConfig($requisition->getConfig());
        $entity->setRequisition($requisition);
        $branch = $em->getRepository(Branch::class)->find($store);
        $parent = $branch->getParent();
        $entity->setBranch($parent);
        $entity->setBranchShare(100);
        $entity->setAmount($requisition->getTotal());
        $em->persist($entity);
        $em->flush();
    }

    public function insertRequisitionShare(Requisition $requisition, $data)
    {

        $em = $this->_em;
        foreach ($data['branchId'] as $key => $value):
            $exist = $this->findOneBy(array('requisition' => $requisition, 'branch' => $value));
            if (empty($exist)) {
                $entity = new ComapnyRequisitionShare();
                $entity->setConfig($requisition->getConfig());
                $entity->setRequisition($requisition);
                $branch = $em->getRepository(Branch::class)->find($value);
                $entity->setBranch($branch);
                if ($data['branchShare'][$key] > 0) {
                    $share = floatval($data['branchShare'][$key]);
                    $amount = $this->percentageAmount($requisition->getTotal(), $share);
                    $entity->setBranchShare($share);
                    $entity->setAmount($amount);
                } else {
                    $entity->setBranchShare(0);
                    $entity->setAmount(0);
                }
                $em->persist($entity);
                $em->flush();
            } else {
                $entity = $exist;
                if ($data['branchShare'][$key] > 0) {
                    $share = floatval($data['branchShare'][$key]);
                    $amount = $this->percentageAmount($requisition->getTotal(), $share);
                    $entity->setBranchShare($share);
                    $entity->setAmount($amount);
                } else {
                    $entity->setBranchShare(0);
                    $entity->setAmount(0);
                }
                $em->persist($entity);
                $em->flush();
            }
        endforeach;
    }

    public function insertRequisitionShareSingle(Requisition $jobRequisition)
    {
        $em = $this->_em;
        $exist = $em->getRepository(ComapnyRequisitionShare::class)->findOneBy(array('requisition'=>$jobRequisition));
        if(empty($exist)){
            $entity = new ComapnyRequisitionShare();
            $entity->setConfig($jobRequisition->getConfig());
            $entity->setRequisition($jobRequisition);
            $entity->setBranch($jobRequisition->getCompanyUnit()->getParent());
            $entity->setBranchShare(floatval(100));
            $entity->setAmount($jobRequisition->getTotal());
            $em->persist($entity);
            $em->flush();
        }elseif($exist){
            $entity = $exist;
            $entity->setBranch($jobRequisition->getCompanyUnit()->getParent());
            $entity->setAmount($jobRequisition->getTotal());
            $em->persist($entity);
            $em->flush();
        }


    }

    public function percentageAmount($amount, $share)
    {
        if(!empty($share)){
            $sahreAmount = (($amount * $share) / 100);
            return $sahreAmount;
        }
        return 0;

    }

    public function jobRequisitionShares(JobRequisition $jobRequisition)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id', 'e.branchShare as branchShare');
        $qb->addSelect('b.id as branchId','b.name as branchName');
        $qb->join('e.branch','b');
        $qb->where('e.jobRequisition = :jobRequisition')->setParameter('jobRequisition', "{$jobRequisition->getId()}");
        $qb->andWhere('e.branchShare > 0');
        $result = $qb->getQuery()->getArrayResult();
        $shares = array();
        foreach ($result as $row){
            $shares[$row['branchId']] = $row;
        }
        return $shares;
    }
    
    public function requisitionShares(Requisition $requisition)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id', 'e.branchShare as branchShare');
        $qb->addSelect('b.id as branchId','b.name as branchName');
        $qb->join('e.branch','b');
        $qb->where('e.requisition = :requisition')->setParameter('requisition', "{$requisition->getId()}");
        $qb->andWhere('e.branchShare > 0');
        $result = $qb->getQuery()->getArrayResult();
        $shares = array();
        foreach ($result as $row){
            $shares[$row['branchId']] = $row;
        }
        return $shares;
    }

    public function reverseRequisitionShares(Requisition $entity, Requisition $requisition)
    {
        $em = $this->_em;
        if($entity->getComapnyRequisitionShares()){
            foreach ($entity->getComapnyRequisitionShares() as $share){
                $entity = new ComapnyRequisitionShare();
                $entity->setRequisition($requisition);
                $entity->setBranch($share->getBranch());
                $entity->setBranchShare($share->getBranchShare());
                $entity->setAmount($share->getAmount());
                $em->persist($entity);
                $em->flush();

            }
        }
    }
    public function updateShareRequisition(Requisition $requisition,Branch $branch,$amount)
    {
        $em = $this->_em;
        $find = $this->findOneBy(array('requisition' => $requisition,'branch' => $branch));
        if($find){
            if($amount == 0 ){
                $find->setBranchShare(0);
                $find->setAmount(0);
            }else{
                $find->setBranchShare($amount);
                $shareAmount = $this->percentageAmount($requisition->getTotal(), $amount);
                $find->setAmount($shareAmount);
            }
            $em->flush();

        }elseif($amount > 0 ){
            $entity = new ComapnyRequisitionShare();
            $entity->setRequisition($requisition);
            $entity->setBranch($branch);
            $entity->setBranchShare($amount);
            $shareAmount = $this->percentageAmount($requisition->getTotal(), $amount);
            $entity->setAmount($shareAmount);
            $em->persist($entity);
            $em->flush();
        }
        exit;

    }

    public function jobApprovalToRequisitionShares(JobRequisition $jobRequisition, Requisition $requisition)
    {
        $em = $this->_em;
        if($jobRequisition->getComapnyRequisitionShares()){
            foreach ($jobRequisition->getComapnyRequisitionShares() as $share){
                $entity = new ComapnyRequisitionShare();
                $entity->setRequisition($requisition);
                $entity->setBranch($share->getBranch());
                $entity->setBranchShare($share->getBranchShare());
                $entity->setAmount($share->getAmount());
                $em->persist($entity);
                $em->flush();

            }
        }

    }




}
