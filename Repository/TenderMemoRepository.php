<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Repository;

use App\Entity\Domain\ApprovalUser;
use App\Entity\Domain\ModuleProcess;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\ProcurementBundle\Entity\ParticularType;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Tender;
use Terminalbd\ProcurementBundle\Entity\TenderMemo;


/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class TenderMemoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TenderMemo::class);
    }

    protected function handleSearchBetween($qb, $form)
    {

        if (isset($form['requisition_filter_form'])) {

            $data = $form['requisition_filter_form'];
            $startDate = isset($data['startDate']) ? $data['startDate'] : '';
            $endDate = isset($data['endDate']) ? $data['endDate'] : '';
            $department = isset($data['department']) ? $data['department'] : '';
            $process = isset($data['process']) ? $data['process'] : '';
            $invoice = !empty($data['invoice']) ? $data['invoice'] : '';
            $createdBy = !empty($data['createdBy']) ? $data['createdBy'] : '';

            if (!empty($process)) {
                $qb->andWhere('e.process = :process')->setParameter('process',$process);
            }
            if (!empty($invoice)) {
                $qb->andWhere($qb->expr()->like("e.invoice", "'%$invoice%'"));
            }
            if (!empty($createdBy)) {
                $qb->andWhere('u.id = :user')->setParameter('user', $createdBy);
            }
            if (!empty($department)) {
                $qb->andWhere('d.id = :department')->setParameter('department', $department);
            }
            if (!empty($startDate)) {
                $datetime = new \DateTime($startDate);
                $startDate = $datetime->format('Y-m-d 00:00:00');
                $qb->andWhere("e.created >= :startDate");
                $qb->setParameter('startDate', $startDate);
            }

            if (!empty($endDate)) {
                $datetime = new \DateTime($endDate);
                $endDate = $datetime->format('Y-m-d 23:59:59');
                $qb->andWhere("e.created <= :endDate");
                $qb->setParameter('endDate', $endDate);
            }

        }
    }

    public function getPurchaseCommitteeUsers($terminal)
    {
        $em = $this->_em;
        $qb = $em->createQueryBuilder();
        $qb->from(User::class,'e');
        $qb->select('e.id as id','e.name as name');
        $qb->addSelect('dsg.name as designation');
        $qb->addSelect('d.name as department');
        $qb->join('e.profile','p');
        $qb->leftJoin('p.designation','dsg');
        $qb->join('p.department','d');
        $qb->where('e.enabled =1');
        $qb->andWhere("e.terminal ='{$terminal}'");
        $qb->orderBy('e.name', 'ASC');
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

    public function findBankSearchQuery($config, User $user, $data = [])
    {

        $sort = isset($data['sort']) ? $data['sort'] : 'e.created';
        $direction = isset($data['direction']) ? $data['direction'] : 'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id', 'e.invoice as invoice','e.created as created', 'e.process as process', 'e.waitingProcess as waitingProcess');
        $qb->addSelect('rto.id as reportTo','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->addSelect('apto.id as approveTo','apto.name as approveToName');
        $qb->addSelect('cs.id as csId','cs.invoice as csInvoice');
        $qb->addSelect('management.id as managementId','management.invoice as managementInvoice');
        $qb->addSelect('tcm.id as tenderCommittee','tcm.invoice as tenderCommitteeInvoice');
        $qb->join('e.tenderComparative', 'cs');
        $qb->leftJoin('e.createdBy', 'u');
        $qb->leftJoin('e.approveTo', 'apto');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->leftJoin('e.tenderCommittee', 'tcm');
        $qb->leftJoin('e.children', 'management');
        $qb->where('e.config = :config')->setParameter('config', $config);
        $qb->andWhere("e.module = 'purchase-memo'");
        if($data['mode'] == "in-progress") {
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
        }elseif($data['mode'] == "approve"){
          //  $qb->andWhere('e.approveTo IS NULL');
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "Approve");
        }elseif($data['mode'] == "list"){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }elseif($data['mode'] == "archive"){
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', ["Approved","Closed"]);
        }
        $this->handleSearchBetween($qb, $data);
        $qb->orderBy("{$sort}", $direction);
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

    public function findBankPurchaseMemoSearchQuery($config, User $user, $data = [])
    {

        $sort = isset($data['sort']) ? $data['sort'] : 'e.created';
        $direction = isset($data['direction']) ? $data['direction'] : 'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id', 'e.invoice as invoice','e.created as created', 'e.process as process', 'e.waitingProcess as waitingProcess');
        $qb->addSelect('rto.id as reportTo','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->addSelect('apto.id as approveTo','apto.name as approveToName');
        $qb->addSelect('cs.id as csId','cs.invoice as csInvoice');
        $qb->addSelect('management.id as managementId','management.invoice as managementInvoice');
        $qb->addSelect('tcm.id as tenderCommittee','tcm.invoice as tenderCommitteeInvoice','tcm.expectedDate as expectedDate');
        $qb->join('e.tenderComparative', 'cs');
        $qb->leftJoin('e.createdBy', 'u');
        $qb->leftJoin('e.approveTo', 'apto');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->leftJoin('e.tenderCommittee', 'tcm');
        $qb->leftJoin('e.children', 'management');
        $qb->where('e.config = :config')->setParameter('config', $config);
        $qb->andWhere("e.module = 'purchase-memo'");
        if($data['mode'] == "in-progress") {
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
        }elseif($data['mode'] == "approve"){
            $qb->andWhere('e.reportTo =:report')->setParameter('report',"{$user->getId()}");
           // $qb->andWhere('e.approveTo IS NULL');
          //  $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "Approve");
        }elseif($data['mode'] == "list"){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }elseif($data['mode'] == "archive"){
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', ["Approved","Closed"]);
        }
        $this->handleSearchBetween($qb, $data);
        $qb->orderBy("{$sort}", $direction);
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

    public function findBankManagementMemoQuery($config, User $user,$tenderMode = "tender",$data = [])
    {

        $sort = isset($data['sort']) ? $data['sort'] : 'e.created';
        $direction = isset($data['direction']) ? $data['direction'] : 'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id', 'e.invoice as invoice','e.created as created', 'e.tenderAmount as tenderAmount', 'e.process as process', 'e.waitingProcess as waitingProcess');
        $qb->addSelect('rto.id as reportTo','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->addSelect('apto.id as approveTo','apto.name as approveToName');
        $qb->addSelect('pm.id as purchaseMemo','pm.invoice as purchaseMemoInvoice','pm.tenderBudget as tenderBudget');
       // $qb->addSelect('av.id as approvedVendor','av.name as approvedVendorName','av.subTotal as subTotal','av.revisedTotal as revisedTotal');
        $qb->addSelect('board.id as boardMemo','board.invoice as boardMemoInvoice');
//        $qb->addSelect('wo.id as workorder');
        $qb->addSelect('cs.id as tenderComparative','cs.invoice as tenderComparativeInvoice');
        $qb->addSelect('t.id as tender','t.processMode as processMode','t.invoice as tenderInvoice');
        $qb->leftJoin('e.createdBy', 'u');
        $qb->leftJoin('e.approveTo', 'apto');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->leftJoin('e.tenderCommittee', 'tcm');
        $qb->leftJoin('e.parent', 'pm');
        $qb->leftJoin('pm.tenderComparative', 'cs');
        $qb->leftJoin('cs.tender', 't');
      //  $qb->leftJoin('e.approvedVendor', 'av');
        $qb->leftJoin('e.children', 'board');
//        $qb->leftJoin('e.workorder', 'wo');
        $qb->where('e.config = :config')->setParameter('config', $config);
        $qb->andWhere('t.processMode = :mode')->setParameter('mode', $tenderMode);
        $qb->andWhere("e.module = 'management-memo'");
        if($data['mode'] == "in-progress") {
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
        }elseif($data['mode'] == "approve"){
            $qb->andWhere('e.reportTo =:report')->setParameter('report',"{$user->getId()}");
        }elseif($data['mode'] == "list"){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }elseif($data['mode'] == "archive"){
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', ["Approved","Closed"]);
        }
        $this->handleSearchBetween($qb, $data);
        $qb->orderBy("{$sort}", $direction);
        $result = $qb->getQuery();
        return $result;
    }

    public function findFloatingMemoQuery($config, User $user, $data = [])
    {
        $sort = isset($data['sort']) ? $data['sort'] : 'e.created';
        $direction = isset($data['direction']) ? $data['direction'] : 'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id', 'e.invoice as invoice','e.created as created', 'e.tenderAmount as tenderAmount', 'e.process as process', 'e.waitingProcess as waitingProcess');
        $qb->addSelect('rto.id as reportTo','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->addSelect('apto.id as approveTo','apto.name as approveToName');
        $qb->addSelect('t.id as tender','t.invoice as tenderInvoice','t.waitingProcess as tenderProcess','t.processMode as processMode');
        $qb->addSelect('cs.id as tenderComparative');
        $qb->addSelect('tac.id as tenderCommittee','tac.invoice as tenderCommitteeInvoice','tac.expectedDate as expectedDate');
        $qb->leftJoin('e.createdBy', 'u');
        $qb->leftJoin('e.approveTo', 'apto');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->leftJoin('e.tender', 't');
        $qb->leftJoin('t.tenderComparative', 'cs');
        $qb->leftJoin('t.tenderCommittee', 'tac');
        $qb->where('e.config = :config')->setParameter('config', $config);
        $qb->andWhere("e.module = 'floating-memo'");
        $qb->andWhere("t.processMode = 'tender'");
        if($data['mode'] == "in-progress") {
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
        }elseif($data['mode'] == "approve"){
            $qb->andWhere('e.reportTo =:report')->setParameter('report',"{$user->getId()}");
        }elseif($data['mode'] == "list"){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }elseif($data['mode'] == "archive"){
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', ["Approved","Closed"]);
        }
        $this->handleSearchBetween($qb, $data);
        $qb->orderBy("{$sort}", $direction);
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

    public function findCsVendorMemoQuery($config, User $user, $data = [])
    {
        $sort = isset($data['sort']) ? $data['sort'] : 'e.created';
        $direction = isset($data['direction']) ? $data['direction'] : 'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id', 'e.invoice as invoice','e.created as created', 'e.tenderAmount as tenderAmount', 'e.process as process', 'e.waitingProcess as waitingProcess');
        $qb->addSelect('rto.id as reportTo','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->addSelect('apto.id as approveTo','apto.name as approveToName');
        $qb->addSelect('t.id as tender','t.invoice as tenderInvoice','t.waitingProcess as tenderProcess','t.processMode as processMode');
        $qb->addSelect('cs.id as tenderComparative');
        $qb->addSelect('tac.id as tenderCommittee','tac.invoice as tenderCommitteeInvoice','tac.expectedDate as expectedDate');
        $qb->leftJoin('e.createdBy', 'u');
        $qb->leftJoin('e.approveTo', 'apto');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->leftJoin('e.tender', 't');
        $qb->leftJoin('t.tenderComparative', 'cs');
        $qb->leftJoin('t.tenderCommittee', 'tac');
        $qb->where('e.config = :config')->setParameter('config', $config);
        $qb->andWhere('u.id = :user')->setParameter('user', $user->getId());
        $qb->andWhere("e.module = 'floating-memo'");
        $qb->andWhere("t.processMode = 'tender'");
        $qb->andWhere("e.waitingProcess = 'Approved'");
        $qb->andWhere("cs.id IS NULL");
        $this->handleSearchBetween($qb, $data);
        $qb->orderBy("{$sort}", $direction);
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

    public function findBankBoardMemoQuery($config, User $user, $data = [])
    {
        $sort = isset($data['sort']) ? $data['sort'] : 'e.created';
        $direction = isset($data['direction']) ? $data['direction'] : 'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id', 'e.invoice as invoice','e.created as created', 'e.process as process', 'e.waitingProcess as waitingProcess');
        $qb->addSelect('rto.id as reportTo','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->addSelect('apto.id as approveTo','apto.name as approveToName');
        $qb->addSelect('pm.id as purchaseMemo','pm.id as parentMemo','pm.invoice as purchaseMemoInvoice');
        $qb->addSelect('wo.id as workorder');
        $qb->leftJoin('e.createdBy', 'u');
        $qb->leftJoin('e.approveTo', 'apto');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->leftJoin('e.tenderCommittee', 'tcm');
        $qb->leftJoin('e.parent', 'pm');
        $qb->leftJoin('pm.workorder', 'wo');
        $qb->where('e.config = :config')->setParameter('config', $config);
        $qb->andWhere("e.module = 'board-memo'");
        if($data['mode'] == "in-progress") {
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
        }elseif($data['mode'] == "approve"){
            $qb->andWhere('e.reportTo =:report')->setParameter('report',"{$user->getId()}");
        }elseif($data['mode'] == "list"){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }elseif($data['mode'] == "archive"){
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', ["Approved","Closed"]);
        }
        $this->handleSearchBetween($qb, $data);
        $qb->orderBy("{$sort}", $direction);
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

    public function findBankMemoLists($config)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id', 'e.invoice as invoice', 'e.subject as subject', 'e.created as created');
        $qb->join('e.tenderComparative','tc');
        $qb->join('tc.tender','t');
        $qb->where('e.config = :config')->setParameter('config', $config);
        $qb->andWhere('e.tenderCommittee IS NULL');
        $qb->andWhere("t.processMode ='tender'");
        $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', ["Approved"]);
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }

    public function boradMemoGenerate($terminal,TenderMemo $tenderMemo)
    {
        $em = $this->_em;
        $module = "board-memo";
        $user = $tenderMemo->getCreatedBy();
        $moduleProcess = $em->getRepository(ModuleProcess::class)->existModuleProcess($terminal, $module);
        if (!empty($moduleProcess) and empty($tenderMemo->getChildren())) {
            $entity = new TenderMemo();
            $entity->setConfig($tenderMemo->getConfig());
            $entity->setModuleProcess($moduleProcess);
            $entity->setParent($tenderMemo);
            $entity->setModule($module);
            $entity->setWaitingProcess("New");
            $entity->setProcess("New");
            $entity->setCreatedBy($user);
            $em->persist($entity);
            $em->flush();
            $assignUsers = $em->getRepository(ApprovalUser::class)->getApprovalAssignUser($terminal, $entity);
            $em->getRepository(ProcurementProcess::class)->insertProcurementProcessAssign($entity, $entity->getModule(), $assignUsers);
        }
    }

    public function floatingMemoGenerate($terminal,Tender $tender)
    {
        $em = $this->_em;
        $module = "floating-memo";
        $user = $tender->getCreatedBy();
        $moduleProcess = $em->getRepository(ModuleProcess::class)->existModuleProcess($terminal, $module);
        if (!empty($moduleProcess) and empty($tender->getTenderMemo())) {
            $entity = new TenderMemo();
            $entity->setConfig($tender->getConfig());
            $entity->setModuleProcess($moduleProcess);
            $entity->setSubject($tender->getSubject());
            $entity->setTender($tender);
            $entity->setModule($module);
            $entity->setWaitingProcess("New");
            $entity->setProcess("New");
            $entity->setCreatedBy($user);
            $em->persist($entity);
            $em->flush();
            $assignUsers = $em->getRepository(ApprovalUser::class)->getApprovalAssignUser($terminal, $entity);
            $em->getRepository(ProcurementProcess::class)->insertProcurementProcessAssign($entity, $entity->getModule(), $assignUsers);
        }
    }


}
