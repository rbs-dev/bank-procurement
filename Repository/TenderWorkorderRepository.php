<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Repository;

use App\Entity\Application\Procurement;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\ProcurementBundle\Entity\ProcurementCondition;
use Terminalbd\ProcurementBundle\Entity\ProcurementInvoiceConditionItem;
use Terminalbd\ProcurementBundle\Entity\Tender;
use Terminalbd\ProcurementBundle\Entity\TenderComparativeItem;
use Terminalbd\ProcurementBundle\Entity\TenderItem;
use Terminalbd\ProcurementBundle\Entity\TenderItemDetails;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorder;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderItem;


/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */

class TenderWorkorderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TenderWorkorder::class);
    }

    protected function handleSearchBetween($qb, $form)
    {

        if (isset($form['requisition_filter_form'])) {

            $data = $form['requisition_filter_form'];
            $startDate = isset($data['startDate']) ? $data['startDate'] : '';
            $endDate = isset($data['endDate']) ? $data['endDate'] : '';
            $department = isset($data['department']) ? $data['department'] : '';
            $process = isset($data['process']) ? $data['process'] : '';
            $invoice = !empty($data['invoice']) ? $data['invoice'] : '';
            $tenderVendor = !empty($data['vendor']) ? $data['vendor'] : '';
            $branch = !empty($data['unit']) ? $data['unit'] : '';
            $createdBy = !empty($data['createdBy']) ? $data['createdBy'] : '';

            if (!empty($process)) {
                $qb->andWhere('e.process = :process')->setParameter('process',$process);
            }
            if (!empty($invoice)) {
                $qb->andWhere($qb->expr()->like("e.invoice", "'%$invoice%'"));
            }
            if (!empty($createdBy)) {
                $qb->andWhere('u.id = :user')->setParameter('user', $createdBy);
            }
            if (!empty($department)) {
                $qb->andWhere('d.id = :department')->setParameter('department', $department);
            }
            if (!empty($branch)) {
                $qb->andWhere('e.branch = :branch')->setParameter('branch', $branch);
            }
            if (!empty($tenderVendor)) {
                $qb->join('tv.vendor','v');
                $qb->andWhere('v.id = :tenderVendor')->setParameter('tenderVendor', $tenderVendor);
            }
            if (!empty($startDate)) {
                $datetime = new \DateTime($startDate);
                $startDate = $datetime->format('Y-m-d 00:00:00');
                $qb->andWhere("e.created >= :startDate");
                $qb->setParameter('startDate', $startDate);
            }

            if (!empty($endDate)) {
                $datetime = new \DateTime($endDate);
                $endDate = $datetime->format('Y-m-d 23:59:59');
                $qb->andWhere("e.created <= :endDate");
                $qb->setParameter('endDate', $endDate);
            }

        }
    }

   
    public function findBankSearchQuery($config, User $user, $data = []): array
    {

        $sort = isset($data['sort']) ? $data['sort'] : 'e.created';
        $direction = isset($data['direction']) ? $data['direction'] : 'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id', 'e.invoice as invoice','e.total as total','e.recurring as recurring','e.workorderDate as workorderDate','e.validateDate as validateDate','e.created as created', 'e.process as process', 'e.waitingProcess as waitingProcess');
        $qb->addSelect('rto.id as reportTo','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->addSelect('r.id as requisition','r.requisitionNo as requisitionNo');
        $qb->addSelect('t.id as tenderComparative','t.invoice as tenderComparativeInvoice');
        $qb->addSelect('tv.id as tenderVendorId','tv.name as tenderVendor');
        $qb->addSelect('pm.id as paymentModeId','pm.name as paymentMode');
        $qb->addSelect('dm.id as deliveryModeId','dm.name as deliveryMode');
        $qb->addSelect('mm.id as managementMemo','mm.invoice as managementMemoInvoice');
        $qb->leftJoin('e.tenderComparative', 't');
        $qb->leftJoin('e.requisition', 'r');
        $qb->leftJoin('e.managementMemo', 'mm');
        $qb->leftJoin('e.createdBy', 'u');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->leftJoin('e.tenderVendor', 'tv');
        $qb->leftJoin('e.paymentMode', 'pm');
        $qb->leftJoin('e.deliveryMode', 'dm');
        $qb->where('e.config = :config')->setParameter('config', $config);
        if($data['mode'] == "in-progress") {
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
        }elseif($data['mode'] == "approve"){
            $qb->andWhere('e.reportTo =:report')->setParameter('report',"{$user->getId()}");
        }elseif($data['mode'] == "list"){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }elseif($data['mode'] == "archive"){
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', ["Approved","Closed"]);
        }
        $this->handleSearchBetween($qb, $data);
        $qb->orderBy("{$sort}", $direction);
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }

    public function findBankBranchRequisitionWorkorderQuery($config, User $user, $data = []): array
    {

        $sort = isset($data['sort']) ? $data['sort'] : 'e.created';
        $direction = isset($data['direction']) ? $data['direction'] : 'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id', 'e.invoice as invoice','e.total as total','e.recurring as recurring','e.workorderDate as workorderDate','e.validateDate as validateDate','e.created as created', 'e.process as process', 'e.waitingProcess as waitingProcess');
        $qb->addSelect('rto.id as reportTo','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->addSelect('r.id as requisition','r.requisitionNo as requisitionNo');
        $qb->addSelect('t.id as tenderComparative','t.invoice as tenderComparativeInvoice');
        $qb->addSelect('tv.id as tenderVendorId','tv.name as tenderVendor');
        $qb->addSelect('pm.id as paymentModeId','pm.name as paymentMode');
        $qb->addSelect('dm.id as deliveryModeId','dm.name as deliveryMode');
        $qb->addSelect('mm.id as managementMemo','mm.invoice as managementMemoInvoice');
        $qb->leftJoin('e.tenderComparative', 't');
        $qb->join('e.requisition', 'r');
        $qb->leftJoin('e.managementMemo', 'mm');
        $qb->leftJoin('e.createdBy', 'u');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->leftJoin('e.tenderVendor', 'tv');
        $qb->leftJoin('e.paymentMode', 'pm');
        $qb->leftJoin('e.deliveryMode', 'dm');
        $qb->where('e.config = :config')->setParameter('config', $config);
        $qb->andWhere('e.config = :config')->setParameter('config', $config);
        $qb->andWhere('r.createdBy = :createdBy')->setParameter('createdBy', $user->getId());
        $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', ["Approved","Closed"]);
        $this->handleSearchBetween($qb, $data);
        $qb->orderBy("{$sort}", $direction);
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }
    
    public function findSearchQuery($config, User $user, $data = []): array
    {

        $sort = isset($data['sort']) ? $data['sort'] : 'e.created';
        $direction = isset($data['direction']) ? $data['direction'] : 'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id', 'e.invoice as invoice','e.total as total','e.recurring as recurring','e.workorderDate as workorderDate','e.validateDate as validateDate','e.created as created', 'e.process as process', 'e.waitingProcess as waitingProcess');
        $qb->addSelect('rto.id as reportTo','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->addSelect('t.id as tenderComparative','t.invoice as tenderComparativeInvoice');
        $qb->addSelect('tv.id as tenderVendorId','tv.name as tenderVendor');
        $qb->addSelect('ev.id as enlistedVendorId','ev.name as enlistedVendor');
        $qb->addSelect('pm.id as paymentModeId','pm.name as paymentMode');
        $qb->addSelect('dm.id as deliveryModeId','dm.name as deliveryMode');
        $qb->addSelect('b.code as unit');
        $qb->leftJoin('e.tenderComparative', 't');
        $qb->leftJoin('e.createdBy', 'u');
        $qb->leftJoin('e.branch', 'b');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->leftJoin('e.tenderVendor', 'tv');
        $qb->leftJoin('e.enlistedVendor', 'ev');
        $qb->leftJoin('e.paymentMode', 'pm');
        $qb->leftJoin('e.deliveryMode', 'dm');
        $qb->where('e.config = :config')->setParameter('config', $config);
        if($data['mode'] == "in-progress") {
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
        }elseif($data['mode'] == "approve"){
            $qb->andWhere('e.reportTo =:report')->setParameter('report',"{$user->getId()}");
        }elseif($data['mode'] == "list" and !empty($user->getProfile()->getDepartment())){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }elseif($data['mode'] == "archive"){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', ["Approved","Closed"]);
        }elseif($data['mode'] == "archived"){
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', ["Approved","Closed"]);
        }
        $this->handleSearchBetween($qb, $data);
        $qb->orderBy("{$sort}", $direction);
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }

    public function goodsReceiveQuery($config, User $user, $data = []): array
    {

        $sort = isset($data['sort']) ? $data['sort'] : 'e.created';
        $direction = isset($data['direction']) ? $data['direction'] : 'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id', 'e.invoice as invoice','e.total as total','e.recurring as recurring','e.workorderDate as workorderDate','e.validateDate as validateDate','e.created as created', 'e.process as process', 'e.waitingProcess as waitingProcess');
        $qb->addSelect('rto.id as reportTo','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->addSelect('t.id as tenderComparative','t.invoice as tenderComparativeInvoice');
        $qb->addSelect('tv.id as tenderVendorId','tv.name as tenderVendor');
        $qb->addSelect('ev.id as enlistedVendorId','ev.name as enlistedVendor');
        $qb->addSelect('pm.id as paymentModeId','pm.name as paymentMode');
        $qb->addSelect('dm.id as deliveryModeId','dm.name as deliveryMode');
        $qb->leftJoin('e.tenderComparative', 't');
        $qb->leftJoin('e.createdBy', 'u');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->leftJoin('e.tenderVendor', 'tv');
        $qb->leftJoin('e.enlistedVendor', 'ev');
        $qb->leftJoin('e.paymentMode', 'pm');
        $qb->leftJoin('e.deliveryMode', 'dm');
        $qb->where('e.config = :config')->setParameter('config', $config);
        $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', ["Approved"]);
        $this->handleSearchBetween($qb, $data);
        $qb->orderBy("{$sort}", $direction);
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }


    public function insertCondition(TenderWorkorder $workorder)
    {
        $em = $this->_em;
        $config = $workorder->getConfig();
        $entities = $em->getRepository(ProcurementCondition::class)->findBy(array('config'=>$config));

        /* @var $row ProcurementCondition */
        if(!empty($entities)){
            foreach ($entities as $row){
                $exist = $em->getRepository(ProcurementInvoiceConditionItem::class)->findOneBy(array('workorder'=>$workorder,'condition'=>$row));
                if(empty($exist)){
                    $entity = new ProcurementInvoiceConditionItem();
                    $entity->setWorkorder($workorder);
                    $entity->setCondition($row);
                    $entity->setDescription($row->getDescription());
                    $entity->setStatus(1);
                    $em->persist($entity);
                    $em->flush();
                }
            }
        }

    }
    public function insertWorkOrderItem(TenderWorkorder $workorder,$data)
    {
        $em = $this->_em;
        $config = $workorder->getConfig();
        if(isset($data['statusCheck']) and !empty($data['statusCheck'])){
            foreach ($data['statusCheck'] as $key => $row){
                $exist = $em->getRepository(TenderWorkorderItem::class)->findOneBy(array('tenderWorkorder' => $workorder,'tenderComparativeItem'=>$row));
                if(empty($exist)){
                    /** @var  $item TenderComparativeItem */
                    $item = $em->getRepository(TenderComparativeItem::class)->find($row);
                    $entity = new TenderWorkorderItem();
                    $entity->setTenderWorkorder($workorder);
                    $entity->setTenderComparativeItem($item);
                    $entity->setStock($item->getStock());
                    $entity->setStockBook($item->getStockBook());
                    $entity->setGl($entity->getStockBook()->getCategory()->getGeneralLedger());
                    $entity->setRequisition($item->getTenderComparative()->getTender()->getRequisitionItem()->getRequisition());
                    $entity->setQuantity($data['quantity'][$key]);
                    //$entity->setUnitPrice($data['price'][$key]);
                    $subTotal = floatval($entity->getQuantity() * $entity->getUnitPrice());
                    $entity->setSubTotal($subTotal);
                    $entity->setStatus(1);
                    $em->persist($entity);
                    $em->flush();

                }
            }
        }
        $this->getItemSummary($workorder);

    }

    public function insertBankWorkOrderItem(TenderWorkorder $workorder,$data)
    {
        $em = $this->_em;
        $config = $workorder->getConfig();
        if(isset($data['statusCheck']) and !empty($data['statusCheck'])){
            foreach ($data['statusCheck'] as $key => $row){
                $tenderComparativeItem = $data['tenderComparativeItem'][$row];
                $exist = $em->getRepository(TenderWorkorderItem::class)->findOneBy(array('tenderWorkorder' => $workorder,'tenderComparativeItem' => $tenderComparativeItem,'tenderItemDetail' => $row));
                if(empty($exist)){
                    
                    /** @var  $comparativeItem TenderComparativeItem */
                    /** @var  $item TenderItemDetails */

                    $comparativeItem = $em->getRepository(TenderComparativeItem::class)->find($tenderComparativeItem);
                    $item = $em->getRepository(TenderItemDetails::class)->find($row);
                    $entity = new TenderWorkorderItem();
                    $entity->setTenderWorkorder($workorder);
                    $entity->setTenderComparativeItem($comparativeItem);
                    $entity->setTenderItem($item->getTenderItem());
                    $entity->setTenderItemDetail($item);
                    $entity->setRequisitionItem($item->getRequisitionItem());
                    $entity->setRequisition($item->getRequisitionItem()->getRequisition());
                    $entity->setStock($item->getStock());
                    $entity->setQuantity($item->getIssueQuantity());
                    if(empty($comparativeItem->getRevisedUnitPrice())){
                        $entity->setUnitPrice($comparativeItem->getUnitPrice());
                        $entity->setPrice($comparativeItem->getUnitPrice());
                    }else{
                        $entity->setUnitPrice($comparativeItem->getRevisedUnitPrice());
                        $entity->setPrice($comparativeItem->getRevisedUnitPrice());
                    }
                    $subTotal = floatval($entity->getQuantity() * $entity->getUnitPrice());
                    $entity->setSubTotal($subTotal);
                    $entity->setStatus(1);
                    $em->persist($entity);
                    $em->flush();
                    $item->setStatus(1);
                    $em->persist($item);
                    $em->flush();
                }

            }
        }
        $this->getItemSummary($workorder);

    }
    
    public function getItemSummary(TenderWorkorder $invoice)
    {
        $commission = 0;
        $vat = 0;
        $id = $invoice->getId();
        $em = $this->_em;
        $qb = $em->createQueryBuilder();
        $qb->from(TenderWorkorderItem::class,'e');
        $qb->select('SUM(e.subTotal) as subTotal');
        $qb->join('e.tenderWorkorder','i');
        $qb->where("i.id = '{$id}'");
        $result = $qb->getQuery()->getOneOrNullResult();
        if(!empty($result) and !empty($result['subTotal'])){
            $subTotal = $result['subTotal'];
            $invoice->setSubTotal($subTotal);
            if($invoice->getEnlistedVendor()){
                $commission = $this->getEnlistedVendorCommission($invoice,$subTotal);
                $invoice->setVendorCommission($commission);
                $invoice->setVendorCommissionPercent($invoice->getEnlistedVendor()->getCommissionPercent());
            }
            $invoice->setVat($vat);
            $total = $commission + $subTotal;
            $invoice->setTotal($total);
        }else{
            $invoice->setSubTotal(0);
            $invoice->setVat(0);
            $invoice->setVendorCommissionPercent(0);
            $invoice->setVendorCommission(0);
            $invoice->setTotal(0);
        }
        $em->persist($invoice);
        $em->flush();

    }

    public function currentAmount(TenderWorkorder $invoice)
    {
        $id = $invoice->getId();
        $em = $this->_em;
        $qb = $em->createQueryBuilder();
        $qb->from(TenderWorkorderItem::class,'e');
        $qb->select('SUM(e.remaining * e.price) as subTotal');
        $qb->join('e.tenderWorkorder','i');
        $qb->where("i.id = '{$id}'");
        $result = $qb->getQuery()->getOneOrNullResult();
        return $result['subTotal'];
    }

    

    

    public function getCalculationVat($totalAmount)
    {
        $vat = (($totalAmount * (int)10)/100 );
        //$vat = ( ($totalAmount * (int)$sales->getRestaurantConfig()->getVatPercentage())/100 );
        return round($vat);
    }

    public function getEnlistedVendorCommission(TenderWorkorder $invoice,$totalAmount)
    {
        $commission = $invoice->getEnlistedVendor()->getCommissionPercent();
        if($commission){
            $amount = ( ($totalAmount * floatval($commission))/100);
            return round($amount);
        }
        return  false;

    }

    public function inHouseWorkorder($config , $users)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->from(TenderWorkorderItem::class,'woi');
        $qb->join('woi.tenderWorkorder','wo');
        $qb->join('woi.requisition','r');
        $qb->join('r.createdBy','u');
        $qb->select('COUNT(r.id) as count');
        $qb->addSelect('u.id as userId');
        $qb->where('wo.config = :config')->setParameter('config',"{$config}");
        $qb->andWhere('wo.waitingProcess = :process')->setParameter('process',"Approved");
        $qb->andWhere('u.id IN(:users)')->setParameter('users',$users);
        $qb->groupBy('userId');
        $result = $qb->getQuery()->getArrayResult();
        $data = array();
        foreach ($result as $row)
        {
            $data[$row['userId']] = $row;
        }
        return $data;
    }

    public function searchWorkorderAutoComplete($config,$user,$q)
    {
        $query = $this->createQueryBuilder('item');
        $query->addSelect('item.id as id','item.invoice AS invoice');
        $query->where("item.config = :config")->setParameter('config',$config);
        $query->andWhere("item.createdBy = :createdBy")->setParameter('createdBy',$user);
        $query->andWhere($query->expr()->like("item.invoice", "'%$q%'"  ));
        $query->andWhere("item.waitingProcess = 'Approved'");
        $query->orderBy('item.created', 'DESC');
        $query->setMaxResults( '50' );
        $result =  $query->getQuery()->getArrayResult();
        $entities = array();
        foreach ($result as $row){
            $entities[$row['id']] = $row['invoice'];
        }
        return $entities;

    }
    public function getTenderWorkorder($requisitions)
    {
        $ids = array();
        foreach ($requisitions as $requisition){
            $ids[]= $requisition['id'];
        }
        $qb = $this->_em->createQueryBuilder();
        $qb->from(TenderWorkorderItem::class,'e');
        $qb->select('r.id','r.requisitionNo as requisitionNo','w.id as workOrderId','w.invoice as workOrderInvoice');
        $qb->join('e.requisitionItem','ri');
        $qb->join('ri.requisition','r');
        $qb->leftJoin('e.tenderWorkorder', 'w');
        $qb->andWhere('r.id IN (:ids)')->setParameter('ids',$ids);
        $qb->groupBy('r.id');
        $qb->groupBy('w.id');
        $result = $qb->getQuery()->getArrayResult();
        $tenders = array();
        foreach ($result as $tender):
            $tenders[$tender['requisitionNo']][] = $tender;
        endforeach;
        return $tenders;
    }

    public function getTenderWorkorderRequisition($requisitions)
    {
        $ids = array();
        foreach ($requisitions as $requisition){
            $ids[]= $requisition['id'];
        }
        $qb = $this->_em->createQueryBuilder();
        $qb->from(TenderWorkorderItem::class,'e');
        $qb->select('r.id as requisition','r.requisitionNo as requisitionNo','w.id as workOrder');
        $qb->join('e.requisitionItem','ri');
        $qb->join('ri.requisition','r');
        $qb->join('e.tenderWorkorder', 'w');
        $qb->andWhere('w.id IN (:ids)')->setParameter('ids',$ids);
        $qb->groupBy('r.id');
        $qb->groupBy('w.id');
        $result = $qb->getQuery()->getArrayResult();
        $tenders = array();
        foreach ($result as $tender):
            $tenders[$tender['workOrder']][] = $tender;
        endforeach;
        return $tenders;
    }




}


