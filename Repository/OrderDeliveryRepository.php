<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Repository;

use App\Entity\Admin\AppModule;
use App\Entity\Application\Procurement;
use App\Entity\Domain\ModuleProcess;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\ProcurementBundle\Entity\DeliveryBatch;
use Terminalbd\ProcurementBundle\Entity\OrderDelivery;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItemHistory;
use Terminalbd\ProcurementBundle\Entity\RequisitionOrder;
use Terminalbd\ProcurementBundle\Entity\RequisitionOrderItem;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class OrderDeliveryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrderDelivery::class);
    }

    public function updateOrderDelivery($data)
    {

        /* @var $delivery OrderDelivery */
        $em = $this->_em;
        foreach ($data['deliverId'] as $key => $value){
            $delivery = $this->find($value);
            if(!empty($delivery) and !empty($delivery->getRequisitionOrderItems()) and $delivery->getDeliveryMethod()->getSlug() == "courier") {
                $courier = $em->getRepository(Particular::class)->find($data['courier']);
                $delivery->setCourier($courier);
                $delivery->setCnNo($data['cnNo']);
                $delivery->setComments($data['comments']);
                $em->persist($delivery);
                $em->flush();
            }elseif(!empty($delivery) and !empty($delivery->getRequisitionOrderItems()) and $delivery->getDeliveryMethod()->getSlug() == "hand-delivery") {
                $delivery->setReceiverName($data['receiverName']);
                $delivery->setReceiverDesignation($data['receiverDesignation']);
                $delivery->setReceiverAddress($data['receiverAddress']);
                $em->persist($delivery);
                $em->flush();
            }


        }
    }

    protected function handleSearchBetween($qb,$form)
    {

        if(isset($form['lssd_filter_form'])){

            $data = $form['lssd_filter_form'];
            $startDate = isset($data['startDate'])? $data['startDate'] :'';
            $endDate = isset($data['endDate'])? $data['endDate'] :'';
            $branch = isset($data['branch'])? $data['branch'] :'';
            $department = isset($data['department'])? $data['department'] :'';
            $priroty = isset($data['priority'])? $data['priority'] :'';
            $process = isset($data['process'])? $data['process'] :'';
            $requisitionType = isset($data['requisitionType'])? $data['requisitionType'] :'';
            $requisitionNo = !empty($data['requisitionNo'])? $data['requisitionNo'] :'';
            $requisitionOrderNo = !empty($data['requisitionOrderNo'])? $data['requisitionOrderNo'] :'';

            if(!empty($process)){
                $qb->andWhere($qb->expr()->like("e.process", "'%$process%'"));
            }
            if(!empty($requisitionNo)){
                $qb->andWhere($qb->expr()->like("r.requisitionNo", "'%$requisitionNo%'"));
            }
            if(!empty($requisitionOrderNo)){
                $qb->andWhere($qb->expr()->like("ro.orderNo", "'%$requisitionOrderNo%'"));
            }
            if(!empty($branch)){
                $qb->where('b.id = :branch')->setParameter('branch',$branch);
            }
            if(!empty($department)){
                $qb->where('d.id = :department')->setParameter('department',$department);
            }
            if(!empty($priroty)){
                $qb->andWhere('p.id = :priority')->setParameter('priority',$priroty);
            }
            if(!empty($requisitionType)){
                $qb->andWhere('t.id = :requisitionType')->setParameter('requisitionType',$requisitionType);
            }
            if (!empty($startDate) ) {
                $datetime = new \DateTime($startDate);
                $startDate = $datetime->format('Y-m-d 00:00:00');
                $qb->andWhere("e.created >= :startDate");
                $qb->setParameter('startDate', $startDate);
            }

            if (!empty($endDate)) {
                $datetime = new \DateTime($endDate);
                $endDate = $datetime->format('Y-m-d 23:59:59');
                $qb->andWhere("e.created <= :endDate");
                $qb->setParameter('endDate', $endDate);
            }

        }
    }



    /**
     * @return Purchase[]
     */
    public function findLssdWithQuery( $config,$user,$data = "" )
    {



        $sort = isset($data['sort'])? $data['sort'] :'e.created';
        $direction = isset($data['direction'])? $data['direction'] :'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id','e.created as created','e.process as process','e.filename as filename','e.approved as approved','e.subTotal as subTotal');
        $qb->addSelect('e.cnNo','e.comment as comment','e.receiverName as receiverName','e.receiverDesignation as receiverDesignation','e.receiverAddress as receiverAddress');
        $qb->addSelect('ro.orderNo as orderNo','ro.id as requisitionOrderId');
        $qb->addSelect('r.requisitionNo as requisitionNo','r.id as requisitionId');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('c.id as courierId','c.name as courierName');
        $qb->addSelect('d.name as department');
        $qb->addSelect('dm.name as deliveryMethod');
        $qb->addSelect('t.name as requisitionType');
        $qb->join('e.requisitionOrder','ro');
        $qb->join('ro.requisition','r');
        $qb->leftJoin('e.deliveryMethod','dm');
        $qb->leftJoin('e.courier','c');
        $qb->leftJoin('r.requisitionType','t');
        $qb->leftJoin('r.branch','b');
        $qb->leftJoin('r.department','d');
        $qb->leftJoin('r.priority','p');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $this->handleSearchBetween($qb,$data);
        if(!empty($user->getRoles())  and in_array('ROLE_PROCUREMENT_STORE',$user->getRoles())){
            //$qb->andWhere('e.createdBy = :createdBy')->setParameter('createdBy',$user->getId());
        }elseif(!empty($user->getProfile())  and $user->getProfile()->getBranch()){
            $branchId = $user->getProfile()->getBranch()->getId();
            $qb->andWhere('b.id = :branchId')->setParameter('branchId',$branchId);
        }elseif(!empty($user->getProfile()) and !empty($user->getProfile()->getDepartment()) and $user->getProfile()->getDepartment()){
            $dId = $user->getProfile()->getDepartment()->getId();
            $qb->andWhere('d.id = :dId')->setParameter('dId',$dId);
        }
        $qb->orderBy("{$sort}",$direction);
        $result = $qb->getQuery();
        return $result;

    }

    /**
     * @return Purchase[]
     */
    public function findHeadLssdWithQuery( $config,$user,$data = "" )
    {

        $sort = isset($data['sort'])? $data['sort'] :'e.created';
        $direction = isset($data['direction'])? $data['direction'] :'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id','e.created as created','e.process as process','e.filename as filename','e.approved as approved');
        $qb->addSelect('e.cnNo','e.comment as comment','e.receiverName as receiverName','e.receiverDesignation as receiverDesignation','e.receiverAddress as receiverAddress');
        $qb->addSelect('ro.orderNo as orderNo','ro.id as requisitionOrderId');
        $qb->addSelect('r.requisitionNo as requisitionNo','r.id as requisitionId');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('c.id as courierId','c.name as courierName');
        $qb->addSelect('d.name as department');
        $qb->addSelect('dm.name as deliveryMethod');
        $qb->addSelect('t.name as requisitionType');
        $qb->join('e.requisitionOrder','ro');
        $qb->join('ro.requisition','r');
        $qb->leftJoin('e.deliveryMethod','dm');
        $qb->leftJoin('e.courier','c');
        $qb->leftJoin('r.requisitionType','t');
        $qb->leftJoin('r.branch','b');
        $qb->leftJoin('r.department','d');
        $qb->leftJoin('r.priority','p');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $this->handleSearchBetween($qb,$data);
        $qb->orderBy("{$sort}",$direction);
        $result = $qb->getQuery();
        return $result;

    }

    public function resetBatch($batch)
    {
        $queryBuilder = $this->createQueryBuilder('e');
        $query = $queryBuilder->update();
        $query ->set('e.deliveryBatch', ':deliveryBatch')->setParameter('deliveryBatch', NULL);
        $query->where('e.deliveryBatch = :editId')->setParameter('editId', $batch);
        $query->getQuery()->execute();
    }

    public function generateBatch(DeliveryBatch $batch)
    {
        $this->resetBatch($batch->getId());
        $config = $batch->getConfig()->getId();
        $startDate = isset($data['startDate'])? $data['startDate'] :'';
        $endDate = isset($data['endDate'])? $data['endDate'] :'';
        $qb = $this->createQueryBuilder('e');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $qb->andWhere('e.process IN (:process)')->setParameter('process',array('Approved',"Close"));
        $qb->andWhere('e.deliveryBatch IS NULL');
        $datetime = new \DateTime($startDate);
        $startDate = $batch->getStartDate()->format('Y-m-d 00:00:00');
        $qb->andWhere("e.created >= :startDate")->setParameter('startDate', $startDate);
        $endDate = $batch->getEndDate()->format('Y-m-d 23:59:59');
        $qb->andWhere("e.created <= :endDate")->setParameter('endDate', $endDate);
        $result = $qb->getQuery()->getResult();
        $em = $this->_em;

        /* @var $item OrderDelivery */

        foreach ($result as $item){
            $item->setDeliveryBatch($batch);
            $em->persist($item);
            $em->flush();
        }
        $qb = $this->createQueryBuilder('e');
        $qb->select('SUM(e.subTotal) as total');
        $qb->where('e.deliveryBatch = :config')->setParameter('config',$batch->getId());
        $total = $qb->getQuery()->getOneOrNullResult();
        $batch->setSubTotal($total['total']);
        $em->persist($batch);
        $em->flush();

    }


    public function getDeliveryOrders(DeliveryBatch $batch)
    {
        $config = $batch->getConfig()->getId();

       /* $conn = $this->getEntityManager()
            ->getConnection();
        $sql = '
            SELECT od.sub_total as subTotal,DATE_FORMAT(od.created,\'%d-%m-%Y\') as date
            FROM procu_order_delivery od
            INNER JOIN procu_requisition_order cat ON cat.id = od.requisition_order_id
            WHERE od.delivery_batch_id = :deliveryBatch 
            ';
        $stmt = $conn->prepare($sql);
        $stmt->execute(array('deliveryBatch' => $batch->getId()));
        var_dump($stmt->fetchAll());die;*/

        $qb = $this->createQueryBuilder('e');
        $qb->select('DATE_FORMAT(e.created,\'%d-%m-%Y\') as date, SUM(e.subTotal) as subTotal');
        $qb->where('e.deliveryBatch = :config')->setParameter('config',$batch->getId());
        $qb->groupBy('date');
        $dates = $qb->getQuery()->getArrayResult();
       // dd($dates);

        $qb = $this->createQueryBuilder('e');
        $qb->select('DATE_FORMAT(e.created,\'%d-%m-%Y\') as date, e.subTotal as subTotal, e.id as id');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('d.name as department');
        $qb->join('e.requisitionOrder','ro');
        $qb->join('ro.requisition','r');
        $qb->leftJoin('r.branch','b');
        $qb->leftJoin('r.department','d');
        $qb->where('e.deliveryBatch = :config')->setParameter('config',$batch->getId());
        $result = $qb->getQuery()->getArrayResult();
        $data = array();
        foreach ($result as $row):
            $data[$row['date']][] = $row;
        endforeach;
        return array('dates' => $dates,'data' => $data);
    }


    public function bankCreditNotes($config)
    {

        $qb = $this->createQueryBuilder('e');
        $qb->join('e.deliveryBatch','db');
        $qb->select('DATE_FORMAT(e.created,\'%d-%m-%Y\') as createdDate, SUM(e.subTotal) as subTotal');
        $qb->where('db.config = :config')->setParameter('config',$config);
        $qb->groupBy('createdDate');
        $dates = $qb->getQuery()->getArrayResult();

        $qb = $this->createQueryBuilder('e');
        $qb->select('DATE_FORMAT(e.created,\'%d-%m-%Y\') as createdDate, SUM(e.subTotal) as subTotal');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('d.name as department');
        $qb->join('e.requisitionOrder','ro');
        $qb->join('ro.requisition','r');
        $qb->leftJoin('r.branch','b');
        $qb->leftJoin('r.department','d');
        $qb->join('e.deliveryBatch','db');
        $qb->where('db.config = :config')->setParameter('config',$config);
        $qb->groupBy('b.id','createdDate');
        $result = $qb->getQuery()->getArrayResult();
        $data = array();
        foreach ($result as $row):
            $data[$row['createdDate']][] = $row;
        endforeach;
        return array('dates' => $dates,'data' => $data);


    }







}
