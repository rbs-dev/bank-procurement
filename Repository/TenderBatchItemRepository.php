<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Repository;

use App\Entity\Application\Procurement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\InventoryBundle\Entity\StockBook;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\Tender;
use Terminalbd\ProcurementBundle\Entity\TenderBatch;
use Terminalbd\ProcurementBundle\Entity\TenderBatchItem;
use Terminalbd\ProcurementBundle\Entity\TenderItem;
use Terminalbd\ProcurementBundle\Entity\TenderItemDetails;


/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */

class TenderBatchItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TenderBatchItem::class);
    }

    public function insertTenderBatchItem(TenderBatch $tender , $prItems,$data)
    {
        $em = $this->_em;
        foreach ($prItems as $key => $item ){
            $find = $this->findOneBy(array('requisitionItem' => $item));
            if(empty($find) and $item){
                $entity = new TenderBatchItem();
                $entity->setTenderBatch($tender);
                $entity->setRequisitionItem($item);
                $em->persist($entity);
                $em->flush();
            }
        }

        foreach ($data['quantity'] as $key => $value){
            $find = $this->findOneBy(array('requisitionItem' => $key));
            if($find){
                if($value > 0){
                    echo $value.'xx';
                    $find->setQuantity($value);
                }else{
                    echo $find->getRequisitionItem()->getQuantity();
                    $find->setQuantity($find->getRequisitionItem()->getQuantity());
                }
                $em->persist($find);
                $em->flush();
            }
            if($value > 0){
                /* @var $requisitionItem RequisitionItem */
                $requisitionItem = $find->getRequisitionItem();
                $requisitionItem->setActualQuantity($requisitionItem->getQuantity());
                $requisitionItem->setActualQuantity($value);
                $requisitionItem->setQuantity($value);
                $requisitionItem->setRemainigQuantity($value);
                $em->persist($requisitionItem);
                $em->flush();
            }
            
        }
    }

    public function insertTenderStockBookItem(Tender $tender)
    {
        $em = $this->_em;
        $qb = $em->createQueryBuilder();
        $qb->from('TerminalbdProcurementBundle:TenderItemDetails','e');
        $qb->select('SUM(e.issueQuantity) as issueQuantity','SUM(e.poQuantity) as poQuantity');
        $qb->addSelect('GROUP_CONCAT(e.id) as ids');
        $qb->addSelect('stockBook.id as stockBookId','stockBook.cmp as price');
        $qb->join('e.stockBook','stockBook');
        $qb->where('e.tender = :tender')->setParameter('tender', $tender->getId());
        $qb->groupBy('stockBook.id');
        $result = $qb->getQuery()->getArrayResult();
        foreach ($result as $item){

            $stockBook = $em->getRepository(StockBook::class)->find($item['stockBookId']);
            $find = $em->getRepository(TenderItem::class)->findOneBy(array('tender' => $tender,'stockBook'=>$stockBook));
            if(empty($find)){
                $entity = new TenderItem();
                $entity->setTender($tender);
                $entity->setStockBook($stockBook);
                $entity->setStock($stockBook->getStock());
                $entity->setPoQuantity($item['poQuantity']);
                $entity->setIssueQuantity($item['issueQuantity']);
                $entity->setPrice($stockBook->getPrice());
                $entity->setSubTotal($stockBook->getPrice() * $entity->getIssueQuantity());
                $em->persist($entity);
                $em->flush();
                $em->getRepository(TenderItemDetails::class)->updatePrItem($entity,$item['ids']);
            }
        }
    }

    public function insertTenderStockItem(Tender $tender , $data)
    {
        $em = $this->_em;
        $qb = $em->createQueryBuilder();
        $qb->from('TerminalbdProcurementBundle:TenderItemDetails','e');
        $qb->select('SUM(e.issueQuantity) as issueQuantity','SUM(e.poQuantity) as poQuantity');
        $qb->addSelect('GROUP_CONCAT(e.id) as ids');
        $qb->addSelect('stock.id as stockId','stock.price as price');
        $qb->join('e.stock','stock');
        $qb->where('e.tender = :tender')->setParameter('tender', $tender->getId());
        $qb->groupBy('stock.id');
        $result = $qb->getQuery()->getArrayResult();
        foreach ($result as $item){

            $stock = $em->getRepository(Stock::class)->find($item['stockId']);
            $find = $em->getRepository(TenderItem::class)->findOneBy(array('tender' => $tender,'stock'=>$stock));
            if(empty($find)){
                $entity = new TenderItem();
                $entity->setTender($tender);
                $entity->setStock($stock);
                $entity->setPoQuantity($item['poQuantity']);
                $entity->setIssueQuantity($item['issueQuantity']);
                $entity->setPrice($stock->getPrice());
                $entity->setSubTotal($stock->getPrice() * $entity->getIssueQuantity());
                $em->persist($entity);
                $em->flush();
                $em->getRepository(TenderItemDetails::class)->updatePrItem($entity,$item['ids']);
            }
        }
    }


    public function updateTenderItem(Tender $entity , $data)
    {
        $em = $this->_em;
        foreach ($data['itemId'] as $key => $item ){
            if($item){
                /* @var $entity TenderItem */
                $entity = $em->getRepository(TenderItem::class)->find($item);
                if($entity){
                    $entity->setIssueQuantity($data['issue'][$key]);
                    $em->persist($entity);
                    $em->flush();
                }
            }
        }
    }

    public function getTenderItemSummary(Tender $invoice)
    {
        $id = $invoice->getId();
        $em = $this->_em;

        /* @var $item TenderItem */
        foreach ($invoice->getTenderItems() as $item){
            $em->getRepository(TenderItemDetails::class)->getTenderItemSummary($item);
        }
        $qb = $this->createQueryBuilder('e');
        $qb->select('SUM(e.subTotal) as subTotal');
        $qb->where("e.tender = '{$id}'");
        $result = $qb->getQuery()->getOneOrNullResult();
        if(!empty($result) and !empty($result['subTotal'])){
            $subTotal = $result['subTotal'];
            $invoice->setSubTotal($subTotal);
        }else{
            $invoice->setSubTotal(0);
        }
        $em->persist($invoice);
        $em->flush();

    }

    public function countTenderQuantity(RequisitionItem $item)
    {
        $id = $item->getId();
        $qb = $this->createQueryBuilder('e');
        $qb->select('SUM(e.issueQuantity) as quantity');
        $qb->join('e.requisitionItem','r');
        $qb->join('e.tender','t');
        $qb->where("r.id = '{$id}'");
        $qb->andWhere('t.waitingProcess =:process')->setParameter('process', "Approved");
        $result = $qb->getQuery()->getSingleScalarResult();
        return $result;
    }

}


