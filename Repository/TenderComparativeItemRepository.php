<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Repository;

use App\Entity\Application\Procurement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\DmsBundle\Entity\DmsFile;
use Terminalbd\ProcurementBundle\Entity\JobRequisition;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\Tender;
use Terminalbd\ProcurementBundle\Entity\TenderComparative;
use Terminalbd\ProcurementBundle\Entity\TenderComparativeItem;
use Terminalbd\ProcurementBundle\Entity\TenderPreparetion;
use Terminalbd\ProcurementBundle\Entity\TenderVendor;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorder;


/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */

class TenderComparativeItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TenderComparativeItem::class);
    }

    public function getVendorItemSummary(TenderWorkorder $invoice)
    {
        $id = $invoice->getTenderVendor()->getId();
        $em = $this->_em;
        $qb = $this->createQueryBuilder('e');
        $qb->select('SUM(e.subTotal) as subTotal','SUM(e.revisedSubTotal) as revisedSubTotal');
        $qb->join('e.tenderVendor','i');
        $qb->where("i.id = '{$id}'");
        $result = $qb->getQuery()->getOneOrNullResult();
        if(!empty($result) and !empty($result['subTotal'])){
            $subTotal = $result['revisedSubTotal'];
            $invoice->setSubTotal($subTotal);
            $invoice->setTotal($subTotal);
            $commission = 0;
            if($invoice->getEnlistedVendor() and $invoice->getEnlistedVendor()->getCommissionPercent()) {
                $invoice->setVendorCommissionPercent($invoice->getEnlistedVendor()->getCommissionPercent());
                $commission = ((floatval($subTotal) * floatval($invoice->getEnlistedVendor()->getCommissionPercent())) / 100);
                $invoice->setVendorCommission($commission);
            }
            $subTotal = ($invoice->getSubTotal() - $invoice->getDiscount());
            $total = ($subTotal+$commission);
            $invoice->setTotal($total);

        }else{

            $invoice->setSubTotal(0);
            $invoice->setTotal(0);
            $invoice->setVendorCommissionPercent(0);
            $invoice->setVendorCommission(0);
            $invoice->setTotal(0);
            $invoice->setDiscount(0);
        }
        $em->persist($invoice);
        $em->flush();

    }

    public function updateSummary(TenderVendor $vendor)
    {
        $id = $vendor->getId();
        $em = $this->_em;
        $qb = $this->createQueryBuilder('e');
        $qb->select('SUM(e.subTotal) as subTotal','SUM(e.revisedSubTotal) as revisedSubTotal');
        $qb->join('e.tenderVendor','i');
        $qb->where("i.id = '{$id}'");
        $result = $qb->getQuery()->getOneOrNullResult();
        if(!empty($result) and !empty($result['subTotal'])){
            $subTotal = $result['subTotal'];
            $revisedSubTotal = $result['revisedSubTotal'];
            $vendor->setSubTotal($subTotal);
            $vendor->setRevisedTotal($revisedSubTotal);
        }else{
            $vendor->setSubTotal(0);
            $vendor->setRevisedTotal(0);
        }
        $em->persist($vendor);
        $em->flush();
    }

    public function getTenderVendorItems($vendor)
    {
        $array = array();
        $items = $this->findBy(array('tenderVendor'=>$vendor));
        foreach ($items as $item){
            $array[$item->getStockBook()->getId()] = $item;
        }
        return $array;
    }
    public function getBankTenderVendorItems($vendor)
    {
        $array = array();
        $items = $this->findBy(array('tenderVendor'=>$vendor));
        foreach ($items as $item){
            $array[$item->getStock()->getId()] = $item;
        }
        return $array;
    }

}


