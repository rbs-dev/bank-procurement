<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Repository;

use App\Entity\Application\Procurement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\ProcurementBundle\Entity\Particular;


/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */

class ParticularRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Particular::class);
    }


    public function getChildRecords($config,$parent)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.particularType','p');
        $qb->select('e.id as id','e.name as name','e.slug as slug','e.code as code');
        $qb->where('p.slug = :slug')->setParameter('slug',$parent);
        $qb->andWhere('e.config = :config')->setParameter('config',$config);
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

    public function getBranchRecords($config,$type)
    {

        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id as id','e.name as name','e.mobile as mobile','e.email as email','e.code as code','e.address as address','e.status as status');
        $qb->addSelect('c.name as company');
        $qb->addSelect('l.name as location');
        $qb->join('e.particularType','p');
        $qb->join('e.customer','c');
        $qb->join('e.location','l');
        $qb->where('p.slug = :slug')->setParameter('slug',$type);
        $qb->andWhere('e.config = :config')->setParameter('config',$config);
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

    public function getProcurementProcess(Procurement $config)
    {
        return $data = array(
            'New' => 'New',
            'In-progress' => 'In-progress',
            'Hold' => 'Hold',
            'Rejected' => 'Rejected'
        );
    }

}


