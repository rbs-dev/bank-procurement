<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Repository;

use App\Entity\Application\Procurement;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\Tender;
use Terminalbd\ProcurementBundle\Entity\TenderBatch;
use Terminalbd\ProcurementBundle\Entity\TenderCommittee;
use Terminalbd\ProcurementBundle\Entity\TenderItem;
use Terminalbd\ProcurementBundle\Entity\TenderItemDetails;
use Terminalbd\ProcurementBundle\Entity\TenderPreparetion;


/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */

class TenderCommitteeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TenderCommittee::class);
    }

    protected function handleSearchBetween($qb, $form)
    {

        if (isset($form['requisition_filter_form'])) {

            $data = $form['requisition_filter_form'];
            $startDate = isset($data['startDate']) ? $data['startDate'] : '';
            $endDate = isset($data['endDate']) ? $data['endDate'] : '';
            $department = isset($data['department']) ? $data['department'] : '';
            $process = isset($data['process']) ? $data['process'] : '';
            $invoice = !empty($data['invoice']) ? $data['invoice'] : '';
            $createdBy = !empty($data['createdBy']) ? $data['createdBy'] : '';

            if (!empty($process)) {
                $qb->andWhere('e.process = :process')->setParameter('process',$process);
            }
            if (!empty($invoice)) {
                $qb->andWhere($qb->expr()->like("e.invoice", "'%$invoice%'"));
            }
            if (!empty($createdBy)) {
                $qb->andWhere('u.id = :user')->setParameter('user', $createdBy);
            }
            if (!empty($department)) {
                $qb->andWhere('d.id = :department')->setParameter('department', $department);
            }
            if (!empty($startDate)) {
                $datetime = new \DateTime($startDate);
                $startDate = $datetime->format('Y-m-d 00:00:00');
                $qb->andWhere("e.created >= :startDate");
                $qb->setParameter('startDate', $startDate);
            }

            if (!empty($endDate)) {
                $datetime = new \DateTime($endDate);
                $endDate = $datetime->format('Y-m-d 23:59:59');
                $qb->andWhere("e.created <= :endDate");
                $qb->setParameter('endDate', $endDate);
            }

        }
    }

    public function findBankSearchQuery($config, User $user, $module = "", $data = [])
    {

        $sort = isset($data['sort']) ? $data['sort'] : 'e.created';
        $direction = isset($data['direction']) ? $data['direction'] : 'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id', 'e.invoice as invoice','e.created as created','e.expectedDate as expectedDate','e.expectedTime as expectedTime', 'e.process as process', 'e.waitingProcess as waitingProcess');
        $qb->addSelect('d.name as department');
        $qb->addSelect('rto.id as reportTo','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->addSelect('ato.name as assignTo');
        $qb->addSelect('parent.invoice as parentInvoice','parent.id as parentId');
        $qb->addSelect('apto.id as approveTo','apto.name as approveToName');
        $qb->addSelect('mm.id as mettingModaretor','mm.name as mettingModaretorName');
        $qb->leftJoin('e.department', 'd');
        $qb->leftJoin('e.createdBy', 'u');
        $qb->leftJoin('e.mettingModaretor', 'mm');
        $qb->leftJoin('e.assignTo', 'ato');
        $qb->leftJoin('e.approveTo', 'apto');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->leftJoin('e.parent', 'parent');
        $qb->where('e.config = :config')->setParameter('config', $config);
        $qb->andWhere('e.module = :module')->setParameter('module', $module);
        if($data['mode'] == "in-progress") {
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
        }elseif($data['mode'] == "approve"){
            $qb->andWhere("e.mettingModaretor = {$user->getId()}");
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "Approve");
        }elseif($data['mode'] == "list"){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }elseif($data['mode'] == "meeting"){
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', ["Meeting"]);
        }elseif($data['mode'] == "archive"){
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', ["Rejected","Approved","Closed"]);
        }
        $this->handleSearchBetween($qb, $data);
        $qb->orderBy("{$sort}", $direction);
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }


    public function findSearchQuery($config, User $user, $data = []): array
    {

        $sort = isset($data['sort']) ? $data['sort'] : 'e.created';
        $direction = isset($data['direction']) ? $data['direction'] : 'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id', 'e.invoice as invoice','e.created as created', 'e.process as process', 'e.waitingProcess as waitingProcess');
        $qb->addSelect('d.name as department');
        $qb->addSelect('rto.id as reportTo','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->addSelect('tc.id as tenderComparative','tc.invoice as tenderComparativeInvoice','tc.process as comparativeProcess');
        $qb->leftJoin('e.tenderComparative', 'tc');
        $qb->leftJoin('e.department', 'd');
        $qb->leftJoin('e.createdBy', 'u');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->where('e.config = :config')->setParameter('config', $config);
        if($data['mode'] == "in-progress") {
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
        }elseif($data['mode'] == "approve"){
            $qb->andWhere('e.reportTo =:report')->setParameter('report',"{$user->getId()}");
        }elseif($data['mode'] == "list" and !empty($user->getProfile()->getDepartment())){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }elseif($data['mode'] == "archive"){
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', ["Approved","Closed"]);
        }
        $this->handleSearchBetween($qb, $data);
        $qb->orderBy("{$sort}", $direction);
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }

    public function updateOpenItem(Tender $tender)
    {
        $em = $this->_em;
        /* @var $item TenderItem */
        $items = $em->getRepository(TenderItemDetails::class)->findBy(['tender'=>$tender]);
        foreach ($items as $item){
            $requisitionItem = $item->getRequisitionItem();
            $issue = $em->getRepository(TenderItemDetails::class)->countTenderQuantity($requisitionItem);
            $requisitionItem->getQuantity();
            $requisitionItem->setIssueQuantity($issue);
            $remin = ($requisitionItem->getQuantity() - $issue);
            $requisitionItem->setRemainigQuantity($remin);
            $em->persist($requisitionItem);
            $em->flush();
        }
    }
}


