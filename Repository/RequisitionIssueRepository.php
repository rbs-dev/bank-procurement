<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Repository;

use App\Entity\Admin\AppModule;
use App\Entity\Domain\ModuleProcess;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\DemoBundle\TerminalbdBankReconciliationBundle;
use Terminalbd\DmsBundle\Entity\DmsFile;
use Terminalbd\InventoryBundle\Entity\PurchaseItem;
use Terminalbd\ProcurementBundle\Entity\JobRequisition;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionIssue;
use Terminalbd\ProcurementBundle\Entity\RequisitionIssueItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItemHistory;
use Terminalbd\ProcurementBundle\Service\Image;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class RequisitionIssueRepository extends ServiceEntityRepository
{


    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RequisitionIssue::class);
    }


    protected function handleSearchBetween($qb, $form)
    {

        if (isset($form['requisition_filter_form'])) {

            $data = $form['requisition_filter_form'];
            $startDate = isset($data['startDate']) ? $data['startDate'] : '';
            $endDate = isset($data['endDate']) ? $data['endDate'] : '';
            $branch = isset($data['branch']) ? $data['branch'] : '';
            $department = isset($data['department']) ? $data['department'] : '';
            $priroty = isset($data['priroty']) ? $data['priroty'] : '';
            $process = isset($data['process']) ? $data['process'] : '';
            $requisitionNo = !empty($data['requisitionNo']) ? $data['requisitionNo'] : '';
            $styleNo = !empty($data['styleNo']) ? $data['styleNo'] : '';
            $buyerName = !empty($data['buyerName']) ? $data['buyerName'] : '';
            $createdBy = !empty($data['createdBy']) ? $data['createdBy'] : '';

            if (!empty($process) && $process == 'RUNNING') {
                $qb->andWhere('e.process IN (:processes)')->setParameter('processes', ['New', 'Checked']);
            }
            if (!empty($process)) {
                $qb->andWhere('e.process = :process')->setParameter('process',$process);
            }
            if (!empty($requisitionNo)) {
                $qb->andWhere($qb->expr()->like("e.requisitionNo", "'%$requisitionNo%'"));
            }
            if (!empty($requisitionNo)) {
                $qb->andWhere($qb->expr()->like("e.requisitionNo", "'%$requisitionNo%'"));
            }
             if (!empty($styleNo)) {
                $qb->andWhere($qb->expr()->like("e.styleNo", "'%$styleNo%'"));
            }
             if (!empty($buyerName)) {
                $qb->andWhere($qb->expr()->like("e.buyerName", "'%$buyerName%'"));
            }
            if (!empty($branch)) {
                $qb->andWhere('b.id = :branch')->setParameter('branch', $branch);
            }
            if (!empty($createdBy)) {
                $qb->andWhere('u.id = :user')->setParameter('user', $createdBy);
            }
            if (!empty($department)) {
                $qb->andWhere('d.id = :department')->setParameter('department', $department);
            }
            if (!empty($startDate)) {
                $datetime = new \DateTime($startDate);
                $startDate = $datetime->format('Y-m-d 00:00:00');
                $qb->andWhere("e.created >= :startDate");
                $qb->setParameter('startDate', $startDate);
            }

            if (!empty($endDate)) {
                $datetime = new \DateTime($endDate);
                $endDate = $datetime->format('Y-m-d 23:59:59');
                $qb->andWhere("e.created <= :endDate");
                $qb->setParameter('endDate', $endDate);
            }

        }
    }


    /**
     * @return RequisitionIssue[]
     */
    public function findRequisitionSearchQuery($config, User $user, $data = []): array
    {
        $report = isset($data[0]['reportTo']) ? $data[0]['reportTo']:'';
        $sort = isset($data['sort']) ? $data['sort'] : 'e.created';
        $direction = isset($data['direction']) ? $data['direction'] : 'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id', 'e.requisitionNo as requisitionNo', 'e.buyerName as buyerName','e.created as created', 'e.subTotal as subTotal', 'e.total as total', 'e.process as process','e.processOrdering as ordering', 'e.waitingProcess as waitingProcess', 'e.path as path');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('w.name as wearhouse');
        $qb->addSelect('d.name as department');
        $qb->addSelect('rto.id as reportToId','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->leftJoin('e.branch', 'b');
        $qb->leftJoin('e.wearhouse', 'w');
        $qb->leftJoin('e.department', 'd');
        $qb->leftJoin('e.createdBy', 'u');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->where('e.config = :config')->setParameter('config', $config);
        if($data['mode'] == "in-progress") {
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
        }elseif($data['mode'] == "approve"){
            $qb->andWhere('e.reportTo =:report')->setParameter('report',"{$user->getId()}");
        }elseif($data['mode'] == "list"){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }elseif($data['mode'] == "archive"){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', array('Approved','Closed'));
        }elseif($data['mode'] == "archived"){
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', array('Approved','Closed'));
        }
        $this->handleSearchBetween($qb, $data);
        $qb->orderBy("{$sort}", $direction);
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }

    /**
     * @return RequisitionIssue[]
     */
    public function findRequisitionIssuedSearchQuery($config, User $user, $data = ""): array
    {

        $sort = isset($data['sort']) ? $data['sort'] : 'e.created';
        $direction = isset($data['direction']) ? $data['direction'] : 'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id', 'e.requisitionNo as requisitionNo', 'e.buyerName as buyerName','e.created as created', 'e.subTotal as subTotal', 'e.total as total', 'e.process as process', 'e.waitingProcess as waitingProcess');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('d.name as department');
        $qb->addSelect('rto.id as reportToId','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->leftJoin('e.branch', 'b');
        $qb->leftJoin('e.department', 'd');
        $qb->leftJoin('e.createdBy', 'u');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->where('e.config = :config')->setParameter('config', $config);
        $qb->andWhere('e.waitingProcess = :process')->setParameter('process', "Approved");
        if($data['mode'] == "in-progress") {
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
        }elseif($data['mode'] == "approve"){
            $qb->andWhere('e.reportTo =:report')->setParameter('report',"{$user->getId()}");
        }elseif($data['mode'] == "list" and !empty($user->getProfile()->getDepartment())){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
            $qb->andWhere('d.id = :departmentId')->setParameter('departmentId', $user->getProfile()->getDepartment()->getId());
        }elseif($data['mode'] == "archive"){
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', ["Approved","Closed"]);
        }
        $this->handleSearchBetween($qb, $data);
        $qb->orderBy("{$sort}", $direction);
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }

    public function requisitionItemHistory(RequisitionIssue $requisition)
    {
        $em = $this->_em;
        /* @var $item RequisitionIssueItem */

        if (!empty($requisition->getRequisitionItems())) {

            foreach ($requisition->getRequisitionItems() as $item) {

                $entity = new RequisitionItemHistory();
                $entity->setRequisitionIssueItem($item);
                $entity->setActualQuantity($item->getActualQuantity());
                $entity->setQuantity($item->getQuantity());
                $em->persist($entity);
                $em->flush();

            }
        }
    }

    public function closeRequisitionIssue(RequisitionIssue $issue)
    {
        $issue->setProcess('Closed');
        $issue->setWaitingProcess('Closed');
        $this->_em->persist($issue);
        $this->_em->flush();
    }

}
