<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Repository;

use App\Entity\Application\Procurement;
use App\Entity\Domain\ApprovalUser;
use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\Vendor;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\DmsBundle\Entity\DmsFile;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\InventoryBundle\Entity\StockBook;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Tender;
use Terminalbd\ProcurementBundle\Entity\TenderComparative;
use Terminalbd\ProcurementBundle\Entity\TenderComparativeItem;
use Terminalbd\ProcurementBundle\Entity\TenderComparativeItemAttribute;
use Terminalbd\ProcurementBundle\Entity\TenderItem;
use Terminalbd\ProcurementBundle\Entity\TenderItemDetails;
use Terminalbd\ProcurementBundle\Entity\TenderMemo;
use Terminalbd\ProcurementBundle\Entity\TenderPreparetion;
use Terminalbd\ProcurementBundle\Entity\TenderVendor;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorder;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderItem;


/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */

class TenderComparativeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TenderComparative::class);
    }

    protected function handleSearchBetween($qb, $form)
    {

        if (isset($form['requisition_filter_form'])) {

            $data = $form['requisition_filter_form'];
            $startDate = isset($data['startDate']) ? $data['startDate'] : '';
            $endDate = isset($data['endDate']) ? $data['endDate'] : '';
            $department = isset($data['department']) ? $data['department'] : '';
            $process = isset($data['process']) ? $data['process'] : '';
            $invoice = !empty($data['invoice']) ? $data['invoice'] : '';
            $createdBy = !empty($data['createdBy']) ? $data['createdBy'] : '';

            if (!empty($process)) {
                $qb->andWhere('e.process = :process')->setParameter('process',$process);
            }
            if (!empty($invoice)) {
                $qb->andWhere($qb->expr()->like("e.invoice", "'%$invoice%'"));
            }
            if (!empty($createdBy)) {
                $qb->andWhere('u.id = :user')->setParameter('user', $createdBy);
            }
            if (!empty($department)) {
                $qb->andWhere('d.id = :department')->setParameter('department', $department);
            }
            if (!empty($startDate)) {
                $datetime = new \DateTime($startDate);
                $startDate = $datetime->format('Y-m-d 00:00:00');
                $qb->andWhere("e.created >= :startDate");
                $qb->setParameter('startDate', $startDate);
            }

            if (!empty($endDate)) {
                $datetime = new \DateTime($endDate);
                $endDate = $datetime->format('Y-m-d 23:59:59');
                $qb->andWhere("e.created <= :endDate");
                $qb->setParameter('endDate', $endDate);
            }

        }
    }

    public function findBankSearchQuery($config, User $user, $data = [])
    {

        $sort = isset($data['sort']) ? $data['sort'] : 'e.created';
        $direction = isset($data['direction']) ? $data['direction'] : 'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id', 'e.invoice as invoice','e.created as created', 'e.process as process', 'e.waitingProcess as waitingProcess');
        $qb->addSelect('rto.id as reportTo','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->addSelect('t.id as tender','t.invoice as tenderInvoice');
        $qb->addSelect('tm.id as tenderMemo','tm.invoice as tenderMemoNo');
        $qb->addSelect('rd.id as recomendationDepartment','rd.name as recomendationDepartmentName');
        $qb->leftJoin('e.tender', 't');
        $qb->leftJoin('e.createdBy', 'u');
        $qb->leftJoin('e.recomendationDepartment', 'rd');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->leftJoin('e.tenderMemo', 'tm');
        $qb->where('e.config = :config')->setParameter('config', $config);
        $qb->andWhere('t.processMode = :processMode')->setParameter('processMode', 'tender');
        if($data['mode'] == "in-progress") {
            $qb->andWhere('tm.id IS NULL');
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', ["Approved","In-progress"]);
        }elseif($data['mode'] == "approve"){
            $qb->andWhere('e.reportTo =:report')->setParameter('report',"{$user->getId()}");
        }elseif($data['mode'] == "list"){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }elseif($data['mode'] == "archive"){
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', ["Closed","Approved"]);
        }
        $this->handleSearchBetween($qb, $data);
        $qb->orderBy("{$sort}", $direction);
        $result = $qb->getQuery();
        return $result;

    }

    public function bankCsAcknowledge($config , $user , $data = "")
    {
        $department = $user->getProfile()->getDepartment();
        $sort = isset($data['sort']) ? $data['sort'] : 'e.created';
        $direction = isset($data['direction']) ? $data['direction'] : 'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id', 'e.invoice as invoice','e.created as created', 'e.process as process', 'e.waitingProcess as waitingProcess', 'e.recomendationContent as recomendationContent');
        $qb->addSelect('rto.id as reportTo','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->addSelect('t.id as tender','t.invoice as tenderInvoice');
        $qb->addSelect('tm.id as tenderMemo','tm.invoice as tenderMemoNo');
        $qb->addSelect('ap.id as approvedId','ap.name as approvedBy');
        $qb->addSelect('ra.id as recomendationAssignUserId','ra.name as recomendationAssignUser');
        $qb->addSelect('rd.id as recomendationDepartment','rd.name as recomendationDepartmentName');
        $qb->leftJoin('e.tender', 't');
        $qb->leftJoin('e.createdBy', 'u');
        $qb->leftJoin('e.recomendationDepartment', 'rd');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->leftJoin('e.tenderMemo', 'tm');
        $qb->leftJoin('e.approvedBy', 'ap');
        $qb->leftJoin('e.recomendationAssignUser', 'ra');
        $qb->where('e.config = :config')->setParameter('config', $config);
        $qb->andWhere('t.processMode = :processMode')->setParameter('processMode', 'tender');
        $qb->andWhere('rd.id = :department')->setParameter('department', $department);
        $this->handleSearchBetween($qb, $data);
        $qb->orderBy("{$sort}", $direction);
        $result = $qb->getQuery();
        return $result;

    }

    public function bankCsAcknowledgeAssesment($config , $user , $data = "")
    {
        $department = $user->getProfile()->getDepartment();
        $sort = isset($data['sort']) ? $data['sort'] : 'e.created';
        $direction = isset($data['direction']) ? $data['direction'] : 'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id', 'e.invoice as invoice','e.created as created', 'e.process as process', 'e.waitingProcess as waitingProcess', 'e.recomendationContent as recomendationContent');
        $qb->addSelect('rto.id as reportTo','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->addSelect('t.id as tender','t.invoice as tenderInvoice');
        $qb->addSelect('tm.id as tenderMemo','tm.invoice as tenderMemoNo');
        $qb->addSelect('ap.id as approvedId','ap.name as approvedBy');
        $qb->addSelect('ra.id as recomendationAssignUserId','ra.name as recomendationAssignUser');
        $qb->addSelect('rd.id as recomendationDepartment','rd.name as recomendationDepartmentName');
        $qb->leftJoin('e.tender', 't');
        $qb->leftJoin('e.createdBy', 'u');
        $qb->leftJoin('e.recomendationDepartment', 'rd');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->leftJoin('e.tenderMemo', 'tm');
        $qb->leftJoin('e.approvedBy', 'ap');
        $qb->leftJoin('e.recomendationAssignUser', 'ra');
        $qb->where('e.config = :config')->setParameter('config', $config);
        $qb->andWhere('t.processMode = :processMode')->setParameter('processMode', 'tender');
        $qb->andWhere('rd.id = :department')->setParameter('department', $department);
        $qb->andWhere('e.recomendationAssignUser = :recomendationAssignUser')->setParameter('recomendationAssignUser', $user->getId());
        $this->handleSearchBetween($qb, $data);
        $qb->orderBy("{$sort}", $direction);
        $result = $qb->getQuery();
        return $result;

    }
    
    public function findSearchQuery($config, User $user, $data = [])
    {

        $sort = isset($data['sort']) ? $data['sort'] : 'e.created';
        $direction = isset($data['direction']) ? $data['direction'] : 'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id', 'e.invoice as invoice','e.created as created', 'e.process as process', 'e.waitingProcess as waitingProcess');
        $qb->addSelect('rto.id as reportTo','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->addSelect('t.id as tender','t.invoice as tenderInvoice');
        $qb->leftJoin('e.tender', 't');
        $qb->leftJoin('e.createdBy', 'u');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->where('e.config = :config')->setParameter('config', $config);
        if($data['mode'] == "in-progress") {
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
        }elseif($data['mode'] == "approve"){
            $qb->andWhere('e.reportTo =:report')->setParameter('report',"{$user->getId()}");
        }elseif($data['mode'] == "list" and !empty($user->getProfile()->getDepartment())){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }elseif($data['mode'] == "archive"){
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', ["Approved","Closed"]);
        }
        $this->handleSearchBetween($qb, $data);
        $qb->orderBy("{$sort}", $direction);
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }

    public function csItemGenerate(Tender $tender, TenderComparative $comparative)
    {
        $em = $this->_em;

        /* @var $item TenderItem  */


        foreach ($tender->getTenderItems() as $item){
            foreach ($tender->getTenderVendors() as $tenderVendor)
            {
                if($tenderVendor->isStatus() == 1){

                    $find = $em->getRepository(TenderComparativeItem::class)->findOneBy(array('tenderComparative' => $comparative, 'tenderVendor' => $tenderVendor, 'tenderItem' => $item));
                    if(empty($find)){
                        $comparativeItem = new TenderComparativeItem();
                        $comparativeItem->setTenderComparative($comparative);
                        $comparativeItem->setTenderVendor($tenderVendor);
                        $comparativeItem->setTenderItem($item);
                        $comparativeItem->setStockBook($item->getStockBook());
                        $comparativeItem->setStock($item->getStock());
                        $comparativeItem->setQuantity($item->getIssueQuantity());
                        $em->persist($comparativeItem);
                        $em->flush();
                    }
                }

            }
        }
        $this->csAttributeGenerate($tender,$comparative);

    }

    public function csAttributeGenerate(Tender $tender, TenderComparative $comparative)
    {
        $em = $this->_em;
        /* @var $item TenderItem */
        foreach ($tender->getTenderItems() as $item){
            if(!empty($item->getStockBook())){
                if($item->getStockBook()->getCategory() and !empty($item->getStockBook()->getCategory()->getCategoryMetas())){
                    $metas =  $item->getStockBook()->getCategory()->getCategoryMetas();
                    foreach ($metas as $meta ){
                        foreach ($tender->getTenderVendors() as $tenderVendor)
                        {
                            $find = $em->getRepository(TenderComparativeItemAttribute::class)->findOneBy(array('tenderComparative' => $comparative , 'tenderVendor' => $tenderVendor, 'tenderItem' => $item,'categoryMeta'=> $meta));
                            if(empty($find)) {
                                echo $meta->getId();
                                $comparativeItem = new TenderComparativeItemAttribute();
                                $comparativeItem->setTenderComparative($comparative);
                                $comparativeItem->setTenderVendor($tenderVendor);
                                $comparativeItem->setTenderItem($item);
                                $comparativeItem->setCategoryMeta($meta);
                                $comparativeItem->setMetaKey($meta->getMetaKey());
                                $em->persist($comparativeItem);
                                $em->flush();

                            }
                        }
                    }
                }

            }
        }
    }

    public function csStockItemGenerate(Tender $tender, TenderComparative $comparative)
    {
        $em = $this->_em;

        /* @var $item TenderItem  */

        foreach ($tender->getTenderItems() as $item){
            foreach ($tender->getTenderVendors() as $tenderVendor)
            {
                if($tenderVendor->isStatus() == 1){
                    $find = $em->getRepository(TenderComparativeItem::class)->findOneBy(array('tenderComparative' => $comparative, 'tenderVendor' => $tenderVendor, 'tenderItem' => $item));
                    if(empty($find)){
                        $comparativeItem = new TenderComparativeItem();
                        $comparativeItem->setTenderComparative($comparative);
                        $comparativeItem->setTenderVendor($tenderVendor);
                        $comparativeItem->setTenderItem($item);
                        $comparativeItem->setStockBook($item->getStockBook());
                        $comparativeItem->setStock($item->getStock());
                        $comparativeItem->setQuantity($item->getIssueQuantity());
                        $em->persist($comparativeItem);
                        $em->flush();
                    }
                }
            }
        }
        $this->csStockAttributeGenerate($tender,$comparative);
    }

    public function csStockAttributeGenerate(Tender $tender, TenderComparative $comparative)
    {
        $em = $this->_em;
        /* @var $item TenderItem */
        foreach ($tender->getTenderItems() as $item){
            if(!empty($item->getStock())){
                if($item->getStock()->getItem()->getCategory() and !empty($item->getStock()->getItem()->getCategory()->getCategoryMetas())){
                    $metas =  $item->getStock()->getItem()->getCategory()->getCategoryMetas();
                    foreach ($metas as $meta ){
                        foreach ($tender->getTenderVendors() as $tenderVendor)
                        {
                            $find = $em->getRepository(TenderComparativeItemAttribute::class)->findOneBy(array('tenderComparative' => $comparative , 'tenderVendor' => $tenderVendor, 'tenderItem' => $item,'categoryMeta'=> $meta));
                            if(empty($find)) {
                                $comparativeItem = new TenderComparativeItemAttribute();
                                $comparativeItem->setTenderComparative($comparative);
                                $comparativeItem->setTenderVendor($tenderVendor);
                                $comparativeItem->setTenderItem($item);
                                $comparativeItem->setCategoryMeta($meta);
                                $comparativeItem->setMetaKey($meta->getMetaKey());
                                $em->persist($comparativeItem);
                                $em->flush();

                            }
                        }
                    }
                }

            }
        }
    }


    public function csGroupStockBookItemGenerate(Tender $tender, TenderComparative $comparative)
    {
        $em = $this->_em;
        $qb = $em->createQueryBuilder();
        $qb->from('TerminalbdProcurementBundle:TenderItem','e');
        $qb->select('SUM(e.issueQuantity) as quantity');
        $qb->addSelect('stockBook.id as id','stockBook.cmp as price');
        $qb->join('e.stockBook','stockBook');
        $qb->where('e.tender = :tender')->setParameter('tender', $tender->getId());
        $qb->groupBy('stockBook.id');
        $result = $qb->getQuery()->getArrayResult();
        foreach ($result as $item){
            foreach ($tender->getTenderVendors() as $tenderVendor)
            {
                if($tenderVendor->isStatus() == 1){
                    $stockBook = $em->getRepository(StockBook::class)->find($item['id']);
                    $find = $em->getRepository(TenderComparativeItem::class)->findOneBy(array('tenderComparative' => $comparative,'tenderVendor'=>$tenderVendor,'stockBook'=>$stockBook));
                    if(empty($find)){
                        $comparativeItem = new TenderComparativeItem();
                        $comparativeItem->setTenderComparative($comparative);
                        $comparativeItem->setTenderVendor($tenderVendor);
                        $comparativeItem->setStockBook($stockBook);
                        $comparativeItem->setStock($stockBook->getStock());
                        $comparativeItem->setQuantity($item['quantity']);
                        $em->persist($comparativeItem);
                        $em->flush();
                    }
                }

            }
        }

        $comparativeItems = $em->getRepository(TenderComparativeItem::class)->findBy(array('tenderComparative'=>$comparative));

        foreach ($comparativeItems as $item){
            if($item->getStockBook() and $item->getStockBook()->getCategory() and !empty($item->getStockBook()->getCategory()->getCategoryMetas())){
                $metas =  $item->getStockBook()->getCategory()->getCategoryMetas();
                foreach ($metas as $meta ){
                    $find = $em->getRepository(TenderComparativeItemAttribute::class)->findOneBy(array('tenderComparative'=> $comparative,'tenderVendor'=> $item->getTenderVendor(),'tenderComparativeItem'=> $item,'stockBook'=> $item->getStockBook(),'categoryMeta'=> $meta));
                    if(empty($find)){
                        $comparativeItem = new TenderComparativeItemAttribute();
                        $comparativeItem->setTenderComparative($comparative);
                        $comparativeItem->setTenderVendor($item->getTenderVendor());
                        $comparativeItem->setTenderComparativeItem($item);
                        $comparativeItem->setStockBook($item->getStockBook());
                        $comparativeItem->setStock($item->getStock());
                        $comparativeItem->setCategoryMeta($meta);
                        $comparativeItem->setMetaKey($meta->getMetaKey());
                        $em->persist($comparativeItem);
                        $em->flush();
                    }
                }
            }
        }
        return $result;

    }

    public function csGroupStockItemGenerate(Tender $tender, TenderComparative $comparative)
    {
        $em = $this->_em;
        $qb = $em->createQueryBuilder();
        $qb->from('TerminalbdProcurementBundle:TenderItem','e');
        $qb->select('SUM(e.issueQuantity) as quantity');
        $qb->addSelect('stock.id as stockId','stock.price as price');
        $qb->join('e.stock','stock');
        $qb->where('e.tender = :tender')->setParameter('tender', $tender->getId());
        $qb->groupBy('stock.id');
        $result = $qb->getQuery()->getArrayResult();
        foreach ($result as $item){
            foreach ($tender->getTenderVendors() as $tenderVendor)
            {
                if($tenderVendor->isStatus() == 1){

                    $stock = $em->getRepository(Stock::class)->find($item['stockId']);
                    $comparativeItem = new TenderComparativeItem();
                    $comparativeItem->setTenderComparative($comparative);
                    $comparativeItem->setTenderVendor($tenderVendor);
                    $comparativeItem->setStock($stock);
                    $comparativeItem->setQuantity($item['quantity']);
                    $em->persist($comparativeItem);
                    $em->flush();

                }

            }
        }

        $comparativeItems = $em->getRepository(TenderComparativeItem::class)->findBy(array('tenderComparative'=>$comparative));

        foreach ($comparativeItems as $item){

            if($item->getStock()->getCategory() and !empty($item->getStock()->getCategory()->getCategoryMetas())){
                $metas =  $item->getStock()->getCategory()->getCategoryMetas();
                foreach ($metas as $meta ){
                    $comparativeItem = new TenderComparativeItemAttribute();
                    $comparativeItem->setTenderComparative($comparative);
                    $comparativeItem->setTenderVendor($item->getTenderVendor());
                    $comparativeItem->setTenderComparativeItem($item);
                    $comparativeItem->setStock($item->getStock());
                    $comparativeItem->setCategoryMeta($meta);
                    $comparativeItem->setMetaKey($meta->getMetaKey());
                    $em->persist($comparativeItem);
                    $em->flush();
                }
            }

        }

    }

    public function csItemAttributeGenerate(TenderComparative $comparative)
    {
        $em = $this->_em;
        /* @var $item TenderItem */
        foreach ($tender->getTenderItems() as $item){
            if(!empty($item->getRequisitionItem()->getItem())){
                if($item->getRequisitionItem()->getItem()->getCategory() and !empty($item->getRequisitionItem()->getItem()->getCategory()->getCategoryMetas())){
                    $metas =  $item->getRequisitionItem()->getItem()->getCategory()->getCategoryMetas();
                    foreach ($metas as $meta ){
                        foreach ($tender->getTenderVendors() as $tenderVendor)
                        {
                            $comparativeItem = new TenderComparativeItemAttribute();
                            $comparativeItem->setTenderComparative($comparative);
                            $comparativeItem->setTenderVendor($tenderVendor);
                            $comparativeItem->setTenderItem($item);
                            $comparativeItem->setCategoryMeta($meta);
                            $comparativeItem->setMetaKey($meta->getMetaKey());
                            $em->persist($comparativeItem);
                            $em->flush();
                        }
                    }
                }

            }
        }

    }


    public function comparativeItems($entity)
    {
        $em = $this->_em;
        $qb = $em->createQueryBuilder();
        $qb->from('TerminalbdProcurementBundle:TenderComparativeItem','e');
        $qb->join('e.tenderItem','ti');
        $qb->join('e.tenderVendor','tv');
        $qb->select('e.id as comparativeId','e.quantity','e.unitPrice','e.subTotal','e.revisedUnitPrice','e.revisedSubTotal');
        $qb->addSelect('ti.id as itemId');
        $qb->addSelect('tv.id as vendorId');
        $qb->where('e.tenderComparative = :tenderComparative')->setParameter('tenderComparative',$entity->getId());
        $result = $qb->getQuery()->getArrayResult();
        $data = array();
        foreach ($result as $row){
            $unique = "{$row['itemId']}-{$row['vendorId']}";
            $data[$unique] = $row;
        }
        return $data;

    }

    public function comparativeStockBookItems($entity)
    {
        $em = $this->_em;
        $qb = $em->createQueryBuilder();
        $qb->from('TerminalbdProcurementBundle:TenderComparativeItem','e');
        $qb->join('e.stockBook','ti');
        $qb->join('e.tenderVendor','tv');
        $qb->select('e.id as comparativeId','e.quantity','e.unitPrice','e.subTotal');
        $qb->addSelect('ti.id as itemId');
        $qb->addSelect('tv.id as vendorId');
        $qb->where('e.tenderComparative = :tenderComparative')->setParameter('tenderComparative',$entity->getId());
        $result = $qb->getQuery()->getArrayResult();
        $data = array();
        foreach ($result as $row){
            $unique = "{$row['itemId']}-{$row['vendorId']}";
            $data[$unique] = $row;
        }
        return $data;

    }

    public function comparativeItemAttribites($entity)
    {
        $em = $this->_em;
        $qb = $em->createQueryBuilder();
        $qb->from('TerminalbdProcurementBundle:TenderComparativeItemAttribute','e');
        $qb->join('e.tenderItem','ti');
        $qb->join('e.tenderVendor','tv');
        $qb->join('e.categoryMeta','cm');
        $qb->select('e.id as attributeId','e.metaKey','e.metaValue');
        $qb->addSelect('ti.id as itemId');
        $qb->addSelect('tv.id as vendorId');
        $qb->addSelect('cm.id as metaId');
        $qb->where('e.tenderComparative = :tenderComparative')->setParameter('tenderComparative',$entity->getId());
        $result = $qb->getQuery()->getArrayResult();
        $data = array();
        foreach ($result as $row){
            $unique = "{$row['itemId']}-{$row['vendorId']}-{$row['metaId']}";
            $data[$unique] = $row;
        }
        return $data;
    }

    public function comparativeStockBookItemAttribites($entity)
    {


        $em = $this->_em;
        $qb = $em->createQueryBuilder();
        $qb->from('TerminalbdProcurementBundle:TenderComparativeItemAttribute','e');
        $qb->select('cm.id as id','cm.metaKey as metaKey','cm.metaValue as metaValue');
        $qb->addSelect('stockBook.id as stockBookId');
        $qb->join('e.stockBook','stockBook');
        $qb->join('e.categoryMeta','cm');
        $qb->where('e.tenderComparative = :tenderComparative')->setParameter('tenderComparative',$entity->getId());
        $qb->groupBy('cm.id');
        $metaGroup = $qb->getQuery()->getArrayResult();
        $metaIds = array();
        foreach ($metaGroup as $row){
            $metaIds[$row['stockBookId']][] = $row;
        }
        $qb = $em->createQueryBuilder();
        $qb->from('TerminalbdProcurementBundle:TenderComparativeItemAttribute','e');
        $qb->join('e.stockBook','ti');
        $qb->join('e.tenderVendor','tv');
        $qb->join('e.categoryMeta','cm');
        $qb->select('e.id as attributeId','e.metaKey','e.metaValue');
        $qb->addSelect('ti.id as itemId');
        $qb->addSelect('tv.id as vendorId');
        $qb->addSelect('cm.id as metaId');
        $qb->where('e.tenderComparative = :tenderComparative')->setParameter('tenderComparative',$entity->getId());
        $result = $qb->getQuery()->getArrayResult();

        $data = array();
        foreach ($result as $row){
            $unique = "{$row['itemId']}-{$row['vendorId']}-{$row['metaId']}";
            $data[$unique] = $row;
        }
        return array('metaIds' => $metaIds, 'metaData' => $data);
    }


    public function directCsWorkorderProcess(Tender $tender)
    {

        $em = $this->_em;
        $terminal = $tender->getConfig()->getTerminal();
        $find = $this->findOneBy(array('tender' => $tender));
        if(empty($find)){
            $cs = new TenderComparative();
            $module = "tender-comparative";
            $moduleProcess = $em->getRepository(ModuleProcess::class)->existModuleProcess($terminal, $module);
            if (!empty($moduleProcess)) {
                $cs->setModuleProcess($moduleProcess);
                $cs->setModule($module);
            }
            $cs->setCreatedBy($tender->getCreatedBy());
            $cs->setConfig($tender->getConfig());
            $cs->setTender($tender);
            $cs->setBusinessGroup("garment");
            $cs->setWaitingProcess("Approved");
            $cs->setProcess("Direct Workorder");
            $em->persist($cs);
            $em->flush();
        }else{
            $cs = $find;
        }
        $tenderVendor = $em->getRepository(TenderVendor::class)->findOneBy(array('tender'=>$tender,'isDirect'=>1));
        if($tenderVendor){
            $tenderItems = $em->getRepository(TenderItemDetails::class)->findBy(array('tender'=>$tender));
            foreach ($tenderItems as $item ){
                $find = $em->getRepository(TenderComparativeItem::class)->findOneBy(array('tenderComparative' => $cs,'tenderVendor'=>$tenderVendor,'stockBook'=> $item->getStockBook()));
                if(empty($find)){
                    $comparativeItem = new TenderComparativeItem();
                    $comparativeItem->setTenderComparative($cs);
                    $comparativeItem->setTenderVendor($tenderVendor);
                    $comparativeItem->setStockBook($item->getStockBook());
                    $comparativeItem->setUnitPrice($item->getPrice());
                    $comparativeItem->setPrice($item->getPrice());
                    $comparativeItem->setQuantity($item->getIssueQuantity());
                    $comparativeItem->setSubTotal($item->getIssueQuantity() * $item->getPrice());
                    $comparativeItem->setRevisedUnitPrice($item->getPrice());
                    $comparativeItem->setRevisedSubTotal($comparativeItem->getSubTotal());
                    $comparativeItem->setSubTotal($item->getIssueQuantity() * $item->getPrice());
                    $em->persist($comparativeItem);
                    $em->flush();
                }else{
                    $find->setUnitPrice($item->getPrice());
                    $find->setPrice($item->getPrice());
                    $find->setQuantity($item->getIssueQuantity());
                    $find->setSubTotal($item->getIssueQuantity() * $item->getPrice());
                    $find->setRevisedUnitPrice($item->getPrice());
                    $find->setRevisedSubTotal($find->getSubTotal());
                    $em->persist($find);
                    $em->flush();
                }
            }
        }
        $subTotal = $em->getRepository(TenderVendor::class)->updateSubTotal($cs,$tenderVendor);
        $workorder = new TenderWorkorder();
        $module = "work-order";
        $moduleProcess = $em->getRepository(ModuleProcess::class)->existModuleProcess($terminal, $module);
        if (!empty($moduleProcess)) {
            $workorder->setModuleProcess($moduleProcess);
            $workorder->setModule($module);
        }
        $date1 = new \DateTime();
        $date1->add(new \DateInterval('P15D'));
        $deliveryDate = $date1->format('Y-m-d');
        if($cs->getTender()->getRequisition()){
            $workorder->setRequisition($cs->getTender()->getRequisition());
        }
        $workorder->setDeliveryDate($date1);
        $workorder->setCreatedBy($tender->getCreatedBy());
        $workorder->setConfig($tender->getConfig());
        $workorder->setTenderComparative($cs);
        $workorder->setTenderVendor($tenderVendor);
        $workorder->setShipTo($cs->getTender()->getShipTo());
        $workorder->setPaymentMode($cs->getTender()->getPaymentMode());
        $workorder->setTenderVendor($tenderVendor);
        $workorder->setSubTotal($subTotal);
        $workorder->setTotal($subTotal);
        $workorder->setGrandTotal($subTotal);
        $workorder->setBusinessGroup("garment");
        $workorder->setWaitingProcess("New");
        $workorder->setProcess("New");
        $em->persist($workorder);
        $em->flush();
        if($workorder->getModuleProcess()->isStatus() == 1){
            $assignUsers = $em->getRepository(ApprovalUser::class)->getApprovalAssignUser($terminal,$workorder);
            $em->getRepository(ProcurementProcess::class)->insertProcurementProcessAssign($workorder,$workorder->getModule(),$assignUsers);
        }
       // $em->getRepository(TenderWorkorderItem::class)->insertGarmentWorkorderItem($workorder);
        $em->getRepository(TenderWorkorderItem::class)->insertGarmentWorkorderPrItem($workorder);
        return $workorder;

    }

    public function quotationCsVendorItem(Tender $tender, TenderComparative $comparative)
    {
        $em = $this->_em;
        foreach ($tender->getTenderItems() as $item){

            foreach ($tender->getTenderVendors() as $tenderVendor)
            {
                $find = $em->getRepository(TenderComparativeItem::class)->findOneBy(array('tenderComparative' => $comparative, 'tenderVendor' => $tenderVendor, 'tenderItem' => $item));
                if(empty($find)){
                    $comparativeItem = new TenderComparativeItem();
                    $comparativeItem->setTenderComparative($comparative);
                    $comparativeItem->setTenderVendor($tenderVendor);
                    $comparativeItem->setTenderItem($item);
                    $comparativeItem->setStock($item->getStock());
                    $comparativeItem->setQuantity($item->getIssueQuantity());
                    $em->persist($comparativeItem);
                    $em->flush();
                }

            }
        }
        $this->csStockAttributeGenerate($tender,$comparative);
    }

    public function quotationCsProcess(Tender $tender)
    {

        $em = $this->_em;
        $terminal = $tender->getConfig()->getTerminal();
        $find = $this->findOneBy(array('tender' => $tender));
        if(empty($find)){
            $cs = new TenderComparative();
            $module = "tender-comparative";
            $moduleProcess = $em->getRepository(ModuleProcess::class)->existModuleProcess($terminal, $module);
            if (!empty($moduleProcess)) {
                $cs->setModuleProcess($moduleProcess);
                $cs->setModule($module);
            }
            $cs->setCreatedBy($tender->getCreatedBy());
            $cs->setConfig($tender->getConfig());
            $cs->setTender($tender);
            $cs->setSubject($tender->getSubject());
            $cs->setContent($tender->getContent());
            $cs->setTender($tender);
            $cs->setBusinessGroup("bank");
            $cs->setWaitingProcess("Approved");
            $cs->setProcess("Quotation");
            $em->persist($cs);
            $em->flush();
        }else{
            $cs = $find;
        }
        $this->quotationCsVendorItem($tender,$cs);
        $module = "purchase-memo";
        $moduleProcess = $em->getRepository(ModuleProcess::class)->existModuleProcess($terminal,$module);
        if(!empty($moduleProcess)) {
            $purchaseMemo = new TenderMemo();
            $purchaseMemo->setConfig($tender->getConfig());
            $purchaseMemo->setCreatedBy($tender->getCreatedBy());
            $purchaseMemo->setModuleProcess($moduleProcess);
            $purchaseMemo->setTenderComparative($cs);
            $purchaseMemo->setSubject($tender->getSubject());
            $purchaseMemo->setContent($tender->getContent());
            $purchaseMemo->setModule($module);
            $purchaseMemo->setWaitingProcess("Approved");
            $purchaseMemo->setProcess("Approved");
            $em->persist($purchaseMemo);
            $em->flush();
        }
        $module = "management-memo";
        $moduleProcess = $em->getRepository(ModuleProcess::class)->existModuleProcess($terminal, $module);
        if (!empty($moduleProcess)) {
            $managementMemo = new TenderMemo();
            $managementMemo->setModuleProcess($moduleProcess);
            $managementMemo->setModule($module);
            $managementMemo->setSubject($tender->getSubject());
            $managementMemo->setContent($tender->getContent());
            //$managementMemo->setApprovedVendor($tenderVendor);
            $managementMemo->setCreatedBy($tender->getCreatedBy());
            $managementMemo->setConfig($tender->getConfig());
            $managementMemo->setParent($purchaseMemo);
            $managementMemo->setWaitingProcess("New");
            $managementMemo->setProcess("New");
            $em->persist($managementMemo);
            $em->flush();
            if ($managementMemo->getModuleProcess()->isStatus() == 1) {
                $assignUsers = $em->getRepository(ApprovalUser::class)->getApprovalAssignUser($terminal, $managementMemo);
                $em->getRepository(ProcurementProcess::class)->insertProcurementProcessAssign($managementMemo, $managementMemo->getModule(), $assignUsers);
                $em->getRepository(ProcurementProcess::class)->approvalAssign($managementMemo);
            }
        }
        return $managementMemo;

    }

    public function repeatOrderCsProcess(Tender $tender)
    {

        $em = $this->_em;
        $terminal = $tender->getConfig()->getTerminal();
        $vendor = $tender->getWorkOrder()->getTenderVendor();
        if($vendor){
            $workorder = $tender->getWorkOrder();
            $vendorId = $vendor->getVendor()->getId();
            $exist = $em->getRepository(TenderVendor::class)->findOneBy(array('tender' => $tender,'vendor' => $vendorId,'isDirect' => 1));
            if(empty($exist)){
                $entity = new TenderVendor();
                $entity->setTender($tender);
                if($vendor->getVendor()){
                    $entity->setVendor($vendor->getVendor());
                }
                $entity->setName($vendor->getName());
                $entity->setContactPerson($vendor->getName());
                $entity->setPhone($vendor->getPhone());
                $entity->setAddress($vendor->getAddress());
                $entity->setStatus(1);
                $entity->isDirect(1);
                $em->persist($entity);
                $em->flush();
                $tenderVendor = $entity;
            }else{
                $tenderVendor = $exist;
            }
            $find = $this->findOneBy(array('config'=> $tender->getConfig(),'tender' => $tender));
            if(empty($find)){
                $cs = new TenderComparative();
                $module = "tender-comparative";
                $moduleProcess = $em->getRepository(ModuleProcess::class)->existModuleProcess($terminal, $module);
                if (!empty($moduleProcess)) {
                    $cs->setModuleProcess($moduleProcess);
                    $cs->setModule($module);
                }
                $cs->setCreatedBy($tender->getCreatedBy());
                $cs->setConfig($tender->getConfig());
                $cs->setTender($tender);
                $cs->setBusinessGroup("bank");
                $cs->setWaitingProcess("Approved");
                $cs->setProcess("Repeat Order");
                $em->persist($cs);
                $em->flush();
            }else{
                $cs = $find;
            }

            /* @var $item TenderItem */

            foreach ($tender->getTenderItems() as $item){

                $find = $em->getRepository(TenderComparativeItem::class)->findOneBy(array('tenderComparative' => $cs,'tenderItem'=>$item));
                $stockItem = $em->getRepository(TenderWorkorderItem::class)->findOneBy(array('tenderWorkorder' => $workorder,'stock' => $item->getStock()));
                if(empty($find) and !empty($stockItem)){
                    $itemPrice = $stockItem->getPrice();
                    $comparativeItem = new TenderComparativeItem();
                    $comparativeItem->setTenderComparative($cs);
                    $comparativeItem->setTenderVendor($tenderVendor);
                    $comparativeItem->setTenderItem($item);
                    $comparativeItem->setStock($item->getStock());
                    $comparativeItem->setQuantity($item->getPoQuantity());
                    if($itemPrice){
                        $comparativeItem->setUnitPrice($itemPrice);
                        $comparativeItem->setPrice($itemPrice);
                        $comparativeItem->setRevisedUnitPrice($itemPrice);
                    }
                    $comparativeItem->setSubTotal($comparativeItem->getQuantity() * $comparativeItem->getUnitPrice());
                    $comparativeItem->setRevisedSubTotal($comparativeItem->getQuantity() * $comparativeItem->getUnitPrice());
                    $em->persist($comparativeItem);

                    $em->flush();
                }

                /*elseif(!empty($stockItem)){
                    $itemPrice = $stockItem->getPrice();
                    if($itemPrice){
                        $find->setUnitPrice($itemPrice);
                        $find->setPrice($itemPrice);
                        $find->setRevisedUnitPrice($itemPrice);
                    }
                    $find->setSubTotal($find->getQuantity() * $find->getUnitPrice());
                    $find->setRevisedSubTotal($find->getQuantity() * $find->getUnitPrice());
                    $em->persist($find);
                    $em->flush();
                }*/
                $em->getRepository(TenderComparativeItem::class)->updateSummary($tenderVendor);

            }
            $module = "purchase-memo";
            $moduleProcess = $em->getRepository(ModuleProcess::class)->existModuleProcess($terminal,$module);
            if(!empty($moduleProcess)) {
                $find = $em->getRepository(TenderMemo::class)->findOneBy(array('tenderComparative' => $cs));
                if(empty($find)){
                    $purchaseMemo = new TenderMemo();
                    $purchaseMemo->setConfig($tender->getConfig());
                    $purchaseMemo->setCreatedBy($tender->getCreatedBy());
                    $purchaseMemo->setModuleProcess($moduleProcess);
                    $purchaseMemo->setTenderComparative($cs);
                    $purchaseMemo->setModule($module);
                    $purchaseMemo->setWaitingProcess("Approved");
                    $purchaseMemo->setProcess("Approved");
                    $em->persist($purchaseMemo);
                    $em->flush();
                }else{
                    $purchaseMemo = $find;
                }

            }

            $module = "management-memo";
            $moduleProcess = $em->getRepository(ModuleProcess::class)->existModuleProcess($terminal, $module);
            if (!empty($moduleProcess)) {
                $find = $em->getRepository(TenderMemo::class)->findOneBy(array('parent' => $purchaseMemo));
                if(empty($find)) {
                    $managementMemo = new TenderMemo();
                    $managementMemo->setModuleProcess($moduleProcess);
                    $managementMemo->setModule($module);
                    $managementMemo->setApprovedVendor(array($tenderVendor));
                    $managementMemo->setCreatedBy($tender->getCreatedBy());
                    $managementMemo->setConfig($tender->getConfig());
                    $managementMemo->setParent($purchaseMemo);
                    $managementMemo->setWaitingProcess("New");
                    $managementMemo->setProcess("New");
                    $em->persist($managementMemo);
                    $em->flush();
                    if ($managementMemo->getModuleProcess()->isStatus() == 1) {
                        $assignUsers = $em->getRepository(ApprovalUser::class)->getApprovalAssignUser($terminal, $managementMemo);
                        $em->getRepository(ProcurementProcess::class)->insertProcurementProcessAssign($managementMemo, $managementMemo->getModule(), $assignUsers);
                        $em->getRepository(ProcurementProcess::class)->approvalAssign($managementMemo);
                    }
                }else{
                    $managementMemo = $find;
                }
            }
            return $managementMemo;
        }
    }


}


