<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\GenericBundle\Entity\ItemBrand;
use Terminalbd\GenericBundle\Entity\ItemColor;
use Terminalbd\GenericBundle\Entity\ItemSize;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\InventoryBundle\Entity\StockBook;
use Terminalbd\InventoryBundle\Entity\StockHistory;
use Terminalbd\ProcurementBundle\Entity\JobRequisition;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionBudgetItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\TenderItemDetails;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class RequisitionItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RequisitionItem::class);
    }

    protected function handleSearchBetween($qb,$form)
    {

        if(isset($form['requisition_filter_form'])){

            $data = $form['requisition_filter_form'];
            $startDate = isset($data['startDate'])? $data['startDate'] :'';
            $endDate = isset($data['endDate'])? $data['endDate'] :'';
            $branch = isset($data['branch'])? $data['branch'] :'';
            $unit = isset($data['unit'])? $data['unit'] :'';
            $department = isset($data['department'])? $data['department'] :'';
            $priroty = isset($data['priroty'])? $data['priroty'] :'';
            $process = isset($data['process'])? $data['process'] :'';
            $requisitionType = isset($data['requisitionType'])? $data['requisitionType'] :'';
            $requisitionNo = !empty($data['requisitionNo'])? trim($data['requisitionNo']) :'';
            $itemName = !empty($data['itemName'])? trim($data['itemName']) :'';
            $keyword = !empty($data['keyword'])? trim($data['keyword']) :'';
            if(!empty($requisitionNo)){
                $qb->andWhere($qb->expr()->like("r.requisitionNo", "'%$requisitionNo%'"));
                echo $requisitionNo;
            }
            if(!empty($itemName)){
                $qb->andWhere($qb->expr()->like("item.name", "'%$itemName%'"));
            }
            if(!empty($branch)){
                $qb->andWhere('b.id = :branch')->setParameter('branch',$branch);
            }
            if(!empty($unit)){
                $qb->andWhere('unit.id = :cuunit')->setParameter('cuunit',$unit);
            }
            if(!empty($department)){
                $qb->andWhere('d.id = :department')->setParameter('department',$department);
            }
            if (!empty($startDate) ) {
                $datetime = new \DateTime($startDate);
                $startDate = $datetime->format('Y-m-d 00:00:00');
                $qb->andWhere("e.created >= :startDate");
                $qb->setParameter('startDate', $startDate);
            }
            if (!empty($endDate)) {
                $datetime = new \DateTime($endDate);
                $endDate = $datetime->format('Y-m-d 23:59:59');
                $qb->andWhere("e.created <= :endDate");
                $qb->setParameter('endDate', $endDate);
            }

        }
    }

    protected function handleReportBetween($qb,$form)
    {
        if(isset($form)){
            $data = $form['report_filter_form'];
            $startDate = isset($data['startDate'])? $data['startDate'] :'';
            $endDate = isset($data['endDate'])? $data['endDate'] :'';
            $company = isset($data['company'])? $data['company'] :'';
            $section = isset($data['section'])? $data['section'] :'';
            $unit       = isset($data['unit'])? $data['unit'] :'';
            $priroty = isset($data['priroty'])? $data['priroty'] :'';
            $process = isset($data['process'])? $data['process'] :'';
            $requisitionType = isset($data['requisitionType'])? $data['requisitionType'] :'';
            $requisitionNo = !empty($data['requisitionNo'])? trim($data['requisitionNo']) :'';
            $keyword            = !empty($data['keyword'])? trim($data['keyword']) :'';
            $item               = isset($data['item'])? $data['item'] :'';
            $itemCode           = isset($data['itemCode'])? $data['itemCode'] :'';
            $category           = isset($data['category'])? $data['category'] :'';
            $itemMode           = isset($data['itemMode'])? $data['itemMode'] :'';
            $item               = isset($data['name'])? $data['name'] :'';
            $department         = isset($data['department'])? $data['department'] :'';


            if(!empty($requisitionNo)){
                $qb->andWhere($qb->expr()->like("r.requisitionNo", "'%$requisitionNo%'"));
            }
            if(!empty($company)){
                $qb->andWhere('b.id = :branch')->setParameter('branch',$company);
            }
            if(!empty($department)){
                $qb->andWhere('d.id = :department')->setParameter('department',$department);
            }
            if (!empty($startDate) ) {
                $datetime = new \DateTime($startDate);
                $startDate = $datetime->format('Y-m-d 00:00:00');
                $qb->andWhere("e.created >= :startDate");
                $qb->setParameter('startDate', $startDate);
            }
            if (!empty($endDate)) {
                $datetime = new \DateTime($endDate);
                $endDate = $datetime->format('Y-m-d 23:59:59');
                $qb->andWhere("e.created <= :endDate");
                $qb->setParameter('endDate', $endDate);
            }
            if (!empty($category)) {
                $qb->andWhere("cat.id = :cid")->setParameter('cid', $category);
            }
            if (!empty($department)) {
                $qb->andWhere("d.id = :did")->setParameter('did', $department);
            }
            if (!empty($unit)) {
                $qb->andWhere("cu.id = :unitId")->setParameter('unitId', $unit);
            }
            if (!empty($section)) {
                $qb->andWhere("sec.id = :sectionId")->setParameter('sectionId', $section);
            }
            if (!empty($department)) {
                $qb->andWhere("d.id = :did")->setParameter('did', $department);
            }
            if (!empty($item)) {
                $qb->andWhere("item.id = :pid")->setParameter('pid', $item);
            }
            if (!empty($itemMode)) {
                $qb->andWhere("item.itemMode = :itemMode")->setParameter('itemMode', $itemMode);
            }


        }
    }



    public function getItemWisePrReport($config,$data)
    {

        $config = $config->getId();
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.requisition','r');
        $qb->leftJoin('r.comapnyRequisitionShares','cm');
        $qb->leftJoin('r.section','sec');
        $qb->leftJoin('r.createdBy','u');
        $qb->leftJoin('u.profile','profile');
        $qb->leftJoin('r.companyUnit','cu');
        $qb->leftJoin('cu.parent','b');
        $qb->leftJoin('r.department','d');
        $qb->join('e.stockBook','sb');
        $qb->join('sb.item','item');
        $qb->leftJoin('item.unit','unit');
        $qb->leftJoin('sb.category','cat');
        $qb->select('e.id as id','e.quantity as quantity','e.price as price','e.subTotal as subTotal','e.remainigQuantity as remaining');
        $qb->addSelect('r.created as created','r.id as requisitionId','r.requisitionNo as requisitionNo','r.updated as requisitionDate','r.requisitionMode as requisitionMode');
        $qb->addSelect('b.code as company');
        $qb->addSelect('sec.name as section');
        $qb->addSelect('sb.id as sbId');
        $qb->addSelect('cat.name as category');
        $qb->addSelect('d.name as department');
        $qb->addSelect('item.name as itemName','item.id as itemId');
        $qb->addSelect('unit.name as uom');
        $qb->where('r.config = :config')->setParameter('config',"{$config}");
        $qb->andWhere('r.waitingProcess = :process')->setParameter('process',"Approved");
        $this->handleReportBetween($qb,$data);
        $qb->orderBy('r.created','ASC');
        $qb->groupBy('id');
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }

    public function getItemSummary(Requisition $invoice)
    {
        $id = $invoice->getId();
        $em = $this->_em;
        $qb = $this->createQueryBuilder('e');
        $qb->select('SUM(e.subTotal) as subTotal');
        $qb->join('e.requisition','i');
        $qb->where("i.id = '{$id}'");
        $result = $qb->getQuery()->getOneOrNullResult();
        if(!empty($result) and !empty($result['subTotal'])){
            $subTotal = $result['subTotal'];
            $invoice->setSubTotal($subTotal);
            $vat = $this->getCalculationVat($subTotal);
            $invoice->setVat($vat);
            $total = $vat + $subTotal;
            $invoice->setTotal($subTotal);
        }else{
            $invoice->setSubTotal(0);
            $invoice->setVat(0);
            $invoice->setTotal(0);
        }
        $em->persist($invoice);
        $em->flush();

    }

    public function getJobItemSummary(JobRequisition $invoice)
    {
        $id = $invoice->getId();
        $em = $this->_em;
        $qb = $this->createQueryBuilder('e');
        $qb->select('SUM(e.subTotal) as subTotal');
        $qb->join('e.jobRequisition','i');
        $qb->where("i.id = '{$id}'");
        $result = $qb->getQuery()->getOneOrNullResult();
        if(!empty($result) and !empty($result['subTotal'])){
            $subTotal = $result['subTotal'];
            $invoice->setSubTotal($subTotal);
            $vat = $this->getCalculationVat($subTotal);
            $invoice->setVat($vat);
            $total = $vat + $subTotal;
            $invoice->setTotal($subTotal);
        }else{
            $invoice->setSubTotal(0);
            $invoice->setVat(0);
            $invoice->setTotal(0);
        }
        $em->persist($invoice);
        $em->flush();

    }

    public function getCalculationVat($totalAmount)
    {
        $vat = ( ($totalAmount * (int)10)/100 );
        //$vat = ( ($totalAmount * (int)$sales->getRestaurantConfig()->getVatPercentage())/100 );
        return round($vat);
    }

    public function insertJobRequisitionItem(JobRequisition $requisition, $data)
    {


        $em = $this->_em;


        if(isset($data['item']) and $data['item'] > 0 and $data['quantity'] > 0) {
            /* @var $stock Stock */
            $stock = $em->getRepository(Stock::class)->findOneBy(array('item' => $data['item']));

            /* @var $entity RequisitionItem */
            $entity = new RequisitionItem();
            $entity->setJobRequisition($requisition);
            if ($data['quantity']) {
                $entity->setQuantity($data['quantity']);
                $entity->setApproveQuantity($data['quantity']);
                $entity->setActualQuantity($data['quantity']);
                $entity->setRemainigQuantity($data['quantity']);
            }
            if ($data['brand']) {
                $brand = $em->getRepository(ItemBrand::class)->find($data['brand']);
                $entity->setBrand($brand);
            }
            if ($data['size']) {
                $size = $em->getRepository(ItemSize::class)->find($data['size']);
                $entity->setSize($size);
            }
            if ($data['color']) {
                $color = $em->getRepository(ItemColor::class)->find($data['color']);
                $entity->setColor($color);
            }
            $stockBook = $em->getRepository(StockBook::class)->existStockBookItem($stock, $data);
            if ($stockBook) {
                $entity->setStockIn($stockBook->getStockIn() - $stockBook->getStockOut());
                $entity->setStockBook($stockBook);
                $entity->setPrice($stockBook->getPrice());
                $entity->setCurrentPrice($stockBook->getCmp());
                $entity->setCmp($stockBook->getCmp());
                $entity->setCmpDate($stockBook->getUpdated());
                $lastPurchase = $em->getRepository(StockHistory::class)->getLastPurchaseItemInformation($stockBook->getId());
                if ($lastPurchase) {
                    $entity->setLastPurchaseQuantity($lastPurchase['purchaseQuantity']);
                    $entity->setLastPurchasePrice($lastPurchase['price']);
                    $entity->setLastPurchaseDate($lastPurchase['updated']);
                }
            }
            $entity->setStock($stock);
            $entity->setItem($stock->getItem());
            $entity->setName($stock->getItem()->getName());
            $entity->setSubTotal($entity->getPrice() * $entity->getQuantity());
            $em->persist($entity);
            $em->flush();
        }

    }

    public function insertGarmentRequisitionItem(Requisition $requisition, $data)
    {

        $em = $this->_em;
        if(isset($data['item']) and $data['item'] > 0 and $data['quantity'] > 0){
            /* @var $stock Stock */
            $stock = $em->getRepository(Stock::class)->findOneBy(array('item' => $data['item']));
            /* @var $entity RequisitionItem */

            $entity = new RequisitionItem();
            $entity->setRequisition($requisition);
            if(isset($data['created']) and $data['created']){
                $created = new \DateTime($data['created']);
                $entity->setCreated($created);
            }
            if(isset($data['price']) and $data['price']){
                $entity->setPrice($data['price']);
            }
            if(isset($data['description']) and $data['description']){
                $entity->setDescription($data['description']);
            }
            if($data['quantity']) {
                $entity->setQuantity($data['quantity']);
                $entity->setApproveQuantity($data['quantity']);
                $entity->setActualQuantity($data['quantity']);
                $entity->setRemainigQuantity($data['quantity']);
            }
            if(isset($data['brand']) and $data['brand']){
                $brand = $em->getRepository(ItemBrand::class)->find($data['brand']);
                $entity->setBrand($brand);
            }
            if(isset($data['size']) and $data['size']){
                $size = $em->getRepository(ItemSize::class)->find($data['size']);
                $entity->setSize($size);
            }
            if(isset($data['color']) and $data['color']){
                $color = $em->getRepository(ItemColor::class)->find($data['color']);
                $entity->setColor($color);
            }
            $stockBook = $em->getRepository(StockBook::class)->existStockBookItem($stock,$data);
            $entity->setStockBook($stockBook);
            if($stockBook and $requisition->getRequisitionMode() != 'Expense'){
                $entity->setStockIn($stockBook->getStockIn() - $stockBook->getStockOut());
                $entity->setPrice($stockBook->getPrice());
                $entity->setCurrentPrice($stockBook->getCmp());
                $entity->setCmp($stockBook->getCmp());
                $entity->setCmpDate($stockBook->getUpdated());
                $lastPurchase = $em->getRepository(StockHistory::class)->getLastPurchaseItemInformation($stockBook->getId());
                if($lastPurchase){
                    $entity->setLastPurchaseQuantity($lastPurchase['purchaseQuantity']);
                    $entity->setLastPurchasePrice($lastPurchase['price']);
                    $entity->setLastPurchaseDate($lastPurchase['updated']);
                }
            }
            $entity->setStock($stock);
            $entity->setItem($stock->getItem());
            $entity->setName($stock->getItem()->getName());
            $entity->setSubTotal($entity->getPrice() * $entity->getQuantity());
            $em->persist($entity);
            $em->flush();
            /* This function use for specific requisition budget generate */
            if($requisition->getRequisitionMode() !="CAPEX"){
                $em->getRepository(RequisitionBudgetItem::class)->insertBudgetItem($requisition);
            }

        }

    }

    public function insertStoreItem(Requisition $requisition, $data)
    {

        $em = $this->_em;

        /* @var $stock Stock */

        $stock = $em->getRepository(Stock::class)->findOneBy(array('item' => $data['item']));
        if($stock){
            $lastReceiveItem = $this->getLastBranchItem($requisition,$stock);
            if($stock and $data['quantity'] > 0){
                $exist = $this->findOneBy(array('requisition'=> $requisition,'stock' => $stock));
                if(empty($exist)){
                    $entity = new RequisitionItem();
                    $entity->setRequisition($requisition);
                    $entity->setQuantity($data['quantity']);
                    $entity->setApproveQuantity($data['quantity']);
                    $entity->setActualQuantity($data['quantity']);
                    $entity->setRemainigQuantity($data['quantity']);
                    if($data['stockIn']){
                        $entity->setStockIn($data['stockIn']);
                    }
                    $entity->setDescription($data['description']);
                    $entity->setStock($stock);
                    $entity->setItem($stock->getItem());
                    $entity->setName($stock->getItem()->getName());
                    $entity->setPrice($stock->getPurchasePrice());
                    if(!empty($lastReceiveItem)){
                        $item = $this->find($lastReceiveItem);
                        $entity->setLastRequisitionItem($item);
                    }
                    $entity->setSubTotal($entity->getPrice() * $entity->getQuantity());
                    $em->persist($entity);
                    $em->flush();
                }else{
                    $entity = $exist;
                    $entity->setQuantity($data['quantity']);
                    $entity->setApproveQuantity($data['quantity']);
                    $entity->setActualQuantity($data['quantity']);
                    $entity->setRemainigQuantity($data['quantity']);
                    $entity->setStockIn($data['stockIn']);
                    $entity->setDescription($data['description']);
                    $entity->setSubTotal($entity->getPrice() * $entity->getQuantity());
                    $em->persist($entity);
                    $em->flush();
                }

            }
        }
    }

    public function insertBankStoreItem(Requisition $requisition, RequisitionItem $entity)
    {
        $em = $this->_em;
        $lastReceiveItem = $this->getLastBranchItem($requisition,$entity->getStock());
        if(!empty($lastReceiveItem)){
            $item = $this->find($lastReceiveItem);
            $entity->setLastRequisitionItem($item);
            $em->flush();
        }
    }

    public function updateRequisitionItemQuantity($requisition, $items)
    {
        $em = $this->_em;
        foreach ($items as $itemId => $quantity){
            /* @var $entity RequisitionItem */
            $entity = $this->find($itemId);
            if($entity){
                $entity->setApproveQuantity($entity->getQuantity());
                $entity->setQuantity($quantity);
                $entity->setRemainigQuantity($entity->getQuantity());
                $entity->setSubTotal($quantity * $entity->getPrice());
                $em->persist($entity);
                $em->flush();
            }
        }

        $this->getItemSummary($requisition);
    }

    public function prOpenItem($config , $data = "")
    {
        $qb = $this->createQueryBuilder('e');
        $qb->leftJoin('e.requisition','r');
        $qb->leftJoin('r.comapnyRequisitionShares','cm');
        $qb->leftJoin('cm.branch','b');
        $qb->leftJoin('r.companyUnit','unit');
        $qb->leftJoin('r.department','d');
        $qb->leftJoin('e.stockBook','sb');
        $qb->leftJoin('sb.item','item');
        $qb->leftJoin('item.category','category');
        $qb->leftJoin('category.generalLedger','gl');
        $qb->leftJoin('sb.brand','brand');
        $qb->leftJoin('sb.size','size');
        $qb->leftJoin('sb.color','color');
        $qb->select('e.id as id','e.quantity as quantity','e.price as price','(e.quantity -  COALESCE(e.issueQuantity,0)) as remaining');
        $qb->addSelect('r.id as requisitionId','r.requisitionNo as requisitionNo','r.updated as requisitionDate');
        $qb->addSelect('sb.id as sbId');
        $qb->addSelect('brand.name as brandName');
        $qb->addSelect('unit.name as unitName');
        $qb->addSelect('d.name as department');
        $qb->addSelect('size.name as sizeName');
        $qb->addSelect('color.name as colorName');
        $qb->addSelect('gl.name  as glCode');
        $qb->addSelect('GROUP_CONCAT(b.name) as branches');
        $qb->addSelect('item.name as itemName','item.id as itemId');
        $qb->where('r.config = :config')->setParameter('config',"{$config}");
        $qb->andWhere('r.waitingProcess = :process')->setParameter('process',"Approved");
        $qb->andWhere('r.requisitionMode IN (:type)')->setParameter('type',array("CAPEX","OPEX"));
        $qb->andWhere('e.isClose = 1');
        $qb->andHaving('remaining > 0');
        $qb->orderBy('e.created',"DESC");
        $this->handleSearchBetween($qb,$data);
        $qb->groupBy('id');
        if(!empty($data['items'])){
            $qb->andWhere($qb->expr()->in("e.id", $data['items'] ));
        }
        $result = $qb->getQuery()->getArrayResult();
        $data = array();
        foreach ($result as $row)
        {
            $data[$row['itemId']][] = $row;
        }
        return $data;
    }

    public function requisitionItemReport($config , $data = "")
    {
        $qb = $this->createQueryBuilder('e');
        $qb->leftJoin('e.requisition','r');
//        $qb->leftJoin('r.comapnyRequisitionShares','cm');
//        $qb->leftJoin('cm.branch','b');
        $qb->leftJoin('r.companyUnit','unit');
        $qb->leftJoin('unit.parent','b');
        $qb->leftJoin('r.department','d');
        $qb->leftJoin('e.stockBook','sb');
        $qb->leftJoin('sb.item','item');
        $qb->leftJoin('item.category','category');
        $qb->leftJoin('item.unit','itemUnit');
        $qb->leftJoin('category.generalLedger','gl');
        $qb->leftJoin('sb.brand','brand');
        $qb->leftJoin('sb.size','size');
        $qb->leftJoin('sb.color','color');
        $qb->select('e.id as id','e.quantity as quantity','e.price as price','(e.quantity -  COALESCE(e.issueQuantity,0)) as remaining', '(e.quantity * e.price) AS subTotal', 'e.created');
        $qb->addSelect('r.id as requisitionId','r.requisitionNo as requisitionNo','r.updated as requisitionDate');
        $qb->addSelect('sb.id as sbId');
        $qb->addSelect('brand.name as brandName');
        $qb->addSelect('unit.code as unitName');
        $qb->addSelect('d.name as department');
        $qb->addSelect('size.name as sizeName');
        $qb->addSelect('color.name as colorName');
        $qb->addSelect('gl.name  as glCode');
        $qb->addSelect('b.name AS companyName');
        $qb->addSelect('itemUnit.name AS uom');
        $qb->addSelect('category.name AS categoryName');
//        $qb->addSelect('GROUP_CONCAT(b.name) as branches');
        $qb->addSelect('item.name as itemName','item.id as itemId');
        $qb->where('r.config = :config')->setParameter('config',"{$config}");
//        $qb->andWhere('r.waitingProcess = :process')->setParameter('process',"Approved");
        $qb->andWhere('r.waitingProcess IN (:process)')->setParameter('process',['Approved', 'Closed']);
        $qb->andWhere('r.requisitionMode IN (:type)')->setParameter('type',array("CAPEX","OPEX"));
//        $qb->andWhere('e.isClose = 1');
//        $qb->andHaving('remaining > 0');
        $qb->orderBy('e.created',"DESC");
        $this->handleSearchBetween($qb,$data);
        $qb->groupBy('id');
        if(!empty($data['items'])){
            $qb->andWhere($qb->expr()->in("e.id", $data['items'] ));
        }
        
        $result = $qb->getQuery()->getArrayResult();
        $data = array();
        foreach ($result as $row)
        {
            $data[$row['itemId']][] = $row;
        }
//        return $data;
        return $result;
    }

    public function prBankOpenItem($config , $data = "")
    {

        $qb = $this->createQueryBuilder('e');
        $qb->join('e.requisition','r');
        $qb->leftJoin('r.requisitionType','rt');
        $qb->leftJoin('r.department','d');
        $qb->leftJoin('r.branch','b');
        $qb->leftJoin('e.batchItem','bi');
        $qb->leftJoin('bi.tenderBatch','tb');
        $qb->leftJoin('tb.assignTo','tbt');
        $qb->join('e.item','item');
        $qb->select('e.id as id','e.quantity as quantity','e.approveQuantity as aapquantity','e.remainigQuantity as remaining','e.price as price','e.isClose as close');
        $qb->addSelect('r.id as requisitionId','r.requisitionNo as requisitionNo','r.updated as requisitionDate');
        $qb->addSelect('d.name as department');
        $qb->addSelect('bi.id as batchItemId','bi.quantity as approveQuantity');
        $qb->addSelect('tb.id as batch','tb.content as content');
        $qb->addSelect('tbt.name as assignedTo');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('item.name as itemName','item.id as itemId');
        $qb->where('r.config = :config')->setParameter('config',"{$config}");
        $qb->andWhere('rt.slug IN (:type)')->setParameter('type',array("purchase-requisition","repair-requisition","replacement-requisition"));
        $qb->andWhere('r.waitingProcess = :process')->setParameter('process',"Approved");
        if(isset($data['mode']) and $data['mode'] == "close"){
            $qb->andWhere('e.isClose = 1');
            $qb->andWhere('tb.id IS NOT NULL');
        }elseif(isset($data['mode']) and $data['mode'] == "in-progress"){
            $qb->andWhere('tb.id IS NULL');
        }elseif( isset($data['mode']) and $data['mode'] == "archive"){
            $qb->andWhere('e.isClose != 1');
        }
        $this->handleSearchBetween($qb,$data);
        $qb->orderBy('e.created',"DESC");
        $qb->groupBy('id');
        if(!empty($data['items'])){
            $qb->andWhere($qb->expr()->in("e.id", $data['items'] ));
        }
        $result = $qb->getQuery()->getArrayResult();
        $data = array();
        foreach ($result as $row)
        {
            $data[$row['itemId']][] = $row;
        }
        return $data;
    }

    public function bankBatchItem($config , $user , $data = "")
    {

        $qb = $this->createQueryBuilder('e');
        $qb->join('e.requisition','r');
        $qb->leftJoin('r.requisitionType','rt');
        $qb->leftJoin('r.department','d');
        $qb->leftJoin('r.branch','b');
        $qb->join('e.batchItem','bi');
        $qb->join('bi.tenderBatch','tb');
        $qb->join('e.item','item');
        $qb->select('e.id as id','e.issueQuantity as issueQuantity','e.remainigQuantity as remaining');
        $qb->addSelect('r.id as requisitionId','r.requisitionNo as requisitionNo','r.updated as requisitionDate');
        $qb->addSelect('d.name as department');
        $qb->addSelect('bi.id as batchItemId','bi.quantity as quantity','bi.isClose as isClose');
        $qb->addSelect('tb.id as batch','tb.content as content');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('item.name as itemName','item.id as itemId');
        $qb->where('r.config = :config')->setParameter('config',"{$config}");
        $qb->andWhere('rt.slug IN (:type)')->setParameter('type',array("purchase-requisition","repair-requisition","replacement-requisition"));
        $qb->andWhere('r.waitingProcess = :process')->setParameter('process',"Approved");
        $qb->andWhere('tb.assignTo = :assignTo')->setParameter('assignTo',"{$user}");
        $qb->andWhere('e.remainigQuantity > 0');
        if(isset($data['mode']) and $data['mode'] == "close"){
            $qb->andWhere('bi.isClose = 1');
        }elseif(isset($data['mode']) and $data['mode'] == "in-progress"){
            $qb->andWhere('bi.isClose IS NULL');
        }
        $this->handleSearchBetween($qb,$data);
        $qb->groupBy('id');
        $qb->orderBy('r.created','DESC');
        $result = $qb->getQuery()->getArrayResult();
        $data = array();
        foreach ($result as $row)
        {
            $data[$row['itemId']][] = $row;
        }
        return $data;
    }

    public function bankBatchAcknowledgeItem($config , $user , $data = "")
    {
        $department = $user->getProfile()->getDepartment();
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.requisition','r');
        $qb->leftJoin('r.department','d');
        $qb->leftJoin('r.requisitionType','rt');
        $qb->leftJoin('r.branch','b');
        $qb->join('e.batchItem','bi');
        $qb->leftJoin('bi.assignTo','biReportTo');
        $qb->leftJoin('bi.approvedBy','biApprovedBy');
        $qb->join('bi.tenderBatch','tb');
        $qb->join('tb.department','tbDep');
        $qb->join('e.item','item');
        $qb->select('e.id as id','e.issueQuantity as issueQuantity','e.remainigQuantity as remaining');
        $qb->addSelect('r.id as requisitionId','r.requisitionNo as requisitionNo','r.updated as requisitionDate');
        $qb->addSelect('d.name as department');
        $qb->addSelect('bi.id as batchItemId','bi.comment as comment','bi.quantity as quantity','bi.isClose as isClose');
        $qb->addSelect('biReportTo.id as assignToId','biReportTo.name as assignTo');
        $qb->addSelect('biApprovedBy.id as approvedById','biApprovedBy.name as approvedBy');
        $qb->addSelect('tb.id as batch','tb.content as content');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('item.name as itemName','item.id as itemId');
        $qb->where('r.config = :config')->setParameter('config',"{$config}");
        $qb->andWhere('tbDep.id =:dId')->setParameter('dId', $department->getId());
        if(isset($data['mode']) and $data['mode'] == "close"){
            $qb->andWhere('biApprovedBy.id is not NULL');
        }elseif(isset($data['mode']) and $data['mode'] == "in-progress"){
            $qb->andWhere('biApprovedBy.id is NULL');
        }
        $this->handleSearchBetween($qb,$data);
        $qb->groupBy('id');
        $result = $qb->getQuery()->getArrayResult();
        $data = array();
        foreach ($result as $row)
        {
            $data[$row['itemId']][] = $row;
        }
        return $result;
    }

    public function bankBatchLssdAcknowledgeItem($config , $user , $data = "")
    {
        $department = $user->getProfile()->getDepartment();
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.requisition','r');
        $qb->leftJoin('r.department','d');
        $qb->leftJoin('r.requisitionType','rt');
        $qb->leftJoin('r.branch','b');
        $qb->join('e.batchItem','bi');
        $qb->leftJoin('bi.assignTo','biReportTo');
        $qb->leftJoin('bi.approvedBy','biApprovedBy');
        $qb->join('bi.tenderBatch','tb');
        $qb->leftJoin('tb.assignTo','tbassignTo');
        $qb->leftJoin('tb.department','tbDep');
        $qb->join('e.item','item');
        $qb->select('e.id as id','e.issueQuantity as issueQuantity','e.remainigQuantity as remaining');
        $qb->addSelect('r.id as requisitionId','r.requisitionNo as requisitionNo','r.updated as requisitionDate');
        $qb->addSelect('d.name as department');
        $qb->addSelect('bi.id as batchItemId','bi.comment as comment','bi.quantity as quantity','bi.isClose as isClose');
        $qb->addSelect('biReportTo.id as assignToId','biReportTo.name as assignTo');
        $qb->addSelect('biApprovedBy.id as approvedById','biApprovedBy.name as approvedBy');
        $qb->addSelect('tb.id as batch','tb.content as content');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('item.name as itemName','item.id as itemId');
        $qb->addSelect('tbDep.name as assessmentDepartment');
        $qb->addSelect('tbassignTo.id as requisitionAssignToId','tbassignTo.name as requisitionAssignTo');
        $qb->where('r.config = :config')->setParameter('config',"{$config}");
        $qb->andWhere('tbDep.id != :dId')->setParameter('dId', $department->getId());
        $this->handleSearchBetween($qb,$data);
        $qb->groupBy('id');
        $result = $qb->getQuery()->getArrayResult();
        $data = array();
        foreach ($result as $row)
        {
            $data[$row['itemId']][] = $row;
        }
        return $data;
    }

    public function bankBatchAcknowledgeItemComment($config , $user , $data = "")
    {

        $qb = $this->createQueryBuilder('e');
        $qb->join('e.requisition','r');
        $qb->leftJoin('r.department','d');
        $qb->leftJoin('r.requisitionType','rt');
        $qb->leftJoin('r.branch','b');
        $qb->join('e.batchItem','bi');
        $qb->leftJoin('bi.assignTo','biReportTo');
        $qb->leftJoin('bi.approvedBy','biApprovedBy');
        $qb->join('bi.tenderBatch','tb');
        $qb->join('tb.department','tbDep');
        $qb->join('e.item','item');
        $qb->select('e.id as id','e.issueQuantity as issueQuantity','e.remainigQuantity as remaining');
        $qb->addSelect('r.id as requisitionId','r.requisitionNo as requisitionNo','r.updated as requisitionDate');
        $qb->addSelect('d.name as department');
        $qb->addSelect('bi.id as batchItemId','bi.comment as comment','bi.quantity as quantity','bi.isClose as isClose');
        $qb->addSelect('biReportTo.id as assignToId','biReportTo.name as assignTo');
        $qb->addSelect('biApprovedBy.id as approvedById','biApprovedBy.name as approvedBy');
        $qb->addSelect('tb.id as batch','tb.content as content');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('item.name as itemName','item.id as itemId');
        $qb->where('r.config = :config')->setParameter('config',"{$config}");
        $qb->andWhere('biReportTo.id =:biReportToUser')->setParameter('biReportToUser', $user->getId());
        $this->handleSearchBetween($qb,$data);
        $qb->groupBy('id');
        $result = $qb->getQuery()->getArrayResult();
        $data = array();
        foreach ($result as $row)
        {
            $data[$row['itemId']][] = $row;
        }
        return $data;
    }

    public function prBankOpenBatchItem($config , $items)
    {

        $qb = $this->createQueryBuilder('e');
        $qb->join('e.requisition','r');
        $qb->leftJoin('r.requisitionType','rt');
        $qb->where('r.config = :config')->setParameter('config',"{$config}");
        $qb->andWhere('r.config = :config')->setParameter('config',"{$config}");
        $qb->andWhere('rt.slug IN (:type)')->setParameter('type',array("purchase-requisition","repair-requisition","replacement-requisition"));
        $qb->andWhere('r.waitingProcess = :process')->setParameter('process',"Approved");
        $qb->andWhere('e.isClose = 1');
        $qb->andWhere($qb->expr()->in("e.id", $items ));
        $result = $qb->getQuery()->getResult();
        return $result;
    }

    public function updateStoreItem(RequisitionItem $item,$quantity,$stockIn)
    {
        $em = $this->_em;
        $item->setQuantity($quantity);
        $item->setApproveQuantity($item->getQuantity());
        $item->setRemainigQuantity($item->getQuantity());
        $item->setStockIn($stockIn);
        $item->setSubTotal($quantity * $item->getPrice());
        $em->persist($item);
        $em->flush();
    }

    public function updateApproveQuantity($data)
    {
        $em = $this->_em;
        /* @var $item RequisitionItem */
        foreach ($data['purchaseItem'] as $key => $value){
            $item = $this->find($value);
            $quantity = (integer)$data['quantity'][$value];
            $item->setActualQuantity($item->getQuantity());
            $item->setApproveQuantity($item->getQuantity());
            $item->setRemainigQuantity($quantity);
            $item->setQuantity($quantity);
            $item->setSubTotal($quantity * $item->getPrice());
            $em->persist($item);
            $em->flush();
        }
    }

    public function getLastBranchItem(Requisition $requisition,$stock)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.requisition','r');
        $qb->leftJoin('r.branch','b');
        $qb->leftJoin('r.department','d');
        $qb->select('e.id as id');
        $qb->where('e.stock = :stock')->setParameter('stock',$stock->getId());
        if(!empty($requisition->getBranch())){
            $qb->andWhere('b.id = :id')->setParameter('id',"{$requisition->getBranch()->getId()}");
        }elseif(!empty($requisition->getDepartment())){
            $qb->andWhere('d.id = :id')->setParameter('id',"{$requisition->getDepartment()->getId()}");
        }
        $qb->setMaxResults(1);
        $qb->orderBy('e.id',"DESC");
        $result = $qb->getQuery()->getOneOrNullResult();
        if($result){
            $lastId = $result['id'];
        }else{
            $lastId = '';
        }
        return $lastId;
    }

    public function updateRequisitionItemForTender($ids)
    {
        $em = $this->_em;
        foreach ($ids as $pri):
            $issueQuantity  = $this->getTenderItemDetailsIssueQuantity($pri);
            $requisitionItem = $this->find($pri);
            $requisitionItem->setIssueQuantity($issueQuantity);
            $remain = ($requisitionItem->getQuantity() - $requisitionItem->getIssueQuantity());
            $requisitionItem->setRemainigQuantity($remain);
            $em->persist($requisitionItem);
            $em->flush();
        endforeach;

    }

    private function getTenderItemDetailsIssueQuantity($id){
        $em = $this->_em;
        $qb = $this->_em->createQueryBuilder();
        $qb->from(TenderItemDetails::class,'e');
        $qb->select('SUM(e.issueQuantity) as issueQuantity');
        $qb->where('e.requisitionItem =:ids')->setParameter('ids',$id);
        $result = $qb->getQuery()->getOneOrNullResult();
        return ($result['issueQuantity'] > 0)? $result['issueQuantity']:0;

    }



}
