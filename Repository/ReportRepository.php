<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Repository;

use App\Entity\Domain\Vendor;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\InventoryBundle\Entity\StockHistory;
use Terminalbd\InventoryBundle\Entity\StockWearhouse;
use Terminalbd\ProcurementBundle\Entity\Report;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionOrder;
use Terminalbd\ProcurementBundle\Entity\RequisitionOrderItem;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorder;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderItem;


/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class ReportRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Report::class);
    }

    protected function handleReportBetween($qb,$form)
    {
        if(isset($form)){
            $data = $form['report_filter_form'];
            $startDate = isset($data['startDate'])? $data['startDate'] :'';
            $endDate = isset($data['endDate'])? $data['endDate'] :'';
            $company = isset($data['company'])? $data['company'] :'';
            $section = isset($data['section'])? $data['section'] :'';
            $unit       = isset($data['unit'])? $data['unit'] :'';
            $priroty = isset($data['priroty'])? $data['priroty'] :'';
            $process = isset($data['process'])? $data['process'] :'';
            $requisitionType = isset($data['requisitionType'])? $data['requisitionType'] :'';
            $requisitionNo = !empty($data['requisitionNo'])? trim($data['requisitionNo']) :'';
            $keyword            = !empty($data['keyword'])? trim($data['keyword']) :'';
            $item               = isset($data['item'])? $data['item'] :'';
            $itemCode           = isset($data['itemCode'])? $data['itemCode'] :'';
            $category           = isset($data['category'])? $data['category'] :'';
            $itemMode           = isset($data['itemMode'])? $data['itemMode'] :'';
            $item               = isset($data['name'])? $data['name'] :'';
            $department         = isset($data['department'])? $data['department'] :'';


            if(!empty($requisitionNo)){
                $qb->andWhere($qb->expr()->like("r.requisitionNo", "'%$requisitionNo%'"));
            }
            if(!empty($company)){
                $qb->andWhere('b.id = :branch')->setParameter('branch',$company);
            }
            if(!empty($department)){
                $qb->andWhere('d.id = :department')->setParameter('department',$department);
            }
            if (!empty($category)) {
                $qb->andWhere("cat.id = :cid")->setParameter('cid', $category);
            }
            if (!empty($department)) {
                $qb->andWhere("d.id = :did")->setParameter('did', $department);
            }
            if (!empty($unit)) {
                $qb->andWhere("cu.id = :unitId")->setParameter('unitId', $unit);
            }
            if (!empty($section)) {
                $qb->andWhere("sec.id = :sectionId")->setParameter('sectionId', $section);
            }
            if (!empty($department)) {
                $qb->andWhere("d.id = :did")->setParameter('did', $department);
            }
            if (!empty($item)) {
                $qb->andWhere("item.id = :pid")->setParameter('pid', $item);
            }
            if (!empty($itemMode)) {
                $qb->andWhere("item.itemMode = :itemMode")->setParameter('itemMode', $itemMode);
            }


        }
    }

    protected function handleSearchStockBetween($qb, $form)
    {

        if (isset($form['procurement_report_filter_form'])) {
            $data = $form['procurement_report_filter_form'];
            $productGroup           = isset($data['productGroup'])? $data['productGroup'] :'';
            $category               = isset($data['category'])? $data['category'] :'';
            $itemCode               = isset($data['itemCode'])? $data['itemCode'] :'';
            $keyword                = isset($data['keyword'])? $data['keyword'] :'';
            $wearhouse              = isset($data['wearhouse'])? $data['wearhouse'] :'';
            $company                = isset($data['company'])? $data['company'] :'';
            $itemMode               = isset($data['itemMode'])? $data['itemMode'] :'';
            if($company){
                $qb->join("store.parent",'c');
                $qb->andWhere("c.id = :company")->setParameter('company', $company);
            }

            if (!empty($category)) {
                $qb->andWhere("category.id = :cid")->setParameter('cid', $category);
            }
            if($wearhouse){
                $qb->andWhere("store.id = :wearhouse")->setParameter('wearhouse', $wearhouse);
            }
            if (!empty($itemMode)) {
                $qb->andWhere('item.itemMode LIKE :searchTerm');
                $qb->setParameter('searchTerm', '%'.$itemMode.'%');
            }
            if (!empty($itemCode)) {
                $qb->andWhere('item.itemCode LIKE :searchTerm');
                $qb->setParameter('searchTerm', '%'.$itemCode.'%');
            }
            if (!empty($productGroup)) {
                $qb->andWhere("productGroup.id = :pid")->setParameter('pid', $productGroup);
            }
        }
    }

    protected function handleDateRangeBetween($qb, $form)
    {

        if (isset($form['procurement_report_filter_form'])) {
            $data = $form['procurement_report_filter_form'];
            $startDate              = isset($data['startDate'])? $data['startDate'] :'';
            $endDate                = isset($data['endDate'])? $data['endDate'] :'';
            if (!empty($startDate) ) {
                $datetime = new \DateTime($startDate);
                $startDate = $datetime->format('Y-m-d 00:00:00');
                $qb->andWhere("e.created >= :startDate");
                $qb->setParameter('startDate', $startDate);
            }

            if (!empty($endDate)) {
                $datetime = new \DateTime($endDate);
                $endDate = $datetime->format('Y-m-d 23:59:59');
                $qb->andWhere("e.created <= :endDate");
                $qb->setParameter('endDate', $endDate);
            }
        }
    }

    protected function handleSearchStockOpeningBetween($qb, $form)
    {

        if (isset($form['procurement_report_filter_form'])) {
            $data = $form['procurement_report_filter_form'];
            $productGroup           = isset($data['productGroup'])? $data['productGroup'] :'';
            $category               = isset($data['category'])? $data['category'] :'';
            $itemCode               = isset($data['itemCode'])? $data['itemCode'] :'';
            $keyword                = isset($data['keyword'])? $data['keyword'] :'';
            $wearhouse              = isset($data['wearhouse'])? $data['wearhouse'] :'';
            $company                = isset($data['company'])? $data['company'] :'';
            $itemMode               = isset($data['itemMode'])? $data['itemMode'] :'';
            $startDate              = isset($data['startDate'])? $data['startDate'] :'';
            $endDate                = isset($data['endDate'])? $data['endDate'] :'';

            if($company){
                $qb->join("store.parent",'c');
                $qb->andWhere("c.id = :company")->setParameter('company', $company);
            }

            if (!empty($category)) {
                $qb->andWhere("category.id = :cid")->setParameter('cid', $category);
            }
            if($wearhouse){
                $qb->andWhere("store.id = :wearhouse")->setParameter('wearhouse', $wearhouse);
            }
            if (!empty($itemMode)) {
                $qb->andWhere('item.itemMode LIKE :searchTerm');
                $qb->setParameter('searchTerm', '%'.$itemMode.'%');
            }
            if (!empty($itemCode)) {
                $qb->andWhere('item.itemCode LIKE :searchTerm');
                $qb->setParameter('searchTerm', '%'.$itemCode.'%');
            }
            if (!empty($productGroup)) {
                $qb->andWhere("productGroup.id = :pid")->setParameter('pid', $productGroup);
            }
            if (!empty($startDate) ) {
                $datetime = new \DateTime($startDate);
                $startDate = $datetime->format('Y-m-d 00:00:00');
                $qb->andWhere("e.created < :startDate");
                $qb->setParameter('startDate', $startDate);
            }
        }
    }

    /**
     * @return Requisition[]
     */
    public function companyWisePrReport( $config,$form = "" )
    {
        $data = $form['report_filter_form'];
        $startDate = isset($data['startDate'])? $data['startDate'] :'';
        $endDate = isset($data['endDate'])? $data['endDate'] :'';
        $branch = isset($data['company'])? $data['company'] :'';
        $department = isset($data['department'])? $data['department'] :'';
        $qb = $this->_em->createQueryBuilder();
        $qb->from(Requisition::class,'e');
        $qb->select('e.id','e.requisitionNo as requisitionNo','e.created as created','e.expectedDate as expectedDate','e.subTotal as subTotal','e.total as total', 'e.process as process','e.processOrdering as ordering', 'e.waitingProcess as waitingProcess', 'e.requisitionMode as requisitionMode', 'e.isEmergency as isEmergency');
        $qb->addSelect('b.name as company');
        $qb->addSelect('d.name as department');
        $qb->addSelect('pd.name as processDepartment');
        $qb->addSelect('sec.name as section');
        $qb->addSelect('t.name as requisitionType');
        $qb->addSelect('p.name as priority');
        $qb->leftJoin('e.section','sec');
        $qb->leftJoin('e.department','d');
        $qb->leftJoin('e.processDepartment','pd');
        $qb->leftJoin('e.requisitionType','t');
        $qb->leftJoin('e.priority','p');
        $qb->leftJoin('e.companyUnit','cu');
        $qb->leftJoin('cu.parent','b');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "Approved");
        if(!empty($department)){
            $qb->andWhere('d.id = :department')->setParameter('department',$department);
        }
        $qb->andWhere('b.id = :branch')->setParameter('branch',$branch);
        $datetime = new \DateTime($startDate);
        $startDate = $datetime->format('Y-m-d 00:00:00');
        $qb->andWhere("e.created >= :startDate");
        $qb->setParameter('startDate', $startDate);
        $datetime = new \DateTime($endDate);
        $endDate = $datetime->format('Y-m-d 23:59:59');
        $qb->andWhere("e.created <= :endDate");
        $qb->setParameter('endDate', $endDate);
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }

    /**
     * @return Requisition[]
     */
    public function departmentWisePrReport( $config,$form = "" )
    {
        $data = $form['report_filter_form'];
        $startDate = isset($data['startDate'])? $data['startDate'] :'';
        $endDate = isset($data['endDate'])? $data['endDate'] :'';
        $branch = isset($data['company'])? $data['company'] :'';
        $department = isset($data['department'])? $data['department'] :'';
        $qb = $this->_em->createQueryBuilder();
        $qb->from(Requisition::class,'e');
        $qb->select('e.id','e.requisitionNo as requisitionNo','e.created as created','e.expectedDate as expectedDate','e.subTotal as subTotal','e.total as total', 'e.process as process','e.processOrdering as ordering', 'e.waitingProcess as waitingProcess', 'e.requisitionMode as requisitionMode', 'e.isEmergency as isEmergency');
        $qb->addSelect('b.name as company');
        $qb->addSelect('d.name as department');
        $qb->addSelect('pd.name as processDepartment');
        $qb->addSelect('sec.name as section');
        $qb->addSelect('t.name as requisitionType');
        $qb->addSelect('p.name as priority');
        $qb->leftJoin('e.section','sec');
        $qb->leftJoin('e.department','d');
        $qb->leftJoin('e.processDepartment','pd');
        $qb->leftJoin('e.requisitionType','t');
        $qb->leftJoin('e.priority','p');
        $qb->leftJoin('e.companyUnit','cu');
        $qb->leftJoin('cu.parent','b');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "Approved");
        if(!empty($branch)){
            $qb->andWhere('b.id = :branch')->setParameter('branch',$branch);
        }
        $qb->andWhere('d.id = :department')->setParameter('department',$department);
        $datetime = new \DateTime($startDate);
        $startDate = $datetime->format('Y-m-d 00:00:00');
        $qb->andWhere("e.created >= :startDate");
        $qb->setParameter('startDate', $startDate);
        $datetime = new \DateTime($endDate);
        $endDate = $datetime->format('Y-m-d 23:59:59');
        $qb->andWhere("e.created <= :endDate");
        $qb->setParameter('endDate', $endDate);
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }

    public function stockBookItemReports( $config, $data ): array
    {
        $em = $this->_em;
        $sort = isset($data['sort'])? $data['sort'] :'e.id';
        $direction = isset($data['direction'])? $data['direction'] :'DESC';
        if (isset($data['procurement_report_filter_form'])) {
            $form = $data['procurement_report_filter_form'];
            $qb = $em->createQueryBuilder();
            $qb->from(StockHistory::class,'e');
            $qb->join('e.wearhouse','store');
            $qb->leftJoin('e.stockBook','sb');
            $qb->leftJoin('sb.item','item');
            $qb->leftJoin('sb.brand','b');
            $qb->leftJoin('sb.category','category');
            $qb->leftJoin('category.generalLedger','gl');
            $qb->leftJoin('sb.brand','brand');
            $qb->leftJoin('sb.size','size');
            $qb->leftJoin('sb.color','color');
            $qb->leftJoin('item.unit','unit');
            $qb->select("sb.id as stockId","sb.price as price","sb.cmp as cmp","sb.updated as updated");
            $qb->addSelect("item.itemCode as itemCode","item.name as name");
            $qb->addSelect("store.id as wearhouse","store.name as storeName");
            $qb->addSelect("item.id as itemId");
            $qb->addSelect("unit.name as unitName");
            $qb->addSelect("category.name as categoryName");
            $qb->addSelect("size.name as sizeName");
            $qb->addSelect("color.name as colorName");
            $qb->addSelect("brand.name as brandName");
            $qb->addSelect("gl.name as glName","gl.generalLedgerCode as glCode");
            $qb->where("e.config = :config")->setParameter('config', $config->getId());
            $this->handleSearchStockBetween($qb,$data);
            $qb->groupBy("sb.id");
            $qb->orderBy("item.name",'ASC');
            $result = $qb->getQuery()->getArrayResult();
            return $result;
        }
        return array();

    }

    public function stockOpeningReport( $config, $data ): array
    {
        $em = $this->_em;
        $sort = isset($data['sort'])? $data['sort'] :'e.id';
        $direction = isset($data['direction'])? $data['direction'] :'DESC';
        if (isset($data['procurement_report_filter_form'])) {
            $form = $data['procurement_report_filter_form'];
            $qb = $em->createQueryBuilder();
            $qb->from(StockHistory::class,'e');
            $qb->join('e.wearhouse','store');
            $qb->leftJoin('e.stockBook','sb');
            $qb->leftJoin('e.item','item');
            $qb->leftJoin('item.unit','unit');
            $qb->select("(SUM(e.stockIn) - SUM(e.stockOut)) as remainingQuantity");
            $qb->addSelect("sb.id as stockId");
            $qb->where("e.config = :config")->setParameter('config', $config->getId());
            $this->handleSearchStockOpeningBetween($qb,$data);
            $qb->groupBy("sb.id");
            $qb->orderBy("item.name",'ASC');
            $result = $qb->getQuery()->getArrayResult();
            $openines = array();
            foreach ($result as $row):
                $openines[$row['stockId']] = $row;
            endforeach;
            return $openines;
        }
        return array();

    }

    public function stockDateRangeReport( $config, $data ): array
    {
        $em = $this->_em;
        $sort = isset($data['sort'])? $data['sort'] :'e.id';
        $direction = isset($data['direction'])? $data['direction'] :'DESC';
        if (isset($data['procurement_report_filter_form'])) {
            $form = $data['procurement_report_filter_form'];
            $qb = $em->createQueryBuilder();
            $qb->from(StockHistory::class,'e');
            $qb->join('e.wearhouse','store');
            $qb->leftJoin('e.stockBook','sb');
            $qb->select("sb.id as stockId");
            $qb->addSelect("SUM(e.stockIn) as stockIn","SUM(e.stockOut) as stockOut");
            $qb->where("e.config = :config")->setParameter('config', $config->getId());
            $this->handleSearchStockBetween($qb,$data);
            $this->handleDateRangeBetween($qb,$data);
            $qb->groupBy("sb.id");
            $result = $qb->getQuery()->getArrayResult();
            $entities = array();
            foreach ($result as $row):
                $entities[$row['stockId']] = $row;
            endforeach;
            return $entities;
        }
        return array();

    }

    public function stockBookCategoryConsumptionReports( $config, $form )
    {
        $em = $this->_em;
        if (isset($form['procurement_report_filter_form'])) {
            $data = $form['procurement_report_filter_form'];
            $category  = isset($data['category'])? $data['category'] :'';
            $qb = $em->createQueryBuilder();
            $qb->from(StockHistory::class,'e');
            $qb->join('e.wearhouse','store');
            $qb->leftJoin('e.stockBook','sb');
            $qb->leftJoin('sb.item','item');
            $qb->leftJoin('sb.brand','b');
            $qb->leftJoin('sb.category','category');
            $qb->leftJoin('category.generalLedger','gl');
            $qb->leftJoin('item.unit','unit');
            $qb->select("sb.id as stockId","sb.price as price","sb.cmp as cmp","sb.updated as updated");
            $qb->addSelect("SUM(e.stockIn) as stockIn","SUM(e.stockOut) as stockOut");
            $qb->addSelect("item.itemCode as itemCode","item.name as name");
            $qb->addSelect("store.id as wearhouse","store.name as storeName");
            $qb->addSelect("item.id as itemId","item.itemMode as itemMode");
            $qb->addSelect("unit.name as unitName");
            $qb->addSelect("category.name as categoryName");
/*            $qb->addSelect("size.name as sizeName");
            $qb->addSelect("color.name as colorName");
            $qb->addSelect("brand.name as brandName");*/
            $qb->addSelect("gl.name as glName","gl.generalLedgerCode as glCode");
            $qb->where("e.config = :config")->setParameter('config', $config->getId());
            $qb->andWhere("category.id = :cid")->setParameter('cid', $category);
            $this->handleDateRangeBetween($qb,$form);
            $qb->groupBy("sb.id");
            $qb->orderBy("item.name",'ASC');
            $result = $qb->getQuery()->getArrayResult();
            return $result;
        }
        return array();

    }

    public function stockBookDepartmentConsumptionReports( $config, $form )
    {
        $em = $this->_em;
        if (isset($form['procurement_report_filter_form'])) {
            $data = $form['procurement_report_filter_form'];
            $qb = $em->createQueryBuilder();
            $qb->from(RequisitionOrderItem::class,'e');
            $qb->join('e.stockBook','sb');
            $qb->join('e.requisitionOrder','ro');
            $qb->join('ro.requisitionIssue','ri');
            $qb->join('ri.createdBy','u');
            $qb->join('ri.department','d');
            $qb->select("d.id as departmentId","d.name as department");
            $qb->addSelect("SUM(e.quantity * sb.price) as price");
            $qb->where("ro.config = :config")->setParameter('config', $config->getId());
            $this->handleDateRangeBetween($qb,$form);
            $qb->groupBy("d.id");
            $qb->orderBy("d.name",'ASC');
            $result = $qb->getQuery()->getArrayResult();
            return $result;
        }
        return array();

    }

    public function itemWisePoReports( $config, $form )
    {
        $em = $this->_em;
        if (isset($form['procurement_report_filter_form'])) {
            $qb = $em->createQueryBuilder();
            $qb->from(TenderWorkorderItem::class,'e');
            $qb->join('e.requisition','r');
            $qb->join('r.department','d');
            $qb->join('e.tenderWorkorder','tw');
            $qb->join('tw.tenderVendor','tv');
            $qb->leftJoin('e.stockBook','sb');
            $qb->leftJoin('sb.item','item');
            $qb->leftJoin('item.unit','unit');
            $qb->select("e.unitPrice as unitPrice","e.price as price","e.quantity as quantity");
            $qb->addSelect("item.itemCode as itemCode","item.name as name");
            $qb->addSelect("item.id as itemId","item.itemMode as itemMode");
            $qb->addSelect("unit.name as unitName");
            $qb->addSelect("tw.created as created","tw.invoice as invoice");
            $qb->addSelect("tv.name as vendor");
            $qb->addSelect("d.name as department");
            $qb->where("tw.config = :config")->setParameter('config', $config->getId());
            $qb->andWhere("tw.waitingProcess = :process")->setParameter('process',"Approved");
            $this->handleDateRangeBetween($qb,$form);
            $qb->orderBy("tw.created",'ASC');
            $result = $qb->getQuery()->getArrayResult();
            return $result;
        }
        return array();

    }

    public function vendorWisePoReports( $config, $form )
    {
        $em = $this->_em;
        if (isset($form['procurement_report_filter_form'])) {
            $data = $form['procurement_report_filter_form'];
            $vendor = isset($data['vendor'])? $data['vendor'] :'';
            $vendorName = $em->getRepository(Vendor::class)->find($vendor);
            $startDate              = isset($data['startDate'])? $data['startDate'] :'';
            $endDate                = isset($data['endDate'])? $data['endDate'] :'';
            $qb = $em->createQueryBuilder();
            $qb->from(TenderWorkorderItem::class,'e');
            $qb->join('e.requisition','r');
            $qb->join('r.department','d');
            $qb->join('e.tenderWorkorder','tw');
            $qb->join('tw.tenderVendor','tv');
            $qb->leftJoin('e.stockBook','sb');
            $qb->leftJoin('sb.item','item');
            $qb->leftJoin('item.unit','unit');
            $qb->select("e.unitPrice as unitPrice","e.price as price","e.quantity as quantity");
            $qb->addSelect("item.itemCode as itemCode","item.name as name");
            $qb->addSelect("item.id as itemId","item.itemMode as itemMode");
            $qb->addSelect("unit.name as unitName");
            $qb->addSelect("tw.created as created","tw.invoice as invoice");
            $qb->addSelect("tv.name as vendor");
            $qb->addSelect("d.name as department");
            $qb->where("tw.config = :config")->setParameter('config', $config->getId());
            $qb->andWhere("tw.waitingProcess = :process")->setParameter('process',"Approved");
            $qb->andWhere("tv.name = :vendorId")->setParameter('vendorId',$vendorName->getName());
            $datetime = new \DateTime($startDate);
            $startDate = $datetime->format('Y-m-d 00:00:00');
            $qb->andWhere("e.created >= :startDate");
            $qb->setParameter('startDate', $startDate);
            $datetime = new \DateTime($endDate);
            $endDate = $datetime->format('Y-m-d 23:59:59');
            $qb->andWhere("e.created <= :endDate");
            $qb->setParameter('endDate', $endDate);
            $qb->orderBy("tw.created",'ASC');
            $result = $qb->getQuery()->getArrayResult();
            return $result;
        }
        return array();
    }

    public function reportItemMonthlyConsumption($config)
    {

        if (isset($form['procurement_report_filter_form'])) {
            $formData = $form['procurement_report_filter_form'];
            $year = isset($formData['year']) ? $formData['year'] : '';
        }else{
            $compare = new \DateTime();
            $year =  $compare->format('Y');
        }
        $sql = "SELECT stock.id as stockId,item.name as itemName
                FROM procu_requisition_order_item as sales
                JOIN inv_stock_book as stock ON sales.stock_book_id = stock.id 
                JOIN  gmb_item as item ON stock.item_id = item.id
                JOIN procu_requisition_order as ro ON sales.requisition_order_id = ro.id
                WHERE ro.config_id = :config AND ro.waiting_process = :process  AND YEAR(sales.updated) =:year
                GROUP BY stock.id ORDER BY item.name ASC";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->bindValue('config', $config);
        $stmt->bindValue('process', 'Approved');
        $stmt->bindValue('year', $year);
        $stmt->execute();
        $items =  $stmt->fetchAll();

        $compare = new \DateTime();
        $year =  $compare->format('Y');
        $year = isset($data['year'])? $data['year'] :$year;
        $sql = "SELECT MONTH (ro.updated) as month,SUM(sales.quantity) AS quantity,SUM(sales.quantity * sales.price) AS subTotal,stock.id as stockId,item.name as itemName
                FROM procu_requisition_order_item as sales
                JOIN inv_stock_book as stock ON sales.stock_book_id = stock.id 
                JOIN  gmb_item as item ON stock.item_id = item.id
                JOIN procu_requisition_order as ro ON sales.requisition_order_id = ro.id
                WHERE ro.config_id = :config AND ro.waiting_process = :process  AND YEAR(sales.updated) =:year
                GROUP BY month,stock.id ORDER BY month ASC";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->bindValue('config', $config);
        $stmt->bindValue('process', 'Approved');
        $stmt->bindValue('year', $year);
        $stmt->execute();
        $result =  $stmt->fetchAll();
        $entities = array();
        foreach ($result as $row){
            $key = "{$row['month']}-{$row['stockId']}";
            $entities[$key] = $row;
        }
        $data = array('items'=>$items,'entities'=>$entities);
        return $data;
    }

    public function reportDepartmentMonthlyConsumption($config,$form)
    {
        if (isset($form['procurement_report_filter_form'])) {
            $formData = $form['procurement_report_filter_form'];
            $year = isset($formData['year']) ? $formData['year'] : '';
        }else{
            $compare = new \DateTime();
            $year =  $compare->format('Y');
        }
        $sql = "SELECT d.id as departmentId , d.name as department
                FROM procu_requisition_order_item as sales
                JOIN procu_requisition_order as ro ON sales.requisition_order_id = ro.id
                JOIN procu_requisition_issue as ri ON ro.requisition_issue_id = ri.id
                JOIN core_setting as d ON ri.department_id = d.id
                WHERE ro.config_id = :config AND ro.waiting_process = :process  AND YEAR(sales.updated) =:year
                GROUP BY d.id ORDER BY d.name ASC";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->bindValue('config', $config);
        $stmt->bindValue('process', 'Approved');
        $stmt->bindValue('year', $year);
        $stmt->execute();
        $departments =  $stmt->fetchAll();

        $compare = new \DateTime();
        $year =  $compare->format('Y');
        $year = isset($data['year'])? $data['year'] :$year;
        $sql = "SELECT MONTH (ro.updated) as month,SUM(sales.quantity * sales.price) AS amount,s.id as departmentId
                FROM procu_requisition_order_item as sales
                JOIN procu_requisition_order as ro ON sales.requisition_order_id = ro.id
                JOIN procu_requisition_issue as ri ON ro.requisition_issue_id = ri.id
                JOIN core_setting as s ON ri.department_id = s.id
                WHERE ro.config_id = :config AND ro.waiting_process = :process  AND YEAR(sales.updated) =:year
                GROUP BY month,s.id ORDER BY month ASC";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->bindValue('config', $config);
        $stmt->bindValue('process', 'Approved');
        $stmt->bindValue('year', $year);
        $stmt->execute();
        $result =  $stmt->fetchAll();
        $entities = array();
        foreach ($result as $row){
            $key = "{$row['month']}-{$row['departmentId']}";
            $entities[$key] = $row;
        }
        $data = array('items' => $departments,'entities'=>$entities);
        return $data;
    }

    public function reportPoSummary($config,$form)
    {

        $compare = new \DateTime();
        $year =  $compare->format('Y');
        $year = isset($data['year'])? $data['year'] :$year;
        $sql = "SELECT COUNT(wo.id) AS quantity,SUM(wo.total) AS price ,r.requisition_mode as mode
                FROM procu_tender_workorder as wo
                JOIN procu_tender_workorder_item as woi ON wo.id = woi.tender_workorder_id
                JOIN procu_requisition as r ON woi.requisition_id = r.id
                WHERE wo.config_id = :config AND wo.waiting_process = :process  AND YEAR(wo.updated) =:year
                GROUP BY r.requisition_mode";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->bindValue('config', $config);
        $stmt->bindValue('process', 'Approved');
        $stmt->bindValue('year', $year);
        $stmt->execute();
        $result =  $stmt->fetchAll();
        return $result;
    }

    public function reportPoSummaryMonthly($config,$form)
    {
        $compare = new \DateTime();
        $year =  $compare->format('Y');
        $year = isset($data['year'])? $data['year'] :$year;
        $sql = "SELECT r.requisition_mode as mode
                FROM procu_tender_workorder as wo
                JOIN procu_tender_workorder_item as woi ON wo.id = woi.tender_workorder_id
                JOIN procu_requisition as r ON woi.requisition_id = r.id
                WHERE wo.config_id = :config AND wo.waiting_process = :process  AND YEAR(wo.updated) =:year
                GROUP BY r.requisition_mode";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->bindValue('config', $config);
        $stmt->bindValue('process', 'Approved');
        $stmt->bindValue('year', $year);
        $stmt->execute();
        $modes =  $stmt->fetchAll();

        $compare = new \DateTime();
        $year =  $compare->format('Y');
        $year = isset($data['year'])? $data['year'] :$year;
        $sql = "SELECT MONTH (wo.updated) as month,COUNT(wo.id) AS quantity,SUM(wo.total) AS price ,r.requisition_mode as mode
                FROM procu_tender_workorder as wo
                JOIN procu_tender_workorder_item as woi ON wo.id = woi.tender_workorder_id
                JOIN procu_requisition as r ON woi.requisition_id = r.id
                WHERE wo.config_id = :config AND wo.waiting_process = :process  AND YEAR(wo.updated) =:year
                GROUP BY month,r.requisition_mode ORDER BY month ASC";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->bindValue('config', $config);
        $stmt->bindValue('process', 'Approved');
        $stmt->bindValue('year', $year);
        $stmt->execute();
        $result =  $stmt->fetchAll();
        $entities = array();
        foreach ($result as $row){
            $key = "{$row['month']}-{$row['mode']}";
            $entities[$key] = $row;
        }
        $data = array('items' => $modes,'entities'=>$entities);
        return $data;
    }




    public function getSupplierSummaryReport($config,$form)
    {
        $em = $this->_em;
        $data = $form['report_filter_form'];
        $startDate = isset($data['startDate']) ? $data['startDate'] : '';
        $endDate = isset($data['endDate']) ? $data['endDate'] : '';

        $qb = $this->_em->createQueryBuilder();
        $qb->from(TenderWorkorder::class, 'e');
        $qb->join('e.tenderVendor','tv');
        $qb->select('SUM(e.total) as amount','COUNT(e.id) as count');
        $qb->addSelect('tv.name as company');
        $qb->groupBy('tv.name');
        $qb->where('e.config = :config')->setParameter('config', $config);
        $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', ["Approved","Closed"]);
        if (!empty($startDate)) {
            $datetime = new \DateTime($startDate);
            $startDate = $datetime->format('Y-m-d 00:00:00');
            $qb->andWhere("e.created >= :startDate");
            $qb->setParameter('startDate', $startDate);
        }
        if (!empty($endDate)) {
            $datetime = new \DateTime($endDate);
            $endDate = $datetime->format('Y-m-d 23:59:59');
            $qb->andWhere("e.created <= :endDate");
            $qb->setParameter('endDate', $endDate);
        }
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

    public function workorderReport($config,$filter)
    {
        $em = $this->_em;
        $data = $filter['report_filter_form'];
        $startDate = isset($data['startDate']) ? $data['startDate'] : '';
        $endDate = isset($data['endDate']) ? $data['endDate'] : '';
        $qb = $this->_em->createQueryBuilder();
        $qb->from(TenderWorkorder::class, 'e');
        $qb->join('e.tenderVendor','tv');
        $qb->select('(e.total) as amount','e.invoice as invoice','e.created as created');
        $qb->addSelect('tv.name as company');
        $qb->where("e.waitingProcess = 'Approved'");
        $qb->orderBy('e.created',"DESC");
        $qb->where('e.config = :config')->setParameter('config', $config);
        $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', ["Approved","Closed"]);
        if (!empty($startDate)) {
            $datetime = new \DateTime($startDate);
            $startDate = $datetime->format('Y-m-d 00:00:00');
            $qb->andWhere("e.created >= :startDate");
            $qb->setParameter('startDate', $startDate);
        }
        if (!empty($endDate)) {
            $datetime = new \DateTime($endDate);
            $endDate = $datetime->format('Y-m-d 23:59:59');
            $qb->andWhere("e.created <= :endDate");
            $qb->setParameter('endDate', $endDate);
        }
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }


}
