<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Repository;

use App\Entity\Admin\AppModule;
use App\Entity\Domain\ModuleProcess;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use phpDocumentor\Reflection\Types\False_;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\ProcurementBundle\Entity\JobRequisition;
use Terminalbd\ProcurementBundle\Entity\OrderDelivery;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionIssue;
use Terminalbd\ProcurementBundle\Entity\RequisitionIssueItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItemHistory;
use Terminalbd\ProcurementBundle\Entity\RequisitionOrder;
use Terminalbd\ProcurementBundle\Entity\RequisitionOrderItem;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class RequisitionOrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RequisitionOrder::class);
    }

    protected function handleSearchBetween($qb, $form)
    {

        if (isset($form['requisition_filter_form'])) {

            $data = $form['requisition_filter_form'];
            $startDate = isset($data['startDate']) ? $data['startDate'] : '';
            $endDate = isset($data['endDate']) ? $data['endDate'] : '';
            $branch = isset($data['branch']) ? $data['branch'] : '';
            $department = isset($data['department']) ? $data['department'] : '';
            $priroty = isset($data['priroty']) ? $data['priroty'] : '';
            $process = isset($data['process']) ? $data['process'] : '';
            $requisitionNo = !empty($data['requisitionNo']) ? $data['requisitionNo'] : '';
            $orderNo = !empty($data['orderNo']) ? $data['orderNo'] : '';
            $styleNo = !empty($data['styleNo']) ? $data['styleNo'] : '';
            $buyerName = !empty($data['buyerName']) ? $data['buyerName'] : '';

            if (!empty($process) && $process == 'RUNNING') {
                $qb->andWhere('e.process IN (:processes)')->setParameter('processes', ['New', 'Checked']);
            }
            if (!empty($process)) {
                $qb->andWhere('e.process = :process')->setParameter('process',$process);
            }
            if (!empty($requisitionNo)) {
                $qb->andWhere($qb->expr()->like("e.requisitionNo", "'%$requisitionNo%'"));
            }
            if (!empty($orderNo)) {
                $qb->andWhere($qb->expr()->like("ro.orderNo", "'%$orderNo%'"));
            }
            if (!empty($styleNo)) {
                $qb->andWhere($qb->expr()->like("e.styleNo", "'%$styleNo%'"));
            }
            if (!empty($buyerName)) {
                $qb->andWhere($qb->expr()->like("e.buyerName", "'%$buyerName%'"));
            }
            if (!empty($branch)) {
                $qb->andWhere('b.id = :branch')->setParameter('branch', $branch);
            }
            if (!empty($department)) {
                $qb->andWhere('d.id = :department')->setParameter('department', $department);
            }
            if (!empty($startDate)) {
                $datetime = new \DateTime($startDate);
                $startDate = $datetime->format('Y-m-d 00:00:00');
                $qb->andWhere("e.created >= :startDate");
                $qb->setParameter('startDate', $startDate);
            }

            if (!empty($endDate)) {
                $datetime = new \DateTime($endDate);
                $endDate = $datetime->format('Y-m-d 23:59:59');
                $qb->andWhere("e.created <= :endDate");
                $qb->setParameter('endDate', $endDate);
            }

        }
    }

    /**
     * @return RequisitionIssue[]
     */
    public function findWithSearchQuery($config, User $user, $data = ""): array
    {
        $em = $this->_em;
        $terminal = $user->getTerminal()->getId();
        $moduleProcess = $em->getRepository(ModuleProcess::class)->existModuleProcess($terminal,'requisition-order');
        $sort = isset($data['sort']) ? $data['sort'] : 'e.created';
        $direction = isset($data['direction']) ? $data['direction'] : 'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id as id','e.orderNo as orderNo','e.created as created', 'e.process as process','e.processOrdering as ordering', 'e.waitingProcess as waitingProcess');
        $qb->addSelect('mp.id as moduleId','mp.approveType as moduleApproveType');
        $qb->addSelect('approve.id as approveId','approve.assignToRole as assignToRole','approve.assignToRoleName as roleToName');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('d.name as department');
        $qb->addSelect('rto.id as reportToId','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->addSelect('r.requisitionNo as requisitionNo','r.id as requisitionId');
        $qb->addSelect('p.name as priority');
        $qb->addSelect('rt.name as requisitionType');
        $qb->join('e.moduleProcess','mp');
        $qb->leftJoin('e.requisition', 'r');
        $qb->leftJoin('r.requisitionType', 'rt');
        $qb->leftJoin('r.priority', 'p');
        $qb->leftJoin('r.branch', 'b');
        $qb->leftJoin('r.department', 'd');
        $qb->leftJoin('e.createdBy', 'u');
        $qb->leftJoin('e.approveProcess', 'approve');
        $qb->leftJoin('approve.assignTo', 'rto');
        $qb->where('e.config = :config')->setParameter('config', $config);
        /*if($data['mode'] == "in-progress") {
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
        }elseif($data['mode'] == "approve" and $moduleProcess->getApproveType() == "role" and !empty($user->getApprovalRole())){
            $qb->andWhere('approve.assignToRole =:role')->setParameter('role',$user->getApprovalRole()->getRoleName());
        }elseif($data['mode'] == "approve" and $moduleProcess->getApproveType() == "user"){
            $qb->andWhere('approve.assignTo =:assignTo')->setParameter('assignTo',"{$user->getId()}");
        }elseif($data['mode'] == "list"){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }elseif($data['mode'] == "archive"){
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "Approved");
        }
        if(!empty($user->getProfile()) and $user->getProfile()->getBranch()){
            $branchId = $user->getProfile()->getBranch()->getId();
            $qb->andWhere('b.id = :branchId')->setParameter('branchId',$branchId);
        }elseif(!empty($user->getProfile()) and $user->getProfile()->getDepartment()){
            $dId = $user->getProfile()->getDepartment()->getId();
            $qb->andWhere('d.id = :dId')->setParameter('dId',$dId);
        }*/
        $this->handleSearchBetween($qb, $data);
        $qb->orderBy("{$sort}", $direction);
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }


    /**
     * @return RequisitionIssue[]
     */
    public function requisitionOrders($config, User $user, $data = ""): array
    {
        $em = $this->_em;
        $terminal = $user->getTerminal()->getId();
        $moduleProcess = $em->getRepository(ModuleProcess::class)->existModuleProcess($terminal,'requisition-order');
        $sort = isset($data['sort']) ? $data['sort'] : 'e.created';
        $direction = isset($data['direction']) ? $data['direction'] : 'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id as id','e.orderNo as orderNo','e.created as created', 'e.process as process','e.processOrdering as ordering', 'e.waitingProcess as waitingProcess');
        $qb->addSelect('mp.id as moduleId','mp.approveType as moduleApproveType');
        $qb->addSelect('approve.id as approveId','approve.assignToRole as assignToRole','approve.assignToRoleName as roleToName');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('d.name as department');
        $qb->addSelect('rto.id as reportToId','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->addSelect('r.requisitionNo as requisitionNo','r.id as requisitionId');
        $qb->addSelect('p.name as priority');
        $qb->addSelect('rt.name as requisitionType');
        $qb->join('e.moduleProcess','mp');
        $qb->leftJoin('e.requisition', 'r');
        $qb->leftJoin('r.requisitionType', 'rt');
        $qb->leftJoin('r.priority', 'p');
        $qb->leftJoin('r.branch', 'b');
        $qb->leftJoin('r.department', 'd');
        $qb->leftJoin('e.createdBy', 'u');
        $qb->leftJoin('e.approveProcess', 'approve');
        $qb->leftJoin('approve.assignTo', 'rto');
        $qb->where('e.config = :config')->setParameter('config', $config);
        $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "Approved");
        /*if($data['mode'] == "in-progress") {
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
        }elseif($data['mode'] == "approve" and $moduleProcess->getApproveType() == "role" and !empty($user->getApprovalRole())){
            $qb->andWhere('approve.assignToRole =:role')->setParameter('role',$user->getApprovalRole()->getRoleName());
        }elseif($data['mode'] == "approve" and $moduleProcess->getApproveType() == "user"){
            $qb->andWhere('approve.assignTo =:assignTo')->setParameter('assignTo',"{$user->getId()}");
        }elseif($data['mode'] == "list"){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }elseif($data['mode'] == "archive"){
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "Approved");
        }
        if(!empty($user->getProfile()) and $user->getProfile()->getBranch()){
            $branchId = $user->getProfile()->getBranch()->getId();
            $qb->andWhere('b.id = :branchId')->setParameter('branchId',$branchId);
        }elseif(!empty($user->getProfile()) and $user->getProfile()->getDepartment()){
            $dId = $user->getProfile()->getDepartment()->getId();
            $qb->andWhere('d.id = :dId')->setParameter('dId',$dId);
        }*/
        $this->handleSearchBetween($qb, $data);
        $qb->orderBy("{$sort}", $direction);
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }

    /**
     * @return RequisitionIssue[]
     */
    public function findRequisitionSearchQuery($config, User $user, $data = ""): array
    {

        $sort = isset($data['sort']) ? $data['sort'] : 'e.created';
        $direction = isset($data['direction']) ? $data['direction'] : 'DESC';
        $qb = $this->createQueryBuilder('ro');
        $qb->select('e.id as requisitionIssueId', 'e.requisitionNo as requisitionNo','e.processOrdering as ordering', 'e.buyerName as buyerName', 'e.subTotal as subTotal', 'e.total as total');
        $qb->addSelect('ro.id as id','ro.orderNo as orderNo','ro.created as created', 'ro.process as process', 'ro.waitingProcess as waitingProcess');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('d.name as department');
        $qb->addSelect('rto.id as reportToId','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->leftJoin('ro.requisitionIssue', 'e');
        $qb->leftJoin('e.branch', 'b');
        $qb->leftJoin('e.department', 'd');
        $qb->leftJoin('e.createdBy', 'u');
        $qb->leftJoin('ro.reportTo', 'rto');
        $qb->where('e.config = :config')->setParameter('config', $config);
        if($data['mode'] == "in-progress") {
            $qb->andWhere('ro.waitingProcess =:process')->setParameter('process', "In-progress");
        }elseif($data['mode'] == "approve"){
            $qb->andWhere('ro.reportTo =:report')->setParameter('report',"{$user->getId()}");
        }elseif($data['mode'] == "list"){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }elseif($data['mode'] == "archive"){
            $qb->andWhere('ro.waitingProcess =:process')->setParameter('process', "Approved");
        }
        $this->handleSearchBetween($qb, $data);
        $qb->orderBy("{$sort}", $direction);
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }

    public function insertOrderOld(Requisition $requisition,$deliveryModes,$data, $user)
    {


        $em = $this->_em;
        $entity = new RequisitionOrder();
        $entity->setConfig($requisition->getConfig());
        $entity->setRequisition($requisition);
        $entity->setProcess('New');
        $entity->setCreatedBy($user);
        $em->persist($entity);
        $em->flush();

        foreach ($deliveryModes as $mode){

            $particular = $em->getRepository(Particular::class)->find($mode['id']);
            //  $exist = $this->findOneBy(array('requisitionOrder'=> $entity,'deliveryMethod' => $particular));
            $delivery = new OrderDelivery();
            $delivery->setRequisitionOrder($entity);
            $delivery->setDeliveryMethod($particular);
            $delivery->setConfig($requisition->getConfig());
            $delivery->setCreatedBy($user);
            $em->persist($delivery);
            $em->flush();

            foreach ($data['requisitionItem'] as $key => $item){

                if($delivery->getDeliveryMethod()->getId() == $data['deliveryMethod'][$key]){
                    $quantity = empty($data['quantity'][$key]) ? 0 :$data['quantity'][$key];
                    $purchaseItem = $em->getRepository(RequisitionItem::class)->find($item);
                    $orderItem = new RequisitionOrderItem();
                    $orderItem->setRequisitionOrder($entity);
                    $orderItem->setRequisitionItem($purchaseItem);
                    $orderItem->setStock($purchaseItem->getStock());
                    $orderItem->setOrderDelivery($delivery);
                    $orderItem->setQuantity($quantity);
                    $orderItem->setPrice($purchaseItem->getStock()->getPrice());
                    $subTotal = (float)($orderItem->getPrice() * $orderItem->getQuantity());
                    $orderItem->setSubTotal($subTotal);
                    $em->persist($orderItem);
                    $em->flush();
                }
            }
            $em->getRepository(RequisitionOrderItem::class)->getSumSubTotal($delivery);
         //   $this->removeOrder($delivery);

        }
        return $entity;
    }

    public function insertOrder(Requisition $requisition,User $user,$data)
    {
        $em = $this->_em;
        $deliveryMode = $data['store_credit_note_form']['deliveryMethod'];
        $comment = $data['store_credit_note_form']['comment'];
        $method = $em->getRepository(Particular::class)->findOneBy(['id' => $deliveryMode]);
        if ($method) {
            $order = new RequisitionOrder();
            $order->setConfig($requisition->getConfig());
            $terminal = $user->getTerminal()->getId();
            $moduleProcess = $em->getRepository(ModuleProcess::class)->existModuleProcess($terminal, "requisition-order");
            if (!empty($moduleProcess)) {
                $order->setModuleProcess($moduleProcess);
                $order->setModule($moduleProcess->getModule()->getSlug());
            }
            $order->setContent($comment);
            $order->setRequisition($requisition);
            $order->setProcess('New');
            $order->setWaitingProcess('New');
            $order->setCreatedBy($user);
            $em->persist($order);
            $em->flush();
            if ($order->getModuleProcess()->getApproveType() == "user") {
                $em->getRepository(ProcurementProcess::class)->insertProcurementProcessAssign($order);
                $em->getRepository(ProcurementProcess::class)->approvalAssign($order);
            } elseif ($order->getModuleProcess()->getApproveType() == "role") {
                $em->getRepository(ProcurementProcess::class)->insertRoleProcessAssign($order);
                $em->getRepository(ProcurementProcess::class)->approvalRoleAssign($order);
            }
            $delivery = new OrderDelivery();
            $delivery->setRequisitionOrder($order);
            $delivery->setDeliveryMethod($method);
            $delivery->setConfig($requisition->getConfig());
            $delivery->setCreatedBy($user);
            $delivery->setComment($comment);
            /*if($method->getSlug() == "courier"){
                $courier = $em->getRepository(Particular::class)->find($data['courier']);
                $delivery->setCourier($courier);
                $delivery->setCnNo($data['cnNo']);
                $delivery->setComments($data['comments']);
            }elseif ($data['deliveryMode'] == "hand-delivery"){
                $delivery->setReceiverName($data['receiverName']);
                $delivery->setReceiverDesignation($data['receiverDesignation']);
                $delivery->setReceiverAddress($data['receiverAddress']);
            }*/
            $em->persist($delivery);
            $em->flush();
            foreach ($data['requisitionItem'] as $key => $item) {
                $quantity = empty($data['quantity'][$key]) ? 0 : $data['quantity'][$key];
                $purchaseItem = $em->getRepository(RequisitionItem::class)->find($item);
                $orderItem = new RequisitionOrderItem();
                $orderItem->setRequisitionOrder($order);
                $orderItem->setRequisitionItem($purchaseItem);
                $orderItem->setStock($purchaseItem->getStock());
                $orderItem->setOrderDelivery($delivery);
                $orderItem->setQuantity($quantity);
                $orderItem->setPrice($purchaseItem->getPrice());
                $subTotal = (float)($orderItem->getPrice() * $orderItem->getQuantity());
                $orderItem->setSubTotal($subTotal);
                $em->persist($orderItem);
                $em->flush();
            }
            $em->getRepository(RequisitionOrderItem::class)->getSumSubTotal($delivery);
            return $order;
        }
        return false;
    }

    private function removeOrder(OrderDelivery $delivery){
        $em = $this->_em;
       $exist = $em->getRepository(RequisitionOrderItem::class)->findBy(array('orderDelivery'=>$delivery));
        if(empty($exist)){
            $em->remove($delivery);
            $em->flush();
        }

    }

    public function requisitionItemHistory(Requisition $requisition)
    {
        $em =$this->_em;
        /* @var $item RequisitionItem */

        if(!empty($requisition->getRequisitionItems())){

            foreach ($requisition->getRequisitionItems() as $item){
                $entity = new RequisitionItemHistory();
                $entity->setRequisitionItem($item);
                $entity->setActualQuantity($item->getActualQuantity());
                $entity->setQuantity($item->getQuantity());
                $em->persist($entity);
                $em->flush();

            }
        }
    }

    public function bankRequisitionOrderItem(OrderDelivery $delivery, $data)
    {
        $em = $this->_em;
        foreach ($data['requisitionItem'] as $key => $item) {
            $quantity = empty($data['quantity'][$key]) ? 0 : $data['quantity'][$key];
            $purchaseItem = $em->getRepository(RequisitionItem::class)->find($item);
            $exist = $em->getRepository(RequisitionOrderItem::class)->findOneBy(array('orderDelivery'=>$delivery,'requisitionItem'=>$purchaseItem));
            if(empty($exist)){
                $orderItem = new RequisitionOrderItem();
                $orderItem->setRequisitionOrder($delivery->getRequisitionOrder());
                $orderItem->setRequisitionItem($purchaseItem);
                $orderItem->setStock($purchaseItem->getStock());
                $orderItem->setOrderDelivery($delivery);
                $orderItem->setQuantity($quantity);
                $orderItem->setPrice($purchaseItem->getStock()->getPurchasePrice());
                $subTotal = (float)($orderItem->getPrice() * $orderItem->getQuantity());
                $orderItem->setSubTotal($subTotal);
                $em->persist($orderItem);
                $em->flush();
                
            }else{

                $exist->setQuantity($quantity);
                $exist->setPrice($purchaseItem->getStock()->getPurchasePrice());
                $subTotal = (float)($exist->getPrice() * $exist->getQuantity());
                $exist->setSubTotal($subTotal);
                $em->persist($exist);
                $em->flush();
            }

        }
        $order = $em->getRepository(RequisitionOrderItem::class)->getSumSubTotal($delivery);
        return $order;
    }

    public function insertOrderIssueItem(RequisitionOrder $requisition,$data)
    {
        $em =$this->_em;

        /* @var $item RequisitionItem */
        $requisitionIssueItem = $data['requisitionIssueItem'];
            foreach ($requisitionIssueItem as $key => $value){
                if(isset($data['issueQuantity'][$value]) and $data['issueQuantity'][$value] > 0){
                    $item = $em->getRepository(RequisitionIssueItem::class)->find(array('id'=>"{$value}"));
                    $exist = $em->getRepository(RequisitionOrderItem::class)->findOneBy(array('requisitionOrder'=> $requisition,'requisitionIssueItem'=>$item));
                    if(empty($exist)){
                        $quantity = $data['issueQuantity'][$value] ;
                        $entity = new RequisitionOrderItem();
                        $entity->setRequisitionOrder($requisition);
                        $entity->setRequisitionIssueItem($item);
                        $entity->setStockBook($item->getStockBook());
                        $entity->setQuantity($quantity);
                        $em->persist($entity);
                        $em->flush();
                    }
                }

            }
        }

    public function orderItemStockItemUpdate(Stock $stock)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.requsitionOrderItem', 'mp');
        $qb->select('SUM(e.quantity) AS quantity');
        $qb->where('e.stock = :stock')->setParameter('stock', $stock->getId());
     //   $qb->andWhere('mp.process = :process')->setParameter('process', 'Approved');
        $qnt = $qb->getQuery()->getOneOrNullResult();
        return $qnt['quantity'];
    }

    public function insertOrderFromRequisitionIssue(RequisitionIssue $requisitionIssue)
    {
        $em = $this->_em;
        $module = "order-issue";
        $terminal = $requisitionIssue->getCreatedBy()->getTerminal();
        $entity = new RequisitionOrder();
        $moduleProcess = $em->getRepository(ModuleProcess::class)->existModuleProcess($terminal,$module);
        if(!empty($moduleProcess)) {
            $entity->setModuleProcess($moduleProcess);
        }
        $entity->setRequisitionIssue($requisitionIssue);
        $entity->setConfig($requisitionIssue->getConfig());
        $entity->setCreatedBy($requisitionIssue->getCreatedBy());
        $entity->setBusinessGroup($requisitionIssue->getBusinessGroup());
        $entity->setProcess("New");
        $entity->setWaitingProcess("New");
        $entity->setModule("order-issue");
        $entity->setDepartment($requisitionIssue->getDepartment());
        $em->persist($entity);
        $em->flush();
        return $entity;

    }



}
