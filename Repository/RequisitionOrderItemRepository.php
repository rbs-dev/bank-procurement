<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\InventoryBundle\Entity\StockBook;
use Terminalbd\ProcurementBundle\Entity\OrderDelivery;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionOrderItem;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class RequisitionOrderItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RequisitionOrderItem::class);
    }

    public function getSumSubTotal(OrderDelivery $order)
    {
        $id = $order->getId();
        $em = $this->_em;
        $qb = $this->createQueryBuilder('e');
        $qb->select('SUM(e.subTotal) as subTotal');
        $qb->join('e.orderDelivery','i');
        $qb->where("i.id = '{$id}'");
        $result = $qb->getQuery()->getOneOrNullResult();
        if(!empty($result) and !empty($result['subTotal'])){
            $subTotal = $result['subTotal'];
            $order->setSubTotal($subTotal);
        }else{
            $order->setSubTotal(0);
        }
        $em->persist($order);
        $em->flush();
        return $order;

    }
    


    public function getCalculationVat($totalAmount)
    {
        $vat = ( ($totalAmount * (int)10)/100 );
        //$vat = ( ($totalAmount * (int)$sales->getRestaurantConfig()->getVatPercentage())/100 );
        return round($vat);
    }

    public function insertStoreItem(Requisition $requisition, $data)
    {

        $em = $this->_em;

        /* @var $stock Stock */

        $stock = $em->getRepository(Stock::class)->findOneBy(array('item' => $data['item']));
        $lastReceiveItem = $this->getLastBranchItem($requisition,$stock);

        if($stock and $data['quantity'] > 0){
            $exist = $this->findOneBy(array('requisition'=> $requisition,'stock' => $stock));
            if(empty($exist)){
                $entity = new RequisitionItem();
                $entity->setRequisition($requisition);
                $entity->setQuantity($data['quantity']);
                $entity->setActualQuantity($data['quantity']);
                $entity->setRemainigQuantity($data['quantity']);
                $entity->setStockIn($data['stockIn']);
                $entity->setDescription($data['description']);
                $entity->setStock($stock);
                $entity->setItem($stock->getItem());
                $entity->setName($stock->getItem()->getName());
                $entity->setPrice($stock->getSalesPrice());
                if(!empty($lastReceiveItem)){
                    $item = $this->find($lastReceiveItem);
                    $entity->setLastRequisitionItem($item);
                }
                $entity->setPrice($stock->getSalesPrice());
                $entity->setSubTotal($stock->getSalesPrice() * $entity->getQuantity());
                $em->persist($entity);
                $em->flush();
            }else{
                $entity = $exist;
                $entity->setQuantity($data['quantity']);
                $entity->setRemainigQuantity($data['quantity']);
                $entity->setStockIn($data['stockIn']);
                $entity->setDescription($data['description']);
                $entity->setSubTotal($stock->getSalesPrice() * $entity->getQuantity());
                $em->persist($entity);
                $em->flush();
            }

        }

    }

    public function updateStoreItem(RequisitionItem $item,$quantity)
    {
        $em = $this->_em;
        $item->setQuantity($quantity);
        $item->setActualQuantity($quantity);
        $item->setRemainigQuantity($quantity);
        $item->setSubTotal($quantity * $item->getPrice());
        $em->persist($item);
        $em->flush();
    }

    public function orderItemStockItemUpdate(Stock $stockItem)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.requisitionOrder', 'mp');
        $qb->select('SUM(e.quantity) AS quantity');
        $qb->where('e.stock = :stock')->setParameter('stock', $stockItem->getId());
       // $qb->andWhere('mp.process = :process')->setParameter('process', 'Approved');
        $qnt = $qb->getQuery()->getSingleScalarResult();
        return $qnt;
    }

    public function getLastBranchItem(Requisition $requisition,$stock)
    {

        $workingArea = $requisition->getCreatedBy()->getProfile()->getServiceMode()->getSlug();
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.requisition','r');
        $qb->leftJoin('r.branch','b');
        $qb->leftJoin('r.department','d');
        $qb->select('e.id as id');
        $qb->where('e.stock = :stock')->setParameter('stock',$stock->getId());
        if($workingArea == "branch" and !empty($requisition->getBranch())){
            $qb->andWhere('b.id = :id')->setParameter('id',"{$requisition->getBranch()->getId()}");
        }elseif($workingArea == "department" and !empty($requisition->getDepartment())){
            $qb->andWhere('d.id = :id')->setParameter('id',"{$requisition->getDepartment()->getId()}");
        }
        $qb->setMaxResults(1);
        $qb->orderBy('e.id',"DESC");
        $result = $qb->getQuery()->getOneOrNullResult();
        if($result){
            $lastId = $result['id'];
        }else{
            $lastId = '';
        }
        return $lastId;
    }

    public function requisitionOrderStockBookItemUpdate(StockBook $stockBook)
    {

        $qb = $this->createQueryBuilder('e');
        $qb->join('e.requisitionOrder', 'mp');
        $qb->select('SUM(e.quantity) AS quantity');
        $qb->where('e.stockBook = :stock')->setParameter('stock', $stockBook->getId());
        $qb->andWhere('mp.waitingProcess = :process')->setParameter('process', 'Approved');
        $qnt = $qb->getQuery()->getOneOrNullResult();
        return $qnt['quantity'];
    }

}
