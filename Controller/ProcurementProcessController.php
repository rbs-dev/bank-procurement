<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Controller;

use App\Entity\Application\GenericMaster;
use App\Entity\Application\Procurement;
use App\Entity\Domain\ModuleProcess;
use App\Entity\User;
use App\Repository\Application\GenericMasterRepository;
use App\Repository\Application\InventoryRepository;
use App\Repository\Application\ProcurementRepository;
use App\Repository\Domain\ApprovalUserRepository;
use ContainerBlo4KOZ\getRequisitionItemRepositoryService;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\DmsBundle\Entity\DmsFile;
use Terminalbd\DmsBundle\Repository\DmsFileRepository;
use Terminalbd\InventoryBundle\Repository\StockRepository;
use Terminalbd\ProcurementBundle\Entity\JobRequisition;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionIssue;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItemHistory;
use Terminalbd\ProcurementBundle\Entity\RequisitionOrder;
use Terminalbd\ProcurementBundle\Entity\Tender;
use Terminalbd\ProcurementBundle\Entity\TenderComparative;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorder;
use Terminalbd\ProcurementBundle\Form\ProcurementProcessFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFormType;
use Terminalbd\ProcurementBundle\Repository\JobRequisitionAdditionalItemRepository;
use Terminalbd\ProcurementBundle\Repository\JobRequisitionRepository;
use Terminalbd\ProcurementBundle\Repository\ProcurementProcessRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionItemRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionRepository;


/**
 * @Route("/procurement/process")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ProcurementProcessController extends AbstractController
{

    /**
     * Status a ProcurementProcess entity.
     *
     * @Route("/{id}/{module}/process-form", methods={"GET"}, name="procure_requisition_process_form" , options={"expose"=true})
     */

    public function processUserForm(Request $request,$id,$module,ProcurementRepository $procurementRepository , ApprovalUserRepository $approvalUserRepository , ProcurementProcessRepository $processRepository)
    {
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $em = $this->getDoctrine()->getManager();
        $entity = $processRepository->getEntityCheck($id,$module);
        /** @var  $moduleProcess ModuleProcess */
        $moduleProcess = $entity->getModuleProcess();
        $approvals = $processRepository->findBy(array('entityId'=>$id,'module'=>$module),array('ordering'=>'ASC'));
        if(empty($approvals)){
            // $assignUsers = $approvalUserRepository->getApprovalAssignUser($terminal,$entity);
           // $processRepository->insertProcurementProcessAssign($entity,$entity->getModule(),$assignUsers);
        }
        if($moduleProcess->isApproverForm() == 1 ){
            $approveUsers = $processRepository->getExistApprovalUsers($entity->getId(),$entity->getModule());
            $assignForm = $this->createForm(ProcurementProcessFormType::class, new ProcurementProcess() ,array('config' => $config,'users' => $approveUsers));
            return $this->render('@TerminalbdProcurement/procurement-process/form-approver.html.twig', [
                'entity'                => $entity,
                'moduleProcess'         => $moduleProcess,
                'approvals'             => $approvals,
                'assignForm'            => $assignForm->createView(),
            ]);
        }else{
            return $this->render('@TerminalbdProcurement/procurement-process/form-approver.html.twig', [
                'entity'                => $entity,
                'moduleProcess'         => $moduleProcess,
                'approvals'             => $approvals,
            ]);
        }

    }

    /**
     * Status a ProcurementProcess entity.
     *
     * @Route("/{id}/{module}/process-role", methods={"GET"}, name="procure_requisition_bank_process_role" , options={"expose"=true})
     */
    public function processRoleForm(Request $request,$id,$module,ProcurementRepository $procurementRepository , ApprovalUserRepository $approvalUserRepository , ProcurementProcessRepository $processRepository)
    {

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $em = $this->getDoctrine()->getManager();
        $entity = $processRepository->getEntityCheck($id,$module);
        /** @var  $moduleProcess ModuleProcess */
        $moduleProcess = $entity->getModuleProcess();
        $approvals = $processRepository->findBy(array('entityId'=>$id,'module'=> $module),array('ordering'=>'ASC'));
        if(empty($approvals)){
            $processRepository->insertRoleProcessAssign($entity);
        }
        if($moduleProcess->isApproverForm() == 1 ){
            $approveUsers = $processRepository->getExistApprovalUsers($entity->getId(),$entity->getModule());
            $assignForm = $this->createForm(ProcurementProcessFormType::class, new ProcurementProcess() ,array('config' => $config,'users' => $approveUsers));
            return $this->render('@TerminalbdProcurement/procurement-process/role/form-approver.html.twig', [
                'entity'                => $entity,
                'moduleProcess'         => $moduleProcess,
                'approvals'             => $approvals,
                'assignForm'            => $assignForm->createView(),
            ]);
        }else{
            return $this->render('@TerminalbdProcurement/procurement-process/role/form-approver.html.twig', [
                'entity'                => $entity,
                'moduleProcess'         => $moduleProcess,
                'approvals'             => $approvals,
            ]);
        }

    }
    

    /**
     * Show a ApprovalProcess entity.
     *
     * @Route("/{id}/{module}/create", methods={"GET","POST"}, name="procure_process_create" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function create(Request $request, $id ,$module,ProcurementRepository $procurementRepository, ProcurementProcessRepository $processRepository): Response
    {
        /* @var $config  Procurement */

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);

        $em = $this->getDoctrine()->getManager();
        $data = $request->request->all();
        $formData = $data['procurement_process_form'];
        $assignUser = $formData["assignTo"];
        $process = $formData["process"];
        $isMandatory = isset($formData["isMandatory"]) ? 1:NULL;
        $assign = $this->getDoctrine()->getRepository(User::class)->find($assignUser);
        $ordering = $this->getDoctrine()->getRepository(ProcurementProcess::class)->count(array('entityId'=>$id,'module'=>$module));
        $entity = $processRepository->getEntityCheck($id,$module);
        $exist = $this->getDoctrine()->getRepository(ProcurementProcess::class)->findOneBy(array('entityId'=> $id,'assignTo'=>$assign));
        if(empty($exist)){
            $approval = new ProcurementProcess();
            $approval->setConfig($config);
            $approval->setEntityId($id);
            $approval->setModule($module);
            $approval->setAssignTo($assign);
            $approval->setProcess($process);
            $approval->setIsMandatory($isMandatory);
            $approval->setCustom(true);
            $approval->setOrdering($ordering+1);
            if(isset($data['isMandatory'])){
                $approval->setIsMandatory(true);
            }
            $em->persist($approval);
            $em->flush();
        }
        $approvals = $processRepository->findBy(array('entityId'=>$id,'module'=>$module),array('ordering'=>'ASC'));
        $html = $this->renderView(
            '@TerminalbdProcurement/procurement-process/process.html.twig', array(
                'entity' => $entity,
                'procurementProcess' => $approvals
            )
        );
        return new Response($html);
    }

    /**
     * Status a ProcurementProcess entity.
     * @Route("/{id}/{module}/attachment-form", methods={"GET"}, name="procure_requisition_attachment_form" , options={"expose"=true})
     */
    public function attachmentForm(Request $request,$id,$module,ProcurementProcessRepository $processRepository)
    {
        $attchments = $processRepository->getDmsAttchmentFile($id,$module);
        return $this->render('@TerminalbdProcurement/procurement-process/form-attachment.html.twig',['attchments'=>$attchments]);
    }

    /**
     * Status a ProcurementProcess entity.
     * @Route("/{id}/{module}/attachment-files", methods={"GET"}, name="procure_requisition_attachmentfiles" , options={"expose"=true})
     */
    public function attachmentFiles(Request $request,$id,$module,ProcurementProcessRepository $processRepository)
    {
        $attchments = $processRepository->getDmsAttchmentFile($id,$module);
        return $this->render('@TerminalbdProcurement/procurement-process/form-attachment.html.twig',['attchments'=>$attchments]);
    }

    /**
     * Status a ProcurementProcess entity.
     * @Route("/{id}/{module}/approve-committee", methods={"GET"}, name="procure_process_approve_committee" , options={"expose"=true})
     */
    public function approveCommittee(Request $request,$id,$module,ProcurementProcessRepository $processRepository)
    {
        $entity = $processRepository->getEntityCheck($id,$module);
        $approvals = $processRepository->findBy(array('entityId'=>$id,'module'=>$module),array('ordering'=>'ASC'));
        return $this->render('@TerminalbdProcurement/procurement-process/approve-committee.html.twig',
            [
                'entityId' => $id,
                'entity' => $entity,
                'approvals' => $approvals,
            ]
        );
    }

     /**
     * Status a ProcurementProcess entity.
     * @Route("/{id}/{module}/approve-process", methods={"GET"}, name="procure_requisition_attachment_form" , options={"expose"=true})
     */
    public function approveUers(Request $request,$id,$module,ProcurementProcessRepository $processRepository)
    {
        $entity = $processRepository->getEntityCheck($id,$module);
        $approvals = $processRepository->findBy(array('entityId'=>$id,'module'=>$module),array('ordering'=>'ASC'));
        $attchments = $processRepository->getDmsAttchmentFile($id,$module);
        return $this->render('@TerminalbdProcurement/procurement-process/approve-process.html.twig',
            [
                'entityId' => $id,
                'entity' => $entity,
                'approvals' => $approvals,
                'attchments' => $attchments
            ]
        );
    }

    /**
     * Status a ProcurementProcess entity.
     * @Route("/{id}/{module}/approve-roles", methods={"GET"}, name="procure_requisition_attachment_form" , options={"expose"=true})
     */
    public function approveRoles(Request $request,$id,$module,ProcurementProcessRepository $processRepository)
    {
        $approvals = $processRepository->findBy(array('entityId'=>$id,'module'=>$module),array('ordering'=>'ASC'));
        $attchments = $processRepository->getDmsAttchmentFile($id,$module);
        return $this->render('@TerminalbdProcurement/procurement-process/role/approve-process.html.twig',
            [
                'entityId' => $id,
                'approvals' => $approvals,
                'attchments' => $attchments
            ]
        );
    }



    /**
     * Show a ApprovalProcess entity.
     *
     * @Route("/{id}/{module}/bank-process", methods={"GET","POST"}, name="procure_process_bank" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function bankProcess(Request $request, $id ,$module,ProcurementRepository $procurementRepository, ProcurementProcessRepository $processRepository): Response
    {
        /* @var $config  Procurement */

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);

        $em = $this->getDoctrine()->getManager();
        $data = $request->request->all();
        $formData = $data['procurement_bank_process_form'];
        $assignUser = $formData["assignTo"];
        $process = $formData["process"];
        $isMandatory = isset($formData["isMandatory"]) ? 1:NULL;
        $assign = $this->getDoctrine()->getRepository(User::class)->find($assignUser);
        $ordering = $this->getDoctrine()->getRepository(ProcurementProcess::class)->count(array('entityId'=>$id,'module'=>$module));
        $exist = $this->getDoctrine()->getRepository(ProcurementProcess::class)->findOneBy(array('entityId'=> $id,'assignTo'=>$assign));
        /* @var $requisition Requisition */
        $requisition = $this->getDoctrine()->getRepository(Requisition::class)->find($id);
        if(empty($exist)){
            $approval = new ProcurementProcess();
            $approval->setConfig($config);
            $approval->setRequisition($requisition);
            $approval->setEntityId($id);
            $approval->setModule($requisition->getRequisitionType()->getSlug());
            $approval->setAssignTo($assign);
            $approval->setProcess($process);
            $approval->setIsMandatory(false);
            $approval->setCustom(true);
            $approval->setOrdering($ordering+1);
            $em->persist($approval);
            $em->flush();
        }
        $html = $this->renderView(
            '@TerminalbdProcurement/procurement-process/bank-process.html.twig', array(
                'entity' => $requisition,
                'procurementProcess' => $requisition->getProcurementProcess()
            )
        );
        return new Response($html);
    }

    /**
     * Deletes a ApprovalProcess entity.
     *
     * @Route("/{id}/{entity}/{module}/approval-user-delete", methods={"GET"}, name="procure_process_delete")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function deleteApprovalUser($id,$entity,$module): Response
    {
        $em = $this->getDoctrine()->getManager();
        $removeEntity = $this->getDoctrine()->getRepository(ProcurementProcess::class)->findOneBy(array('id' => "{$id}"));
        $response = "invalid";
        if (!$removeEntity) {
            throw $this->createNotFoundException('Unable to find procurement process entity.');
        }
        try {
            $em->remove($removeEntity);
            $em->flush();
            $response = 'valid';
            $this->getDoctrine()->getRepository(ProcurementProcess::class)->updateOrdering($entity,$module);

        } catch (ForeignKeyConstraintViolationException $e) {
            $this->get('session')->getFlashBag()->add(
                'notice',"Data has been relation another Table"
            );
        }catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add(
                'notice', 'Please contact system administrator further notification.'
            );
        }
        return new Response($response);
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/status", methods={"GET"}, name="procure_process_status" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_PROCUREMENT_ADMIN') or is_granted('ROLE_DOMAIN')")
     */
    public function status($id, ProcurementProcessRepository $repository): Response
    {
        $entity = $repository->find($id);
        $status = $_REQUEST['status'];
        if($status == "false"){
            $entity->setIsRejected(false);
        }else{
            $entity->setIsRejected(true);
        }
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/attachment-delete", methods={"GET"}, name="procure_process_attachment_delete" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_PROCUREMENT_ADMIN') or is_granted('ROLE_DOMAIN')")
     */
    public function attachmentDelete(DmsFile $file): Response
    {

        $em = $this->getDoctrine()->getManager();
        $filepath = $this->get('kernel')->getProjectDir() . "/public/uploads/procurement/{$file->getFilename()}";
        if (!empty($file->getFilename()) and file_exists($filepath))
        {
            unlink($filepath);
        }
        $em->remove($file);
        $em->flush();
        return new Response('success');
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/process-approval-ordering", methods={"GET"}, name="procure_process_ordering" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function invoiceItemOrdering(ProcurementProcessRepository $repository): Response
    {

        $data = $_REQUEST;
        $id = $data['columnId'];
        $sorting = ((integer)$data['sorting']);
        $entity = $repository->find($id);
        $entity->setOrdering($sorting);
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');

    }

    /**
     * Status a ProcurementProcess entity.
     *
     * @Route("/{id}/{module}/template-process", methods={"GET"}, name="procure_requisition_template_process_form" , options={"expose"=true})
     */

    public function templateDataProcess(Request $request,$id,$module,ProcurementRepository $procurementRepository , ApprovalUserRepository $approvalUserRepository , ProcurementProcessRepository $processRepository)
    {
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $em = $this->getDoctrine()->getManager();
        $entity = $processRepository->getEntityCheck($id,$module);
        /** @var  $moduleProcess ModuleProcess */
        $moduleProcess = $entity->getModuleProcess();
        $approvals = $processRepository->findBy(array('entityId'=>$id,'module'=>$module),array('ordering'=>'ASC'));
        if(empty($approvals)){
            // $assignUsers = $approvalUserRepository->getApprovalAssignUser($terminal,$entity);
            // $processRepository->insertProcurementProcessAssign($entity,$entity->getModule(),$assignUsers);
        }
        if($moduleProcess->isApproverForm() == 1 ){
            $approveUsers = $processRepository->getExistApprovalUsers($entity->getId(),$entity->getModule());
            $assignForm = $this->createForm(ProcurementProcessFormType::class, new ProcurementProcess() ,array('config' => $config,'users' => $approveUsers));
            return $this->render('@TerminalbdProcurement/procurement-process/form-approver.html.twig', [
                'entity'                => $entity,
                'moduleProcess'         => $moduleProcess,
                'approvals'             => $approvals,
                'assignForm'            => $assignForm->createView(),
            ]);
        }else{
            return $this->render('@TerminalbdProcurement/procurement-process/form-approver.html.twig', [
                'entity'                => $entity,
                'moduleProcess'         => $moduleProcess,
                'approvals'             => $approvals,
            ]);
        }

    }



    /**
     * Update a RequisitionItem entity.
     * @Route("/{id}/download-attachment", methods={"GET","POST"}, name="procure_process_download_attachment", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function downloadAttachFile(Request $request,$id, DmsFileRepository $dmsFileRepository)
    {

        $entity =  $dmsFileRepository->find($id);
        $filepath = $this->get('kernel')->getProjectDir() . "/public/uploads/procurement/{$entity->getFilename()}";
        if (!empty($entity->getFilename()))
        {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($filepath));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filepath));
            ob_clean();
            flush();
            readfile($filepath);
        }
        exit;
    }

    /**
     * Update a RequisitionItem entity.
     * @Route("/{id}/download-single-attachment", methods={"GET","POST"}, name="procure_process_single_download_attachment", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function downloadSingleAttachFile(Request $request,RequisitionIssue $entity)
    {

        $filepath = $this->get('kernel')->getProjectDir() . "/public/uploads/procurement/{$entity->getPath()}";
        if (!empty($entity->getPath()))
        {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($filepath));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filepath));
            ob_clean();
            flush();
            readfile($filepath);
        }
        exit;
    }




}
