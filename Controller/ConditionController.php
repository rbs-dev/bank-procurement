<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Controller;

use App\Entity\Application\Procurement;
use App\Repository\Application\ProcurementRepository;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\ProcurementBundle\Entity\EnlistedVendor;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementCondition;
use Terminalbd\ProcurementBundle\Entity\ProcurementConditionItem;
use Terminalbd\ProcurementBundle\Form\ConditionFormType;
use Terminalbd\ProcurementBundle\Form\EnlistedVendorFormType;
use Terminalbd\ProcurementBundle\Form\ParticularFomFormType;
use Terminalbd\ProcurementBundle\Form\ParticularFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFormType;
use Terminalbd\ProcurementBundle\Repository\EnlistedVendorRepository;
use Terminalbd\ProcurementBundle\Repository\ParticularRepository;


/**
 * @Route("/procure/terms-condition")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ConditionController extends AbstractController
{

    /**
     * @Route("/", methods={"GET", "POST"}, name="procure_condition")
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_LSSD_HEAD')")
     */
    public function index(Request $request, TranslatorInterface $translator,ProcurementRepository $procurementRepository , EnlistedVendorRepository $repository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $entities = $this->getDoctrine()->getRepository(ProcurementCondition::class)->findBy(['config'=>$config],['name'=>'ASC']);
        return $this->render('@TerminalbdProcurement/condition/index.html.twig', [
            'entities' => $entities
        ]);
    }

    /**
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_LSSD_HEAD')")
     * @Route("/new", methods={"GET", "POST"}, name="procure_condition_new")
     */
    public function new(Request $request, TranslatorInterface $translator, ProcurementRepository $procurementRepository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $entity = new ProcurementCondition();
        $data = $request->request->all();
        $form = $this->createForm(ConditionFormType::class , $entity,['config'=>$config])
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setConfig($config);
            $em->persist($entity);
            $em->flush();
            $this->getDoctrine()->getRepository(ProcurementConditionItem::class)->insertConditionItem($entity,$data);
            $message = $translator->trans('data.created_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('procure_condition');
        }
        return $this->render('@TerminalbdProcurement/condition/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="procure_condition_edit")
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_LSSD_HEAD')")
     */
    public function edit(Request $request, ProcurementCondition $entity,TranslatorInterface $translator , ProcurementRepository $procurementRepository , EnlistedVendorRepository $repository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $data = $request->request->all();
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $form = $this->createForm(ConditionFormType::class, $entity,['config'=>$config])
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->getDoctrine()->getRepository(ProcurementConditionItem::class)->insertConditionItem($entity,$data);
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('procure_condition');
        }
        return $this->render('@TerminalbdProcurement/condition/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }


    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET"}, name="procure_condition_show" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_LSSD_HEAD')")
     */
    public function show($id, EnlistedVendorRepository $repository): Response
    {
        $entity = $repository->find($id);
        $html = $this->renderView(
            '@TerminalbdProcurement/vendor/show.html.twig', array(
                'entity' => $entity,
            )
        );
        return new Response($html);
    }


     /**
     * Status a Setting entity.
     *
     * @Route("/{id}/status", methods={"GET"}, name="procure_condition_status" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_LSSD_HEAD')")
     */
    public function status($id, EnlistedVendorRepository $repository): Response
    {
        $entity = $repository->find($id);
        $status = $_REQUEST['status'];
        if($status == "false"){
            $entity->setStatus(false);
        }else{
            $entity->setStatus(true);
        }
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');
    }

    /**
     * Status a Setting entity.
     * @Route("/{id}/meta-delete", methods={"GET"}, name="procure_condition_meta_delete" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_LSSD_HEAD')")
     */
    public function metaDelete(ProcurementConditionItem $item): Response
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($item);
        $em->flush();
        return new Response('success');
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="procure_condition_delete")
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_LSSD_HEAD')")
     */

    public function delete(ProcurementCondition $entity): Response
    {
        $em = $this->getDoctrine()->getManager();
        $response = "invalid";
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Particular entity.');
        }
        try {
            $em->remove($entity);
            $em->flush();
            $response = 'valid';

        } catch (ForeignKeyConstraintViolationException $e) {
            $this->get('session')->getFlashBag()->add(
                'notice',"Data has been relation another Table"
            );
        }catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add(
                'notice', 'Please contact system administrator further notification.'
            );
        }
        return new Response($response);
    }



}
