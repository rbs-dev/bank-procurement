<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Controller\Bank;

use App\Entity\Application\Procurement;
use App\Entity\Core\Setting;
use App\Entity\Domain\ApprovalUser;
use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\Vendor;
use App\Repository\Application\ProcurementRepository;
use App\Repository\Domain\ApprovalUserRepository;
use App\Service\FormValidationManager;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementCondition;
use Terminalbd\ProcurementBundle\Entity\ProcurementConditionItem;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\Tender;
use Terminalbd\ProcurementBundle\Entity\TenderComparative;
use Terminalbd\ProcurementBundle\Entity\TenderComparativeItem;
use Terminalbd\ProcurementBundle\Entity\TenderCompare;
use Terminalbd\ProcurementBundle\Entity\TenderConditionItem;
use Terminalbd\ProcurementBundle\Entity\TenderItem;
use Terminalbd\ProcurementBundle\Entity\TenderItemDetails;
use Terminalbd\ProcurementBundle\Entity\TenderMemo;
use Terminalbd\ProcurementBundle\Entity\TenderVendor;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorder;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderItem;
use Terminalbd\ProcurementBundle\Form\ApproveCommentFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderFloatingFormType;
use Terminalbd\ProcurementBundle\Form\Garments\GarmentRequisitionFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFilterFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderCsFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderPreparetionFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderVendorFormType;
use Terminalbd\ProcurementBundle\Repository\ProcurementProcessRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionRepository;
use Terminalbd\ProcurementBundle\Repository\TenderItemRepository;
use Terminalbd\ProcurementBundle\Repository\TenderRepository;
use Terminalbd\ProcurementBundle\Repository\TenderVendorRepository;
use Terminalbd\ProcurementBundle\Entity\ComapnyRequisitionShare;



/**
 * @Route("/procure/bssf/tender")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class TenderController extends AbstractController
{

    public function paginate(Request $request ,$entities)
    {
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $request->query->get('page', 1)/*page number*/,
            25  /*limit per page*/
        );
        return $pagination;
    }

    /**
     * @Route("/", methods={"GET", "POST"}, name="procure_tender_bank")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_ADMIN')")
     */
    public function index(Request $request, TranslatorInterface $translator,ProcurementRepository $procurementRepository , TenderRepository $repository): Response
    {
        /* @var $config  Procurement */

        $terminal = $this->getUser()->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $data = $request->request->all();
        $entities = $this->getDoctrine()->getRepository(Tender::class)->findBy(array('config' => $config),array('code'=>"DESC"));
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , NULL,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $mode = array('mode'=> $mode);
            $data = array_merge($data,$mode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
        }
        $processMode = "tender";
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->findTenderBankSearchQuery($config,$this->getUser(),$processMode,$data);
        } else {
            $search = $repository->findTenderBankSearchQuery($config,$this->getUser(),$processMode,$data);
        }

        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/bank/tender/index.html.twig', [
            'pagination' => $pagination,
            'mode' => $mode,
            'searchForm' => $searchForm->createView()
        ]);
    }

    /**
     * @Route("/repeat-order", methods={"GET", "POST"}, name="procure_tender_repeatorder_bank")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_ADMIN')")
     */
    public function indexRepeatOrder(Request $request, TranslatorInterface $translator,ProcurementRepository $procurementRepository , TenderRepository $repository): Response
    {
        /* @var $config  Procurement */

        $terminal = $this->getUser()->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $data = $request->request->all();
        $entities = $this->getDoctrine()->getRepository(Tender::class)->findBy(array('config' => $config),array('code'=>"DESC"));
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , NULL,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $mode = array('mode'=> $mode);
            $data = array_merge($data,$mode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
        }
        $processMode = "repeat-order";
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->findTenderBankRepeatOrderSearchQuery($config,$this->getUser(),$processMode,$data);
        } else {
            $search = $repository->findTenderBankRepeatOrderSearchQuery($config,$this->getUser(),$processMode,$data);
        }
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/bank/tender/repeat-order.html.twig', [
            'pagination' => $pagination,
            'mode' => $mode,
            'searchForm' => $searchForm->createView()
        ]);
    }

    /**
     * @Route("/quotation", methods={"GET", "POST"}, name="procure_tender_quotation_bank")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_ADMIN')")
     */
    public function indexQuotation(Request $request, TranslatorInterface $translator,ProcurementRepository $procurementRepository , TenderRepository $repository): Response
    {
        /* @var $config  Procurement */

        $terminal = $this->getUser()->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $data = $request->request->all();
        $entities = $this->getDoctrine()->getRepository(Tender::class)->findBy(array('config' => $config),array('code'=>"DESC"));
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , NULL,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $mode = array('mode'=> $mode);
            $data = array_merge($data,$mode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
        }
        $processMode = "quotation";
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->findTenderBankSearchQuery($config,$this->getUser(),$processMode,$data);
        } else {
            $search = $repository->findTenderBankSearchQuery($config,$this->getUser(),$processMode,$data);
        }
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/bank/tender/quotation.html.twig', [
            'pagination' => $pagination,
            'mode' => $mode,
            'searchForm' => $searchForm->createView()
        ]);
    }

    /**
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_ADMIN')")
     * @Route("/open-item", methods={"GET", "POST"}, name="procure_tender_bank_open_item")
     */
    public function openItem(Request $request, ProcurementRepository $procurementRepository, ApprovalUserRepository $approvalUserRepository, ProcurementProcessRepository $processRepository, RequisitionRepository $requisitionRepository, TenderRepository $repository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $data = $request->request->all();
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , NULL,array('terminal'=>$this->getUser()->getTerminal(),'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        $items = array('items'=> '');
        $data = array_merge($data,$items);
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "in-progress";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "in-progress";
            $mode = array('mode'=> $mode);
            $data = array_merge($data,$mode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "in-progress";
        }
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $this->getDoctrine()->getRepository(RequisitionItem::class)->prBankOpenItem($config->getId(),$data);
        } else {
            $search = $this->getDoctrine()->getRepository(RequisitionItem::class)->prBankOpenItem($config->getId(),$data);
        }
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/bank/tender/open-item.html.twig', [
            'pagination' => $pagination,
            'selected' => explode(',', $request->cookies->get('itemIds', '')),
            'searchForm' => $searchForm->createView(),
        ]);
    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/preparetion", methods={"GET", "POST"}, name="procure_tender_bank_preparetion")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function preparetion(Request $request, Tender $entity,TranslatorInterface $translator , ProcurementRepository $procurementRepository , TenderRepository $repository, TenderVendorRepository $vendorRepository, ProcurementProcessRepository $processRepository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $data = $request->request->all();
        $terminal = $this->getUser()->getTerminal()->getId();
        $form = $this->createForm(TenderPreparetionFormType::class, $entity, array('terminal'=>$this->getUser()->getTerminal(),'config'=>$entity->getConfig()));
        $form->handleRequest($request);
        $data = $request->request->all();
        if ($form->isSubmitted() && $form->isValid() and count($entity->getTenderItemDetails()) > 0) {
            $items  = 'itemIds';
            unset($items);
            setcookie('itemIds', null, -1, '/');
            $data = $request->request->all();
            $processMode    = (isset($data['tender_preparetion_form']['processMode'])) ? $data['tender_preparetion_form']['processMode']:'' ;
            $vendorSelect   = (isset($data['vendorSelect'])) ? sizeof($data['vendorSelect']) : 0;
            if($processMode == 'quotation' and $vendorSelect == 0 ){
                $message = "Quotation method tender must be need to vendor selection";
                $this->addFlash('notice', $message);
                return $this->redirectToRoute('procure_tender_bank_preparetion',array('id' => $entity->getId()));
            }
            $entity->setWaitingProcess("Approve");
            $entity->setProcess("Approve");
            $processRepository->approvalAssign($entity);
            $this->getDoctrine()->getManager()->flush();

            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
          //  $this->getDoctrine()->getRepository(TenderItem::class)->updateTenderItem($entity,$data);
            $this->getDoctrine()->getRepository(TenderItemDetails::class)->updateBankTenderPrItem($entity,$data);
            $this->getDoctrine()->getRepository(TenderVendor::class)->insertTendorVendor($entity,$data);
            $this->getDoctrine()->getRepository(TenderConditionItem::class)->insertBankTenderConditionItem($entity,$data);
            $files = isset( $_FILES['files']) ?  $_FILES['files'] : "";
            if(!empty($files)){
                $processRepository->insertAttachmentFile($entity,$data,$files);
            };
            $this->getDoctrine()->getRepository(TenderVendor::class)->insertSelectVendor($entity,$data);
            if($entity->getWaitingProcess() == "Approved"){
                $this->getDoctrine()->getRepository(TenderMemo::class)->floatingMemoGenerate($terminal,$entity);
                $this->getDoctrine()->getRepository(Tender::class)->updateOpenItem($entity);
            }
            if($entity->getWaitingProcess() == "Approved" and $entity->getProcessMode() == "quotation"){
               // $this->getDoctrine()->getRepository(TenderComparative::class)->quotationCsProcess($entity);
            }
            if($entity->getProcessMode() == "repeat-order"){
                $this->getDoctrine()->getRepository('TerminalbdProcurementBundle:Tender')->repeatApproved($entity);
                $this->getDoctrine()->getRepository(TenderComparative::class)->repeatOrderCsProcess($entity);
            }
            if($entity->getProcessMode() == 'tender'){
                return $this->redirectToRoute('procure_tender_bank',array('mode' => 'list'));
            }elseif($entity->getProcessMode() == 'quotation'){
                return $this->redirectToRoute('procure_tender_quotation_bank',array('mode' => 'list'));
            }elseif($entity->getProcessMode() == 'repeat-order'){
                return $this->redirectToRoute('procure_tender_repeatorder_bank',array('mode' => 'list'));
            }
        }
        if($entity->getVendorType()){
            $vendorType = $entity->getVendorType();
            $vendors = $this->getDoctrine()->getRepository(Vendor::class)->findBy(array('terminal' => $terminal,'vendorType'=>$vendorType));
        }else{
            $vendors ="";
        }
        $workorderItems = "";
        if($entity->getWorkOrder()){
            $workorderItems = $this->getDoctrine()->getRepository(TenderWorkorderItem::class)->getItemWithPrice($entity->getWorkOrder());
        }
        $selectedRows = $this->getDoctrine()->getRepository(TenderItemDetails::class)->bankTenderExistingSelectedItem($entity);
        return $this->render('@TerminalbdProcurement/bank/tender/new.html.twig', [
            'entity' => $entity,
            'selectedRows' => $selectedRows,
            'workorderItems' => $workorderItems,
            'vendors' => $vendors,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/process", methods={"GET", "POST"}, name="procure_tender_bank_process", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function process(Request $request, $id,TranslatorInterface $translator , ProcurementRepository $procurementRepository , TenderRepository $repository, ProcurementProcessRepository $processRepository): Response
    {
        
        $terminal = $this->getUser()->getTerminal();
        $data = $request->request->all();
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        /* @var $entity Tender */
        $entity = $repository->findOneBy(array('config' => $config,'id' => $id));
        $form = $this->createForm(ApproveCommentFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $data = $request->request->all();
            $comment = $form["comment"]->getData();
            if(isset($data['reject']) and $data['reject'] == "reject") {
                $entity->setProcess('Rejected');
                $entity->setWaitingProcess('Rejected');
                $entity->setReportTo(null);
                $entity->setApproveTo($this->getUser());
                $entity->setComment($comment);
                $em->persist($entity);
                $em->flush();
                $message = $translator->trans('data.reject_successfully');
                $this->addFlash('success', $message);
            }elseif(isset($data['re-checked']) and $data['re-checked'] == "re-checked"){
                $processRepository->resetRechecked($entity);
                $entity->setProcess('Re-checked');
                $entity->setWaitingProcess('New');
                $entity->setReportTo(null);
                $entity->setApproveTo($this->getUser());
                $entity->setComment($comment);
                $em->persist($entity);
                $em->flush();
                $message = $translator->trans('data.recheck_successfully');
                $this->addFlash('success', $message);
            }else{
                $processRepository->insertApprovalProcessForGenericComment($this->getUser(),$entity,$comment);
                $count = $processRepository->count(array('entityId' => $entity->getId(), 'module' => $entity->getModule(), 'close' => 1));
                $ordering = ($count == 0) ? 1 : $count + 1;
                $processRepository->approvalAssign($entity, $ordering);
                $message = $translator->trans('data.updated_successfully');
                $this->addFlash('success', $message);
            }
            if($entity->getWaitingProcess() == "Approved"){
                $this->getDoctrine()->getRepository(Tender::class)->updateOpenItem($entity);
                $this->getDoctrine()->getRepository(TenderMemo::class)->floatingMemoGenerate($terminal,$entity);
            }
            if($entity->getWaitingProcess() == "Approved" and $entity->getProcessMode() == "quotation"){
               // $this->getDoctrine()->getRepository(TenderComparative::class)->quotationCsProcess($entity);
            }elseif($entity->getWaitingProcess() == "Approved" and $entity->getProcessMode() == "repeat-order"){
                $this->getDoctrine()->getRepository(TenderComparative::class)->repeatOrderCsProcess($entity);
            }
            if($entity->getProcessMode() == 'tender'){
                return $this->redirectToRoute('procure_tender_bank',array('mode' => 'approve'));
            }elseif($entity->getProcessMode() == 'quotation'){
                return $this->redirectToRoute('procure_tender_quotation_bank',array('mode' => 'approve'));
            }elseif($entity->getProcessMode() == 'repeat-order'){
                return $this->redirectToRoute('procure_tender_repeatorder_bank',array('mode' => 'approve'));
            }
        }
        return $this->render('@TerminalbdProcurement/bank/tender/process.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/committee-process", methods={"GET", "POST"}, name="procure_tender_bank_committee_process", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function committeeProcess(Request $request, $id,TranslatorInterface $translator , ProcurementRepository $procurementRepository , TenderRepository $repository, ProcurementProcessRepository $processRepository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $data = $request->request->all();
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        /* @var $entity Tender */
        $entity = $repository->findOneBy(array('config' => $config,'id' => $id));
        $form = $this->createForm(ApproveCommentFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $data = $request->request->all();
            $comment = $form["comment"]->getData();
            $processRepository->insertApprovalProcessForGenericComment($this->getUser(),$entity,$comment);
            if(isset($data['reject']) and $data['reject'] == "reject"){
                $entity->setComment($comment);
                $entity->setProcess('Rejected');
                $entity->setWaitingProcess('Rejected');
                $entity->setReportTo(null);
                $em->persist($entity);
                $em->flush();
            }elseif(isset($data['approve']) and $data['approve'] == "approve"){
                $entity->setComment($comment);
                $entity->setProcess('Closed');
                $entity->setWaitingProcess('Closed');
                $entity->setReportTo(null);
                $em->persist($entity);
                $em->flush();
            }
            if($entity->getWaitingProcess() == "Approved"){
                $this->getDoctrine()->getRepository(Tender::class)->updateOpenItem($entity);
                $this->getDoctrine()->getRepository(TenderMemo::class)->floatingMemoGenerate($terminal,$entity);
            }
//            return $this->redirect($request->server->get('HTTP_REFERER'));
            return $this->redirectToRoute('procure_tender_floating_memo_bank');
        }
        $approver = $this->getDoctrine()->getRepository(ProcurementProcess::class)->checkApproveProcessUser($entity->getId(),$this->getUser()->getId());
        return $this->render('@TerminalbdProcurement/bank/tender/meeting-process.html.twig', [
            'entity' => $entity,
            'approver' => $approver,
            'form' => $form->createView(),
        ]);
    }


    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET"}, name="procure_tender_bank_show" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function show(Tender $entity): Response
    {
        $html = $this->renderView(
            '@TerminalbdProcurement/bank/tender/show.html.twig', array(
                'entity' => $entity,
            )
        );
        return new Response($html);
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/item-close", methods={"GET"}, name="procure_tender_bank_item_close" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function itemClose(RequisitionItem $entity): Response
    {
        $em = $this->getDoctrine()->getManager();
        $entity->setIsClose(0);
        $em->persist($entity);
        $em->flush();
        return $this->redirect($request->server->get('HTTP_REFERER'));
    }


    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/condition-update", methods={"GET","POST"}, name="procure_bank_tender_condition" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function conditionUpdate(Request $request, Tender $tender): Response
    {
        $em = $this->getDoctrine()->getManager();
        $v = $_REQUEST['condition'];
        /** @var  $entity ProcurementCondition */
        $entity = $this->getDoctrine()->getRepository(ProcurementCondition::class)->find($v);
        $tender->setCondition($entity);
        $tender->setSubject($entity->getSubject());
        $tender->setContent($entity->getBody());
        $tender->setFooterContent($entity->getDescription());
        $tender->setEmail($entity->getCcEmail());
        $em->flush();
        $this->getDoctrine()->getRepository(TenderConditionItem::class)->initialBankTenderConditionItem($tender);
        return new Response('success');
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="procure_tender_bank_delete")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function delete(Tender $entity): Response
    {
        $em = $this->getDoctrine()->getManager();
        $response = "invalid";
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Particular entity.');
        }
        try {
            if($entity->getWaitingProcess() == "New"){
                $em->remove($entity);
                $em->flush();
                $response = 'valid';
            }
        } catch (ForeignKeyConstraintViolationException $e) {
            $this->get('session')->getFlashBag()->add(
                'notice',"Data has been relation another Table"
            );
        }catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add(
                'notice', 'Please contact system administrator further notification.'
            );
        }
        return new Response($response);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/vendor-delete", methods={"GET"}, name="procure_tender_bank_vendor_delete")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function deleteVendor(TenderVendor $entity): Response
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();
        return new Response('Success');
    }


    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/item-delete", methods={"GET"}, name="procure_tender_bank_item_delete")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function itemDelete(TenderItem $entity): Response
    {
        $em = $this->getDoctrine()->getManager();
        $response = "invalid";
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Particular entity.');
        }
        try {
            $em->remove($entity);
            $em->flush();
            $response = 'valid';

        } catch (ForeignKeyConstraintViolationException $e) {
            $this->get('session')->getFlashBag()->add(
                'notice',"Data has been relation another Table"
            );
        }catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add(
                'notice', 'Please contact system administrator further notification.'
            );
        }
        return new Response($response);
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/preview", methods={"GET"}, name="procure_tender_bank_preview" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function printPreview(Tender $entity): Response
    {
        $mode = isset($_REQUEST['mode']) ? $_REQUEST['mode'] :'';
        $approvals = $this->getDoctrine()->getRepository(ProcurementProcess::class)->findBy(array('entityId'=>$entity->getId(),'module'=>$entity->getModule()),array('ordering'=>'ASC'));
        return $this->render(
            '@TerminalbdProcurement/bank/tender/preview.html.twig', array(
                'entity' => $entity,
                'approvals' => $approvals,
                'mode' => 'preview'
            )
        );

    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/floating", methods={"GET", "POST"}, name="procure_tender_bank_floating")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function tenderFloting(Request $request, Tender $entity,TranslatorInterface $translator , ProcurementRepository $procurementRepository , TenderRepository $repository, TenderVendorRepository $vendorRepository, ProcurementProcessRepository $processRepository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $data = $request->request->all();
        $terminal = $this->getUser()->getTerminal()->getId();
        $form = $this->createForm(TenderFloatingFormType::class, $entity, array('terminal'=>$this->getUser()->getTerminal(),'config'=>$entity->getConfig()));
        $form->handleRequest($request);
        $data = $request->request->all();
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $request->request->all();
            $entity->setProcess('CS-sheet');
            $entity->setWaitingProcess('CS-sheet');
            $this->getDoctrine()->getManager()->flush();
            $this->getDoctrine()->getRepository(TenderVendor::class)->insertTendorVendor($entity,$data);
            $message = $translator->trans('data.updated_successfully');
            return $this->redirectToRoute('procure_tender_bank',array('mode' => 'cs-sheet'));
        }
        return $this->render('@TerminalbdProcurement/bank/tender/floating.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/select-vendor", methods={"GET"}, name="procure_tender_bank_select_vendor", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN')")
     */

    public function selecteVendor(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $terminal = $this->getUser()->getTerminal();
        $memo = $_REQUEST['memo'];
        $idm = $_REQUEST['id'];
        $vendorType = $this->getDoctrine()->getRepository(Setting::class)->find($idm);
        $tenderMemo = $this->getDoctrine()->getRepository(Tender::class)->find($memo);
        if($tenderMemo and $vendorType){
            $vendor = $this->getDoctrine()->getRepository(Vendor::class)->findBy(array('vendorType'=> $idm,'terminal' => $terminal));
            $tenderMemo->setVendorType($vendorType);
            $em->persist($tenderMemo);
            $em->flush();
            $html = $this->renderView(
                '@TerminalbdProcurement/bank/floating-memo/vendor.html.twig', array(
                    'vendors' => $vendor,
                )
            );
            return new Response($html);
        }else{
            return new Response('No record founds');
        }
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/tender-workorder", methods={"GET"}, name="procure_tender_bank_tender_workorder", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN')")
     */

    public function tenderWorkorder(Request $request, Tender $tender): Response
    {
        $em = $this->getDoctrine()->getManager();
        $terminal = $this->getUser()->getTerminal();
        $id = $_REQUEST['id'];
        $mode = $_REQUEST['mode'];
        $workorder = $this->getDoctrine()->getRepository(TenderWorkorder::class)->find($id);
        $vendor = $this->getDoctrine()->getRepository(Vendor::class)->find($id);
        if($mode = 'workorder' and $workorder){
            $tender->setWorkOrder($workorder);
            $em->persist($tender);
            $em->flush();
            return new Response('success');
        }elseif($mode = "vendor" and $vendor){
            $tender->setDirectVendor($vendor);
            $em->persist($tender);
            $em->flush();
            return new Response('success');
        }else{
            return new Response('invalid');
        }
    }



    /**
     * Deletes a Setting entity.
     *
     * @Route("/select-workorder", methods={"GET"}, name="procure_tender_bank_select_workorder", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN')")
     */

    public function selectWorkorder(Request $request): Response
    {
        $terminal = $this->getUser()->getTerminal()->getId();
        $inventory = $this->getDoctrine()->getRepository(Procurement::class)->config($terminal)->getId();
        $item = trim($_REQUEST['q']);
        if ($item) {
            $item = $this->getDoctrine()->getRepository(TenderWorkorder::class)->searchWorkorderAutoComplete($inventory,$this->getUser()->getId(),$item);
        }
        return new JsonResponse($item);

    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/select-vendor", methods={"GET"}, name="procure_tender_bank_select_vendor", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN')")
     */

    public function selectVendor(Request $request): Response
    {
        $terminal = $this->getUser()->getTerminal()->getId();
        $item = trim($_REQUEST['q']);
        if ($item) {
            $item = $this->getDoctrine()->getRepository(Vendor::class)->searchVendorAutoComplete($terminal,$item);
        }
        return new JsonResponse($item);

    }


    /**
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN')")
     * @Route("/{id}/quotation-generate", methods={"GET", "POST"}, name="procure_tender_quotation_memo_bank")
     */
    public function generateQuotation(Request $request,Tender $entity,TranslatorInterface $translator , ProcurementRepository $procurementRepository ,ProcurementProcessRepository $processRepository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $data = $request->request->all();
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $managementMemo = $this->getDoctrine()->getRepository(TenderComparative::class)->quotationCsProcess($entity);
        return $this->redirectToRoute('procure_management_quotation_memo_bank',['id'=> $managementMemo->getId()]);

    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/ajax-update", methods={"GET", "POST"}, name="procure_tender_preparetion_bank_ajax_update", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_ADMIN')")
     */
    public function ajaxUpadteRequsition(Request $request ,Tender $entity)
    {

        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $form = $this->createForm(TenderPreparetionFormType::class, $entity, array('terminal'=>$this->getUser()->getTerminal(),'config'=>$entity->getConfig()));
        $form->handleRequest($request);
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        /*if($entity->getProcessMode() != 'repeat-order'){
            $entity->setWorkOrder(NULL);
        }*/
        $em->persist($entity);
        $em->flush();
        $files = !empty($_FILES['files']) ? $_FILES['files']:'';
        if($files){
            $this->getDoctrine()->getRepository(ProcurementProcess::class)->insertAttachmentFile($entity,$data,$files);
        }
        return new Response('success');
    }

}
