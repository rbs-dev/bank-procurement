<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Controller\Bank;

use App\Entity\Application\GenericMaster;
use App\Entity\Application\Procurement;
use App\Entity\User;
use App\Repository\Application\GenericMasterRepository;
use App\Repository\Application\InventoryRepository;
use App\Repository\Application\ProcurementRepository;
use ContainerBlo4KOZ\getRequisitionItemRepositoryService;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\GenericBundle\Entity\Item;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\InventoryBundle\Repository\StockRepository;
use Terminalbd\ProcurementBundle\Entity\JobRequisition;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionBudgetItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItemHistory;
use Terminalbd\ProcurementBundle\Form\ApproveCommentFormType;
use Terminalbd\ProcurementBundle\Form\Garments\RequisitionItemFormType;
use Terminalbd\ProcurementBundle\Form\GarmentsRequisitionItemFormType;
use Terminalbd\ProcurementBundle\Form\ProcurementProcessFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFormType;
use Terminalbd\ProcurementBundle\Repository\JobRequisitionAdditionalItemRepository;
use Terminalbd\ProcurementBundle\Repository\JobRequisitionRepository;
use Terminalbd\ProcurementBundle\Repository\ProcurementProcessRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionItemRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionRepository;


/**
 * @Route("/procurement/requisition-item")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class RequisitionItemController extends AbstractController
{

    /**
     * Show a ApprovalProcess entity.
     *
     * @Route("/{id}/{module}/item-create", methods={"GET","POST"}, name="procure_requisition_item_create" , options={"expose"=true})
     */
    public function itemCreate(Request $request, $id ,$module): Response
    {
        /* @var $config  Procurement */

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $this->getDoctrine()->getRepository(Procurement::class)->config($terminal);
        $genricConfig = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal);
        $entity = $this->getDoctrine()->getRepository(ProcurementProcess::class)->getEntityCheck($id,$module);
        $form = $this->createForm(RequisitionItemFormType::class, null,array('config' => $genricConfig));
        $form->handleRequest($request);
        return $this->render('@TerminalbdProcurement/garments/requisition-item/item-form.html.twig', [
            'entity'             => $entity,
            'itemForm'           => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/{module}/save-item", methods={"GET","POST"}, name="procure_requisition_save_item" , options={"expose"=true})
     */

    public function saveItem(Request $request, $id,$module): Response
    {
        $data = $request->request->all();
        $entity = $this->getDoctrine()->getRepository(ProcurementProcess::class)->getEntityCheck($id,$module);
        $purchaseData = $data['requisition_item_form'];
        $itemRepository = $this->getDoctrine()->getRepository(RequisitionItem::class);
        if($entity->getModule() == 'job-approval'){
            $itemRepository->insertJobRequisitionItem($entity,$purchaseData);
            $itemRepository->getJobItemSummary($entity);
            $reponse = $this->returnJobResultData($entity->getId());
        }elseif($entity->getModule() == 'purchase-requisition'){
            $itemRepository->insertGarmentRequisitionItem($entity,$purchaseData);
            $itemRepository->getItemSummary($entity);
            $reponse = $this->returnResultData($entity->getId());
        }
        return new Response(json_encode($reponse));
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/add-item", methods={"GET","POST"}, name="procure_requisition_item_add" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function addItem(Request $request, Requisition $entity, RequisitionRepository $requisitionRepository , RequisitionItemRepository $itemRepository): Response
    {
        $data = $request->request->all();
        $purchaseData = $data['requisition_item_form'];
        $itemRepository->insertStoreItem($entity,$purchaseData);
        $itemRepository->getItemSummary($entity);
        $reponse = $this->returnResultData($requisitionRepository,$entity->getId());
        return new Response(json_encode($reponse));
    }



    /**
     * Update a RequisitionItem entity.
     * @Route("/{entity}/{id}/{module}/update-item", methods={"GET","POST"}, name="procure_requisition_item_update", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function updateItem(Request $request ,$entity,$id,$module,RequisitionItemRepository $itemRepository): Response
    {

        /* @var $item  RequisitionItem */
        $em = $this->getDoctrine()->getManager();
        $data = $request->request->all();
        $item = $itemRepository->find($id);
        $item->setQuantity($data['quantity']);
        $item->setSubTotal($item->getQuantity() * $item->getPrice());
        $this->getDoctrine()->getManager()->flush();
        if($module == "requisition"){
            $requisition = $em->getRepository(Requisition::class)->find($entity);
            $itemRepository->getItemSummary($requisition);
            $return = $this->returnResultData($requisition->getId());
        }else{
            $requisition = $em->getRepository(Requisition::class)->find($entity);
            $itemRepository->getItemSummary($requisition);
            $return = $this->returnResultData($requisition->getId());
        }
        return new Response(json_encode($return));

    }

    /**
     * Deletes a RequisitionItem entity.
     * @Route("/{entity}/{id}/{module}/delete-item", methods={"GET"}, name="procure_requisition_item_delete", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function deleteItem($entity , $id, $module ,RequisitionItemRepository $itemRepository): Response
    {
        $item = $itemRepository->findOneBy(array('id' => "{$id}"));
        $em = $this->getDoctrine()->getManager();
        $em->remove($item);
        $em->flush();
        if($module == "requisition"){
            $requisition = $em->getRepository(Requisition::class)->find($entity);
            $itemRepository->getItemSummary($requisition);
            $return = $this->returnResultData($requisition->getId());
        }else{
            $requisition = $em->getRepository(Requisition::class)->find($entity);
            $itemRepository->getItemSummary($requisition);
            $return = $this->returnResultData($requisition->getId());
        }
        return new Response(json_encode($return));
    }


    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/add-job-item", methods={"GET","POST"}, name="procure_requisition_jobitem_add" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function addJobItem(Request $request, JobRequisition $entity, JobRequisitionRepository $jobRequisitionRepository , RequisitionItemRepository $itemRepository): Response
    {
        $data = $request->request->all();
        $purchaseData = $data['job_requisition_item_form'];
        $itemRepository->insertJobRequisitionItem($entity,$purchaseData);
        $itemRepository->getJobItemSummary($entity);
        $reponse = $this->returnJobResultData($entity->getId());
        return new Response(json_encode($reponse));
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/add-additional-job-item", methods={"GET","POST"}, name="procure_requisition_jobitem_additional" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function addJobItemAdditional(Request $request, JobRequisition $entity, JobRequisitionRepository $jobRequisitionRepository , JobRequisitionAdditionalItemRepository $itemRepository): Response
    {
        $data = $request->request->all();
        $purchaseData = $data['job_requisition_additional_item_form'];
        $itemRepository->insertJobRequisitionItem($entity,$purchaseData);
        $itemRepository->getItemSummary($entity);
        $reponse = $this->returnJobAdditionalResultData($jobRequisitionRepository,$entity->getId());
        return new Response(json_encode($reponse));
    }

    /**
     * Deletes a RequisitionItem entity.
     * @Route("/{entity}/{id}/job-additional-delete-item", methods={"GET"}, name="procure_jobrequisition_additional_item_delete", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function deleteJobAdditionalItem($entity , $id , JobRequisitionRepository $jobRequisitionRepository, JobRequisitionAdditionalItemRepository $itemRepository): Response
    {

        $terminal = $this->getUser()->getTerminal()->getId();
        $requisition = $jobRequisitionRepository->findOneBy(array('id' => "{$entity}"));
        $item = $itemRepository->findOneBy(array('id' => "{$id}"));
        $em = $this->getDoctrine()->getManager();
        $em->remove($item);
        $em->flush();
        $itemRepository->getJobItemSummary($requisition);
        $return = $this->returnJobAdditionalResultData($jobRequisitionRepository,$requisition->getId());
        return new Response(json_encode($return));
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/add-garment-item", methods={"GET","POST"}, name="procure_requisition_garmentitem_add" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function addGarmentItem(Request $request, Requisition $entity, RequisitionRepository $requisitionRepository , RequisitionItemRepository $itemRepository): Response
    {
        $data = $request->request->all();
        $purchaseData = $data['garments_requisition_item_form'];
        $itemRepository->insertGarmentRequisitionItem($entity,$purchaseData);
        $itemRepository->getItemSummary($entity);
        $reponse = $this->returnResultData($requisitionRepository,$entity->getId());
        return new Response(json_encode($reponse));
    }

    public function returnResultData($requisition){

        /* @var $entity Requisition */
        $entity = $this->getDoctrine()->getRepository(Requisition::class)->find($requisition);
        $subTotal = $entity->getSubTotal() > 0 ? $entity->getSubTotal() : 0;
        $total = $entity->getTotal() > 0 ? $entity->getTotal() : 0;
        if($entity->getBusinessGroup() == "garment"){
            $html = $this->renderView(
                '@TerminalbdProcurement/garments/purchase-requisition/requisitionItem.html.twig', array(
                    'entity' => $entity,
                )
            );
        }else{
            $html = $this->renderView(
                '@TerminalbdProcurement/requisition/requisitionItem.html.twig', array(
                    'entity' => $entity,
                )
            );
        }
        $data = array(
            'subTotal' => $subTotal,
            'total' => $total,
            'invoiceItem' => $html
        );
        return $data;

    }

    public function returnGarmentResultData($requisition){

        /* @var $entity Requisition */
        $entity = $this->getDoctrine()->getRepository(Requisition::class)->find($requisition);
        $subTotal = $entity->getSubTotal() > 0 ? $entity->getSubTotal() : 0;
        $total = $entity->getTotal() > 0 ? $entity->getTotal() : 0;
        $html = $this->renderView(
            '@TerminalbdProcurement/garments/purchase-requisition/requisitionItem.html.twig', array(
                'entity' => $entity,
            )
        );
        $data = array(
            'subTotal' => $subTotal,
            'total' => $total,
            'invoiceItem' => $html
        );
        return $data;

    }

    public function returnJobResultData($requisition){

        $entity = $this->getDoctrine()->getRepository(JobRequisition::class)->find($requisition);
        $subTotal = $entity->getSubTotal() > 0 ? $entity->getSubTotal() : 0;
        $total = $entity->getTotal() > 0 ? $entity->getTotal() : 0;
        $html = $this->renderView(
            '@TerminalbdProcurement/garments/job-requisition/requisitionItem.html.twig', array(
                'entity' => $entity,
            )
        );
        $data = array(
            'subTotal' => $subTotal,
            'total' => $total,
            'invoiceItem' => $html
        );
        return $data;

    }

    public function returnJobAdditionalResultData(JobRequisitionRepository $requisitionRepository ,$requisition){

        $entity = $requisitionRepository->find($requisition);
        $subTotal = $entity->getSubTotal() > 0 ? $entity->getSubTotal() : 0;
        $total = $entity->getTotal() > 0 ? $entity->getTotal() : 0;
        $html = $this->renderView(
            '@TerminalbdProcurement/garments/job-requisition/requisitionAdditionalItem.html.twig', array(
                'entity' => $entity,
            )
        );
        $data = array(
            'subTotal' => $subTotal,
            'total' => $total,
            'invoiceItem' => $html
        );
        return $data;

    }

    /**
     * Status a Setting entity.
     *
     * @Route("/category-attribute", methods={"GET","POST"}, name="procure_category_attribute" , options={"expose"=true})
     * @Security("is_granted('ROLE_INVENTORY') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_GENERIC') or is_granted('ROLE_PROCUREMENT')")
     */
    public function categoryAttribute(Request $request , StockRepository $stockRepository): Response
    {
        $id = $_REQUEST['id'];

        /* @var $entity Stock */

        $entity = $this->getDoctrine()->getRepository(Item::class)->find($id);
        $category = $entity->getCategory();

        $brands = $category->getBrand();
        $sizes = $category->getSize();
        $colors = $category->getColor();

        $selectBrand = "";
        $selectBrand .= "<option value=''>---Select Brand---</option>";
        foreach ($brands as $brand){
            $selectBrand .= "<option value='{$brand->getId()}'>{$brand->getName()}</option>";
        }
        $selectSize = "";
        $selectSize .= "<option value=''>---Select Size---</option>";
        foreach ($sizes as $size){
            $selectSize .= "<option value='{$size->getId()}'>{$size->getName()}</option>";
        }

        $selectColor = "";
        $selectColor .= "<option value=''>---Select Color---</option>";
        foreach ($colors as $color){
            $selectColor .= "<option value='{$color->getId()}'>{$color->getName()}</option>";
        }

        $data = array('brand'=>$selectBrand,'size'=>$selectSize,'color'=>$selectColor);
        return new Response(json_encode($data));
    }

}
