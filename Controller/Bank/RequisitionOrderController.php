<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Controller\Bank;

use App\Entity\Application\GenericMaster;
use App\Entity\Application\Procurement;
use App\Entity\Core\Setting;
use App\Entity\Domain\ModuleProcess;
use App\Repository\Application\GenericMasterRepository;
use App\Repository\Application\ProcurementRepository;
use App\Repository\Domain\ModuleProcessItemRepository;
use App\Service\FileUploader;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\InventoryBundle\Entity\StockHistory;
use Terminalbd\InventoryBundle\Repository\StockRepository;
use Terminalbd\ProcurementBundle\Entity\OrderDelivery;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionOrder;
use Terminalbd\ProcurementBundle\Form\ApproveCommentFormType;
use Terminalbd\ProcurementBundle\Form\Bank\StoreCreditNoteFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderPreparetionFormType;
use Terminalbd\ProcurementBundle\Form\EmptyFormType;
use Terminalbd\ProcurementBundle\Form\LssdFilterFormType;
use Terminalbd\ProcurementBundle\Form\OrderReceiveFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionApproveFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFilterFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionOrderFormType;
use Terminalbd\ProcurementBundle\Repository\ComapnyRequisitionShareRepository;
use Terminalbd\ProcurementBundle\Repository\JobRequisitionRepository;
use Terminalbd\ProcurementBundle\Repository\OrderDeliveryRepository;
use Terminalbd\ProcurementBundle\Repository\ParticularRepository;
use Terminalbd\ProcurementBundle\Repository\ProcurementProcessRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionOrderRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionRepository;


/**
 * @Route("/procure/bssf/requisition/order")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class RequisitionOrderController extends AbstractController
{

    public function paginate(Request $request ,$entities)
    {
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $request->query->get('page', 1)/*page number*/,
            25  /*limit per page*/
        );
        return $pagination;
    }

    /**
     * @Route("/", methods={"GET", "POST"}, name="procure_requisition_bank_order")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function index(Request $request, TranslatorInterface $translator, ProcurementRepository $procurementRepository , ProcurementProcessRepository $processRepository,RequisitionOrderRepository $repository): Response
    {
        /* @var $config  Procurement */

        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $profile = $this->getUser()->getProfile();
        $settingRepository = $this->getDoctrine()->getRepository(Setting::class);
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , null,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $mode = array('mode'=> $mode);
            $data = array_merge($data,$mode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
        }
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->requisitionOrders($config,$this->getUser(),$data);
        } else {
            $search = $repository->requisitionOrders($config,$this->getUser(),$data);
        }
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/bank/requisition-order/index.html.twig',[
            'pagination' => $pagination,
            'mode' => $mode,
            'searchForm' => $searchForm->createView()
        ]);
    }

    /**
     * @Route("/procurement-store", methods={"GET", "POST"}, name="procure_requisition_bank_store_approval")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function procurementStore(Request $request, TranslatorInterface $translator, ProcurementRepository $procurementRepository , ProcurementProcessRepository $processRepository,RequisitionRepository $repository): Response
    {
        /* @var $config  Procurement */

        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $profile = $this->getUser()->getProfile();
        $settingRepository = $this->getDoctrine()->getRepository(Setting::class);
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , null,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->findByStoreApproveQuery($config,$this->getUser(),$data);
        } else {
            $search = $repository->findByStoreApproveQuery($config,$this->getUser(),$data);
        }
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/bank/requisition-order/approve.html.twig',[
            'pagination' => $pagination,
            'searchForm' => $searchForm->createView()
        ]);
    }

   /**
     * Show a Setting entity.
     *
     * @Route("/{id}/order-process", methods={"GET","POST"}, name="procure_requisition_bank_order_process" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function storeProcess(Request $request , $id,TranslatorInterface $translator,ParticularRepository $particularRepository , ProcurementRepository $procurementRepository, ProcurementProcessRepository $processRepository, ModuleProcessItemRepository $processItemRepository, RequisitionOrderRepository $requisitionOrderRepository): Response
    {
        /* @var $config  Procurement */
        $em = $this->getDoctrine()->getManager();
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        /* @var $order OrderDelivery */
        $order = $this->getDoctrine()->getRepository(OrderDelivery::class)->find($id);
        $form = $this->createForm( StoreCreditNoteFormType::class,$order, array('config'=>$config));
        $form->handleRequest($request);
        $data = $request->request->all();
        if ($form->isSubmitted() && $form->isValid()) {
            if($order->getDeliveryMethod()->getSlug() == "courier"){
               $courier = $em->getRepository(Particular::class)->find($data['courier']);
                $order->setCourier($courier);
                $order->setCnNo($data['cnNo']);
                $order->setComments($data['comment']);
                $order->setReceiverName(null);
                $order->setReceiverDesignation(null);
                $order->setReceiverAddress(null);
           }elseif($order->getDeliveryMethod()->getSlug() == "hand-delivery"){
                $order->setReceiverName($data['receiverName']);
                $order->setReceiverDesignation($data['receiverDesignation']);
                $order->setReceiverAddress($data['receiverAddress']);
                $order->setCourier(null);
                $order->setCnNo(null);
                $order->setComments(null);
           }

           $em->persist($order);
           $em->flush();
           $order = $this->getDoctrine()->getRepository(RequisitionOrder::class)->bankRequisitionOrderItem($order,$data);
           $this->getDoctrine()->getRepository(Stock::class)->processRequisitionOrderUpdateQnt($order->getRequisitionOrder());
           $this->getDoctrine()->getRepository(StockHistory::class)->processInsertOrderItem($order);
           $message = $translator->trans('data.updated_successfully');
           $this->addFlash('success', $message);
           return $this->redirectToRoute('procure_requisition_bank_store_approval');
        }
        $couriers = $particularRepository->getChildRecords($config,'courier-service');
        return $this->render('@TerminalbdProcurement/bank/requisition-order/storeProcess.html.twig', [
            'entity' => $order->getRequisitionOrder()->getRequisition(),
            'deliver' => $order,
            'couriers' => $couriers,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/new-order", methods={"GET","POST"}, name="procure_requisition_bank_order_new" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function newProcess(Request $request , $id,TranslatorInterface $translator, RequisitionRepository $repository,ParticularRepository $particularRepository , ProcurementRepository $procurementRepository, ProcurementProcessRepository $processRepository, ModuleProcessItemRepository $processItemRepository, RequisitionOrderRepository $requisitionOrderRepository): Response
    {
        /* @var $config  Procurement */
        $em = $this->getDoctrine()->getManager();
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $entity = $repository->find($id);
        $exist = $this->getDoctrine()->getRepository(RequisitionOrder::class)->count(array('requisition'=>$id));
        if(!empty($exist)){
            $message = "This store requisition no {$entity->getRequisitionNo()}  already process";
            $this->addFlash('notice', $message);
            return $this->redirectToRoute('procure_requisition_bank_store_approval');
        }
        $form = $this->createForm( StoreCreditNoteFormType::class, new OrderDelivery(), array('config'=>$config));
        $form->handleRequest($request);
        $order = new RequisitionOrder();
        $order->setConfig($config);
        $moduleProcess = $em->getRepository(ModuleProcess::class)->existModuleProcess($terminal, "requisition-order");
        if (!empty($moduleProcess)) {
            $order->setModuleProcess($moduleProcess);
            $order->setModule($moduleProcess->getModule()->getSlug());
        }
        $order->setRequisition($entity);
        $order->setProcess('New');
        $order->setWaitingProcess('New');
        $order->setCreatedBy($this->getUser());
        $em->persist($order);
        $em->flush();
        $delivery = new OrderDelivery();
        $delivery->setRequisitionOrder($order);
        $delivery->setConfig($config);
        $delivery->setCreatedBy($this->getUser());
        $em->persist($delivery);
        $em->flush();
        return $this->redirectToRoute('procure_requisition_bank_order_process',['id'=>$delivery->getId()]);

    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/delivery-method", methods={"GET"}, name="procure_tender_bank_delivery_method" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function deliveryMode(OrderDelivery $entity): Response
    {
        $mode = $this->getDoctrine()->getRepository(Particular::class)->find( $_REQUEST['mode']);
        $em = $this->getDoctrine()->getManager();
        $entity->setDeliveryMethod($mode);
        $em->persist($entity);
        $em->flush();
        return new Response('success');
    }



    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/receive-print", methods={"GET","POST"}, name="procure_requisition_bank_order_print" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function print(Request $request,$id,OrderDeliveryRepository $repository,ProcurementRepository $procurementRepository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $entity = $repository->find($id);
        return $this->render('@TerminalbdProcurement/requisition-order/print.html.twig', [
            'entity' => $entity,
        ]);
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/preview", methods={"GET"}, name="procure_requisition_bank_order_preview" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function printPreview($id,OrderDeliveryRepository $repository,ProcurementRepository $procurementRepository): Response
    {
        $entity = $repository->find($id);
        if($mode == 'print'){
            return $this->render(
                '@TerminalbdProcurement/bank/requisition-order/preview.html.twig', array(
                    'entity' => $entity,
                    'mode' => 'print'
                )
            );
        }elseif ($mode == 'pdf'){
            $mpdf = new \Mpdf\Mpdf(
            );
            $html = $this->renderView('@TerminalbdProcurement/bank/requisition-order/pdf.html.twig',array(
                'entity' => $entity,
                'mode' => "pdf"
            ));
            $mpdf->WriteHTML($html);
            $mpdf->Output();
        }else{
            return $this->render(
                '@TerminalbdProcurement/bank/requisition-order/preview.html.twig', array(
                    'entity' => $entity,
                    'mode' => 'preview',
                )
            );
        }

    }


    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/receive-print", methods={"GET","POST"}, name="procure_requisition_store_show" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function show(Request $request,$id,OrderDeliveryRepository $repository,ProcurementRepository $procurementRepository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $entity = $repository->find($id);
        return $this->render('@TerminalbdProcurement/requisition-order/print.html.twig', [
            'entity' => $entity,
        ]);
    }


}
