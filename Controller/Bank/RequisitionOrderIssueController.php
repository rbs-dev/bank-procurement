<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Controller\Bank;

use App\Repository\Application\ProcurementRepository;
use App\Repository\Domain\ApprovalUserRepository;
use App\Repository\UserRepository;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\InventoryBundle\Entity\StockBook;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\RequisitionIssue;
use Terminalbd\ProcurementBundle\Entity\RequisitionOrder;
use Terminalbd\ProcurementBundle\Entity\RequisitionOrderItem;
use Terminalbd\ProcurementBundle\Form\ApproveCommentFormType;
use Terminalbd\ProcurementBundle\Form\ProcurementProcessFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFilterFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionOrderApproveFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionOrderFormType;
use Terminalbd\ProcurementBundle\Repository\JobRequisitionRepository;
use Terminalbd\ProcurementBundle\Repository\ProcurementProcessRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionIssueRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionOrderRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionRepository;


/**
 * @Route("/procure/bssf/order-issue")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class RequisitionOrderIssueController extends AbstractController
{

    public function paginate(Request $request ,$entities)
    {

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $request->query->get('page', 1)/*page number*/,
            25  /*limit per page*/
        );
        return $pagination;
    }


    /**
     * @Route("/", methods={"GET", "POST"}, name="procure_requisition_bank_order_issue")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function index(Request $request, TranslatorInterface $translator, RequisitionOrderRepository $repository , ProcurementRepository $procurementRepository): Response
    {
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , null,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $mode = array('mode'=> $mode);
            $data = array_merge($data,$mode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
        }
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->findRequisitionSearchQuery($config,$this->getUser(),$data);
        } else {
            $search = $repository->findRequisitionSearchQuery($config,$this->getUser(),$data);
        }
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/bank/order-issue/index.html.twig',
            [
                'pagination' => $pagination,
                'mode' => $mode,
                'searchForm' => $searchForm->createView()
            ]
        );
    }

    /**
     * @Route("/requisition-issued", methods={"GET", "POST"}, name="procure_requisition_garments_requisition_issued")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function issued(Request $request, RequisitionIssueRepository $repository , ProcurementRepository $procurementRepository): Response
    {
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , null,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)){
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $data = array('mode'=> $mode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
        }
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->findRequisitionIssuedSearchQuery($config,$this->getUser(),$data);
        } else {
            $search = $repository->findRequisitionIssuedSearchQuery($config,$this->getUser(),$data);
        }
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/bank/order-issue/requsition-issued.html.twig',
            [
                'pagination' => $pagination,
                'mode' => $mode,
                'searchForm' => $searchForm->createView()
            ]
        );
    }



    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/generated", methods={"GET"}, name="procure_requisition_bank_order_issue_generate" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function requisitionGenerated($id, RequisitionIssueRepository $issueRepository, RequisitionOrderRepository $requisitionRepository, ProcurementProcessRepository $processRepository,ApprovalUserRepository $approvalUserRepository): Response
    {
        $entity = $issueRepository->find($id);
        $terminal = $this->getUser()->getTerminal()->getId();
        $newEntity = $requisitionRepository->insertOrderFromRequisitionIssue($entity);
        $approveUsers = $approvalUserRepository->getDepartmentApprovalAssignUser($terminal,$newEntity);
        $processRepository->insertProcurementProcessAssign($newEntity,$newEntity->getModule(),$approveUsers);
        return $this->redirectToRoute('procure_requisition_bank_order_issue_edit',array('id' => $newEntity->getId()));
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="procure_requisition_bank_order_issue_edit")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function edit(Request $request, $id,TranslatorInterface $translator, UserRepository $userRepository,ApprovalUserRepository $approvalUserRepository , ProcurementRepository $procurementRepository, RequisitionOrderRepository $repository,ProcurementProcessRepository $processRepository): Response
    {

        $terminal = $this->getUser()->getTerminal();

        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);

        /* @var $entity RequisitionOrder */

        $entity = $repository->findOneBy(array('config' => $config,'id' => $id));
        $form = $this->createForm(ApproveCommentFormType::class , null);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $request->request->all();
            $repository->insertOrderIssueItem($entity,$data);
            $processRepository->approvalAssign($entity);
            $this->getDoctrine()->getManager()->flush();
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('procure_requisition_bank_order_issue',['mode'=>'list']);
        }
        return $this->render('@TerminalbdProcurement/bank/order-issue/edit.html.twig', [
            'entity'            => $entity,
            'form'              => $form->createView(),
        ]);
    }


    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET"}, name="procure_requisition_bank_order_issue_show" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function show($id, RequisitionOrderRepository $repository): Response
    {
        $entity = $repository->find($id);
        $html = $this->renderView(
            '@TerminalbdProcurement/bank/order-issue/show.html.twig', array(
                'entity' => $entity,
            )
        );
        return new Response($html);
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/process", methods={"GET","POST"}, name="procure_requisition_bank_order_issue_process" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function process(Request $request , RequisitionOrder $entity, RequisitionOrderRepository $repository,TranslatorInterface $translator, ProcurementRepository $procurementRepository, ProcurementProcessRepository $processRepository, RequisitionIssueRepository $requisitionIssueRepository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $form = $this->createForm(ApproveCommentFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $data = $request->request->all();
            $comment = $form["comment"]->getData();
            $processRepository->insertApprovalProcessForGenericComment($this->getUser(),$entity,$comment);
            if(isset($data['reject']) and $data['reject'] == "reject"){
                $entity->setProcess('Rejected');
                $entity->setWaitingProcess('Rejected');
                $entity->setReportTo(null);
                $em->persist($entity);
                $em->flush();
                $message = $translator->trans('data.reject_successfully');
                $this->addFlash('success', $message);
            }else{
                $count = $processRepository->count(array('entityId'=> $entity->getId(), 'module'=>$entity->getModule(),'close'=>1));
                $ordering = ($count == 0) ? 1 : $count+1;
                $processRepository->approvalAssign($entity,$ordering);
                $message = $translator->trans('data.updated_successfully');
                $this->addFlash('success', $message);
            }
            if($entity->getWaitingProcess() == "Approved"){
                $issue = $entity->getRequisitionIssue();
                $issue->setWaitingProcess('Closed');
                $em->flush();
                $this->getDoctrine()->getRepository(StockBook::class)->stockOutRequisitionOrderStockQuantity($entity);
            }
            return $this->redirectToRoute('procure_requisition_bank_order_issue',['mode'=>'approve']);
        }
        return $this->render('@TerminalbdProcurement/bank/order-issue/process.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Update a RequisitionItem entity.
     * @Route("/{id}/update-item", methods={"GET","POST"}, name="procure_requisition_bank_order_issue_item_update", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function updateItem(Request $request,RequisitionOrderItem $item ): Response
    {

        /* @var $item  RequisitionOrderItem */

        $data = $request->request->all();
        $item->setQuantity($data['quantity']);
        $this->getDoctrine()->getManager()->flush();
        return  new Response('success');

    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/status", methods={"GET"}, name="procure_requisition_bank_order_issue_status" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function status($id): Response
    {
        $repository = $this->getDoctrine()->getRepository(Particular::class);
        $entity = $repository->find($id);
        $status = $_REQUEST['status'];
        if($status == "false"){
            $entity->setStatus(false);
        }else{
            $entity->setStatus(true);
        }
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');
    }



    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="procure_requisition_bank_order_issue_delete")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function delete($id, ProcurementRepository $procurementRepository, RequisitionOrderRepository $repository): Response
    {

        /* @var $config  GenericMaster */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $entity = $repository->findOneBy(array('config' => $config,'id' => $id));

        $em = $this->getDoctrine()->getManager();
        $response = "invalid";
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Particular entity.');
        }
        try {
            $em->remove($entity);
            $em->flush();
            $response = 'valid';

        } catch (ForeignKeyConstraintViolationException $e) {
            $this->get('session')->getFlashBag()->add(
                'notice',"Data has been relation another Table"
            );
        }catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add(
                'notice', 'Please contact system administrator further notification.'
            );
        }
        return new Response($response);
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/preview", methods={"GET"}, name="procure_requisition_bank_order_issue_preview" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function printPreview(RequisitionOrder $entity): Response
    {
        $mode = isset($_REQUEST['mode']) ? $_REQUEST['mode'] :'';
        $approvals = $this->getDoctrine()->getRepository(ProcurementProcess::class)->findBy(array('entityId'=>$entity->getId(),'module'=>$entity->getModule()),array('ordering'=>'ASC'));

        if($mode == 'print'){
            return $this->render(
                '@TerminalbdProcurement/bank/order-issue/preview.html.twig', array(
                    'entity' => $entity,
                    'approvals' => $approvals,
                    'mode' => 'print'
                )
            );
        }elseif ($mode == 'pdf'){
             $mpdf = new \Mpdf\Mpdf(['tempDir' => __DIR__ . '/../../../../public/temp']);
            $html = $this->renderView('@TerminalbdProcurement/bank/order-issue/pdf.html.twig',array(
                'entity' => $entity,
                'approvals' => $approvals,
                'mode' => "pdf"
            ));
            $mpdf->WriteHTML($html);
            $mpdf->Output();
        }else{
            return $this->render(
                '@TerminalbdProcurement/bank/order-issue/preview.html.twig', array(
                    'entity' => $entity,
                    'approvals' => $approvals,
                    'mode' => 'preview'
                )
            );
        }

    }



}
