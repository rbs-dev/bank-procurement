<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Controller\Bank;

use App\Entity\Application\Procurement;
use App\Entity\Core\Setting;
use App\Entity\Domain\ApprovalUser;
use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\Vendor;
use App\Entity\User;
use App\Repository\Application\ProcurementRepository;
use App\Repository\Domain\ApprovalUserRepository;
use App\Service\FormValidationManager;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\ProcurementBundle\Entity\DeliveryBatch;
use Terminalbd\ProcurementBundle\Entity\OrderDelivery;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementCondition;
use Terminalbd\ProcurementBundle\Entity\ProcurementConditionItem;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\Tender;
use Terminalbd\ProcurementBundle\Entity\TenderBatch;
use Terminalbd\ProcurementBundle\Entity\TenderBatchItem;
use Terminalbd\ProcurementBundle\Entity\TenderCommittee;
use Terminalbd\ProcurementBundle\Entity\TenderComparative;
use Terminalbd\ProcurementBundle\Entity\TenderCompare;
use Terminalbd\ProcurementBundle\Entity\TenderConditionItem;
use Terminalbd\ProcurementBundle\Entity\TenderItem;
use Terminalbd\ProcurementBundle\Entity\TenderMemo;
use Terminalbd\ProcurementBundle\Entity\TenderMemoItem;
use Terminalbd\ProcurementBundle\Entity\TenderMemoUser;
use Terminalbd\ProcurementBundle\Entity\TenderVendor;
use Terminalbd\ProcurementBundle\Form\ApproveCommentFormType;
use Terminalbd\ProcurementBundle\Form\Bank\DeliveryBatchFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderBatchFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderCommitteeFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderFloatingFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderManagementMemoFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderPurchaseMemoFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderQuotationFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderRepeatOrderFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFilterFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderCsFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderPreparetionFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderVendorFormType;
use Terminalbd\ProcurementBundle\Repository\ProcurementProcessRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionItemRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionRepository;
use Terminalbd\ProcurementBundle\Repository\TenderBatchRepository;
use Terminalbd\ProcurementBundle\Repository\TenderCommitteeRepository;
use Terminalbd\ProcurementBundle\Repository\TenderItemRepository;
use Terminalbd\ProcurementBundle\Repository\TenderMemoRepository;
use Terminalbd\ProcurementBundle\Repository\TenderRepository;
use Terminalbd\ProcurementBundle\Repository\TenderVendorRepository;
use Terminalbd\ProcurementBundle\Entity\ComapnyRequisitionShare;



/**
 * @Route("/procure/bssf/delivery-batch")
 * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_LSSD')")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class DeliveryBatchController extends AbstractController
{

    public function paginate(Request $request ,$entities)
    {
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $request->query->get('page', 1)/*page number*/,
            25  /*limit per page*/
        );
        return $pagination;
    }

    /**
     * @Route("/", methods={"GET", "POST"}, name="procure_delivery_batch_bank")
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_LSSD_ADMIN')")
     */
    public function index(Request $request, TranslatorInterface $translator,ProcurementRepository $procurementRepository , TenderMemoRepository $repository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $data = $request->request->all();
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , NULL,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        $search = $this->getDoctrine()->getRepository(DeliveryBatch::class)->findDeliveryBatchQuery($config,$this->getUser(),$data);
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/bank/delivery-batch/index.html.twig', [
            'config' => $config,
            'pagination' => $pagination,
            'mode' => '',
            'searchForm' => $searchForm->createView()
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/new", methods={"GET", "POST"}, name="procure_delivery_batch_bank_new")
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN')")
     */
    public function new(Request $request,TranslatorInterface $translator , ProcurementRepository $procurementRepository ): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $data = $request->request->all();
        $terminal = $this->getUser()->getTerminal()->getId();
        $entity = new DeliveryBatch();
        $config = $procurementRepository->config($terminal);
        $form = $this->createForm(DeliveryBatchFormType::class, $entity)->add('SaveAndCreate', SubmitType::class);;
        $form->handleRequest($request);
        $data = $request->request->all();
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $request->request->all();
            $em = $this->getDoctrine()->getManager();
            $entity->setConfig($config);
            $entity->setCreatedBy($this->getUser());
            $em->persist($entity);
            $em->flush();
            $this->getDoctrine()->getRepository(OrderDelivery::class)->generateBatch($entity);
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('procure_delivery_batch_bank_generate',array('id' => $entity->getId()));
        }
        return $this->render('@TerminalbdProcurement/bank/delivery-batch/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/ajax-update", methods={"GET", "POST"}, name="procure_tender_floating_memo_bank_ajax_update",options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_LSSD')")
     */
    public function ajaxUpadteRequsition(Request $request ,TenderMemo $entity,ProcurementRepository $procurementRepository)
    {

        /* @var $config  Procurement */

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $form = $this->createForm(TenderFloatingFormType::class, $entity, array('terminal' => $this->getUser()->getTerminal(),'config' => $config));
        $form->handleRequest($request);
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
        $files = !empty($_FILES['files']) ? $_FILES['files']:'';
        if($files){
            $this->getDoctrine()->getRepository(ProcurementProcess::class)->insertAttachmentFile($entity,$data,$files);
        }
        return new Response('success');
    }

    /**
     * Show a Setting entity.
     * @Route("/{id}/process", methods={"GET","POST"}, name="procure_delivery_batch_bank_process" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function process(Request $request , TranslatorInterface $translator, DeliveryBatch $entity, ProcurementRepository $procurementRepository, ProcurementProcessRepository $processRepository): Response
    {
        $data = $request->request->all();
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $form = $this->createForm(ApproveCommentFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $comment = $form["comment"]->getData();
            if(isset($data['reject']) and $data['reject'] == "reject"){
                $entity->setProcess('Rejected');
                $entity->setRejectedBy($this->getUser());
                $entity->setRejectComment($comment);
            }elseif(isset($data['approve']) and $data['approve'] == "approve"){
                $entity->setComment($comment);
                $entity->setProcess('Approved');
                $entity->setComment($comment);
                $entity->setApprovedBy($this->getUser());
            }
            $em->persist($entity);
            $em->flush();
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('procure_delivery_batch_bank',['mode'=>'approve']);
        }
        $orders = $this->getDoctrine()->getRepository(OrderDelivery::class)->getDeliveryOrders($entity);
        return $this->render('@TerminalbdProcurement/bank/delivery-batch/process.html.twig', [
            'entity' => $entity,
            'pagination' => $orders,
            'form' => $form->createView(),
        ]);
    }



    /**
     * Show a TenderMemo entity.
     *
     * @Route("/{id}/generate", methods={"GET","POST"}, name="procure_delivery_batch_bank_generate" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN')")
     */
    public function generate(Request $request ,DeliveryBatch $entity,TranslatorInterface $translator): Response
    {
        $orders = $this->getDoctrine()->getRepository(OrderDelivery::class)->getDeliveryOrders($entity);
        $form = $this->createForm(DeliveryBatchFormType::class, $entity)->add('SaveAndCreate', SubmitType::class);;
        $form->add('comment', TextareaType::class, [
            'attr' => ['autofocus' => true,'class'=>'textarea input','rows' => 4],
            'required' => false,
            'help' => "",
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $comment = $form["comment"]->getData();
            $entity->setContent($comment);
            $entity->setProcess("Complete");
            $em->persist($entity);
            $em->flush();
            $this->getDoctrine()->getRepository(OrderDelivery::class)->generateBatch($entity);
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('procure_delivery_batch_bank_generate',['id'=>$entity->getId()]);
        }
        return $this->render('@TerminalbdProcurement/bank/delivery-batch/generate.html.twig', [
            'entity' => $entity,
            'pagination' => $orders,
            'form' => $form->createView(),
        ]);
      
    }

    /**
     * Show a TenderMemo entity.
     *
     * @Route("/{id}/show", methods={"GET","POST"}, name="procure_delivery_batch_bank_show" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN')")
     */
    public function show(Request $request ,DeliveryBatch $entity,TranslatorInterface $translator): Response
    {
        $orders = $this->getDoctrine()->getRepository(OrderDelivery::class)->getDeliveryOrders($entity);
        return $this->render('@TerminalbdProcurement/bank/delivery-batch/show.html.twig', [
            'entity' => $entity,
            'pagination' => $orders,
        ]);

    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="procure_delivery_batch_bank_delete")
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN')")
     */

    public function delete(DeliveryBatch $entity): Response
    {
        $em = $this->getDoctrine()->getManager();
        $response = "invalid";
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Particular entity.');
        }
        $this->getDoctrine()->getRepository(OrderDelivery::class)->resetBatch($entity->getId());
        try {
            $em->remove($entity);
            $em->flush();
            $response = 'valid';

        } catch (ForeignKeyConstraintViolationException $e) {
            $this->get('session')->getFlashBag()->add(
                'notice',"Data has been relation another Table"
            );
        }catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add(
                'notice', 'Please contact system administrator further notification.'
            );
        }
        return new Response($response);
    }


    

}
