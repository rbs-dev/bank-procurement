<?php
/**
 * Created by PhpStorm.
 * User: shafiq
 * Date: 9/29/19
 * Time: 9:09 PM
 */

namespace Terminalbd\ProcurementBundle\Controller\Bank;
use App\Entity\Application\GenericMaster;
use App\Entity\Application\Inventory;
use App\Entity\Application\Procurement;
use App\Entity\Core\Setting;
use App\Entity\Domain\Branch;
use App\Repository\Application\ProcurementRepository;
use App\Repository\Domain\BranchRepository;
use Mpdf\Tag\Option;
use PhpOffice\PhpSpreadsheet\Writer\Pdf\Dompdf;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Terminalbd\BankBundle\Form\FilterFormType;
use Terminalbd\DmsBundle\Repository\DmsFileRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Terminalbd\GenericBundle\Entity\Category;
use Terminalbd\GenericBundle\Entity\Item;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\InventoryBundle\Entity\StockBook;
use Terminalbd\InventoryBundle\Entity\StockHistory;
use Terminalbd\ProcurementBundle\Entity\DeliveryBatch;
use Terminalbd\ProcurementBundle\Entity\OrderDelivery;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\Report;
use Terminalbd\ProcurementBundle\Entity\RequisitionAgingApprovalStatus;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorder;
use Terminalbd\ProcurementBundle\Form\Bank\ReportFilterFormType;
use Terminalbd\ProcurementBundle\Form\ProcurementReportFilterFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFilterFormType;
use Terminalbd\ProcurementBundle\Repository\JobRequisitionRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionAgingApprovalStatusRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionItemRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionOrderRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionRepository;


/**
 * @Route("/procure/report/")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */

class ReportStoreController extends AbstractController
{

    /**
     * @Route("bank-requisition-item", name="bank_procure_report_requisition_item")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    function index(RequisitionRepository $requisitionRepository, RequisitionOrderRepository $requisitionOrderRepository, JobRequisitionRepository $repository) {
        $user = $this->getUser()->getId();
        $jobcount = $repository->count(['reportTo'=> $user]);
        $reqcount = $requisitionRepository->count(['reportTo'=> $user]);
        $ordercount = $requisitionOrderRepository->count(['reportTo'=> $user]);
        return $this->render('@TerminalbdProcurement/default/dashboard.html.twig', [
            'jobcount'=> $jobcount,
            'reqcount'=> $reqcount,
            'ordercount'=> $ordercount
        ]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("monthly-stock-report/{mode}", defaults={"mode"=null}, methods={"GET"}, name="procure_store_report_monthly")
     */
    public function monthlyStockReport(Request $request,$mode ='')
    {
        $entities = [];
        $data = [];
        $terminal = $this->getUser()->getTerminal();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $this->getDoctrine()->getRepository(Procurement::class)->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $getData = $searchForm->getData();
        $filterBy = $_REQUEST;
        $filepath = $this->get('kernel')->getProjectDir();
        if(in_array($mode,['pdf','excel'])){
            $caption = '';
            $data = $request->query->get('filterBy');
            $entities = $this->getDoctrine()->getRepository(Stock::class)->getBankMonthlyStatement($config,$filterBy);
        }

        if ($mode == 'excel'){

            $fileName = 'report-item-wise'.'_'.time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/bank/report-store/monthly-statement/excel.html.twig', ['data' => $entities]);
            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");
            echo $html;
            die();

        }elseif ($mode == 'pdf'){
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);
            if($terminal->getCoreDomain() and $terminal->getCoreDomain()->getWebHeaderPath()){
                $imageHeaderPath = "{$filepath}/public/{$terminal->getCoreDomain()->getWebHeaderPath()}";
//                $mpdf->SetHTMLHeader('<div style="padding-bottom: -50px"><img width="100%" src="'. $imageHeaderPath .'"></div>');
            }
            if($terminal->getCoreDomain() and $terminal->getCoreDomain()->getWebFooterPath()){
                $imageFooterPath = "{$filepath}/public/{$terminal->getCoreDomain()->getWebFooterPath()}";
//                $mpdf->SetHTMLFooter('<div style="background-color:red; height: 20px"><img width="100%" src="'. $imageFooterPath .'"></div>');
            }
            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/bank/report-store/monthly-statement/pdf.html.twig',[
                'data' => $entities,
                'headerImage' => ($terminal->getCoreDomain()->getWebHeaderPath())?:'',
                'footerImage' => $terminal->getCoreDomain()->getWebFooterPath()?:'',
            ]);
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }
        return $this->render('@TerminalbdProcurement/bank/report-store/monthly-statement/index.html.twig',[
            'searchForm' => $searchForm->createView(),
            'company' => '',
            'entities' => $entities,
            'filterBy' => $data,
        ]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("monthly-till-stock-report/{mode}", defaults={"mode"=null}, methods={"GET"}, name="procure_store_report_monthly")
     */
    public function monthlyTillStockReport(Request $request,$mode ='')
    {
        $entities = [];
        $data = [];
        $terminal = $this->getUser()->getTerminal();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $this->getDoctrine()->getRepository(Procurement::class)->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $getData = $searchForm->getData();
        $filterBy = $_REQUEST;
        $filepath = $this->get('kernel')->getProjectDir();
        if ($searchForm->isSubmitted()){
            $filterBy['endDate'] = $searchForm->get('endDate')->getViewData();
        }else{
            $filterBy['endDate'] = date('d-m-Y');
        }
        if(in_array($mode,['pdf','excel'])){
            $caption = '';
            $data = $request->query->get('filterBy');
            $entities = $this->getDoctrine()->getRepository(Stock::class)->getBankMonthlyStatement($config,$filterBy);
        }

        if ($mode == 'excel'){

            $fileName = 'report-item-wise'.'_'.time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/bank/report-store/monthly-statement/excel.html.twig', ['data' => $entities]);
            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");
            echo $html;
            die();

        }elseif ($mode == 'pdf'){
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);
            if($terminal->getCoreDomain() and $terminal->getCoreDomain()->getWebHeaderPath()){
                $imageHeaderPath = "{$filepath}/public/{$terminal->getCoreDomain()->getWebHeaderPath()}";
//                $mpdf->SetHTMLHeader('<div style="padding-bottom: -50px"><img width="100%" src="'. $imageHeaderPath .'"></div>');
            }
            if($terminal->getCoreDomain() and $terminal->getCoreDomain()->getWebFooterPath()){
                $imageFooterPath = "{$filepath}/public/{$terminal->getCoreDomain()->getWebFooterPath()}";
//                $mpdf->SetHTMLFooter('<div style="background-color:red; height: 20px"><img width="100%" src="'. $imageFooterPath .'"></div>');
            }
            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/bank/report-store/monthly-statement/pdf.html.twig',[
                'data' => $entities,
                'filterBy' => $filterBy,
                'headerImage' => ($terminal->getCoreDomain()->getWebHeaderPath())?:'',
                'footerImage' => $terminal->getCoreDomain()->getWebFooterPath()?:'',
            ]);
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }
        return $this->render('@TerminalbdProcurement/bank/report-store/monthly-statement/index.html.twig',[
            'searchForm' => $searchForm->createView(),
            'company' => '',
            'entities' => $entities,
            'filterBy' => $filterBy,
        ]);
    }



/**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("credite-note-report/{mode}", defaults={"mode"=null}, methods={"GET"}, name="procure_store_report_credit_note")
     */
    public function crediteNoteReport(Request $request, $mode, ProcurementRepository $procurementRepository, RequisitionItemRepository $repository)
    {
        $entities = [];
        $data = [];
        $filterBy = [];
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $getData = $searchForm->getData();
        $filepath = $this->get('kernel')->getProjectDir();
        if ($searchForm->isSubmitted()){
            $filterBy['startDate'] = $searchForm->get('startDate')->getViewData();
            $filterBy['endDate'] = $searchForm->get('endDate')->getViewData();
        }
        if ($mode == 'excel'){
            $fileName = 'report-credit-note'.'_'.time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/bank/report-store/credit-note/excel.html.twig', ['entities' => $entities]);
            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");
            echo $html;
            die();

        }elseif ($mode == 'pdf'){
            $data = $request->query->get('filterBy');
            $entities = $this->getDoctrine()->getRepository(OrderDelivery::class)->bankCreditNotes($config,$data);
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);
            if($terminal->getCoreDomain() and $terminal->getCoreDomain()->getWebHeaderPath()){
                $imageHeaderPath = "{$filepath}/public/{$terminal->getCoreDomain()->getWebHeaderPath()}";
            }
            if($terminal->getCoreDomain() and $terminal->getCoreDomain()->getWebFooterPath()){
                $imageFooterPath = "{$filepath}/public/{$terminal->getCoreDomain()->getWebFooterPath()}";
            }
            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/bank/report-store/credit-note/pdf.html.twig',[
                'entities'      => $entities,
                'headerImage'   => ($terminal->getCoreDomain()->getWebHeaderPath()) ? : '',
                'footerImage'   => $terminal->getCoreDomain()->getWebFooterPath() ? : '',
//                'searchForm'    => $searchForm->createView(),
                'filterBy' => $data
            ]);
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }
        return $this->render('@TerminalbdProcurement/bank/report-store/credit-note/index.html.twig',[
            'searchForm' => $searchForm->createView(),
            'entities' => $entities,
            'filterBy' => $filterBy,
        ]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("bank-store-ledger/{mode}", defaults={"mode"=null}, methods={"GET","POST"}, name="procure_store_report_store_ledger")
     */
    public function storeLedger(Request $request, $mode, ProcurementRepository $procurementRepository, RequisitionItemRepository $repository)
    {
        $entities = [];
        $data = [];
        $filterBy = [];
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $getData = $searchForm->getData();
        $filepath = $this->get('kernel')->getProjectDir();
        if ($searchForm->isSubmitted()){
            $filterBy['startDate'] = $searchForm->get('startDate')->getViewData();
            $filterBy['endDate'] = $searchForm->get('endDate')->getViewData();
        }
        if ($mode == 'excel'){
            $fileName = $request->attributes->get('_route').'_'.time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/bank/report-store/ledger/excel.html.twig', ['entities' => $entities]);
            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");
            echo $html;
            die();

        }elseif ($mode == 'pdf'){
            $data = $request->query->get('filterBy');
            $entities = $this->getDoctrine()->getRepository(Stock::class)->getBankStoreLedgerDateRange($config,$data);
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);
            if($terminal->getCoreDomain() and $terminal->getCoreDomain()->getWebHeaderPath()){
                $imageHeaderPath = "{$filepath}/public/{$terminal->getCoreDomain()->getWebHeaderPath()}";
            }
            if($terminal->getCoreDomain() and $terminal->getCoreDomain()->getWebFooterPath()){
                $imageFooterPath = "{$filepath}/public/{$terminal->getCoreDomain()->getWebFooterPath()}";
            }
            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/bank/report-store/ledger/pdf.html.twig',[
                'entities'      => $entities,
                'headerImage'   => ($terminal->getCoreDomain()->getWebHeaderPath()) ? : '',
                'footerImage'   => $terminal->getCoreDomain()->getWebFooterPath() ? : '',
//                'searchForm'    => $searchForm->createView(),
                'filterBy' => $data
            ]);
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }
        return $this->render('@TerminalbdProcurement/bank/report-store/ledger/index.html.twig',[
            'searchForm' => $searchForm->createView(),
            'entities' => $entities,
            'filterBy' => $filterBy,
        ]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("bank-store-item-ledger/{mode}", defaults={"mode"=null}, methods={"GET","POST"}, name="procure_store_report_store_item_ledger")
     */
    public function storeItemLedger(Request $request, $mode, ProcurementRepository $procurementRepository, RequisitionItemRepository $repository)
    {

        $entities = [];
        $data = [];
        $filterBy = [];


        /* @var $config  Procurement */

        $terminal = $this->getUser()->getTerminal();
      //  $config = $procurementRepository->config($terminal->getId());
        $config = $this->getDoctrine()->getRepository(Inventory::class)->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $getData = $searchForm->getData();
        $filepath = $this->get('kernel')->getProjectDir();
         if ($searchForm->isSubmitted()){
            $filterBy['item'] = $searchForm->get('item')->getViewData();
            $filterBy['startDate'] = $searchForm->get('startDate')->getViewData();
            $filterBy['endDate'] = $searchForm->get('endDate')->getViewData();
        }
        if ($mode == 'excel'){
            $fileName = $request->attributes->get('_route').'_'.time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/bank/report-store/ledger/excel.html.twig', ['entities' => $entities]);
            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");
            echo $html;
            die();

        }elseif ($mode == 'pdf'){
            $data = $request->query->get('filterBy');
            $records = $this->getDoctrine()->getRepository(StockHistory::class)->stockItemLedger($config->getId(),$data);
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);
            if($terminal->getCoreDomain() and $terminal->getCoreDomain()->getWebHeaderPath()){
                $imageHeaderPath = "{$filepath}/public/{$terminal->getCoreDomain()->getWebHeaderPath()}";
            }
            if($terminal->getCoreDomain() and $terminal->getCoreDomain()->getWebFooterPath()){
                $imageFooterPath = "{$filepath}/public/{$terminal->getCoreDomain()->getWebFooterPath()}";
            }
            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $item = $this->getDoctrine()->getRepository(Item::class)->find($data['item']);
            $html = $this->renderView('@TerminalbdProcurement/bank/report-store/item-ledger/pdf.html.twig',[
                'item'      => $item,
                'records'      => $records,
                'headerImage'   => ($terminal->getCoreDomain()->getWebHeaderPath()) ? : '',
                'footerImage'   => $terminal->getCoreDomain()->getWebFooterPath() ? : '',
//                'searchForm'    => $searchForm->createView(),
                'filterBy' => $data
            ]);
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }
        return $this->render('@TerminalbdProcurement/bank/report-store/item-ledger/item.html.twig',[
            'searchForm' => $searchForm->createView(),
            'entities' => $entities,
            'filterBy' => $filterBy,
        ]);
    }


}