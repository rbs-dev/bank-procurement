<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Controller\Bank;

use App\Entity\Admin\AppModule;
use App\Entity\Application\GenericMaster;
use App\Entity\Application\Procurement;
use App\Entity\Core\Setting;
use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\ModuleProcessItem;
use App\Entity\User;
use App\Repository\Application\GenericMasterRepository;
use App\Repository\Application\ProcurementRepository;
use App\Repository\Domain\ModuleProcessItemRepository;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\Transport;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\DmsBundle\Repository\DmsFileRepository;
use Terminalbd\InventoryBundle\Repository\StockRepository;
use Terminalbd\ProcurementBundle\Entity\JobRequisition;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Form\ApproveCommentFormType;
use Terminalbd\ProcurementBundle\Form\Garments\GarmentRequisitionFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionApproveFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFilterFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFormType;
use Terminalbd\ProcurementBundle\Repository\ComapnyRequisitionShareRepository;
use Terminalbd\ProcurementBundle\Repository\ParticularRepository;
use Terminalbd\ProcurementBundle\Repository\ProcurementProcessRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionRepository;


/**
 * @Route("/procure/bssf/requisition")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class RequisitionController extends AbstractController
{

    public function paginate(Request $request ,$entities)
    {

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $request->query->get('page', 1)/*page number*/,
            25  /*limit per page*/
        );
        return $pagination;
    }


    /**
     * @Route("/", methods={"GET", "POST"}, name="procure_requisition_bank")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_REQUISITION') or is_granted('ROLE_DOMAIN')")
     */
    public function index(Request $request, TranslatorInterface $translator, RequisitionRepository $repository , ProcurementRepository $procurementRepository): Response
    {
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $settingRepository = $this->getDoctrine()->getRepository(Setting::class);
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , null,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $mode = array('mode'=> $mode);
            $data = array_merge($data,$mode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
        }
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->findBankBaseSearchQuery($config,$this->getUser(),$data);
        } else {
            $search = $repository->findBankBaseSearchQuery($config,$this->getUser(),$data);
        }
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/bank/requisition/index.html.twig',
            [
                'pagination' => $pagination,
                'mode' => $mode,
                'searchForm' => $searchForm->createView()
            ]
        );
    }


    /**
     * Show a Setting entity.
     *
     * @Route("/new", methods={"GET"}, name="procure_requisition_bank_new" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_REQUISITION') or is_granted('ROLE_DOMAIN')")
     */
    public function new(Request $request,TranslatorInterface $translator ,ProcurementRepository $procurementRepository, RequisitionRepository $requisitionRepository, ParticularRepository $particularRepository, ProcurementProcessRepository $processRepository): Response
    {
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $types = $particularRepository->getChildRecords($config->getId(),'requisition-type');
        $type = !empty($_REQUEST['type']) ? $_REQUEST['type'] :"";
        $user = $this->getUser();
        $profile = $this->getUser()->getProfile();
        if ((!empty($type) and $user->getProfile()  and !empty($profile->getBranch())) or (!empty($type)  and $user->getProfile() and !empty($profile->getDepartment()))) {
            $rType = $particularRepository->findOneBy(array('config' => $config,'slug' => $type));
            $entity = new Requisition();
            $em = $this->getDoctrine()->getManager();
            $moduleProcess = $this->getDoctrine()->getRepository(ModuleProcess::class)->existModuleProcess($terminal,$type);
            if(!empty($moduleProcess)) {
                $entity->setModuleProcess($moduleProcess);
                $entity->setModule($moduleProcess->getModule()->getSlug());
            }
            $entity->setWaitingProcess("New");
            $entity->setProcess("New");
            $entity->setConfig($config);
            $entity->setRequisitionType($rType);
            $entity->setCreatedBy($user);
            $entity->setDepartment($user->getProfile()->getDepartment());
            $em->persist($entity);
            $em->flush();
            $processRepository->insertRoleProcessAssign($entity);
            $message = $translator->trans('data.created_successfully');
            $this->addFlash('success', $message);
            if($type == "store-requisition"){
                return $this->redirectToRoute('procure_requisition_bank_store_edit',array('id'=>$entity->getId()));
            }else{
                return $this->redirectToRoute('procure_requisition_bank_purchase_edit',array('id'=>$entity->getId()));
            }
        }
        return $this->render('@TerminalbdProcurement/bank/requisition/new.html.twig',['types'=>$types]);
    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="procure_requisition_bank_edit")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_REQUISITION') or is_granted('ROLE_DOMAIN')")
     */
    public function edit(Request $request, $id,TranslatorInterface $translator, RequisitionRepository $repository): Response
    {
        /* @var $entity Requisition */
        $entity = $repository->find($id);
        if($entity->getRequisitionType()->getSlug() == "store-requisition"){
            return $this->redirectToRoute('procure_requisition_bank_store_edit',array('id'=>$entity->getId()));
        }else{
            return $this->redirectToRoute('procure_requisition_bank_purchase_edit',array('id'=>$entity->getId()));
        }
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/ajax-update", methods={"GET", "POST"}, name="procure_requisition_bank_ajax_update")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_REQUISITION') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_ADMIN')")
     */
    public function ajaxUpadteRequsition(Request $request ,$id)
    {
        $terminal = $this->getUser()->getTerminal();
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config =  $this->getDoctrine()->getRepository(Procurement::class)->config($terminal);
        $entity = $this->getDoctrine()->getRepository(Requisition::class)->findOneBy(array('config' => $config,'id' => $id));
        $form = $this->createForm(RequisitionFormType::class , $entity,array('config' => $config));
        $form->handleRequest($request);
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
        $files = !empty($_FILES['files']) ? $_FILES['files']:'';
        if($files){
            $this->getDoctrine()->getRepository(ProcurementProcess::class)->insertAttachmentFile($entity,$data,$files);
        }
        return new Response('success');
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/process", methods={"GET","POST"}, name="procure_requisition_bank_process" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_REQUISITION') or is_granted('ROLE_DOMAIN')")
     */
    public function process(Request $request , $id, RequisitionRepository $repository,TranslatorInterface $translator, ProcurementRepository $procurementRepository, ProcurementProcessRepository $processRepository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        /* @var $entity Requisition */
        $entity = $repository->find($id);
        $form = $this->createForm(ApproveCommentFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $data = $request->request->all();
            $comment = $form["comment"]->getData();
            $processRepository->insertApprovalProcessForGenericComment($this->getUser(),$entity,$comment);
            if(isset($data['reject']) and $data['reject'] == "reject"){
                $entity->setProcess('Rejected');
                $entity->setWaitingProcess('Rejected');
                $entity->setReportTo(null);
                $entity->setProcessOrdering(0);
                $em->persist($entity);
                $em->flush();
                $message = $translator->trans('data.reject_successfully');
                $this->addFlash('success', $message);
            }else{
                $count = $processRepository->count(array('entityId'=> $entity->getId(), 'module'=>$entity->getModule(),'close'=>1));
                $ordering = ($count == 0) ? 1 : $count+1;
                if($entity->getModuleProcess()->getApproveType() == 'user'){
                    $processRepository->approvalAssign($entity,$ordering);
                }elseif($entity->getModuleProcess()->getApproveType() == 'role'){
                    $processRepository->approvalRoleAssign($entity,$ordering);
                }
                $this->getDoctrine()->getRepository(RequisitionItem::class)->updateApproveQuantity($data);
                $this->getDoctrine()->getRepository(RequisitionItem::class)->getItemSummary($entity);
                $repository->requisitionItemHistory($entity);
                $message = $translator->trans('data.updated_successfully');
                $this->addFlash('success', $message);
            }
            return $this->redirectToRoute('procure_requisition_bank',['mode'=>'approve']);
        }
        return $this->render('@TerminalbdProcurement/bank/requisition/process.html.twig', [
            'entity' => $entity,
            'mode' => 'process',
            'form' => $form->createView(),
        ]);
    }



    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET"}, name="procure_requisition_bank_show" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_REQUISITION') or is_granted('ROLE_DOMAIN')")
     */
    public function show($id, RequisitionRepository $repository): Response
    {
        $entity = $repository->find($id);
        $html = $this->renderView(
            '@TerminalbdProcurement/bank/requisition/show.html.twig', array(
                'entity' => $entity,
                'mode' => 'view',
            )
        );
        return new Response($html);
    }


    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/status", methods={"GET"}, name="procure_requisition_bank_status" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_REQUISITION') or is_granted('ROLE_DOMAIN')")
     */
    public function status($id): Response
    {
        $repository = $this->getDoctrine()->getRepository(Particular::class);
        $entity = $repository->find($id);
        $status = $_REQUEST['status'];
        if($status == "false"){
            $entity->setStatus(false);
        }else{
            $entity->setStatus(true);
        }
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');
    }



    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="procure_requisition_bank_delete")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_REQUISITION') or is_granted('ROLE_DOMAIN')")
     */

    public function delete($id): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $this->getDoctrine()->getRepository(Procurement::class)->config($terminal);

        /* @var $entity Requisition */

        $entity = $this->getDoctrine()->getRepository(Requisition::class)->findOneBy(array('config' => $config,'id' => $id));
        $em = $this->getDoctrine()->getManager();
        $response = "invalid";
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Requisition entity.');
        }
        try {
            $em->remove($entity);
            $em->flush();
            $response = 'valid';

        } catch (ForeignKeyConstraintViolationException $e) {
            $this->get('session')->getFlashBag()->add(
                'notice',"Data has been relation another Table"
            );
        }catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add(
                'notice', 'Please contact system administrator further notification.'
            );
        }
        return new Response($response);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/meta-delete", methods={"GET"}, name="procure_requisition_bank_meta_delete")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_REQUISITION') or is_granted('ROLE_DOMAIN')")
     */
    public function metaDelete($id, ItemKeyValueRepository $keyValueRepository): Response
    {
        $entity = $keyValueRepository->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();
        return new Response('Success');
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/preview", methods={"GET"}, name="procure_requisition_bank_preview" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_REQUISITION') or is_granted('ROLE_DOMAIN')")
     */
    public function printPreview($id, RequisitionRepository $repository,ComapnyRequisitionShareRepository $comapnyRequisitionShareRepository): Response
    {
        $entity = $repository->find($id);
        $requisitionShares = $comapnyRequisitionShareRepository->requisitionShares($entity);
        $mode = isset($_REQUEST['mode']) ? $_REQUEST['mode'] :'';
        $approvals = $this->getDoctrine()->getRepository(ProcurementProcess::class)->findBy(array('entityId'=>$id,'module'=>$entity->getModule()),array('ordering'=>'ASC'));

        if($mode == 'print'){

            return $this->render(
                '@TerminalbdProcurement/bank/requisition/preview.html.twig', array(
                    'entity' => $entity,
                    'approvals' => $approvals,
                    'mode' => 'print'
                )
            );

        }elseif ($mode == 'pdf'){

            $mpdfOptions = [
                'tempDir' => __DIR__ . '/../../../../public/temp',
                'format' => 'A4',
                'setAutoTopMargin' => 'pad',
            ];
            $imagePath = __DIR__ . '/../../../../public/assets/images/bank-asia/';
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);
            $mpdf->SetHTMLHeader('<div style="padding-bottom: -50px"><img width="15%" src="'. $imagePath . '/header.png"></div>');
            $mpdf->SetHTMLFooter('<div class="print-footer-content"><div class="row"><div class="col-md-12"><img width="100%" src="' . $imagePath .'/footer.png"></div></div></div>');

            $fileName = $entity->getRequisitionNo() . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/bank/requisition/pdf.html.twig',[
                'entity' => $entity,
                'approvals' => $approvals,
                'mode' => 'preview'
            ]);
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();

        }else{

            return $this->render(
                '@TerminalbdProcurement/bank/requisition/preview.html.twig', array(
                    'entity' => $entity,
                    'approvals' => $approvals,
                    'mode' => 'preview'
                )
            );
        }

    }



}
