<?php
/**
 * Created by PhpStorm.
 * User: shafiq
 * Date: 9/29/19
 * Time: 9:09 PM
 */

namespace Terminalbd\ProcurementBundle\Controller\Bank;
use App\Entity\Application\GenericMaster;
use App\Entity\Core\Setting;
use App\Entity\Domain\Branch;
use App\Repository\Application\ProcurementRepository;
use App\Repository\Domain\BranchRepository;
use Mpdf\Tag\Option;
use PhpOffice\PhpSpreadsheet\Writer\Pdf\Dompdf;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Terminalbd\DmsBundle\Repository\DmsFileRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Terminalbd\GenericBundle\Entity\Category;
use Terminalbd\InventoryBundle\Entity\StockBook;
use Terminalbd\ProcurementBundle\Entity\Report;
use Terminalbd\ProcurementBundle\Entity\RequisitionAgingApprovalStatus;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorder;
use Terminalbd\ProcurementBundle\Form\Bank\ReportFilterFormType;
use Terminalbd\ProcurementBundle\Form\ProcurementReportFilterFormType;
use Terminalbd\ProcurementBundle\Repository\JobRequisitionRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionAgingApprovalStatusRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionItemRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionOrderRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionRepository;


/**
 * @Route("/procure/report/")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */

class ReportController extends AbstractController
{

    /**
     * @Route("bank-requisition-item", name="bank_procure_report_requisition_item")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    function index(RequisitionRepository $requisitionRepository, RequisitionOrderRepository $requisitionOrderRepository, JobRequisitionRepository $repository) {
        $user = $this->getUser()->getId();
        $jobcount = $repository->count(['reportTo'=> $user]);
        $reqcount = $requisitionRepository->count(['reportTo'=> $user]);
        $ordercount = $requisitionOrderRepository->count(['reportTo'=> $user]);
        return $this->render('@TerminalbdProcurement/default/dashboard.html.twig', [
            'jobcount'=> $jobcount,
            'reqcount'=> $reqcount,
            'ordercount'=> $ordercount
        ]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("bank-item-wise-pr-report/{mode}", defaults={"mode"=null}, methods={"GET"}, name="bank_item_wise_pr_report")
     */
    public function itemWiseReport(Request $request, $mode, ProcurementRepository $procurementRepository, RequisitionItemRepository $repository)
    {
        $entities = [];
        $data = [];
        $terminal = $this->getUser()->getTerminal();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $getData = $searchForm->getData();
        $filterBy = $_REQUEST;
        $filepath = $this->get('kernel')->getProjectDir();
        $company = (isset($getData['company']) and $getData['company']) ? $getData['company']:'' ;
        if ($searchForm->isSubmitted()){
            $data = $_REQUEST;
//            $entities = $repository->getItemWisePrReport($config,$filterBy);
        }
        if(in_array($mode,['pdf','excel'])){

            $caption = '';
            $data = $request->query->get('filterBy');
            $entities = $repository->getItemWisePrReport($config,$data);
            $searchForm = $data['report_filter_form'];
        }
        if ($mode == 'excel'){

            $fileName = 'report-item-wise'.'_'.time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/item-wise-pr/excel.html.twig', ['entities' => $entities]);

            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");

            echo $html;
            die();

        }elseif ($mode == 'pdf'){
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);
            if($terminal->getCoreDomain() and $terminal->getCoreDomain()->getWebHeaderPath()){
                $imageHeaderPath = "{$filepath}/public/{$terminal->getCoreDomain()->getWebHeaderPath()}";
//                $mpdf->SetHTMLHeader('<div style="padding-bottom: -50px"><img width="100%" src="'. $imageHeaderPath .'"></div>');
            }
            if($terminal->getCoreDomain() and $terminal->getCoreDomain()->getWebFooterPath()){
                $imageFooterPath = "{$filepath}/public/{$terminal->getCoreDomain()->getWebFooterPath()}";
//                $mpdf->SetHTMLFooter('<div style="background-color:red; height: 20px"><img width="100%" src="'. $imageFooterPath .'"></div>');
            }
            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/item-wise-pr/pdf.html.twig',[
                'entities' => $entities,
                'company' => '',
                'headerImage' => $terminal->getCoreDomain()->getWebHeaderPath(),
                'footerImage' => $terminal->getCoreDomain()->getWebFooterPath(),
                'searchForm' => $searchForm,
            ]);
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }
        return $this->render('@TerminalbdProcurement/bank/report/item-wise-pr/index.html.twig',[
            'searchForm' => $searchForm->createView(),
            'company' => '',
            'entities' => $entities,
            'filterBy' => $data,
        ]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("bank-company-wise-pr-report/{mode}", defaults={"mode"=null}, methods={"GET"}, name="bank_company_pr_report")
     */
    public function companyPrReport(Request $request, $mode, ProcurementRepository $procurementRepository, RequisitionItemRepository $repository)
    {
        $entities = [];
        $data = [];
        $caption = '';
        $terminal = $this->getUser()->getTerminal();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $getData = $searchForm->getData();
        $company = (isset($getData['company']) and $getData['company']) ? $getData['company']:'' ;
        if ($searchForm->isSubmitted()){
            $data = $_REQUEST;
        }
        if(in_array($mode,['pdf','excel'])){

            $caption = '';
            $data = $request->query->get('filterBy');
            $entities = $this->getDoctrine()->getRepository(Report::class)->companyWisePrReport($config,$data);
            $searchForm = $data['report_filter_form'];
            if ($searchForm['company']) {
                $caption = $this->getDoctrine()->getRepository(Branch::class)->find($searchForm['company']);
                $caption = "Department Name: {$caption->getName()}";
            }
        }
        if ($mode == 'excel'){

            $fileName = 'report-item-wise'.'_'.time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/company-wise-pr/excel.html.twig', [
                'entities' => $entities,
                'searchForm' => $searchForm,
            ]);
            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");
            echo $html;
            die();

        }elseif ($mode == 'pdf'){
            $filepath = $this->get('kernel')->getProjectDir();
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);
            if($terminal->getCoreDomain() and $terminal->getCoreDomain()->getWebHeaderPath()){
                $imageHeaderPath = "{$filepath}/public/{$terminal->getCoreDomain()->getWebHeaderPath()}";
//                $mpdf->SetHTMLHeader('<div style="padding-bottom: -50px"><img width="100%" src="'. $imageHeaderPath .'"></div>');
            }
            if($terminal->getCoreDomain() and $terminal->getCoreDomain()->getWebFooterPath()){
                $imageFooterPath = "{$filepath}/public/{$terminal->getCoreDomain()->getWebFooterPath()}";
//                $mpdf->SetHTMLFooter('<div style="background-color:red; height: 20px"><img width="100%" src="'. $imageFooterPath .'"></div>');
            }
            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/company-wise-pr/pdf.html.twig',[
                'entities' => $entities,
                'caption' => $caption,
                'searchForm' => $searchForm,
                'headerImage' => $terminal->getCoreDomain()->getWebHeaderPath(),
                'footerImage' => $terminal->getCoreDomain()->getWebFooterPath(),
            ]);
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }
        return $this->render('@TerminalbdProcurement/bank/report/company-wise-pr/index.html.twig',[
            'searchForm' => $searchForm->createView(),
            'company' => $company,
            'entities' => $entities,
            'filterBy' => $data,
        ]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("bank-department-wise-pr-report/{mode}", defaults={"mode"=null}, methods={"GET"}, name="bank_department_pr_report")
     */
    public function departmentPrReport(Request $request, $mode, ProcurementRepository $procurementRepository, RequisitionItemRepository $repository)
    {
        $entities = [];
        $data = [];
        $caption = '';
        $terminal = $this->getUser()->getTerminal();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $getData = $searchForm->getData();
        $company = (isset($getData['company']) and $getData['company']) ? $getData['company']:'' ;
        if ($searchForm->isSubmitted()){
            $data = $_REQUEST;
        }
        if(in_array($mode,['pdf','excel'])){

            $caption = '';
            $data = $request->query->get('filterBy');
            $entities = $this->getDoctrine()->getRepository(Report::class)->departmentWisePrReport($config,$data);
            $searchForm = $data['report_filter_form'];
            if ($searchForm['department']) {
                $caption = $this->getDoctrine()->getRepository(Setting::class)->find($searchForm['department']);
                $caption = "Department Name: {$caption->getName()}";
            }
        }
        if ($mode == 'excel'){

            $fileName = $request->get('_route').'_'.time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/department-wise-pr/excel.html.twig', [
                'entities' => $entities,
                'caption' => $caption,
                'searchForm' => $searchForm,
            ]);
            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");
            echo $html;
            die();

        }elseif ($mode == 'pdf'){
            $filepath = $this->get('kernel')->getProjectDir();
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);
            if($terminal->getCoreDomain() and $terminal->getCoreDomain()->getWebHeaderPath()){
                $imageHeaderPath = "{$filepath}/public/{$terminal->getCoreDomain()->getWebHeaderPath()}";
//                $mpdf->SetHTMLHeader('<div style="padding-bottom: -50px"><img width="100%" src="'. $imageHeaderPath .'"></div>');
            }
            if($terminal->getCoreDomain() and $terminal->getCoreDomain()->getWebFooterPath()){
                $imageFooterPath = "{$filepath}/public/{$terminal->getCoreDomain()->getWebFooterPath()}";
//                $mpdf->SetHTMLFooter('<div style="background-color:red; height: 20px"><img width="100%" src="'. $imageFooterPath .'"></div>');
            }
            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/department-wise-pr/pdf.html.twig',[
                'entities' => $entities,
                'caption' => $caption,
                'searchForm' => $searchForm,
                'headerImage' => $terminal->getCoreDomain()->getWebHeaderPath(),
                'footerImage' => $terminal->getCoreDomain()->getWebFooterPath(),
            ]);
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }
        return $this->render('@TerminalbdProcurement/bank/report/department-wise-pr/index.html.twig',[
            'searchForm' => $searchForm->createView(),
            'company' => $company,
            'filterBy' => $data,
        ]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("bank-supplier-financial-report/{mode}", defaults={"mode"=null}, methods={"GET"}, name="bank_supplier_financial_report")
     */
    public function supplierFinancialReport(Request $request, $mode, ProcurementRepository $procurementRepository)
    {
        $entities = [];
        $data = [];
        $terminal = $this->getUser()->getTerminal();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $filterBy = $searchForm->getData();
        $filepath = $this->get('kernel')->getProjectDir();
        $company = (isset($filterBy['company']) and $filterBy['company']) ? $filterBy['company']:'' ;
        if ($searchForm->isSubmitted()){
            $data = $_REQUEST;
        }
        if (in_array($mode,['pdf','excel'])){
            $data = $request->query->get('filterBy');
            $entities = $this->getDoctrine()->getRepository(Report::class)->getSupplierSummaryReport($config, $data);
            $searchForm = $data['report_filter_form'];

        }
        if ($mode == 'excel'){

            $fileName = $request->get('_route').'_'.time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/supplier-financial/excel.html.twig', [
                'entities' => $entities,
                'searchForm' => $searchForm
            ]);

            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");

            echo $html;
            die();

        }elseif ($mode == 'pdf'){

            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);
            if($terminal->getCoreDomain() and $terminal->getCoreDomain()->getWebHeaderPath()){
                $imageHeaderPath = "{$filepath}/public/{$terminal->getCoreDomain()->getWebHeaderPath()}";
//                $mpdf->SetHTMLHeader('<div style="padding-bottom: -50px"><img width="100%" src="'. $imageHeaderPath .'"></div>');
            }
            if($terminal->getCoreDomain() and $terminal->getCoreDomain()->getWebFooterPath()){
                $imageFooterPath = "{$filepath}/public/{$terminal->getCoreDomain()->getWebFooterPath()}";
//                $mpdf->SetHTMLFooter('<div style="background-color:red; height: 20px"><img width="100%" src="'. $imageFooterPath .'"></div>');
            }

            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/supplier-financial/pdf.html.twig',[
                'entities' => $entities,
                'company' => $company,
                'headerImage' => $terminal->getCoreDomain()->getWebHeaderPath(),
                'footerImage' => $terminal->getCoreDomain()->getWebFooterPath(),
                'searchForm' => $searchForm
            ]);
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }
        return $this->render('@TerminalbdProcurement/bank/report/supplier-financial/index.html.twig',[
            'searchForm' => $searchForm->createView(),
//            'company' => $company,
//            'entities' => $entities,
            'filterBy' => $data,
        ]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("bank-work-order-report/{mode}", defaults={"mode"=null}, methods={"GET"}, name="bank_workorder_report")
     */
    public function workOrderReport(Request $request, $mode, ProcurementRepository $procurementRepository)
    {
        $entities = [];
        $data = [];
        $terminal = $this->getUser()->getTerminal();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $filepath = $this->get('kernel')->getProjectDir();
        if ($searchForm->isSubmitted()){
            $data = $_REQUEST;
        }
        if (in_array($mode,['pdf','excel'])){
            $data = $request->query->get('filterBy');
            $entities = $this->getDoctrine()->getRepository(Report::class)->workorderReport($config, $data);
            $searchForm = $data['report_filter_form'];

        }
        if ($mode == 'excel'){

            $fileName = 'report-item-wise'.'_'.time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/workorder/excel.html.twig', [
                'entities' => $entities,
                'searchForm' => $searchForm
            ]);

            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");

            echo $html;
            die();

        }elseif ($mode == 'pdf'){

            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);
            if($terminal->getCoreDomain() and $terminal->getCoreDomain()->getWebHeaderPath()){
                $imageHeaderPath = "{$filepath}/public/{$terminal->getCoreDomain()->getWebHeaderPath()}";
//                $mpdf->SetHTMLHeader('<div style="padding-bottom: -50px"><img width="100%" src="'. $imageHeaderPath .'"></div>');
            }
            if($terminal->getCoreDomain() and $terminal->getCoreDomain()->getWebFooterPath()){
                $imageFooterPath = "{$filepath}/public/{$terminal->getCoreDomain()->getWebFooterPath()}";
//                $mpdf->SetHTMLFooter('<div style="background-color:#ff0000; height: 20px"><img width="100%" src="' . $imageFooterPath .'"></div>');
            }

            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/workorder/pdf.html.twig',[
                'entities' => $entities,
                'company' => '',
                'headerImage' => $terminal->getCoreDomain()->getWebHeaderPath(),
                'footerImage' => $terminal->getCoreDomain()->getWebFooterPath(),
                'searchForm' => $searchForm
            ]);
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }
        return $this->render('@TerminalbdProcurement/bank/report/workorder/index.html.twig',[
            'searchForm' => $searchForm->createView(),
            'company' => '',
            'filterBy' => $data,
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("bank-category-wise-consumption-report/{mode}", name="bank_category_wise_consumption_report", defaults={"mode"=null})
     */
    public function categoryWiseConsumptionReport(Request $request, ProcurementRepository $procurementRepository, $mode)
    {
        $data = [];
        $entities = [];
        $caption = "";
        $filepath = $this->get('kernel')->getProjectDir();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ProcurementReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        if ($searchForm->isSubmitted()){
            $data = $_REQUEST;
        }
        if (in_array($mode,['pdf','excel'])){
            $data = $request->query->get('filterBy');
            $entities = $this->getDoctrine()->getRepository(Report::class)->stockBookCategoryConsumptionReports($config, $data);
            $searchForm = $data['procurement_report_filter_form'];
            if ($searchForm['category']) {
                $caption = $this->getDoctrine()->getRepository(Category::class)->find($searchForm['category']);
                $caption = "Category Name: {$caption->getName()}";
            }
        }
        if ($mode == 'pdf'){
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);
            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/consumption/pdf-category.html.twig',[
                'entities' => $entities,
                'caption' => $caption,
                'searchForm' => $searchForm,
            ]);
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }elseif ($mode == 'excel'){

            $fileName = $request->get('_route') . '-' . time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/consumption/excel-category.html.twig',[
                'entities' => $entities,
                'caption' => $caption,
                'searchForm' => $searchForm,
            ]);

            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");

            echo $html;
            die();
        }
        return $this->render('@TerminalbdProcurement/bank/report/consumption/index-category.html.twig',[
            'searchForm' => $searchForm->createView(),
            'filterBy' => $data,
        ]);
    }

    /**
     * @param Request $request
     * @Route("bank-purchase-order-summary-report/{mode}", name="bank_purchase_order_summary_report", defaults={"mode" = null})
     */
    public function purchaseOrderSummaryReport(Request $request, ProcurementRepository $procurementRepository, $mode)
    {
        $filterBy = [];
        $filepath = $this->get('kernel')->getProjectDir();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ProcurementReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $entities = [];

        if ($searchForm->isSubmitted()){
//            $filterBy = $searchForm->getData();
            $filterBy = ['data' => true];
            $entities = ['data' => true];
        }
        if ($mode == 'pdf'){
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);


            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/purchase-order/report-purchase-order-summary-pdf.html.twig');
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }elseif ($mode == 'excel'){

            $fileName = $request->get('_route') . '-' . time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/purchase-order/report-purchase-order-summary-excel.html.twig');

            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");

            echo $html;
            die();
        }
        return $this->render('@TerminalbdProcurement/bank/report/purchase-order/report-purchase-order-summary.html.twig',[
            'searchForm' => $searchForm->createView(),
            'filterBy' => $filterBy,
            'entities' => $entities,
        ]);
    }

    /**
     * @param Request $request
     * @param ProcurementRepository $procurementRepository
     * @param $mode
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Mpdf\MpdfException
     * @Route("bank-item-wise-po-report/{mode}", name="bank_item_wise_po_report", defaults={"mode" = null})
     */
    public function itemWisePurchaseOrderReport(Request $request, ProcurementRepository $procurementRepository, $mode)
    {
        $data = [];
        $filepath = $this->get('kernel')->getProjectDir();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ProcurementReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $entities = [];

        if ($searchForm->isSubmitted()){
            $data = $_REQUEST;
        }
        if (in_array($mode,['pdf','excel'])){
            $data = $request->query->get('filterBy');
            $entities = $this->getDoctrine()->getRepository(Report::class)->itemWisePoReports($config, $data);
            $searchForm = $data['procurement_report_filter_form'];

        }
        if ($mode == 'pdf'){
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);
            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/purchase-order/pdf-item.html.twig',[
                'entities'=>$entities,
                'searchForm' => $searchForm
            ]);
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }elseif ($mode == 'excel'){

            $fileName = $request->get('_route') . '-' . time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/purchase-order/excel-item.html.twig',[
                'entities' => $entities,
                'searchForm' => $searchForm
            ]);
            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");

            echo $html;
            die();
        }
        return $this->render('@TerminalbdProcurement/bank/report/purchase-order/index-item.html.twig',[
            'searchForm' => $searchForm->createView(),
            'filterBy' => $data,
        ]);
    }

    /**
     * @param Request $request
     * @param ProcurementRepository $procurementRepository
     * @param $mode
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("bank-vendor-wise-po-report/{mode}", name="bank_vendor_wise_po_report", defaults={"mode" = null})
     */
    public function vendorWisePurchaseReport(Request $request, ProcurementRepository $procurementRepository, $mode)
    {

        $data = [];
        $caption ="";
        $filepath = $this->get('kernel')->getProjectDir();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ProcurementReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $entities = [];
        if ($searchForm->isSubmitted()){
            $data = $_REQUEST;
        }
        if (in_array($mode,['pdf','excel'])){
            $data = $request->query->get('filterBy');
            $entities = $this->getDoctrine()->getRepository(Report::class)->vendorWisePoReports($config, $data);
            $searchForm = $data['procurement_report_filter_form'];
        }
        if ($mode == 'pdf'){
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);
            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/purchase-order/pdf-vendor.html.twig',[
                'entities' => $entities,
                'searchForm' => $searchForm
            ]);
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }elseif ($mode == 'excel'){

            $fileName = $request->get('_route') . '-' . time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/purchase-order/excel-vendor.html.twig',[
                'entities' => $entities,
                'searchForm' => $searchForm
            ]);

            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");

            echo $html;
            die();
        }
        return $this->render('@TerminalbdProcurement/bank/report/purchase-order/index-vendor.html.twig',[
            'searchForm' => $searchForm->createView(),
            'filterBy' => $data,
        ]);
    }

    /**
     * @param Request $request
     * @param ProcurementRepository $procurementRepository
     * @param $mode
     * @Route("bank-department-wise-consumption-report/{mode}", name="bank_department_wise_consumption_report", defaults={"mode" = null})
     */
    public function departmentWiseConsumptionReport(Request $request, ProcurementRepository $procurementRepository, $mode)
    {
        $data = [];
        $caption = "";
        $filepath = $this->get('kernel')->getProjectDir();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ProcurementReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $entities = [];
        if ($searchForm->isSubmitted()) {
            $data = $_REQUEST;
        }
        if (in_array($mode,['pdf','excel'])){
            $data = $request->query->get('filterBy');
            $entities = $this->getDoctrine()->getRepository(Report::class)->stockBookDepartmentConsumptionReports($config, $data);
            $searchForm = $data['procurement_report_filter_form'];
        }
        if ($mode == 'pdf'){
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);
            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/consumption/pdf-department.html.twig',[
                'entities'=>$entities,
                'searchForm' => $searchForm,
            ]);
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }elseif ($mode == 'excel'){

            $fileName = $request->get('_route') . '-' . time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/consumption/excel-department.html.twig',[
                'entities' => $entities,
                'searchForm' => $searchForm,
            ]);

            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");

            echo $html;
            die();
        }
        return $this->render('@TerminalbdProcurement/bank/report/consumption/index-department.html.twig',[
            'searchForm' => $searchForm->createView(),
            'filterBy' => $data
        ]);
    }

    /**
     * @param Request $request
     * @param ProcurementRepository $procurementRepository
     * @param $mode
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Mpdf\MpdfException
     * @Route("bank-stock-report/{mode}", name="bank_stock_report", defaults={"mode" = null})
     */
    public function stockReport(Request $request, ProcurementRepository $procurementRepository, $mode)
    {
        $data = [];
        $caption = null;
        $filepath = $this->get('kernel')->getProjectDir();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ProcurementReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $entities = [];
        if ($searchForm->isSubmitted()) {
            $data = $_REQUEST;

        }
        if (in_array($mode,['pdf','excel'])){
            $data = $request->query->get('filterBy');
            $entities = $this->getDoctrine()->getRepository(Report::class)->stockBookItemReports($config, $data);
            $openingEntities = $this->getDoctrine()->getRepository(Report::class)->stockOpeningReport($config, $data);
            $rangEntities = $this->getDoctrine()->getRepository(Report::class)->stockDateRangeReport($config, $data);
            $searchForm = $data['procurement_report_filter_form'];
            if (isset($searchForm['company'])) {
                $caption = $this->getDoctrine()->getRepository(Branch::class)->find($searchForm['company']);
                $caption = "Company Name: {$caption->getName()}";
            }
        }
        if ($mode == 'pdf'){
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);


            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/stock/pdf-stock.html.twig',[
                'entities' => $entities,
                'opening'=> $openingEntities,
                'rangEntities'=> $rangEntities,
                'searchForm' => $searchForm,
                'caption' => $caption
            ]);
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }elseif ($mode == 'excel'){

            $fileName = $request->get('_route') . '-' . time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/stock/excel-stock.html.twig',[
                'entities' => $entities,
                'opening'=> $openingEntities,
                'rangEntities'=> $rangEntities,
                'searchForm' => $searchForm,
                'caption' => $caption
            ]);

            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");

            echo $html;
            die();
        }
        return $this->render('@TerminalbdProcurement/bank/report/stock/index-stock.html.twig',[
            'searchForm' => $searchForm->createView(),
            'filterBy' => $data,
        ]);
    }

    /**
     * @param Request $request
     * @param ProcurementRepository $procurementRepository
     * @param $mode
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Mpdf\MpdfException
     * @Route("bank-stock-report-company/{mode}", name="bank_stock_company_report", defaults={"mode" = null})
     */
    public function companyStockReport(Request $request, ProcurementRepository $procurementRepository, $mode)
    {
        $data = [];
        $caption = "";
        $filepath = $this->get('kernel')->getProjectDir();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ProcurementReportFilterFormType::class, null, array('terminal' => $terminal, 'config' => $config, 'generic' => $generic, 'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $entities = [];
        if ($searchForm->isSubmitted()) {
            $data = $_REQUEST;
        }
        if (in_array($mode,['pdf','excel'])){
            $data = $request->query->get('filterBy');
            $entities = $this->getDoctrine()->getRepository(Report::class)->stockBookItemReports($config, $data);
            $openingEntities = $this->getDoctrine()->getRepository(Report::class)->stockOpeningReport($config, $data);
            $rangEntities = $this->getDoctrine()->getRepository(Report::class)->stockDateRangeReport($config, $data);
            $searchForm = $data['procurement_report_filter_form'];
            if ($searchForm['company']) {
                $caption = $this->getDoctrine()->getRepository(Branch::class)->find($searchForm['company']);
                $caption = "Company Name: {$caption->getName()}";
            }
        }
        if ($mode == 'pdf'){
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);
            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/stock/pdf-company-stock.html.twig',[
                'entities' => $entities,
                'opening'=> $openingEntities,
                'rangEntities'=> $rangEntities,
                'searchForm' => $searchForm,
                'caption' => $caption
            ]);
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }elseif ($mode == 'excel'){
            $fileName = $request->get('_route') . '-' . time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/stock/excel-company-stock.html.twig',[
                'entities' => $entities,
                'opening'=>$openingEntities,
                'rangEntities'=> $rangEntities,
                'searchForm' =>$searchForm,
                'caption' => $caption
            ]);
            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");
            echo $html;
            die();
        }
        return $this->render('@TerminalbdProcurement/bank/report/stock/index-company-stock.html.twig',[
            'searchForm' => $searchForm->createView(),
            'filterBy' => $data
        ]);
    }

    /**
     * @param Request $request
     * @param ProcurementRepository $procurementRepository
     * @param $mode
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Mpdf\MpdfException
     * @Route("bank-report-item-monthly-consumption/{mode}", name="bank_report_item_monthly_consumption", defaults={"mode" = null})
     */
    public function reportItemMonthlyConsumption(Request $request, ProcurementRepository $procurementRepository, $mode)
    {
        $data = [];
        $caption = "";
        $filepath = $this->get('kernel')->getProjectDir();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ProcurementReportFilterFormType::class, null, array('terminal' => $terminal, 'config' => $config, 'generic' => $generic, 'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $entities = [];
        if ($searchForm->isSubmitted()) {
            $data = $_REQUEST;
        }
        if (in_array($mode,['pdf','excel'])){
            $data = $request->query->get('filterBy');
            $entities = $this->getDoctrine()->getRepository(Report::class)->reportItemMonthlyConsumption($config->getId(), $data);
            $searchForm = $data['procurement_report_filter_form'];
        }
        if ($mode == 'pdf'){
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);
            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/consumption/pdf-item.html.twig',['entities' => $entities,'searchForm' =>$searchForm]);
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }elseif ($mode == 'excel'){
            $fileName = $request->get('_route') . '-' . time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/consumption/excel-item.html.twig',['entities' => $entities,'opening'=>$openingEntities]);
            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");
            echo $html;
            die();
        }
        return $this->render('@TerminalbdProcurement/bank/report/consumption/index-item.html.twig',[
            'searchForm' => $searchForm->createView(),
            'filterBy' => $data
        ]);
    }

    /**
     * @param Request $request
     * @param ProcurementRepository $procurementRepository
     * @param $mode
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Mpdf\MpdfException
     * @Route("bank-report-department-monthly-consumption/{mode}", name="bank_report_department_monthly_consumption", defaults={"mode" = null})
     */
    public function reportDepartmentMonthlyConsumption(Request $request, ProcurementRepository $procurementRepository, $mode)
    {
        $data = [];
        $caption = "";
        $filepath = $this->get('kernel')->getProjectDir();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ProcurementReportFilterFormType::class, null, array('terminal' => $terminal, 'config' => $config, 'generic' => $generic, 'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $entities = [];
        if ($searchForm->isSubmitted()) {
            $data = $_REQUEST;
        }
        if (in_array($mode,['pdf','excel'])){
            $data = $request->query->get('filterBy');
            $entities = $this->getDoctrine()->getRepository(Report::class)->reportDepartmentMonthlyConsumption($config->getId(), $data);
            $searchForm = $data['procurement_report_filter_form'];
        }
        if ($mode == 'pdf'){
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);
            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/consumption/pdf-department-monthly.html.twig',['entities' => $entities,'searchForm' =>$searchForm,'caption' => $caption]);
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }elseif ($mode == 'excel'){
            $fileName = $request->get('_route') . '-' . time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/consumption/excel-department-monthly.html.twig',['entities' => $entities]);
            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");
            echo $html;
            die();
        }
        return $this->render('@TerminalbdProcurement/bank/report/consumption/index-department-monthly.html.twig',[
            'searchForm' => $searchForm->createView(),
            'filterBy' => $data
        ]);
    }

    /**
     * @param Request $request
     * @param ProcurementRepository $procurementRepository
     * @param $mode
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Mpdf\MpdfException
     * @Route("bank-stock-report-wearhouse/{mode}", name="bank_stock_wearhouse_report", defaults={"mode" = null})
     */
    public function wearhouseStockReport(Request $request, ProcurementRepository $procurementRepository, $mode)
    {
        $data = [];
        $caption = null;
        $filepath = $this->get('kernel')->getProjectDir();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ProcurementReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $entities = [];
        if ($searchForm->isSubmitted()) {
            $data = $_REQUEST;
        }
        if (in_array($mode,['pdf','excel'])){
            $data = $request->query->get('filterBy');
            $entities = $this->getDoctrine()->getRepository(Report::class)->stockBookWearhouseReports($config, $data);
            $openingEntities = $this->getDoctrine()->getRepository(Report::class)->stockOpeningWearhouseReport($config, $data);
            $rangEntities = $this->getDoctrine()->getRepository(Report::class)->stockDateRangewearhouseReport($config, $data);
            $searchForm = $data['procurement_report_filter_form'];
            if (isset($searchForm['wearhouse'])) {
                $caption = $this->getDoctrine()->getRepository(Branch::class)->find($searchForm['wearhouse']);
                $caption = "Wearhouse Name: {$caption->getName()}";
            }
        }
        if ($mode == 'pdf'){
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);


            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/stock/pdf-wearhouse-stock.html.twig',[
                'entities' => $entities,
                'opening'=> $openingEntities,
                'rangEntities'=> $rangEntities,
                'searchForm' => $searchForm,
                'caption' => $caption
            ]);
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }elseif ($mode == 'excel'){

            $fileName = $request->get('_route') . '-' . time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/stock/excel-wearhouse-stock.html.twig',[
                'entities' => $entities,
                'opening'=> $openingEntities,
                'rangEntities'=> $rangEntities,
                'searchForm' => $searchForm,
                'caption' => $caption
            ]);

            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");

            echo $html;
            die();
        }
        return $this->render('@TerminalbdProcurement/bank/report/stock/index-wearhouse-stock.html.twig',[
            'searchForm' => $searchForm->createView(),
            'filterBy' => $data,
        ]);
    }

    /**
     * @param Request $request
     * @param ProcurementRepository $procurementRepository
     * @param $mode
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Mpdf\MpdfException
     * @Route("bank-stock-report-category/{mode}", name="bank_stock_category_report", defaults={"mode" = null})
     */
    public function categoryStockReport(Request $request, ProcurementRepository $procurementRepository, $mode)
    {
        $data = [];
        $caption = null;
        $filepath = $this->get('kernel')->getProjectDir();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ProcurementReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $entities = [];
        if ($searchForm->isSubmitted()) {
            $data = $_REQUEST;
        }
        if (in_array($mode,['pdf','excel'])){
            $data = $request->query->get('filterBy');
            $entities = $this->getDoctrine()->getRepository(Report::class)->stockBookItemReports($config, $data);
            $openingEntities = $this->getDoctrine()->getRepository(Report::class)->stockOpeningReport($config, $data);
            $rangEntities = $this->getDoctrine()->getRepository(Report::class)->stockDateRangeReport($config, $data);
            $searchForm = $data['procurement_report_filter_form'];
            if (isset($searchForm['category'])) {
                $caption = $this->getDoctrine()->getRepository(Category::class)->find($searchForm['category']);
                $caption = "Category Name: {$caption->getName()}";
            }
        }
        if ($mode == 'pdf'){
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);


            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/stock/pdf-category-stock.html.twig',[
                'entities' => $entities,
                'opening'=> $openingEntities,
                'rangEntities'=> $rangEntities,
                'searchForm' => $searchForm,
                'caption' => $caption
            ]);
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }elseif ($mode == 'excel'){

            $fileName = $request->get('_route') . '-' . time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/stock/excel-category-stock.html.twig',[
                'entities' => $entities,
                'opening'=> $openingEntities,
                'rangEntities'=> $rangEntities,
                'searchForm' => $searchForm,
                'caption' => $caption
            ]);

            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");

            echo $html;
            die();
        }
        return $this->render('@TerminalbdProcurement/bank/report/stock/index-category-stock.html.twig',[
            'searchForm' => $searchForm->createView(),
            'filterBy' => $data,
        ]);
    }

    /**
     * @param Request $request
     * @param ProcurementRepository $procurementRepository
     * @param $mode
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Mpdf\MpdfException
     * @Route("bank-contoso-expenses-report/{mode}", name="bank_contoso_expenses_report", defaults={"mode" = null})
     */
    public function contosoExpensesReport(Request $request, ProcurementRepository $procurementRepository, $mode)
    {
        $filterBy = [];
        $filepath = $this->get('kernel')->getProjectDir();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ProcurementReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $entities = [];

        if ($searchForm->isSubmitted()){
//            $filterBy = $searchForm->getData();
            $filterBy = ['data' => true];
            $entities = ['data' => true];
        }
        if ($mode == 'pdf'){
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);


            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/expense/pdf-contoso.html.twig');
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }elseif ($mode == 'excel'){

            $fileName = $request->get('_route') . '-' . time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/expense/excel-contoso.html.twig');

            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");

            echo $html;
            die();
        }
        return $this->render('@TerminalbdProcurement/bank/report/expense/index-contoso.html.twig',[
            'searchForm' => $searchForm->createView(),
            'filterBy' => $filterBy,
            'entities' => $entities,
        ]);
    }


    /**
     * Status a Branch entity.
     *
     * @Route("bank-/category-item", methods={"GET","POST"}, name="bank_stock_report_item_select" , options={"expose"=true})
     */
    public function subBranch(Request $request): Response
    {
        $id = $_REQUEST['id'];
        /* @var $entity Category */
        $entity = $this->getDoctrine()->getRepository(Category::class)->find($id);
        $selectItem = "";
        $selectItem .= "<option value=''>--- Choose a item name ---</option>";
        if($entity){
            foreach ($entity->getItems() as $item){
                $selectItem .= "<option value='{$item->getId()}'>{$item->getName()}</option>";
            }
        }
        return new Response($selectItem);
    }

    /**
     * @Route("bank-month-wise-requsition-aging-approval-status/{mode}", name="bank_report_requsition_aging_approval_status", defaults={"mode" = null})
     */
    public function monthWiseRequsitionAgingApprovalStatus(Request $request, $mode, ProcurementRepository $procurementRepository)
    {

        $records = [];
        $filterBy = [];
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $filepath = $this->get('kernel')->getProjectDir();

        if ($searchForm->isSubmitted()){
            $filterBy = $_REQUEST['report_filter_form'];
            $records = $this->getDoctrine()->getRepository(RequisitionAgingApprovalStatus::class)->getMonthWiseStatus($filterBy);
        }
        if ($mode == 'excel'){

            $filterBy = $request->query->get('filterBy');
            $records = $this->getDoctrine()->getRepository(RequisitionAgingApprovalStatus::class)->getMonthWiseStatus($filterBy);

            $fileName = 'report-item-wise'.'_'.time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/aging-approval/excel-aging-approval.html.twig', [
                'records' => $records
            ]);

            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");

            echo $html;
            die();

        }elseif ($mode == 'pdf'){

            $filterBy = $request->query->get('filterBy');
            
            $records = $this->getDoctrine()->getRepository(RequisitionAgingApprovalStatus::class)->getMonthWiseStatus($filterBy);
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);
            if($terminal->getCoreDomain() and $terminal->getCoreDomain()->getWebHeaderPath()){
                $imageHeaderPath = "{$filepath}/public/{$terminal->getCoreDomain()->getWebHeaderPath()}";
                $mpdf->SetHTMLHeader('<div style="padding-bottom: -50px"><img width="100%" src="'. $imageHeaderPath .'"></div>');
            }
            if($terminal->getCoreDomain() and $terminal->getCoreDomain()->getWebFooterPath()){
                $imageFooterPath = "{$filepath}/public/{$terminal->getCoreDomain()->getWebFooterPath()}";
                $mpdf->SetHTMLFooter('<div style="background-color:red; height: 20px"><img width="100%" src="'. $imageFooterPath .'"></div>');
            }
            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/aging-approval/pdf-aging-approval.html.twig',[
                'records' => $records,
            ]);
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }
        return $this->render('@TerminalbdProcurement/bank/report/aging-approval/index-aging-approval.html.twig',[
            'searchForm' => $searchForm->createView(),
            'records' => $records,
            'filterBy' => $filterBy,
        ]);
    }

    /**
     * @Route("bank-month-wise-requsition-aging-approval-status-details/{mode}", name="bank_report_requsition_aging_approval_status_details", defaults={"mode" = null})
     */
    public function monthWiseRequsitionAgingApprovalStatusDetails(Request $request, $mode, ProcurementRepository $procurementRepository)
    {
        $data = $request->query->all();

        if (!isset($data['day'])){
            $data['day'] = null;
        }
        $records = $this->getDoctrine()->getRepository(RequisitionAgingApprovalStatus::class)->getMonthWiseStatusDetails($data['approvedById'], $data['filterBy'], $data['day']);

        if ($mode == 'excel'){

            $filterBy = $request->query->get('filterBy');
            $records = $this->getDoctrine()->getRepository(RequisitionAgingApprovalStatus::class)->getMonthWiseStatus($filterBy);

            $fileName = 'report-item-wise'.'_'.time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/aging-approval/excel-aging-approval.html.twig', [
                'records' => $records
            ]);

            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");

            echo $html;
            die();

        }elseif ($mode == 'pdf'){

            $filterBy = $request->query->get('filterBy');

            $records = $this->getDoctrine()->getRepository(RequisitionAgingApprovalStatus::class)->getMonthWiseStatus($filterBy);
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);
            if($terminal->getCoreDomain() and $terminal->getCoreDomain()->getWebHeaderPath()){
                $imageHeaderPath = "{$filepath}/public/{$terminal->getCoreDomain()->getWebHeaderPath()}";
                $mpdf->SetHTMLHeader('<div style="padding-bottom: -50px"><img width="100%" src="'. $imageHeaderPath .'"></div>');
            }
            if($terminal->getCoreDomain() and $terminal->getCoreDomain()->getWebFooterPath()){
                $imageFooterPath = "{$filepath}/public/{$terminal->getCoreDomain()->getWebFooterPath()}";
                $mpdf->SetHTMLFooter('<div style="background-color:red; height: 20px"><img width="100%" src="'. $imageFooterPath .'"></div>');
            }
            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/aging-approval/pdf-aging-approval.html.twig',[
                'records' => $records,
            ]);
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }

        return $this->render('@TerminalbdProcurement/bank/report/aging-approval/aging-approval-details.html.twig',[
            'records' => $records,
        ]);

    }


    /**
     * @param Request $request
     * @param ProcurementRepository $procurementRepository
     * @param $mode
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Mpdf\MpdfException
     * @Route("bank-report-po-summary/{mode}", name="bank_report_po_summary", defaults={"mode" = null})
     */
    public function reportPoSummary(Request $request, ProcurementRepository $procurementRepository, $mode)
    {
        $data = [];
        $caption = "";
        $filepath = $this->get('kernel')->getProjectDir();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ProcurementReportFilterFormType::class, null, array('terminal' => $terminal, 'config' => $config, 'generic' => $generic, 'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $entities = [];
        if ($searchForm->isSubmitted()) {
            $data = $_REQUEST;
        }
        if (in_array($mode,['pdf','excel'])){
            $data = $request->query->get('filterBy');
            $entities = $this->getDoctrine()->getRepository(Report::class)->reportPoSummary($config->getId(), $data);
            $searchForm = $data['procurement_report_filter_form'];
        }
        if ($mode == 'pdf'){
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);
            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/po-order/pdf.html.twig',[
                'entities' => $entities,
                'searchForm' =>$searchForm,
                'caption' => $caption
            ]);
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }elseif ($mode == 'excel'){
            $fileName = $request->get('_route') . '-' . time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/po-order/excel.html.twig',[
                'entities' => $entities,
                'searchForm' =>$searchForm,
                'caption' => $caption
            ]);
            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");
            echo $html;
            die();
        }
        return $this->render('@TerminalbdProcurement/bank/report/po-order/index.html.twig',[
            'searchForm' => $searchForm->createView(),
            'filterBy' => $data
        ]);
    }


    /**
     * @param Request $request
     * @param ProcurementRepository $procurementRepository
     * @param $mode
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Mpdf\MpdfException
     * @Route("bank-report-po-monthly-summary/{mode}", name="bank_report_po_monthly_summary", defaults={"mode" = null})
     */
    public function reportPoMonthlySummary(Request $request, ProcurementRepository $procurementRepository, $mode)
    {
        $data = [];
        $caption = "";
        $filepath = $this->get('kernel')->getProjectDir();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ProcurementReportFilterFormType::class, null, array('terminal' => $terminal, 'config' => $config, 'generic' => $generic, 'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $entities = [];
        if ($searchForm->isSubmitted()) {
            $data = $_REQUEST;
        }
        if (in_array($mode,['pdf','excel'])){
            $data = $request->query->get('filterBy');
            $entities = $this->getDoctrine()->getRepository(Report::class)->reportPoSummaryMonthly($config->getId(), $data);
            $searchForm = $data['procurement_report_filter_form'];
        }
        if ($mode == 'pdf'){
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);
            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/po-order-monthly/pdf.html.twig',['entities' => $entities,'searchForm' =>$searchForm,'caption' => $caption]);
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }elseif ($mode == 'excel'){
            $fileName = $request->get('_route') . '-' . time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/bank/report/po-order-monthly/excel.html.twig',['entities' => $entities]);
            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");
            echo $html;
            die();
        }
        return $this->render('@TerminalbdProcurement/bank/report/po-order-monthly/index.html.twig',[
            'searchForm' => $searchForm->createView(),
            'filterBy' => $data
        ]);
    }


}