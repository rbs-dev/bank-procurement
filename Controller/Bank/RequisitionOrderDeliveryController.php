<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Controller\Bank;

use App\Entity\Application\GenericMaster;
use App\Entity\Application\Procurement;
use App\Entity\Core\Setting;
use App\Entity\Domain\ModuleProcess;
use App\Repository\Application\GenericMasterRepository;
use App\Repository\Application\ProcurementRepository;
use App\Repository\Domain\ModuleProcessItemRepository;
use App\Service\FileUploader;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\InventoryBundle\Repository\StockRepository;
use Terminalbd\ProcurementBundle\Entity\OrderDelivery;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionOrder;
use Terminalbd\ProcurementBundle\Form\ApproveCommentFormType;
use Terminalbd\ProcurementBundle\Form\EmptyFormType;
use Terminalbd\ProcurementBundle\Form\LssdFilterFormType;
use Terminalbd\ProcurementBundle\Form\Bank\OrderReceiveFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionApproveFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFilterFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionOrderFormType;
use Terminalbd\ProcurementBundle\Repository\ComapnyRequisitionShareRepository;
use Terminalbd\ProcurementBundle\Repository\JobRequisitionRepository;
use Terminalbd\ProcurementBundle\Repository\OrderDeliveryRepository;
use Terminalbd\ProcurementBundle\Repository\ParticularRepository;
use Terminalbd\ProcurementBundle\Repository\ProcurementProcessRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionOrderRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionRepository;


/**
 * @Route("/procure/bssf/requisition/order-delivery")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class RequisitionOrderDeliveryController extends AbstractController
{

    public function paginate(Request $request ,$entities)
    {
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $request->query->get('page', 1)/*page number*/,
            25  /*limit per page*/
        );
        return $pagination;
    }

    /**
     * @Route("/order-delivery", methods={"GET", "POST"}, name="procure_requisition_bank_order_delivery")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function index(Request $request, TranslatorInterface $translator, ProcurementRepository $procurementRepository, OrderDeliveryRepository $repository): Response
    {

        /* @var $config  Procurement */

        $profile = $this->getUser()->getProfile();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());

        $purchase = new OrderDelivery();
        $settingRepository = $this->getDoctrine()->getRepository(Setting::class);
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(LssdFilterFormType::class , $purchase,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        $search = $repository->findLssdWithQuery($config,$user,$data);
        $pagination = $this->paginate($request,$search);
        $couriers = $particularRepository->getChildRecords($config,'courier-service');

        return $this->render('@TerminalbdProcurement/bank/order-delivery/index.html.twig',[
            'pagination' => $pagination,
            'couriers' => $couriers,
            'searchForm' => $searchForm->createView()
        ]);
    }


    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/process", methods={"GET","POST"}, name="procure_requisition_bank_order_delivery_process" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function orderDeliverProcess(Request $request , $id, TranslatorInterface $translator, RequisitionOrderRepository $repository, ProcurementRepository $procurementRepository, ProcurementProcessRepository $processRepository, ModuleProcessItemRepository $processItemRepository, ParticularRepository $particularRepository, StockRepository $stockRepository, OrderDeliveryRepository $deliveryRepository): Response
    {
        /* @var $config  Procurement */

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $entity = $repository->find($id);
        $form = $this->createForm(EmptyFormType::class , null);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $request->request->all();
            $this->getDoctrine()->getManager()->flush();
           // $deliveryRepository->updateOrderDelivery($data);
          //  $stockRepository->processRequisitionOrderUpdateQnt($entity);
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('procure_requisition_store_order');
        }
        $couriers = $particularRepository->getChildRecords($config,'courier-service');
        return $this->render('@TerminalbdProcurement/bank/order-delivery/orderDeliveryProcess.html.twig', [
            'entity' => $entity,
            'couriers' => $couriers
        ]);
    }

    /**
     * @Route("{id}/ajax-delivery", methods={"GET", "POST"}, name="procure_requisition_bank_order_delivery_ajax")
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN')")
     */
    public function ajaxDelivery(Request $request,OrderDelivery $entity,TranslatorInterface $translator): Response
    {
        $data = $request->request->all();
        if($data['deliveryMethod'] == "Courier"){
            $courier = $this->getDoctrine()->getRepository(Particular::class)->find($data['courier']);
            if($courier){
                $entity->setCourier($courier);
            }
            $entity->setCnNo($data['cnNo']);
            $entity->setComment('comment');
        }else{
            $entity->setReceiverName($data['receiverName']);
            $entity->setReceiverDesignation($data['receiverDesignation']);
            $entity->setReceiverAddress($data['receiverAddress']);
        }
        $message = $translator->trans('data.updated_successfully');
        $this->addFlash('success', $message);
        $this->getDoctrine()->getManager()->flush();
        return new Response("success");

    }

    /**
     * @Route("{id}/create", methods={"GET", "POST"}, name="procure_requisition_bank_order_delivery_create")
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN')")
     */
    public function createDelivery(Request $request,OrderDelivery $entity,TranslatorInterface $translator): Response
    {
        $data = $request->request->all();
        if($data['deliveryMethod'] == "courier"){
            $courier = $this->getDoctrine()->getRepository(Particular::class)->find($data['courier']);
            if($courier){
                $entity->setCourier($courier);
            }
            $entity->setCnNo($data['cnNo']);
            $entity->setComment('comment');
        }else{
            $entity->setReceiverName($data['receiverName']);
            $entity->setReceiverDesignation($data['receiverDesignation']);
            $entity->setReceiverAddress($data['receiverAddress']);
        }
        $message = $translator->trans('data.updated_successfully');
        $this->addFlash('success', $message);
        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute('procure_requisition_bank_order_delivery_process',['id'=>$entity->getRequisitionOrder()->getId()]);

    }



    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/receive", methods={"GET","POST"}, name="procure_requisition_bank_order_delivery_receive" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function receive(Request $request , $id, TranslatorInterface $translator , OrderDeliveryRepository $repository,ProcurementRepository $procurementRepository, FileUploader $fileUploader ): Response
    {
        /* @var $config  Procurement */

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $entity = $repository->find($id);
        $form = $this->createForm(OrderReceiveFormType::class , $entity);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $request->request->all();
            $this->getDoctrine()->getManager()->flush();
            $brochureFile = $form->get('attachFile')->getData();
            if ($brochureFile) {
                $brochureFileName = $fileUploader->upload($brochureFile);
                $entity->setFilename($brochureFileName);
            }
            $entity->setProcess('Close');
            $this->getDoctrine()->getManager()->flush();
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('procure_requisition_bank_order_delivery');
        }
        return $this->render('@TerminalbdProcurement/bank/order-delivery/receive.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }




    /**
     * @Route("/lssd", methods={"GET", "POST"}, name="procure_requisition_bank_order_delivery_lssd")
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN')")
     */
    public function lssd(Request $request, TranslatorInterface $translator,  OrderDeliveryRepository $repository , ProcurementRepository $procurementRepository): Response
    {
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());

        $purchase = new OrderDelivery();
        $settingRepository = $this->getDoctrine()->getRepository(Setting::class);
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(LssdFilterFormType::class , $purchase,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        $search = $repository->findHeadLssdWithQuery($config,$user,$data);
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/bank/order-delivery/lssd.html.twig',
            [
                'pagination' => $pagination,
                'searchForm' => $searchForm->createView()
            ]
        );
    }

    /**
     * @Route("/lssd-approve", methods={"GET", "POST"}, name="procure_requisition_bank_order_delivery_lssd_approve")
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN')")
     */
    public function lssdApprove(Request $request,OrderDeliveryRepository $repository): Response
    {
        $approves = isset($_REQUEST['approves']) ? $_REQUEST['approves']:'';
        $new_arr = array_map('trim', explode(',', $approves));
        /* @var $entity OrderDelivery */
        foreach ($new_arr as $item){
            $entity = $repository->find($item);
            $entity->setApproved(true);
            $entity->setApprovedBy($this->getUser());
            $entity->setProcess('Approved');
            $this->getDoctrine()->getManager()->flush();
        }
        return new Response("success");
    }



    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/receive-print", methods={"GET","POST"}, name="procure_requisition_bank_order_delivery_print" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function print(Request $request,$id,OrderDeliveryRepository $repository,ProcurementRepository $procurementRepository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $entity = $repository->find($id);
        return $this->render('@TerminalbdProcurement/bank/order-delivery/print.html.twig', [
            'entity' => $entity,
        ]);
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/preview", methods={"GET"}, name="procure_requisition_bank_order_delivery_preview" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function printPreview($id,OrderDeliveryRepository $repository,ProcurementRepository $procurementRepository): Response
    {
        $entity = $repository->find($id);
        if($mode == 'print'){
            return $this->render(
                '@TerminalbdProcurement/bank/requisition-order/preview.html.twig', array(
                    'entity' => $entity,
                    'mode' => 'print'
                )
            );
        }elseif ($mode == 'pdf'){
            $mpdf = new \Mpdf\Mpdf(
            );
            $html = $this->renderView('@TerminalbdProcurement/bank/requisition-order/pdf.html.twig',array(
                'entity' => $entity,
                'mode' => "pdf"
            ));
            $mpdf->WriteHTML($html);
            $mpdf->Output();
        }else{
            return $this->render(
                '@TerminalbdProcurement/bank/requisition-order/preview.html.twig', array(
                    'entity' => $entity,
                    'mode' => 'preview',
                )
            );
        }

    }


    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/receive-print", methods={"GET","POST"}, name="procure_requisition_store_show" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function show(Request $request,$id,OrderDeliveryRepository $repository,ProcurementRepository $procurementRepository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $entity = $repository->find($id);
        return $this->render('@TerminalbdProcurement/requisition-order/print.html.twig', [
            'entity' => $entity,
        ]);
    }


}
