<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Controller\Bank;

use App\Entity\Application\Procurement;
use App\Entity\Core\Profile;
use App\Entity\Core\Setting;
use App\Entity\Domain\ApprovalUser;
use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\Vendor;
use App\Entity\User;
use App\Repository\Application\ProcurementRepository;
use App\Repository\Domain\ApprovalUserRepository;
use App\Service\FormValidationManager;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementCondition;
use Terminalbd\ProcurementBundle\Entity\ProcurementConditionItem;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\Tender;
use Terminalbd\ProcurementBundle\Entity\TenderBatch;
use Terminalbd\ProcurementBundle\Entity\TenderBatchItem;
use Terminalbd\ProcurementBundle\Entity\TenderComparative;
use Terminalbd\ProcurementBundle\Entity\TenderCompare;
use Terminalbd\ProcurementBundle\Entity\TenderConditionItem;
use Terminalbd\ProcurementBundle\Entity\TenderItem;
use Terminalbd\ProcurementBundle\Entity\TenderItemDetails;
use Terminalbd\ProcurementBundle\Entity\TenderVendor;
use Terminalbd\ProcurementBundle\Form\ApproveCommentFormType;
use Terminalbd\ProcurementBundle\Form\Bank\BankFilterFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderBatchFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFilterFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderCsFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderPreparetionFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderVendorFormType;
use Terminalbd\ProcurementBundle\Repository\ProcurementProcessRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionItemRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionRepository;
use Terminalbd\ProcurementBundle\Repository\TenderBatchItemRepository;
use Terminalbd\ProcurementBundle\Repository\TenderBatchRepository;
use Terminalbd\ProcurementBundle\Repository\TenderItemRepository;
use Terminalbd\ProcurementBundle\Repository\TenderRepository;
use Terminalbd\ProcurementBundle\Repository\TenderVendorRepository;
use Terminalbd\ProcurementBundle\Entity\ComapnyRequisitionShare;



/**
 * @Route("/procure/bssf/tender-batch")
 * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_ASSESSMENT') or is_granted('ROLE_PROCUREMENT_TENDER')")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class TenderBatchController extends AbstractController
{

    public function paginate(Request $request ,$entities)
    {
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $request->query->get('page', 1)/*page number*/,
            25  /*limit per page*/
        );
        return $pagination;
    }

    /**
     * @Route("/", methods={"GET", "POST"}, name="procure_tenderbatch_bank")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_ADMIN')")
     */
    public function index(Request $request, TranslatorInterface $translator,ProcurementRepository $procurementRepository , RequisitionItemRepository $repository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $data = $request->request->all();
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(BankFilterFormType::class , NULL,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "in-progress";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "in-progress";
            $mode = array('mode'=> $mode);
            $data = array_merge($data,$mode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "in-progress";
        }
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->bankBatchItem($config->getId(),$this->getUser()->getId(),$data);
        } else {
            $search = $repository->bankBatchItem($config->getId(),$this->getUser()->getId(),$data);
        }
        $pagination = $this->paginate($request,$search);

        $selectedRows = $this->getDoctrine()->getRepository(TenderItemDetails::class)->bankBatchSelectedItem($pagination);
        return $this->render('@TerminalbdProcurement/bank/tender-batch/selected-item.html.twig', [
            'pagination' => $pagination,
            'selectedRows' => $selectedRows,
            'mode' => $mode,
            'selected' => explode(',', $request->cookies->get('itemIds', '')),
            'searchForm' => $searchForm->createView()
        ]);
    }


    /**
     * @Route("/department-acknowledgment", methods={"GET", "POST"}, name="procure_tenderbatch_bank_acknowledgment")
     * @Security("is_granted('ROLE_PROCUREMENT_DEPARTMENT_HEAD') or is_granted('ROLE_PROCUREMENT_LSSD_HEAD')  or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_ADMIN')")
     */
    public function departmentAcknowledgment(Request $request, TranslatorInterface $translator,ProcurementRepository $procurementRepository, RequisitionItemRepository $repository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $data = $request->request->all();
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(BankFilterFormType::class , NULL,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "in-progress";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "in-progress";
            $mode = array('mode'=> $mode);
            $data = array_merge($data,$mode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "in-progress";
        }
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->bankBatchAcknowledgeItem($config->getId(),$this->getUser(),$data);
        } else {
            $search = $repository->bankBatchAcknowledgeItem($config->getId(),$this->getUser(),$data);
        }
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/bank/tender-batch/acknowledgment.html.twig', [
            'pagination' => $pagination,
            'mode'=>$mode,
            'selectedRows' => '',
            'selected' => '',
            'searchForm' => $searchForm->createView()
        ]);
    }

    /**
     * @Route("/lssd-acknowledgment", methods={"GET", "POST"}, name="procure_tenderbatch_bank_lssd_acknowledgment")
     * @Security("is_granted('ROLE_PROCUREMENT_DEPARTMENT_HEAD') or is_granted('ROLE_PROCUREMENT_LSSD_HEAD')  or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_ADMIN')")
     */
    public function lssdAcknowledgment(Request $request, TranslatorInterface $translator,ProcurementRepository $procurementRepository, RequisitionItemRepository $repository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $data = $request->request->all();
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(BankFilterFormType::class , NULL,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "in-progress";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "in-progress";
            $mode = array('mode'=> $mode);
            $data = array_merge($data,$mode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "in-progress";
        }
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->bankBatchLssdAcknowledgeItem($config->getId(),$this->getUser(),$data);
        } else {
            $search = $repository->bankBatchLssdAcknowledgeItem($config->getId(),$this->getUser(),$data);
        }
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/bank/tender-batch/lssd-acknowledgment.html.twig', [
            'pagination' => $pagination,
            'mode'=>$mode,
            'selectedRows' => '',
            'selected' => '',
            'searchForm' => $searchForm->createView()
        ]);
    }

    /**
     * @Route("/department-acknowledgment-comment", methods={"GET", "POST"}, name="procure_tenderbatch_bank_acknowledgment_comment")
     * @Security("is_granted('ROLE_PROCUREMENT_ASSESSMENT') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_ADMIN')")
     */
    public function departmentAcknowledgmentComment(Request $request, TranslatorInterface $translator,ProcurementRepository $procurementRepository, RequisitionItemRepository $repository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $data = $request->request->all();
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(BankFilterFormType::class , NULL,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "in-progress";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "in-progress";
            $mode = array('mode'=> $mode);
            $data = array_merge($data,$mode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "in-progress";
        }
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->bankBatchAcknowledgeItemComment($config->getId(),$this->getUser(),$data);
        } else {
            $search = $repository->bankBatchAcknowledgeItemComment($config->getId(),$this->getUser(),$data);
        }
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/bank/tender-batch/acknowledgmentComment.html.twig', [
            'pagination' => $pagination,
            'mode'=>$mode,
            'selectedRows' => '',
            'selected' => '',
            'searchForm' => $searchForm->createView()
        ]);
    }

    /**
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_ADMIN')")
     * @Route("/open-item", methods={"GET", "POST"}, name="procure_tenderbatch_bank_open_item")
     */
    public function openItem(Request $request, TranslatorInterface $translator, ProcurementRepository $procurementRepository,  ApprovalUserRepository $approvalUserRepository, ProcurementProcessRepository $processRepository, RequisitionRepository $requisitionRepository, TenderRepository $repository): Response
    {

        /* @var $config  Procurement */

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $data = $request->request->all();
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , NULL,array('terminal'=>$this->getUser()->getTerminal(),'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "in-progress";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "in-progress";
            $mode = array('mode'=> $mode);
            $data = array_merge($data,$mode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "in-progress";
        }
        $items = array('items'=> '');
        $data = array_merge($data,$items);
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $this->getDoctrine()->getRepository(RequisitionItem::class)->prBankOpenItem($config->getId(),$data);
        } else {
            $search = $this->getDoctrine()->getRepository(RequisitionItem::class)->prBankOpenItem($config->getId(),$data);
        }
        $pagination = $this->paginate($request,$search);
        $entity = new TenderBatch();
        $form = $this->createForm(TenderBatchFormType::class , $entity,array('terminal'=>$this->getUser()->getTerminal(),'config'=>$config));
        $form->handleRequest($request);
        $data = $request->request->all();
        $items = explode(',',$request->cookies->get('itemIds'));
        if(is_null($items)) {
            return $this->redirect($this->generateUrl('procure_tenderbatch_bank_open_item'));
        }
        $assignTo = $form["assignTo"]->getData();
        $department = $form["department"]->getData();
        if ($form->isSubmitted() && $form->isValid() && ($assignTo || $department)) {
            if (! isset($data['item'])){
                $this->addFlash('notice', "Please Check PO issue");
                return $this->redirectToRoute('procure_tenderbatch_bank_open_item');
            }
//        if (($form->isSubmitted() && $form->isValid() && $assignTo) || ($form->isSubmitted() && $form->isValid() && $department)) {
            $em = $this->getDoctrine()->getManager();
            $entity->setConfig($config);
            $entity->setCreatedBy($this->getUser());
            $module = "tender-batch";
            $moduleProcess = $this->getDoctrine()->getRepository(ModuleProcess::class)->existModuleProcess($terminal,$module);
            if(!empty($moduleProcess)){
                $entity->setModuleProcess($moduleProcess);
            }
            $entity->setModule($module);
            $entity->setWaitingProcess("New");
            $entity->setProcess("New");
            if($entity->getAssignTo()){
                $entity->setDepartment($entity->getAssignTo()->getProfile()->getDepartment());
            }
            $em->persist($entity);
            $em->flush();
            $processRepository->approvalAssign($entity);
            if($entity->getModuleProcess()->isStatus() == 1){
                $assignUsers = $approvalUserRepository->getApprovalAssignUser($terminal,$entity);
                $processRepository->insertProcurementProcessAssign($entity,$entity->getModule(),$assignUsers);
            }
            $prItems = $this->getDoctrine()->getRepository(RequisitionItem::class)->prBankOpenBatchItem($config->getId(),$items);
            $this->getDoctrine()->getRepository(TenderBatchItem::class)->insertTenderBatchItem($entity,$prItems,$data);
            $items  = 'itemIds';
            unset($items);
            setcookie('itemIds', null, -1, '/');
            $message = $translator->trans('data.success_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('procure_tenderbatch_bank_open_item');
        }elseif($form->isSubmitted()){
            $this->addFlash('notice', "Please confirm user or department");
        }
        return $this->render('@TerminalbdProcurement/bank/tender-batch/open-item.html.twig', [
            'pagination' => $pagination,
            'mode' => $mode,
            'selected' => explode(',', $request->cookies->get('itemIds', '')),
            'searchForm' => $searchForm->createView(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/po-selected-item", methods={"GET", "POST"}, name="procure_tender_bank_tenderbatch_poitem_selected", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function poSelectedItem(Request $request,ProcurementRepository $procurementRepository, ApprovalUserRepository $approvalUserRepository, ProcurementProcessRepository $processRepository)
    {
        $em = $this->getDoctrine()->getManager();
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $items = explode(',',$request->cookies->get('itemIds'));
        if(empty($request->cookies->get('itemIds'))) {
            $this->addFlash('notice', 'Please check PO issue');
            return $this->redirect($this->generateUrl('procure_tenderbatch_bank'));
        }
        $entity = new Tender();
        $form = $this->createForm(TenderFormType::class , $entity,array('terminal'=>$this->getUser()->getTerminal(),'config'=>$config));
        $form->handleRequest($request);
        $data = $request->request->all();
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setConfig($config);
            $entity->setCreatedBy($this->getUser());
            $module = "tender";
            $moduleProcess = $this->getDoctrine()->getRepository(ModuleProcess::class)->existModuleProcess($terminal,$module);
            if(!empty($moduleProcess)){
                $entity->setModuleProcess($moduleProcess);
            }
            $entity->setModule($module);
            if($this->getUser()->getProfile()->getDepartment()){
                $entity->setDepartment($this->getUser()->getProfile()->getDepartment());
                $entity->setProcessDepartment($this->getUser()->getProfile()->getDepartment());
            }
            $entity->setBusinessGroup("bank");
            $entity->setWaitingProcess("New");
            $entity->setProcess("New");
            $em->persist($entity);
            $em->flush();
            if($entity->getModuleProcess()->isStatus() == 1){
                $assignUsers = $approvalUserRepository->getApprovalAssignUser($terminal,$entity);
                $processRepository->insertProcurementProcessAssign($entity,$entity->getModule(),$assignUsers);
            }
            $this->getDoctrine()->getRepository(TenderItemDetails::class)->insertTenderItem($entity, $data);
            $this->getDoctrine()->getRepository(TenderItem::class)->insertTenderStockItem($entity,$data);
            return $this->redirectToRoute('procure_tender_bank_preparetion',array('id' => $entity->getId()));
        }
        $data = array('items'=> $items);
        $entities = $this->getDoctrine()->getRepository(RequisitionItem::class)->prBankOpenItem($config->getId(),$data);
        $selectedRows = $this->getDoctrine()->getRepository(TenderItemDetails::class)->bankPoExistingSelectedItem($entities);
        return $this->render('@TerminalbdProcurement/bank/tender-batch/selected-po-item.html.twig', array(
            'entities'      => $entities,
            'selectedRows'      => $selectedRows,
            'form' => $form->createView(),
        ));
    }



    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/process", methods={"GET", "POST"}, name="procure_tenderbatch_bank_process", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function process(Request $request, $id,TranslatorInterface $translator , ProcurementRepository $procurementRepository , TenderRepository $repository, ProcurementProcessRepository $processRepository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $data = $request->request->all();
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        /* @var $entity Tender */
        $entity = $repository->findOneBy(array('config' => $config,'id' => $id));
        $form = $this->createForm(ApproveCommentFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $data = $request->request->all();
            $comment = $form["comment"]->getData();
            $processRepository->insertApprovalProcessForGenericComment($this->getUser(),$entity,$comment);
            if(isset($data['reject']) and $data['reject'] == "reject"){
                $entity->setProcess('Rejected');
                $entity->setWaitingProcess('Rejected');
                $entity->setReportTo(null);
                $em->persist($entity);
                $em->flush();
                $message = $translator->trans('data.reject_successfully');
                $this->addFlash('success', $message);
            }else{
                $count = $processRepository->count(array('entityId'=> $entity->getId(), 'module'=>$entity->getModule(),'close'=>1));
                $ordering = ($count == 0) ? 1 : $count+1;
                $processRepository->approvalAssign($entity,$ordering);
                $message = $translator->trans('data.updated_successfully');
                $this->addFlash('success', $message);
            }
            if($entity->getWaitingProcess() == "Approved"){
                $this->getDoctrine()->getRepository(Tender::class)->updateOpenItem($entity);
            }
            return $this->redirectToRoute('procure_tenderbatch_bank',['mode'=>'approve']);
        }
        return $this->render('@TerminalbdProcurement/bank/tender/process.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/item-close", methods={"GET"}, name="procure_tenderbatch_bank_item_close" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function itemClose(RequisitionItem $entity): Response
    {
        $em = $this->getDoctrine()->getManager();
        $entity->setIsClose(0);
        $em->persist($entity);
        $em->flush();
        return  new Response('success');
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/batch-item-close", methods={"GET"}, name="procure_tenderbatch_bank_batchitem_close" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function batchItemClose($id): Response
    {

        $entity = $this->getDoctrine()->getRepository(TenderBatchItem::class)->find($id);
        if($entity){
            $em = $this->getDoctrine()->getManager();
            $entity->setIsClose(1);
            $em->persist($entity);
            $em->flush();

            $purchaseItem = $entity->getRequisitionItem();
            $purchaseItem->setIsClose(0);
            $em->persist($purchaseItem);
            $em->flush();
        }
        return  new Response('success');
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/batch-item-approve", methods={"GET"}, name="procure_tenderbatch_bank_batchitem_approve" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function batchItemApproved(TenderBatchItem $entity): Response
    {
        if($entity){
            $em = $this->getDoctrine()->getManager();
            $entity->setApprovedBy($this->getUser());
            $em->persist($entity);
            $em->flush();
        }
        return  new Response('success');
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/preview", methods={"GET"}, name="procure_tenderbatch_bank_preview" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function printPreview(Tender $entity): Response
    {
        $mode = isset($_REQUEST['mode']) ? $_REQUEST['mode'] :'';
        $approvals = $this->getDoctrine()->getRepository(ProcurementProcess::class)->findBy(array('entityId'=>$entity->getId(),'module'=>$entity->getModule()),array('ordering'=>'ASC'));

        if($mode == 'print'){
            return $this->render(
                '@TerminalbdProcurement/bank/tender/preview.html.twig', array(
                    'entity' => $entity,
                    'approvals' => $approvals,
                    'mode' => 'print'
                )
            );
        }elseif ($mode == 'pdf'){
             $mpdf = new \Mpdf\Mpdf(['tempDir' => __DIR__ . '/../../../../public/temp']);
            $html = $this->renderView('@TerminalbdProcurement/bank/tender/pdf.html.twig',array(
                'entity' => $entity,
                'approvals' => $approvals,
                'mode' => "pdf"
            ));
            $mpdf->WriteHTML($html);
            $mpdf->Output();
        }else{
            return $this->render(
                '@TerminalbdProcurement/bank/tender/preview.html.twig', array(
                    'entity' => $entity,
                    'approvals' => $approvals,
                    'mode' => 'preview'
                )
            );
        }

    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="procure_tenderbatch_bank_delete")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function delete(Tender $entity): Response
    {
        $em = $this->getDoctrine()->getManager();
        $response = "invalid";
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Particular entity.');
        }
        try {
            $em->remove($entity);
            $em->flush();
            $response = 'valid';

        } catch (ForeignKeyConstraintViolationException $e) {
            $this->get('session')->getFlashBag()->add(
                'notice',"Data has been relation another Table"
            );
        }catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add(
                'notice', 'Please contact system administrator further notification.'
            );
        }
        return new Response($response);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/item-delete", methods={"GET"}, name="procure_tenderbatch_bank_item_delete")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function itemDelete(TenderBatchItem $entity): Response
    {
        $em = $this->getDoctrine()->getManager();
        $response = "invalid";
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Particular entity.');
        }
        try {
            $em->remove($entity);
            $em->flush();
            $response = 'valid';

        } catch (ForeignKeyConstraintViolationException $e) {
            $this->get('session')->getFlashBag()->add(
                'notice',"Data has been relation another Table"
            );
        }catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add(
                'notice', 'Please contact system administrator further notification.'
            );
        }
        return new Response($response);
    }

    /**
     * @Route("/department-assing-user", methods={"GET", "POST"}, name="procure_tenderbatch_bank_assignto", options={"expose"=true})
     * @Security("is_granted('ROLE_USER') or is_granted('ROLE_DOMAIN')")
     */
    public function departmentUser(Request $request)
    {
        $user = $this->getUser();
        $department = $user->getProfile()->getDepartment()->getId();
        $users = $this->getDoctrine()->getRepository(Profile::class)->getDepartmentUsers($user,$department);
        $items  = array();
        $items[]= array('value' => '','text'=>'Select Assign user');
        foreach ($users as $list):
            $employee = $list['employeeId'].'-'.$list['name'];
            $items[]= array('value' => $list['id'],'text' => $employee);
        endforeach;
        return new JsonResponse($items);
    }

    /**
     * @Route("/batch-inline-update", methods={"GET", "POST"}, name="procure_tenderbatch_bank_inline_update", options={"expose"=true})
     * @Security("is_granted('ROLE_USER') or is_granted('ROLE_DOMAIN')")
     */
    public function inlineUpdate(Request $request)
    {

        $data = $request->request->all();
        $entity = $this->getDoctrine()->getRepository(TenderBatchItem::class)->find($data['pk']);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }
        $user = $this->getDoctrine()->getRepository(User::class)->find($data['value']);
        $entity->setAssignto($user);
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');

    }

    /**
     * @Route("/batch-inline-assign-update", methods={"GET", "POST"}, name="procure_tenderbatch_bank_inline_assign_update", options={"expose"=true})
     * @Security("is_granted('ROLE_USER') or is_granted('ROLE_DOMAIN')")
     */
    public function inlineAssignRequisitionUpdate(Request $request)
    {

        $data = $request->request->all();
        $entity = $this->getDoctrine()->getRepository(TenderBatch::class)->find($data['pk']);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }
        $user = $this->getDoctrine()->getRepository(User::class)->find($data['value']);
        $entity->setAssignto($user);
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');

    }

    /**
     * @Route("/{id}/batch-inline-comment-update", methods={"GET", "POST"}, name="procure_tender_bank_tenderbatch_item_comment_update", options={"expose"=true})
     * @Security("is_granted('ROLE_USER') or is_granted('ROLE_DOMAIN')")
     */
    public function inlineCommentUpdate(Request $request, TenderBatchItem $entity)
    {

        $data = $_REQUEST['comment'];
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }
        $entity->setComment($data);
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');

    }




}
