<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Controller\Bank;

use App\Entity\Application\GenericMaster;
use App\Entity\Application\Procurement;
use App\Entity\User;
use App\Repository\Application\GenericMasterRepository;
use App\Repository\Application\InventoryRepository;
use App\Repository\Application\ProcurementRepository;
use ContainerBlo4KOZ\getRequisitionItemRepositoryService;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\GenericBundle\Entity\Item;
use Terminalbd\InventoryBundle\Entity\PurchaseItem;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\InventoryBundle\Repository\StockRepository;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItemHistory;
use Terminalbd\ProcurementBundle\Form\Bank\RequisitionFormType;
use Terminalbd\ProcurementBundle\Form\Bank\RequisitionItemFormType;
use Terminalbd\ProcurementBundle\Repository\RequisitionItemRepository;


/**
 * @Route("/procurement/bank/requisition-item")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class RequisitionBankItemController extends AbstractController
{

    /**
     * Show a ApprovalProcess entity.
     *
     * @Route("/{id}/{module}/item-create", methods={"GET","POST"}, name="procure_requisition_bank_item_create" , options={"expose"=true})
     */
    public function itemCreate(Request $request, $id ,$module): Response
    {
        /* @var $config  Procurement */

        $terminal = $this->getUser()->getTerminal()->getId();
        $genricConfig = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal);
        /* @var $entity Requisition */
        $entity = $this->getDoctrine()->getRepository(ProcurementProcess::class)->getEntityCheck($id,$module);
        //"requisition","purchase-requisition","store-requisition","replacement-requisition","repair-requisition"
        $item = new RequisitionItem();
        if(in_array($entity->getRequisitionType()->getSlug(),["replacement-requisition","repair-requisition"])){
            $form = $this->createForm(RequisitionItemFormType::class, $item,array('config' => $genricConfig));
            $form->remove('stockIn');
        }elseif($entity->getRequisitionType()->getSlug() == "store-requisition"){
            $form = $this->createForm(RequisitionItemFormType::class, $item,array('config' => $genricConfig));
            $form->remove('file');
            $form->remove('previousPurchasePrice');
            $form->remove('previousPurchaseDate');
            $form->remove('repairCost');
            $form->remove('presentBookValue');
            $form->remove('repairCost');
            $form->remove('lastServiceDate');
            $form->remove('warranty');
        }else{
            $form = $this->createForm(RequisitionItemFormType::class, $item,array('config' => $genricConfig));
            $form->remove('stockIn');
            $form->remove('previousPurchasePrice');
            $form->remove('previousPurchaseDate');
            $form->remove('repairCost');
            $form->remove('presentBookValue');
            $form->remove('repairCost');
            $form->remove('lastServiceDate');
            $form->remove('warranty');
        }
        return $this->render('@TerminalbdProcurement/bank/requisition-item/item-form.html.twig', [
            'entity'             => $entity,
            'itemForm'           => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/{module}/save-item", methods={"GET","POST"}, name="procure_requisition_bank_save_item" , options={"expose"=true})
     */

    public function saveItem(Request $request, $id,$module): Response
    {
        $em = $this->getDoctrine()->getManager();
        $terminal = $this->getUser()->getTerminal()->getId();
        $genricConfig = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal);
        $data = $request->request->all();
        $entity = $this->getDoctrine()->getRepository(ProcurementProcess::class)->getEntityCheck($id,$module);
        $purchaseData = $data['requisition_item_form'];
        $item = new RequisitionItem();
        $itemRepository = $this->getDoctrine()->getRepository(RequisitionItem::class);
        if(in_array($entity->getRequisitionType()->getSlug(),["replacement-requisition","repair-requisition"])){
            $form = $this->createForm(RequisitionItemFormType::class, $item,array('config' => $genricConfig));
            $form->remove('stockIn');
        }elseif($entity->getRequisitionType()->getSlug() == "store-requisition"){
            $form = $this->createForm(RequisitionItemFormType::class, $item,array('config' => $genricConfig));
            $form->remove('file');
            $form->remove('previousPurchasePrice');
            $form->remove('previousPurchaseDate');
            $form->remove('repairCost');
            $form->remove('presentBookValue');
            $form->remove('repairCost');
            $form->remove('lastServiceDate');
            $form->remove('warranty');
        }else{
            $form = $this->createForm(RequisitionItemFormType::class, $item,array('config' => $genricConfig));
            $form->remove('stockIn');
            $form->remove('previousPurchasePrice');
            $form->remove('previousPurchaseDate');
            $form->remove('repairCost');
            $form->remove('presentBookValue');
            $form->remove('repairCost');
            $form->remove('lastServiceDate');
            $form->remove('warranty');
        }
        $form->handleRequest($request);
        $data = $request->request->all();
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if ($entity->getRequisitionType()->getSlug() != "store-requisition"){
                $attachFile = $form->get('file')->getData();
                if ($attachFile and $item->getPath()) {
                    $item->removeUpload();
                }
                $item->upload();
            }
            $item->setRequisition($entity);
            $item->setApproveQuantity($item->getQuantity());
            $item->setActualQuantity($item->getQuantity());
            $item->setRemainigQuantity($item->getQuantity());
            $stock = $em->getRepository(Stock::class)->findOneBy(array('item' => $item->getItem()));
            $item->setStock($stock);
            $item->setName($item->getItem()->getName());
            $em->persist($item);
            $em->flush();
            if ($entity->getRequisitionType()->getSlug() == "store-requisition") {
                $purchaseItem = $this->getDoctrine()->getRepository(RequisitionItem::class)->insertBankStoreItem($entity,$item);
            }
        }
        $itemRepository->getItemSummary($entity);
        $reponse = $this->returnResultData($entity->getId());
        return new Response(json_encode($reponse));
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/add-item", methods={"GET","POST"}, name="procure_requisition_item_add" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function addItem(Request $request, Requisition $entity, RequisitionRepository $requisitionRepository , RequisitionItemRepository $itemRepository): Response
    {
        $data = $request->request->all();
        $purchaseData = $data['requisition_item_form'];
        $itemRepository->insertStoreItem($entity,$purchaseData);
        $itemRepository->getItemSummary($entity);
        $reponse = $this->returnResultData($requisitionRepository,$entity->getId());
        return new Response(json_encode($reponse));
    }



    /**
     * Update a RequisitionItem entity.
     * @Route("/{entity}/{id}/{module}/update-item", methods={"GET","POST"}, name="procure_requisition_bank_item_update", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function updateItem(Request $request ,$entity,$id,$module,RequisitionItemRepository $itemRepository): Response
    {

        /* @var $item  RequisitionItem */
        $em = $this->getDoctrine()->getManager();
        $data = $request->request->all();
        $item = $itemRepository->find($id);
        $item->setQuantity($data['quantity']);
        $item->setApproveQuantity($item->getQuantity());
        $item->setActualQuantity($item->getQuantity());
        $item->setSubTotal($item->getQuantity() * $item->getPrice());
        $this->getDoctrine()->getManager()->flush();
        if($module == "store-requisition"){
            $requisition = $em->getRepository(Requisition::class)->find($entity);
            $itemRepository->getItemSummary($requisition);
            $return = $this->returnResultData($requisition->getId());
        }elseif(in_array($module,["purchase-requisition","replacement-requisition","repair-requisition"])){
            $requisition = $em->getRepository(Requisition::class)->find($entity);
            $itemRepository->getItemSummary($requisition);
            $return = $this->returnResultData($requisition->getId());
        }
        return new Response(json_encode($return));

    }

    /**
     * Deletes a RequisitionItem entity.
     * @Route("/{entity}/{id}/{module}/delete-item", methods={"GET"}, name="procure_requisition_bank_item_delete", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function deleteItem($entity , $id, $module ,RequisitionItemRepository $itemRepository): Response
    {
        $item = $itemRepository->findOneBy(array('id' => "{$id}"));
        $em = $this->getDoctrine()->getManager();
        $em->remove($item);
        $em->flush();
        if($module == "requisition"){
            $requisition = $em->getRepository(Requisition::class)->find($entity);
            $itemRepository->getItemSummary($requisition);
            $return = $this->returnResultData($requisition->getId());
        }elseif($module == "purchase-requisition"){
            $requisition = $em->getRepository(Requisition::class)->find($entity);
            $itemRepository->getItemSummary($requisition);
            $return = $this->returnResultData($requisition->getId());
        }
        return new Response(json_encode($return));
    }

    public function returnResultData($requisition){

        /* @var $entity Requisition */
        $entity = $this->getDoctrine()->getRepository(Requisition::class)->find($requisition);
        $subTotal = $entity->getSubTotal() > 0 ? $entity->getSubTotal() : 0;
        $total = $entity->getTotal() > 0 ? $entity->getTotal() : 0;
        $html = $this->renderView(
            '@TerminalbdProcurement/bank/requisition-item/requisitionItem.html.twig', array(
                'entity' => $entity,
            )
        );
        $data = array(
            'subTotal' => $subTotal,
            'total' => $total,
            'invoiceItem' => $html
        );
        return $data;

    }

    public function returnGarmentResultData($requisition){

        /* @var $entity Requisition */
        $entity = $this->getDoctrine()->getRepository(Requisition::class)->find($requisition);
        $subTotal = $entity->getSubTotal() > 0 ? $entity->getSubTotal() : 0;
        $total = $entity->getTotal() > 0 ? $entity->getTotal() : 0;
        $html = $this->renderView(
            '@TerminalbdProcurement/bank/requisition-item/requisitionItem.html.twig', array(
                'entity' => $entity,
            )
        );
        $data = array(
            'subTotal' => $subTotal,
            'total' => $total,
            'invoiceItem' => $html
        );
        return $data;

    }

    /**
     * Status a Setting entity.
     *
     * @Route("/category-attribute", methods={"GET","POST"}, name="procure_category_attribute" , options={"expose"=true})
     * @Security("is_granted('ROLE_INVENTORY') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_GENERIC') or is_granted('ROLE_PROCUREMENT')")
     */
    public function categoryAttribute(Request $request , StockRepository $stockRepository): Response
    {
        $id = $_REQUEST['id'];

        /* @var $entity Stock */

        $entity = $this->getDoctrine()->getRepository(Item::class)->find($id);
        $category = $entity->getCategory();

        $brands = $category->getBrand();
        $sizes = $category->getSize();
        $colors = $category->getColor();

        $selectBrand = "";
        $selectBrand .= "<option value=''>---Select Brand---</option>";
        foreach ($brands as $brand){
            $selectBrand .= "<option value='{$brand->getId()}'>{$brand->getName()}</option>";
        }
        $selectSize = "";
        $selectSize .= "<option value=''>---Select Size---</option>";
        foreach ($sizes as $size){
            $selectSize .= "<option value='{$size->getId()}'>{$size->getName()}</option>";
        }

        $selectColor = "";
        $selectColor .= "<option value=''>---Select Color---</option>";
        foreach ($colors as $color){
            $selectColor .= "<option value='{$color->getId()}'>{$color->getName()}</option>";
        }

        $data = array('brand'=>$selectBrand,'size'=>$selectSize,'color'=>$selectColor);
        return new Response(json_encode($data));
    }

    /**
     * Update a RequisitionItem entity.
     * @Route("/{id}/download-requisition-item-attachment", methods={"GET","POST"}, name="procurement_download_requisition_item_attachment", options={"expose"=true})
     */

    public function downloadAttachFile(Request $request,RequisitionItem $entity)
    {
        $filepath = $this->get('kernel')->getProjectDir() . "/public/uploads/procurement/requisition-item/{$entity->getPath()}";
        if (!empty($entity->getPath()))
        {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($filepath));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filepath));
            ob_clean();
            flush();
            readfile($filepath);
        }
        exit;
    }

}
