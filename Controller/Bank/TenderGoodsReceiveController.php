<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Controller\Bank;

use App\Entity\Application\Procurement;
use App\Entity\Domain\ApprovalUser;
use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\Vendor;
use App\Repository\Application\ProcurementRepository;
use App\Repository\Domain\ApprovalUserRepository;
use App\Service\FormValidationManager;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\InventoryBundle\Entity\StockBook;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\TenderVendor;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorder;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderReceive;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderReceiveItem;
use Terminalbd\ProcurementBundle\Form\Bank\ReceiveAceptenceFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFilterFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderReceiveFormType;
use Terminalbd\ProcurementBundle\Repository\ProcurementProcessRepository;
use Terminalbd\ProcurementBundle\Repository\TenderComparativeRepository;
use Terminalbd\ProcurementBundle\Repository\TenderVendorRepository;
use Terminalbd\ProcurementBundle\Repository\TenderWorkorderReceiveRepository;
use Terminalbd\ProcurementBundle\Repository\TenderWorkorderRepository;


/**
 * @Route("/procure/bssf/goods-receive")
 * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_APPROVER') or is_granted('ROLE_PROCUREMENT_RECEIVE')")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */

class TenderGoodsReceiveController extends AbstractController
{
    public function paginate(Request $request ,$entities)
    {
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $request->query->get('page', 1)/*page number*/,
            25  /*limit per page*/
        );
        return $pagination;
    }

    /**
     * @Route("/", methods={"GET", "POST"}, name="procure_tender_bank_receive")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_ADMIN')")
     */
    public function index(Request $request, TranslatorInterface $translator,ProcurementRepository $procurementRepository, TenderWorkorderReceiveRepository $repository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $data = $request->request->all();
        $entities = $this->getDoctrine()->getRepository(TenderWorkorder::class)->findBy(array('config' => $config),array('code'=>"DESC"));
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , NULL,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $mode = array('mode'=> $mode);
            $data = array_merge($data,$mode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
        }
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->findBankSearchQuery($config,$this->getUser(),$data);
        } else {
            $search = $repository->findBankSearchQuery($config,$this->getUser(),$data);
        }
        $pagination = $this->paginate($request,$search);
        $data = $_REQUEST;
        return $this->render('@TerminalbdProcurement/bank/goods-receive/index.html.twig', [
            'pagination' => $pagination,
            'mode' => $mode,
            'searchForm' => $searchForm->createView(),
        ]);
    }

    /**
     * @Route("/performance-report", methods={"GET", "POST"}, name="procure_tender_bank_performance_report")
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_MANAGER')")
     */
    public function performanceIndex(Request $request, TranslatorInterface $translator,ProcurementRepository $procurementRepository, TenderWorkorderReceiveRepository $repository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $data = $request->request->all();
        $entities = $this->getDoctrine()->getRepository(TenderWorkorder::class)->findBy(array('config' => $config),array('code'=>"DESC"));
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , NULL,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        $search = $repository->findPerformanceReport($config,$this->getUser(),$data);
        $pagination = $this->paginate($request,$search);
        $data = $_REQUEST;
        return $this->render('@TerminalbdProcurement/bank/goods-receive/acceptence-index.html.twig', [
            'pagination' => $pagination,
            'searchForm' => $searchForm->createView(),
        ]);
    }

    /**
     * @Route("/branch", methods={"GET", "POST"}, name="procure_tender_bank_branch_receive")
     * @Security("is_granted('ROLE_PROCUREMENT_INITIATOR') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_MANAGER')")
     */
    public function branchIndex(Request $request, TranslatorInterface $translator,ProcurementRepository $procurementRepository, TenderWorkorderReceiveRepository $repository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $data = $request->request->all();
        $entities = $this->getDoctrine()->getRepository(TenderWorkorder::class)->findBy(array('config' => $config),array('code'=>"DESC"));
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , NULL,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $mode = array('mode'=> $mode);
            $data = array_merge($data,$mode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
        }
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->findBankSearchQuery($config,$this->getUser(),$data);
        } else {
            $search = $repository->findBankSearchQuery($config,$this->getUser(),$data);
        }
        $pagination = $this->paginate($request,$search);
        $data = $_REQUEST;
        return $this->render('@TerminalbdProcurement/bank/goods-receive/index.html.twig', [
            'pagination' => $pagination,
            'mode' => $mode,
            'searchForm' => $searchForm->createView(),
        ]);
    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/receive", methods={"GET", "POST"}, name="procure_tender_bank_workorder_receive")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function receive(Request $request, TenderWorkorder $workorder ,TranslatorInterface $translator, ProcurementRepository $procurementRepository, ProcurementProcessRepository $processRepository, ApprovalUserRepository $approvalUserRepository): Response
    {

        $terminal = $this->getUser()->getTerminal();
        /* @var $config  Procurement */

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $em =  $this->getDoctrine()->getManager();
        $entity = new TenderWorkorderReceive();
        $module = "goods-receive";
        $moduleProcess = $this->getDoctrine()->getRepository(ModuleProcess::class)->existModuleProcess($terminal,$module);
        if(!empty($moduleProcess)){
            $entity->setModuleProcess($moduleProcess);
        }
        $entity->setConfig($config);
        $entity->setWorkorder($workorder);
        $entity->setModule($module);
        $entity->setBusinessGroup("bank");
        $entity->setWaitingProcess("New");
        $entity->setProcess("New");
        $entity->setCreatedBy($this->getUser());
        $em->persist($entity);
        $em->flush();
        $this->getDoctrine()->getRepository(TenderWorkorderReceive::class)->insertBankWorkOrderReceiveItem($entity,$workorder);
        if($entity->getModuleProcess()->isStatus() == 1){
            $assignUsers = $approvalUserRepository->getApprovalAssignUser($terminal,$entity);
            $processRepository->insertProcurementProcessAssign($entity,$entity->getModule(),$assignUsers);
        }
        $message = $translator->trans('data.updated_successfully');
        $this->addFlash('success', $message);
        return $this->redirectToRoute('procure_tender_bank_receive_edit',['id'=>$entity->getId()]);

    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="procure_tender_bank_receive_edit")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function edit(Request $request, TenderWorkorderReceive $entity ,TranslatorInterface $translator, ProcurementRepository $procurementRepository, ProcurementProcessRepository $processRepository): Response
    {

        $terminal = $this->getUser()->getTerminal();
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $form = $this->createForm(TenderReceiveFormType::class , $entity);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $request->request->all();
            $processRepository->approvalAssign($entity);
            $this->getDoctrine()->getManager()->flush();
            $processRepository->approvalAssign($entity);
            $files = !empty($_FILES['files']) ? $_FILES['files']:'';
            if($files){
                $this->getDoctrine()->getRepository(ProcurementProcess::class)->insertAttachmentFile($entity,$data,$files);
            }
            if($entity->getWaitingProcess() == "Approved"){
                $this->getDoctrine()->getRepository(TenderWorkorderReceive::class)->updateOpenItem($entity);
                $this->getDoctrine()->getRepository(Stock::class)->insertBankWorkorderStockQuantity($entity);

            }
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('procure_tender_bank_receive');
        }
       return $this->render('@TerminalbdProcurement/bank/goods-receive/new.html.twig', [
            'entity'            => $entity,
            'form'              => $form->createView()
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/process", methods={"GET", "POST"}, name="procure_tender_bank_receive_process", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function process(Request $request, $id,TranslatorInterface $translator , ProcurementRepository $procurementRepository , TenderWorkorderReceiveRepository $repository, ProcurementProcessRepository $processRepository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $data = $request->request->all();
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        /* @var $entity TenderWorkorderReceive */
        $entity = $repository->findOneBy(array('config' => $config,'id' => $id));
        $form = $this->createForm(ApproveCommentFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $data = $request->request->all();
            $comment = $form["comment"]->getData();
            $processRepository->insertApprovalProcessForGenericComment($this->getUser(),$entity,$comment);
            if(isset($data['reject']) and $data['reject'] == "reject"){
                $entity->setProcess('Rejected');
                $entity->setWaitingProcess('Rejected');
                $entity->setReportTo(null);
                $entity->setProcessOrdering(0);
                $em->persist($entity);
                $em->flush();
                $message = $translator->trans('data.reject_successfully');
                $this->addFlash('success', $message);
            }else{
                $count = $processRepository->count(array('entityId'=> $entity->getId(), 'module'=>$entity->getModule(),'close'=>1));
                $ordering = ($count == 0) ? 1 : $count+1;
                $processRepository->approvalAssign($entity,$ordering);
                $message = $translator->trans('data.updated_successfully');
                $this->addFlash('success', $message);
            }
            if($entity->getWaitingProcess() == "Approved"){
                $this->getDoctrine()->getRepository(TenderWorkorderReceive::class)->updateOpenItem($entity);
                $this->getDoctrine()->getRepository(Stock::class)->insertBankWorkorderStockQuantity($entity);
            }
            return $this->redirectToRoute('procure_tender_bank_receive',['mode'=>'approve']);
        }
        return $this->render('@TerminalbdProcurement/bank/goods-receive/process.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/acceptence", methods={"GET", "POST"}, name="procure_tender_bank_receive_acceptence", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function acceptence(Request $request, $id,TranslatorInterface $translator , ProcurementRepository $procurementRepository , TenderWorkorderReceiveRepository $repository, ProcurementProcessRepository $processRepository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $data = $request->request->all();
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        /* @var $entity TenderWorkorderReceive */
        $entity = $repository->findOneBy(array('config' => $config,'id' => $id));
        $form = $this->createForm(ReceiveAceptenceFormType::class,$entity);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $data = $request->request->all();
            $entity->upload();
            $entity->setProcess('Closed');
            $entity->setWaitingProcess('Closed');
            $em->flush();
            return $this->redirectToRoute('procure_tender_bank_branch_receive',['mode'=>'list']);
        }
        return $this->render('@TerminalbdProcurement/bank/goods-receive/acceptence.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }


    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET"}, name="procure_tender_bank_receive_show" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function show(TenderWorkorderReceive $entity): Response
    {
        $html = $this->renderView(
            '@TerminalbdProcurement/bank/goods-receive/show.html.twig', array(
                'entity' => $entity,
            )
        );
        return new Response($html);
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/preview", methods={"GET"}, name="procure_tender_bank_receive_preview" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function printPreview(TenderWorkorderReceive $entity): Response
    {

        $mode = isset($_REQUEST['mode']) ? $_REQUEST['mode'] :'';
        $approvals = $this->getDoctrine()->getRepository(ProcurementProcess::class)->findBy(array('entityId'=>$entity->getId(),'module'=>$entity->getModule()),array('ordering'=>'ASC'));

        if($mode == 'print'){
            return $this->render(
                '@TerminalbdProcurement/bank/goods-receive/preview.html.twig', array(
                    'entity' => $entity,
                    'approvals' => $approvals,
                    'mode' => 'print'
                )
            );
        }elseif ($mode == 'pdf'){
             $mpdf = new \Mpdf\Mpdf(['tempDir' => __DIR__ . '/../../../../public/temp']);
            $html = $this->renderView('@TerminalbdProcurement/bank/goods-receive/pdf.html.twig',array(
                'entity' => $entity,
                'approvals' => $approvals,
                'mode' => "pdf"
            ));
            $mpdf->WriteHTML($html);
            $mpdf->Output();
        }else{
            return $this->render(
                '@TerminalbdProcurement/bank/goods-receive/preview.html.twig', array(
                    'entity' => $entity,
                    'approvals' => $approvals,
                    'mode' => 'preview'
                )
            );
        }

    }


    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/item-update", methods={"GET","POST"}, name="procure_tender_bank_receive_item_update" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function itemUpdate(Request $request, TenderWorkorderReceiveItem $item ): Response
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->request->all();
        $item->setQuantity($data['quantity']);
        $item->setPrice($data['price']);
        if($data['price'] > 0 ){
            $item->setSubTotal($item->getQuantity() * $item->getPrice());
        }else{
            $price = $item->getTenderWorkorderItem()->getUnitPrice();
            $item->setPrice($price);
            $item->setSubTotal($item->getQuantity() * $price);
        }
        $em->flush();
        $invoice = $this->getDoctrine()->getRepository(TenderWorkorderReceiveItem::class)->getReceiveItemSummary($item->getWorkorderReceive());
        $total = number_format($invoice->getTotal(),2);
        return new Response($total);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="procure_tender_bank_receive_delete")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function delete(TenderWorkorderReceive $entity): Response
    {
        $em = $this->getDoctrine()->getManager();
        $response = "invalid";
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Particular entity.');
        }
        try {
            $em->remove($entity);
            $em->flush();
            $response = 'valid';

        } catch (ForeignKeyConstraintViolationException $e) {
            $this->get('session')->getFlashBag()->add(
                'notice',"Data has been relation another Table"
            );
        }catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add(
                'notice', 'Please contact system administrator further notification.'
            );
        }
        return new Response($response);
    }


    /**
     * Update a RequisitionItem entity.
     * @Route("/{id}/download-attachment", methods={"GET","POST"}, name="procure_tender_bank_receive_attachment", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function downloadAttachFile(Request $request, TenderWorkorderReceive $entity)
    {

        $filepath = $this->get('kernel')->getProjectDir() . "/public/uploads/procurement/{$entity->getPath()}";
        if (!empty($entity->getPath()))
        {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($filepath));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filepath));
            ob_clean();
            flush();
            readfile($filepath);
        }
        exit;
    }

}
