<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Controller\Bank;

use App\Entity\Application\Procurement;
use App\Entity\Domain\ApprovalUser;
use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\Vendor;
use App\Entity\User;
use App\Repository\Application\ProcurementRepository;
use App\Repository\Domain\ApprovalUserRepository;
use App\Service\FormValidationManager;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\Tender;
use Terminalbd\ProcurementBundle\Entity\TenderComparative;
use Terminalbd\ProcurementBundle\Entity\TenderComparativeItem;
use Terminalbd\ProcurementBundle\Entity\TenderComparativeItemAttribute;
use Terminalbd\ProcurementBundle\Entity\TenderCompare;
use Terminalbd\ProcurementBundle\Entity\TenderItem;
use Terminalbd\ProcurementBundle\Entity\TenderVendor;
use Terminalbd\ProcurementBundle\Form\ApproveCommentFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderComparativeFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderCsFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderVendorFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFilterFormType;
use Terminalbd\ProcurementBundle\Repository\ProcurementProcessRepository;
use Terminalbd\ProcurementBundle\Repository\TenderComparativeItemAttributeRepository;
use Terminalbd\ProcurementBundle\Repository\TenderComparativeRepository;
use Terminalbd\ProcurementBundle\Repository\TenderItemRepository;
use Terminalbd\ProcurementBundle\Repository\TenderRepository;
use Terminalbd\ProcurementBundle\Repository\TenderVendorRepository;


/**
 * @Route("/procure/bssf/comparative")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class TenderComparativeController extends AbstractController
{

    public function paginate(Request $request ,$entities)
    {
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $request->query->get('page', 1)/*page number*/,
            25  /*limit per page*/
        );
        return $pagination;
    }

    /**
     * @Route("/", methods={"GET", "POST"}, name="procure_tender_bank_comparative")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_ADMIN')")
     */
    public function index(Request $request, TranslatorInterface $translator,ProcurementRepository $procurementRepository , TenderComparativeRepository $repository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $data = $request->request->all();
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , NULL,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $mode = array('mode'=> $mode);
            $data = array_merge($data,$mode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
        }
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->findBankSearchQuery($config,$this->getUser(),$data);
        } else {
            $search = $repository->findBankSearchQuery($config,$this->getUser(),$data);
        }
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/bank/tender-comparative/index.html.twig', [
            'pagination' => $pagination,
            'mode' => $mode,
            'searchForm' => $searchForm->createView(),
        ]);
    }


    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/create-vendor", methods={"GET","POST"}, name="procure_tender_bank_comparative_create_vendor" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function createVendor(Request $request , Tender $tender, TenderRepository $repository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $entity = new TenderVendor();
        $form = $this->createForm(TenderVendorFormType::class, $entity,array('terminal' => $terminal));
        $form->handleRequest($request);
        $errorValidation = new FormValidationManager();
        $errors = $errorValidation->getErrorsFromForm($form);
        $data = $request->request->all();
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $data = $request->request->all();
            $attachFile = $form->get('file')->getData();
            if ($attachFile and $entity->getPath()) {
                $entity->removeUpload();
            }
            $entity->setTender($tender);
            if(!empty($entity->getVendor()) and empty($entity->getName())){
                $entity->setName($entity->getVendor()->getName());
            }
            if(!empty($entity->getVendor()) and empty($entity->getAddress())){
                $entity->setAddress($entity->getVendor()->getAddress());
            }
            if(!empty($entity->getVendor()) and empty($entity->getPhone())){
                $entity->setPhone($entity->getVendor()->getPhone());
            }
            $entity->setStatus(1);
            $entity->setTender($tender);
            $entity->upload();
            $em->persist($entity);
            $em->flush();
        }elseif(!empty($errors)){
            $this->addFlash('error',$errors);
        }
        return $this->redirectToRoute('procure_tender_bank_comparative_cs_new',array('id' => $tender->getId()));
    }


    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/cs-new", methods={"GET","POST"}, name="procure_tender_bank_comparative_cs_new" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function csNew(Request $request, Tender $tender,ProcurementProcessRepository $processRepository, TranslatorInterface $translator, TenderVendorRepository $tenderVendorRepository, ApprovalUserRepository $approvalUserRepository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $entity = new TenderComparative();
        $form = $this->createForm(TenderCsFormType::class, $entity);
        $form->handleRequest($request);
        $data = $request->request->all();
        $vendorCheck = (isset($data['vendorCheck']) and !empty($data['vendorCheck'])) ? $data['vendorCheck']:'';

        if ($form->isSubmitted() and !empty($vendorCheck)) {
            $vendorCheck = (isset($data['vendorCheck']) and !empty($data['vendorCheck'])) ? $data['vendorCheck']:'';
            $tenderVendorRepository->csBankVendorSelection($tender->getId(),$vendorCheck);
            if($tender->getTenderComparative()){
                return $this->redirectToRoute('procure_tender_bank_comparative_generate', ['id' => $tender->getTenderComparative()->getId()]);
            }
//        if ($form->isSubmitted() && $form->isValid() && !empty($vendorCheck) && empty($tender->getTenderComparative())) {

            $em = $this->getDoctrine()->getManager();
            $module = "tender-comparative";
            $moduleProcess = $this->getDoctrine()->getRepository(ModuleProcess::class)->existModuleProcess($terminal,$module);
            if(!empty($moduleProcess)){
                $entity->setModuleProcess($moduleProcess);
                $entity->setModule($module);
            }
            $entity->setCreatedBy($this->getUser());
            $entity->setConfig($tender->getConfig());
            $entity->setTender($tender);
            $entity->setBusinessGroup("bank");
            $entity->setWaitingProcess("New");
            $entity->setProcess("New");
            $em->persist($entity);
            $em->flush();
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
            $files = !empty($_FILES['vendorFiles']) ?  $_FILES['vendorFiles'] : "";
            if(!empty($files)){
                $tenderVendorRepository->vendorAttachmentFile($tender,$data,$files);
            }
            if($entity->getModuleProcess()->isStatus() == 1){
                $assignUsers = $approvalUserRepository->getApprovalAssignUser($terminal,$entity);
                $processRepository->insertProcurementProcessAssign($entity,$entity->getModule(),$assignUsers);
            }
            $this->getDoctrine()->getRepository(TenderComparative::class)->csStockItemGenerate($tender,$entity);
//            return $this->redirectToRoute('procure_tender_bank_comparative_generate',array('id' => $entity->getId()));
            return $this->redirectToRoute('procure_tender_floating_memo_bank');
        }elseif(!empty($vendorCheck) && empty($tender->getTenderComparative())){
            $message = 'Tender Comparative Statement already created';
            $this->addFlash('notice', $message);
        }
        $vendorForm = $this->createForm(TenderVendorFormType::class, new TenderVendor(),array('terminal' => $terminal));
        return $this->render('@TerminalbdProcurement/bank/tender-comparative/cs-new.html.twig', [
            'tender' => $tender,
            'entity' => $entity,
            'form' => $form->createView(),
            'vendorForm' => $vendorForm->createView(),
        ]);
    }

    /**
     * Show a Setting entity.
     * @Route("/{id}/generate", methods={"GET","POST"}, name="procure_tender_bank_comparative_generate" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function csTender(Request $request , TenderComparative $entity, TenderComparativeRepository $repository, ProcurementProcessRepository $processRepository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $tender = $entity->getTender();
        $comparativeItems = $repository->comparativeItems($entity);
        $comparativeItemAttribites = $repository->comparativeItemAttribites($entity);
        $form = $this->createForm(TenderComparativeFormType::class , $entity,array('terminal' => $terminal));
        $form -> handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if(empty($entity->getRecomendationDepartment()) and empty($entity->getRecomendationHeadUser())){
                 $processRepository->approvalAssign($entity);
            }elseif($entity->getRecomendationDepartment() and $entity->getRecomendationHeadUser()){
                $processRepository->approvalAssign($entity);
            }
            $data = $request->request->all();
            $files = isset($_FILES['files']) ? $_FILES['files'] : '';
            $em->persist($entity);
            $em->flush();
            if(!empty($files)){
                $processRepository->insertAttachmentFile($entity,$data,$files);
            }
            return $this->redirectToRoute('procure_tender_bank_comparative');
        }
        $attchments = $this->getDoctrine()->getRepository(ProcurementProcess::class)->getDmsAttchmentFile($entity->getId(),'tender-comparative');
        return $this->render('@TerminalbdProcurement/bank/tender-comparative/comparetive-statement.html.twig', [
            'tender'                    => $tender,
            'entity'                    => $entity,
            'form'                      => $form->createView(),
            'comparativeItems'          => $comparativeItems,
            'comparativeItemAttribites' => $comparativeItemAttribites,
            'attchments'                => $attchments
        ]);
    }

   /**
     * Show a Setting entity.
     * @Route("/{id}/process", methods={"GET","POST"}, name="procure_tender_bank_comparative_process" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function process(Request $request , TranslatorInterface $translator, TenderComparative $entity, TenderComparativeRepository $repository, ProcurementRepository $procurementRepository, ProcurementProcessRepository $processRepository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $data = $request->request->all();
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $form = $this->createForm(ApproveCommentFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $comment = $form["comment"]->getData();
            $processRepository->insertApprovalProcessForGenericComment($this->getUser(),$entity,$comment);
            if(isset($data['reject']) and $data['reject'] == "reject"){
                $entity->setProcess('Rejected');
                $entity->setWaitingProcess('Rejected');
                $entity->setReportTo(null);
                $entity->setApproveTo($this->getUser());
                $em->persist($entity);
                $em->flush();
                $message = $translator->trans('data.reject_successfully');
                $this->addFlash('success', $message);
            }elseif(isset($data['re-checked']) and $data['re-checked'] == "re-checked"){
                $processRepository->resetRechecked($entity);
                $entity->setProcess('Re-checked');
                $entity->setWaitingProcess('New');
                $entity->setReportTo(null);
                $entity->setApproveTo($this->getUser());
                $entity->setComment($comment);
                $em->persist($entity);
                $em->flush();
                $message = $translator->trans('data.recheck_successfully');
                $this->addFlash('success', $message);
            } else {
                $count = $processRepository->count(array('entityId' => $entity->getId(), 'module' => $entity->getModule(), 'close' => 1));
                $ordering = ($count == 0) ? 1 : $count + 1;
                $processRepository->approvalAssign($entity, $ordering);
                $message = $translator->trans('data.updated_successfully');
                $this->addFlash('success', $message);
            }
            return $this->redirectToRoute('procure_tender_bank_comparative',['mode'=>'approve']);
        }
        $tender = $entity->getTender();
        $comparativeItems = $repository->comparativeItems($entity);
        $comparativeItemAttribites = $repository->comparativeItemAttribites($entity);
        return $this->render('@TerminalbdProcurement/bank/tender-comparative/process.html.twig', [
            'tender'                    => $tender,
            'entity'                    => $entity,
            'form'                      => $form->createView(),
            'comparativeItems'          => $comparativeItems,
            'comparativeItemAttribites' => $comparativeItemAttribites,
            'attchments'                => ''
        ]);
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/cs-vendor", methods={"GET"}, name="procure_tender_bank_comparative_cs_vendor" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function csVendor(TenderComparative $entity, TenderComparativeRepository $repository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $tender = $entity->getTender();
        $comparativeItems = $repository->comparativeItems($entity);
        $comparativeItemAttribites = $repository->comparativeItemAttribites($entity);
        return $this->render('@TerminalbdProcurement/bank/tender-comparative/show.html.twig', [
            'tender' => $tender,
            'entity' => $entity,
            'comparativeItems' => $comparativeItems,
            'comparativeItemAttribites' => $comparativeItemAttribites,
            'attchments' => ''
        ]);
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/item-update", methods={"GET","POST"}, name="procure_tender_bank_comparative_item_update" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function itemUpdate(Request $request, TenderComparativeItem $attribute ): Response
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->request->all();
        $attribute->setUnitPrice($data['unitPrice']);
        $attribute->setQuantity($data['quantity']);
        $attribute->setRevisedUnitPrice($data['unitPrice']);
        $attribute->setSubTotal($attribute->getQuantity() * $attribute->getUnitPrice());
        $attribute->setRevisedSubTotal($attribute->getQuantity() * $attribute->getUnitPrice());
        $em->flush();
        $this->getDoctrine()->getRepository(TenderComparativeItem::class)->updateSummary($attribute->getTenderVendor());
        return new Response('success');
    }


     /**
     * Show a Setting entity.
     *
     * @Route("/{id}/item-revised-update", methods={"GET","POST"}, name="procure_tender_bank_comparative_item_revised_update" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function itemRevisedUpdate(Request $request, TenderComparativeItem $attribute ): Response
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->request->all();
        $attribute->setRevisedUnitPrice($data['unitPrice']);
        $attribute->setRevisedSubTotal($attribute->getQuantity() * $attribute->getRevisedUnitPrice());
        $em->flush();
        $this->getDoctrine()->getRepository(TenderComparativeItem::class)->updateSummary($attribute->getTenderVendor());
        return new Response('success');
    }


     /**
     * Show a Setting entity.
     *
     * @Route("/{id}/attribute-update", methods={"GET","POST"}, name="procure_tender_bank_comparative_attribute_update" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function attributeUpdate(Request $request, TenderComparativeItemAttribute $attribute ): Response
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->request->all();
        $attribute->setMetaValue($data['description']);
        $em->flush();
        return new Response('success');
    }


    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET"}, name="procure_tender_bank_comparative_show" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function show(Request $request , TenderComparative $entity, TenderComparativeRepository $repository, ProcurementProcessRepository $procurementRepository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $tender = $entity->getTender();
        $comparativeItems = $repository->comparativeItems($entity);
        $comparativeItemAttribites = $repository->comparativeItemAttribites($entity);
        return $this->render('@TerminalbdProcurement/bank/tender-comparative/show.html.twig', [
            'tender'                    => $tender,
            'entity'                    => $entity,
            'comparativeItems'          => $comparativeItems,
            'comparativeItemAttribites' => $comparativeItemAttribites,
        ]);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="procure_tender_bank_comparative_delete")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function delete(TenderComparative $entity): Response
    {
        $em = $this->getDoctrine()->getManager();
        $response = "invalid";
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Particular entity.');
        }
        try {
            $em->remove($entity);
            $em->flush();
            $response = 'valid';

        } catch (ForeignKeyConstraintViolationException $e) {
            $this->get('session')->getFlashBag()->add(
                'notice',"Data has been relation another Table"
            );
        }catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add(
                'notice', 'Please contact system administrator further notification.'
            );
        }
        return new Response($response);
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/delete-vendor", methods={"GET"}, name="procure_tender_bank_comparative_vendor_delete" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function deleteVendor(TenderVendor $entity): Response
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();
        return new Response('success');
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/status", methods={"GET"}, name="procure_particular_status" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function status($id, TenderComparativeRepository $repository): Response
    {
        $entity = $repository->find($id);
        $status = $_REQUEST['status'];
        if($status == "false"){
            $entity->setStatus(false);
        }else{
            $entity->setStatus(true);
        }
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/preview", methods={"GET"}, name="procure_tender_bank_comparative_preview" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function printPreview(TenderComparative $entity,TenderComparativeRepository $repository, ProcurementProcessRepository $procurementRepository): Response
    {

        $mode = isset($_REQUEST['mode']) ? $_REQUEST['mode'] :'';
        $terminal = $this->getUser()->getTerminal();
        $tender = $entity->getTender();
        $comparativeItems = $repository->comparativeItems($entity);
        $comparativeItemAttribites = $repository->comparativeItemAttribites($entity);
        $approvals = $this->getDoctrine()->getRepository(ProcurementProcess::class)->findBy(array('entityId'=>$entity->getId(),'module'=>$entity->getModule()),array('ordering'=>'ASC'));
        if($mode == 'print'){
            return $this->render(
                '@TerminalbdProcurement/bank/tender-comparative/preview.html.twig', array(
                    'tender'                    => $tender,
                    'entity'                    => $entity,
                    'approvals'                    => $approvals,
                    'comparativeItems'          => $comparativeItems,
                    'comparativeItemAttribites' => $comparativeItemAttribites,
                    'mode' => 'print'
                )
            );
        }elseif ($mode == 'pdf'){
             $mpdf = new \Mpdf\Mpdf(['tempDir' => __DIR__ . '/../../../../public/temp']);
            $html = $this->renderView('@TerminalbdProcurement/bank/tender-comparative/pdf.html.twig',array(
                'tender'                    => $tender,
                'entity'                    => $entity,
                'approvals'                    => $approvals,
                'comparativeItems'          => $comparativeItems,
                'comparativeItemAttribites' => $comparativeItemAttribites,
                'mode' => "pdf"
            ));
            $mpdf->WriteHTML($html);
            $mpdf->Output();
        }else{
            return $this->render(
                '@TerminalbdProcurement/bank/tender-comparative/preview.html.twig', array(
                    'tender'                    => $tender,
                    'entity'                    => $entity,
                    'approvals'                    => $approvals,
                    'comparativeItems'          => $comparativeItems,
                    'comparativeItemAttribites' => $comparativeItemAttribites,
                    'mode' => 'preview'
                )
            );
        }

    }


    /**
     * @Route("/recomendation-department", methods={"GET", "POST"}, name="procure_tender_bank_comparative_recomendation_department")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_ADMIN')")
     */
    public function recomendDepartment(Request $request, TranslatorInterface $translator,ProcurementRepository $procurementRepository , TenderComparativeRepository $repository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $data = $request->request->all();
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , NULL,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $mode = array('mode'=> $mode);
            $data = array_merge($data,$mode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
        }
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->bankCsAcknowledge($config,$this->getUser(),$data);
        } else {
            $search = $repository->bankCsAcknowledge($config,$this->getUser(),$data);
        }
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/bank/tender-comparative/acknowledgment.html.twig', [
            'pagination' => $pagination,
            'mode' => $mode,
            'searchForm' => $searchForm->createView(),
        ]);
    }

    /**
     * @Route("/department-acknowledgment-assesment", methods={"GET", "POST"}, name="procure_tender_bank_comparative_recomendation_department_assessment")
     * @Security("is_granted('ROLE_PROCUREMENT_ASSESSMENT') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_ADMIN')")
     */
    public function departmentAcknowledgmentAssesment(Request $request, TranslatorInterface $translator,ProcurementRepository $procurementRepository , TenderComparativeRepository $repository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $data = $request->request->all();
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , NULL,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "in-progress";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "in-progress";
            $mode = array('mode'=> $mode);
            $data = array_merge($data,$mode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "in-progress";
        }
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->bankCsAcknowledgeAssesment($config->getId(),$this->getUser(),$data);
        } else {
            $search = $repository->bankCsAcknowledgeAssesment($config->getId(),$this->getUser(),$data);
        }
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/bank/tender-comparative/acknowledgmentComment.html.twig', [
            'pagination' => $pagination,
            'mode'=>$mode,
            'selectedRows' => '',
            'selected' => '',
            'searchForm' => $searchForm->createView()
        ]);
    }

    /**
     * @Route("/{id}/recommendent-inline-update", methods={"GET", "POST"}, name="procure_cs_bank_recommendent_user", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_PROCUREMENT_LSSD') or  is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_ADMIN')")
     */
    public function inlineUpdate(Request $request, TenderComparative $entity)
    {

        $data = $request->request->all();
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }
        $em = $this->getDoctrine()->getManager();
        $user = $this->getDoctrine()->getRepository(User::class)->find($data['value']);
        $entity->setRecomendationAssignUser($user);
        $em->persist($entity);
        $em->flush();
        return new Response('success');

    }

    /**
     * @Route("/{id}/cs-inline-comment-update", methods={"GET", "POST"}, name="procure_cs_bank_recommendent_user_assesment", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_ADMIN')")
     */
    public function inlineCommentUpdate(Request $request, TenderComparative $entity)
    {

        $data = $_REQUEST['comment'];
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }
        $entity->setRecomendationContent($data);
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');

    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/recommendation-cs-approve", methods={"GET"}, name="procure_cs_bank_recommendent_user_approve" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function assesmentApproved(TenderComparative $entity): Response
    {
        if($entity){
            $em = $this->getDoctrine()->getManager();
            $entity->setApprovedBy($this->getUser());
            $em->persist($entity);
            $em->flush();
            $this->getDoctrine()->getRepository(ProcurementProcess::class)->approvalAssign($entity);
        }
        return  new Response('success');
    }


    /**
     * Update a RequisitionItem entity.
     * @Route("/{id}/download-attachment", methods={"GET","POST"}, name="procure_tender_bank_comparative_vendor_attachment", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function downloadAttachFile(Request $request, TenderVendor $entity)
    {

        $filepath = $this->get('kernel')->getProjectDir() . "/public/uploads/procurement/{$entity->getPath()}";
        if (!empty($entity->getPath()))
        {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($filepath));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filepath));
            ob_clean();
            flush();
            readfile($filepath);
        }
        exit;
    }

}
