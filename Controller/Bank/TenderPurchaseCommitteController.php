<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Controller\Bank;

use App\Entity\Application\Procurement;
use App\Entity\Domain\ApprovalUser;
use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\Vendor;
use App\Repository\Application\ProcurementRepository;
use App\Repository\Domain\ApprovalUserRepository;
use App\Service\FormValidationManager;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementCondition;
use Terminalbd\ProcurementBundle\Entity\ProcurementConditionItem;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\Tender;
use Terminalbd\ProcurementBundle\Entity\TenderBatch;
use Terminalbd\ProcurementBundle\Entity\TenderBatchItem;
use Terminalbd\ProcurementBundle\Entity\TenderCommittee;
use Terminalbd\ProcurementBundle\Entity\TenderComparative;
use Terminalbd\ProcurementBundle\Entity\TenderCompare;
use Terminalbd\ProcurementBundle\Entity\TenderConditionItem;
use Terminalbd\ProcurementBundle\Entity\TenderItem;
use Terminalbd\ProcurementBundle\Entity\TenderMemo;
use Terminalbd\ProcurementBundle\Entity\TenderMemoComment;
use Terminalbd\ProcurementBundle\Entity\TenderVendor;
use Terminalbd\ProcurementBundle\Form\ApproveCommentFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderBatchFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderCommitteeFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFilterFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderCsFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderPreparetionFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderVendorFormType;
use Terminalbd\ProcurementBundle\Repository\ProcurementProcessRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionItemRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionRepository;
use Terminalbd\ProcurementBundle\Repository\TenderBatchRepository;
use Terminalbd\ProcurementBundle\Repository\TenderCommitteeRepository;
use Terminalbd\ProcurementBundle\Repository\TenderItemRepository;
use Terminalbd\ProcurementBundle\Repository\TenderRepository;
use Terminalbd\ProcurementBundle\Repository\TenderVendorRepository;
use Terminalbd\ProcurementBundle\Entity\ComapnyRequisitionShare;



/**
 * @Route("/procure/bssf/tender-purchase-committee")
 * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_LSSD_HEAD')  or is_granted('ROLE_PROCUREMENT_LSSD')  or  is_granted('ROLE_APPROVER') ")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class TenderPurchaseCommitteController extends AbstractController
{

    public function paginate(Request $request ,$entities)
    {
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $request->query->get('page', 1)/*page number*/,
            25  /*limit per page*/
        );
        return $pagination;
    }

    /**
     * @Route("/", methods={"GET", "POST"}, name="procure_purchase_committee_bank")
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD')  or  is_granted('ROLE_PROCUREMENT_LSSD_HEAD')  or  is_granted('ROLE_APPROVER') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_LSSD_ADMIN')")
     */
    public function index(Request $request, TranslatorInterface $translator,ProcurementRepository $procurementRepository , TenderCommitteeRepository $repository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $data = $request->request->all();
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , NULL,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        $module = "purchase-committee";
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $mode = array('mode'=> $mode);
            $data = array_merge($data,$mode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
        }
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->findBankSearchQuery($config,$this->getUser(),$module,$data);
        } else {
            $search = $repository->findBankSearchQuery($config,$this->getUser(),$module,$data);
        }
        $pagination = $this->paginate($request,$search);
        $approves = $this->getDoctrine()->getRepository(ProcurementProcess::class)->findApproveProcessUser($pagination,$this->getUser()->getId());
        return $this->render('@TerminalbdProcurement/bank/purchase-committee/index.html.twig', [
            'pagination' => $pagination,
            'approves' => $approves,
            'mode' => $mode,
            'module' => $module,
            'searchForm' => $searchForm->createView()
        ]);
    }

    /**
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD')  or  is_granted('ROLE_PROCUREMENT_LSSD_HEAD') or  is_granted('ROLE_APPROVER') or is_granted('ROLE_DOMAIN')")
     * @Route("/new", methods={"GET", "POST"}, name="procure_purchase_committee_bank_new")
     */
    public function new(Request $request, TranslatorInterface $translator, ProcurementRepository $repository, ProcurementProcessRepository $processRepository, ApprovalUserRepository $approvalUserRepository): Response
    {
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $repository->config($terminal);
        $em = $this->getDoctrine()->getManager();
        $entity = new TenderCommittee();
        $entity->setConfig($config);
        $module = "purchase-committee";
        $user = $this->getUser();
        $moduleProcess = $this->getDoctrine()->getRepository(ModuleProcess::class)->existModuleProcess($terminal,$module);
        if(!empty($moduleProcess)) {
            $entity->setModuleProcess($moduleProcess);
            $entity->setModule($module);
            $entity->setWaitingProcess("New");
            $entity->setProcess("New");
            $entity->setCreatedBy($user);
            $em->persist($entity);
            $em->flush();
            $assignUsers = $approvalUserRepository->getApprovalAssignUser($terminal,$entity);
            $processRepository->insertProcurementProcessAssign($entity,$entity->getModule(),$assignUsers);
            $this->addFlash('success', $translator->trans('data.created_successfully'));
            return $this->redirectToRoute('procure_purchase_committee_bank_edit', array('id' => $entity->getId()));
        }
        return $this->redirectToRoute('procure_purchase_committee_bank');
    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="procure_purchase_committee_bank_edit")
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD')  or  is_granted('ROLE_PROCUREMENT_LSSD_HEAD') or  is_granted('ROLE_APPROVER') or is_granted('ROLE_DOMAIN')")
     */
    public function edit(Request $request,TenderCommittee $entity,TranslatorInterface $translator , ProcurementRepository $procurementRepository ,ProcurementProcessRepository $processRepository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $data = $request->request->all();
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        /* @var $entity TenderCommittee */
        $form = $this->createForm(TenderCommitteeFormType::class, $entity, array('terminal'=>$this->getUser()->getTerminal(),'config'=>$config,'process'=>$entity->getModule()));
        $form->handleRequest($request);
        $data = $request->request->all();
        if ($form->isSubmitted() && $form->isValid() and !empty($entity->getMemos())) {
            $data = $request->request->all();
            $entity->setWaitingProcess("Approve");
            $entity->setProcess("Approve");
            $this->getDoctrine()->getManager()->flush();
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
            $files = isset( $_FILES['files']) ?  $_FILES['files'] : "";
            if(!empty($files)){
                $processRepository->insertAttachmentFile($entity,$data,$files);
            };
            $this->getDoctrine()->getRepository(TenderConditionItem::class)->insertBankCommitteeConditionItem($entity,$data);
            return $this->redirectToRoute('procure_purchase_committee_bank',array('mode' => 'list'));
        }
        $tenders = $this->getDoctrine()->getRepository(TenderMemo::class)->findBankMemoLists($config);
        return $this->render('@TerminalbdProcurement/bank/purchase-committee/new.html.twig', [
            'entity' => $entity,
            'tenders' => $tenders,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/ajax-update", methods={"GET", "POST"}, name="procure_purchase_committee_bank_ajax_update",options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_LSSD')")
     */
    public function ajaxUpadteRequsition(Request $request ,TenderCommittee $entity,ProcurementRepository $procurementRepository)
    {

        /* @var $config  Procurement */

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $form = $this->createForm(TenderCommitteeFormType::class, $entity, array('terminal'=>$this->getUser()->getTerminal(),'config'=>$config,'process' => $entity->getModule()));
        $form->handleRequest($request);
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
        $files = !empty($_FILES['files']) ? $_FILES['files']:'';
        if($files){
            $this->getDoctrine()->getRepository(ProcurementProcess::class)->insertAttachmentFile($entity,$data,$files);
        }
        return new Response('success');
    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/process", methods={"GET", "POST"}, name="procure_purchase_committee_bank_process", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD')  or  is_granted('ROLE_PROCUREMENT_LSSD_HEAD')  or  is_granted('ROLE_APPROVER') or is_granted('ROLE_DOMAIN')")
     */
    public function process(Request $request, $id,TranslatorInterface $translator , ProcurementRepository $procurementRepository , TenderCommitteeRepository $repository, ProcurementProcessRepository $processRepository): Response
    {

        $terminal = $this->getUser()->getTerminal();
        $data = $request->request->all();
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        /* @var $entity TenderCommittee */
        $entity = $repository->findOneBy(array('config' => $config,'id' => $id));
        $form = $this->createForm(ApproveCommentFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $data = $request->request->all();
            $comment = $form["comment"]->getData();
            //$processRepository->insertApprovalProcessForGenericComment($this->getUser(),$entity,$comment);
            $entity->setComment($comment);
            if(isset($data['reject']) and $data['reject'] == "reject"){
                $entity->setProcess('Rejected');
                $entity->setWaitingProcess('Rejected');
                $entity->setReportTo(null);
                $entity->setApproveTo($this->getUser());
                $em->persist($entity);
                $em->flush();
                $message = $translator->trans('data.reject_successfully');
                $this->addFlash('success', $message);
            }else{
                $entity->setProcess('Meeting');
                $entity->setWaitingProcess('Meeting');
                $entity->setReportTo(null);
                $entity->setApproveTo($this->getUser());
                $em->persist($entity);
                $em->flush();
                $this->getDoctrine()->getRepository(ProcurementProcess::class)->meetingApprover($entity,'purchase-memo');
                $message = $translator->trans('data.created_successfully');
                $this->addFlash('success', $message);
            }
            return $this->redirectToRoute('procure_purchase_committee_bank',array('mode'=>'approve'));
        }
        return $this->render('@TerminalbdProcurement/bank/purchase-committee/process.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/closed-meting", methods={"GET","POST"}, name="procure_purchase_committee_bank_closed" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD')  or  is_granted('ROLE_PROCUREMENT_LSSD_HEAD') or  is_granted('ROLE_APPROVER') or is_granted('ROLE_DOMAIN')")
     */
    public function closedTender(Request $request, TenderCommittee $committee): Response
    {
        $em = $this->getDoctrine()->getManager();
        $committee->setProcess('Closed');
        $committee->setWaitingProcess('Closed');
        $em->persist($committee);
        $em->flush();
        return new Response('success');
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/show-meeting-memo", methods={"GET","POST"}, name="procure_purchase_committee_bank_meeting" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD')  or  is_granted('ROLE_PROCUREMENT_LSSD_HEAD') or  is_granted('ROLE_APPROVER') or is_granted('ROLE_DOMAIN')")
     */
    public function meetingMemo(Request $request,TenderCommittee $entity, ProcurementProcessRepository $processRepository): Response
    {

        $comments = array();
        if($entity->getComments()){
            foreach ($entity->getComments() as $comment){
                $id = $comment->getMemo()->getId();
                $comments[$id] = $comment;
            }
        }
        $form = $this->createForm(ApproveCommentFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $data = $request->request->all();
            $comment = $form["comment"]->getData();
            $processRepository->insertApprovalProcessForGenericComment($this->getUser(),$entity,$comment);
            return $this->redirectToRoute('procure_purchase_committee_bank_meeting',array('id' => $entity->getId()));
        }
        return $this->render('@TerminalbdProcurement/bank/purchase-committee/meeting-memo.html.twig', [
            'entity' => $entity,
            'comments' => $comments,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/meetiing-comment", methods={"GET","POST"}, name="procure_purchase_committee_meeting_comment" , options={"expose"=true})
     */
    public function meetiingComment(Request $request, TenderMemo $entity): Response
    {
        $data = $request->request->all();
        $data = $_REQUEST;
        $remark = $data['comment'];
        $process = $data['process'];
        $committeeId = $data['meeting'];
        $committee = $this->getDoctrine()->getRepository(TenderCommittee::class)->find($committeeId);
        $em = $this->getDoctrine()->getManager();
        $exist = $this->getDoctrine()->getRepository(TenderMemoComment::class)->findOneBy(array('committee'=>$committee , 'memo' => $entity));
        if($exist){
            $exist->setComment($remark);
            $exist->setProcess($process);
        }else{
            $comment = new TenderMemoComment();
            $comment->setProcess($process);
            $comment->setComment($remark);
            $comment->setCommittee($committee);
            $comment->setMemo($entity);
            $comment->setCreatedBy($this->getUser());
            $em->persist($comment);
        }
        $em->flush();
        return new Response('success');
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/add-memo", methods={"GET","POST"}, name="procure_purchase_committee_bank_add_memo" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD')  or  is_granted('ROLE_PROCUREMENT_LSSD_HEAD') or  is_granted('ROLE_APPROVER') or is_granted('ROLE_DOMAIN')")
     */
    public function addMemoItem(Request $request, TenderCommittee $entity): Response
    {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $tender = $this->getDoctrine()->getRepository(TenderMemo::class)->find($data['memo']);
        $tender->setTenderCommittee($entity);
        $em->persist($entity);
        $em->flush();
        $html = $this->renderView(
            '@TerminalbdProcurement/bank/purchase-committee/memo-item.html.twig', array(
                'entity' => $entity,
            )
        );
        return new Response($html);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/{memo}/memo-delete", methods={"GET"}, name="procure_purchase_committee_bank_memo_delete", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD')  or  is_granted('ROLE_PROCUREMENT_LSSD_HEAD') or  is_granted('ROLE_APPROVER') or is_granted('ROLE_DOMAIN')")
     */

    public function memoDelete(TenderCommittee $entity, $memo): Response
    {
        $em = $this->getDoctrine()->getManager();
        $tender = $this->getDoctrine()->getRepository(Tender::class)->find($memo);
        $tender->setTenderCommittee(NULL);
        $em->flush();
        $html = $this->renderView(
            '@TerminalbdProcurement/bank/purchase-committee/memo-item.html.twig', array(
                'entity' => $entity,
            )
        );
        return new Response($html);
    }



    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET"}, name="procure_purchase_committee_bank_show" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD')  or  is_granted('ROLE_PROCUREMENT_LSSD_HEAD') or  is_granted('ROLE_APPROVER') or is_granted('ROLE_DOMAIN')")
     */
    public function show(TenderCommittee $entity): Response
    {
        $html = $this->renderView(
            '@TerminalbdProcurement/bank/purchase-committee/show.html.twig', array(
                'entity' => $entity,
            )
        );
        return new Response($html);
    }


    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="procure_purchase_committee_bank_delete")
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD')  or  is_granted('ROLE_PROCUREMENT_LSSD_HEAD') or  is_granted('ROLE_APPROVER') or is_granted('ROLE_DOMAIN')")
     */

    public function delete(TenderCommittee $entity): Response
    {
        $em = $this->getDoctrine()->getManager();
        $response = "invalid";
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Particular entity.');
        }
        try {
            $em->remove($entity);
            $em->flush();
            $response = 'valid';

        } catch (ForeignKeyConstraintViolationException $e) {
            $this->get('session')->getFlashBag()->add(
                'notice',"Data has been relation another Table"
            );
        }catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add(
                'notice', 'Please contact system administrator further notification.'
            );
        }
        return new Response($response);
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/preview", methods={"GET"}, name="procure_purchase_committee_bank_preview" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD')  or  is_granted('ROLE_PROCUREMENT_LSSD_HEAD')  or  is_granted('ROLE_APPROVER') or is_granted('ROLE_DOMAIN')")
     */
    public function printPreview(TenderCommittee $entity): Response
    {
        $mode = isset($_REQUEST['mode']) ? $_REQUEST['mode'] :'';
        $approvals = $this->getDoctrine()->getRepository(ProcurementProcess::class)->findBy(array('entityId'=>$entity->getId(),'module'=>$entity->getModule()),array('ordering'=>'ASC'));
        return $this->render(
            '@TerminalbdProcurement/bank/purchase-committee/preview.html.twig', array(
                'entity' => $entity,
                'approvals' => $approvals,
                'mode' => 'preview'
            )
        );

    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/condition-update", methods={"GET","POST"}, name="procure_bank_tender_committee_condition" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_PROCUREMENT_LSSD_HEAD') or is_granted('ROLE_DOMAIN')")
     */
    public function conditionUpdate(Request $request, TenderCommittee $tender): Response
    {
        $em = $this->getDoctrine()->getManager();
        $v = $_REQUEST['condition'];
        /** @var  $entity ProcurementCondition */
        $entity = $this->getDoctrine()->getRepository(ProcurementCondition::class)->find($v);
        $tender->setCondition($entity);
        $tender->setSubject($entity->getSubject());
        $tender->setContent($entity->getBody());
        $tender->setEmail($entity->getCcEmail());
        $em->flush();
        $this->getDoctrine()->getRepository(TenderConditionItem::class)->initialBankTenderCommitteeConditionItem($tender);
        return new Response('success');
    }



}
