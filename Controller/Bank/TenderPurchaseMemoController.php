<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Controller\Bank;

use App\Entity\Application\Procurement;
use App\Entity\Domain\ApprovalUser;
use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\Vendor;
use App\Entity\User;
use App\Repository\Application\ProcurementRepository;
use App\Repository\Domain\ApprovalUserRepository;
use App\Service\FormValidationManager;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementCondition;
use Terminalbd\ProcurementBundle\Entity\ProcurementConditionItem;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\Tender;
use Terminalbd\ProcurementBundle\Entity\TenderBatch;
use Terminalbd\ProcurementBundle\Entity\TenderBatchItem;
use Terminalbd\ProcurementBundle\Entity\TenderCommittee;
use Terminalbd\ProcurementBundle\Entity\TenderComparative;
use Terminalbd\ProcurementBundle\Entity\TenderCompare;
use Terminalbd\ProcurementBundle\Entity\TenderConditionItem;
use Terminalbd\ProcurementBundle\Entity\TenderItem;
use Terminalbd\ProcurementBundle\Entity\TenderMemo;
use Terminalbd\ProcurementBundle\Entity\TenderMemoItem;
use Terminalbd\ProcurementBundle\Entity\TenderMemoUser;
use Terminalbd\ProcurementBundle\Entity\TenderVendor;
use Terminalbd\ProcurementBundle\Form\ApproveCommentFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderBatchFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderCommitteeFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderPurchaseMemoFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFilterFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderCsFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderPreparetionFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderVendorFormType;
use Terminalbd\ProcurementBundle\Repository\ProcurementProcessRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionItemRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionRepository;
use Terminalbd\ProcurementBundle\Repository\TenderBatchRepository;
use Terminalbd\ProcurementBundle\Repository\TenderCommitteeRepository;
use Terminalbd\ProcurementBundle\Repository\TenderItemRepository;
use Terminalbd\ProcurementBundle\Repository\TenderMemoRepository;
use Terminalbd\ProcurementBundle\Repository\TenderRepository;
use Terminalbd\ProcurementBundle\Repository\TenderVendorRepository;
use Terminalbd\ProcurementBundle\Entity\ComapnyRequisitionShare;



/**
 * @Route("/procure/bssf/tender-purchase-memo")
 * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_LSSD')")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class TenderPurchaseMemoController extends AbstractController
{

    public function paginate(Request $request ,$entities)
    {
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $request->query->get('page', 1)/*page number*/,
            25  /*limit per page*/
        );
        return $pagination;
    }

    /**
     * @Route("/", methods={"GET", "POST"}, name="procure_purchase_memo_bank")
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_LSSD_ADMIN')")
     */
    public function index(Request $request, TranslatorInterface $translator,ProcurementRepository $procurementRepository , TenderMemoRepository $repository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $data = $request->request->all();
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , NULL,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $mode = array('mode'=> $mode);
            $data = array_merge($data,$mode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
        }
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->findBankPurchaseMemoSearchQuery($config,$this->getUser(),$data);
        } else {
            $search = $repository->findBankPurchaseMemoSearchQuery($config,$this->getUser(),$data);
        }
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/bank/purchase-memo/index.html.twig', [
            'config' => $config,
            'pagination' => $pagination,
            'approves' => '',
            'mode' => $mode,
            'searchForm' => $searchForm->createView()
        ]);
    }

    /**
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN')")
     * @Route("/{id}/generate", methods={"GET", "POST"}, name="procure_purchase_memo_bank_generate")
     */
    public function generateMemo(Request $request, TenderComparative $comparative, TranslatorInterface $translator, ProcurementRepository $repository, ProcurementProcessRepository $processRepository, ApprovalUserRepository $approvalUserRepository): Response
    {
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $repository->config($terminal);
        $em = $this->getDoctrine()->getManager();
        $module = "purchase-memo";
        $user = $this->getUser();
        $moduleProcess = $this->getDoctrine()->getRepository(ModuleProcess::class)->existModuleProcess($terminal,$module);
        if(!empty($moduleProcess) and empty($comparative->getTenderMemo())) {
            $entity = new TenderMemo();
            $entity->setConfig($config);
            $user = $this->getUser();
            $entity->setModuleProcess($moduleProcess);
            $entity->setTenderComparative($comparative);
            $entity->setSubject($comparative->getTender()->getSubject());
            $entity->setModule($module);
            $entity->setWaitingProcess("New");
            $entity->setProcess("New");
            $entity->setCreatedBy($user);
            $em->persist($entity);
            $em->flush();
            $assignUsers = $approvalUserRepository->getApprovalAssignUser($terminal,$entity);
            $processRepository->insertProcurementProcessAssign($entity,$entity->getModule(),$assignUsers);
            $this->addFlash('success', $translator->trans('data.created_successfully'));
            return $this->redirectToRoute('procure_purchase_memo_bank_edit', array('id' => $entity->getId()));
        }else{
            $this->addFlash('notice', "This cs already generated purchase committe memo");
            $referer = $request->headers->get('referer');
            return new RedirectResponse($referer);
        }

    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="procure_purchase_memo_bank_edit")
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN')")
     */
    public function edit(Request $request,TenderMemo $entity,TranslatorInterface $translator , ProcurementRepository $procurementRepository ,ProcurementProcessRepository $processRepository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $data = $request->request->all();
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $form = $this->createForm(TenderPurchaseMemoFormType::class, $entity, array('terminal'=>$this->getUser()->getTerminal(),'config'=>$config,'tenderComparative'=> $entity->getTenderComparative()));
        $form->handleRequest($request);
        $data = $request->request->all();
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $request->request->all();
            $entity->setProcess('Approve');
            $entity->setWaitingProcess('Approve');
            $this->getDoctrine()->getManager()->flush();
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
            $processRepository->approvalAssign($entity);
            $this->getDoctrine()->getRepository(TenderMemoItem::class)->tenderMemoItem($entity,$data);
            $this->getDoctrine()->getRepository(TenderConditionItem::class)->insertTenderPurchaseMemoConditionItem($entity,$data);
            return $this->redirectToRoute('procure_purchase_memo_bank',array('mode' => 'list'));
        }
        $committeeMembers = $this->getDoctrine()->getRepository(TenderMemo::class)->getPurchaseCommitteeUsers($terminal);
        $comparativeItems = $this->getDoctrine()->getRepository(TenderComparative::class)->comparativeItems($entity->getTenderComparative());
        $comparativeItemAttribites = $this->getDoctrine()->getRepository(TenderComparative::class)->comparativeItemAttribites($entity->getTenderComparative());

        return $this->render('@TerminalbdProcurement/bank/purchase-memo/new.html.twig', [
            'entity' => $entity,
            'tender' => $entity->getTenderComparative()->getTender(),
            'committeeMembers' => $committeeMembers,
            'comparativeItems' => $comparativeItems,
            'comparativeItemAttribites' => $comparativeItemAttribites,
            'form' => $form->createView(),
        ]);
    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/process", methods={"GET", "POST"}, name="procure_purchase_memo_bank_process", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN')")
     */
    public function process(Request $request,TenderMemo $entity,TranslatorInterface $translator , ProcurementRepository $procurementRepository, ProcurementProcessRepository $processRepository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $data = $request->request->all();
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $form = $this->createForm(ApproveCommentFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $data = $request->request->all();
            $comment = $form["comment"]->getData();
           // $entity->setComment($comment);
            if(isset($data['reject']) and $data['reject'] == "reject"){
                $entity->setProcess('Rejected');
                $entity->setWaitingProcess('Rejected');
                $entity->setReportTo(null);
                $entity->setApproveTo($this->getUser());
                $em->persist($entity);
                $em->flush();
                $message = $translator->trans('data.reject_successfully');
                $this->addFlash('success', $message);
            }elseif(isset($data['re-checked']) and $data['re-checked'] == "re-checked"){
                $processRepository->resetRechecked($entity);
                $entity->setProcess('Re-checked');
                $entity->setWaitingProcess('New');
                $entity->setReportTo(null);
                $entity->setApproveTo($this->getUser());
                $entity->setComment($comment);
                $em->persist($entity);
                $em->flush();
                $message = $translator->trans('data.recheck_successfully');
                $this->addFlash('success', $message);
            }else{
                $processRepository->insertApprovalProcessForGenericComment($this->getUser(),$entity,$comment);
                $count = $processRepository->count(array('entityId' => $entity->getId(), 'module' => $entity->getModule(), 'close' => 1));
                $ordering = ($count == 0) ? 1 : $count + 1;
                $processRepository->approvalAssign($entity, $ordering);
                $message = $translator->trans('data.updated_successfully');
                $this->addFlash('success', $message);
            }
            return $this->redirectToRoute('procure_purchase_memo_bank',array('mode'=>'approve'));
        }
        return $this->render('@TerminalbdProcurement/bank/purchase-memo/process.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/committee-process", methods={"GET", "POST"}, name="procure_purchase_memo_bank_committee_process", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN')")
     */
    public function committeeProcess(Request $request,TenderMemo $entity,TranslatorInterface $translator , ProcurementRepository $procurementRepository, ProcurementProcessRepository $processRepository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $data = $request->request->all();
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $form = $this->createForm(ApproveCommentFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $data = $request->request->all();
            $comment = $form["comment"]->getData();
            $processRepository->insertApprovalProcessForGenericComment($this->getUser(),$entity,$comment);
            if(isset($data['reject']) and $data['reject'] == "reject"){
                $entity->setComment($comment);
                $entity->setProcess('Rejected');
                $entity->setWaitingProcess('Rejected');
                $entity->setReportTo(null);
                $em->persist($entity);
                $em->flush();
            }elseif(isset($data['approve']) and $data['approve'] == "approve"){
                $entity->setComment($comment);
                $entity->setProcess('Management Memo');
                $entity->setWaitingProcess('Approved');
                $entity->setReportTo(null);
                $em->persist($entity);
                $em->flush();
            }
            $committee = $entity->getTenderCommittee();
            $committee->setProcess("Closed");
            $committee->setWaitingProcess("Closed");
            $em->persist($committee);
            $em->flush();
//            return $this->redirect($request->server->get('HTTP_REFERER'));
            return $this->redirectToRoute('procure_purchase_memo_bank');
        }
        $approver = $this->getDoctrine()->getRepository(ProcurementProcess::class)->checkApproveProcessUser($entity->getId(),$this->getUser()->getId());
        return $this->render('@TerminalbdProcurement/bank/purchase-memo/meeting-process.html.twig', [
            'entity' => $entity,
            'approver' => $approver,
            'form' => $form->createView(),
        ]);
    }


    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/revised-memo", methods={"GET","POST"}, name="procure_purchase_memo_bank_revised_memo" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN')")
     */
    public function revisedMemo(Request $request, TenderMemo $entity, TranslatorInterface $translator): Response
    {
        $form = $this->createForm(ApproveCommentFormType::class);
        $form->handleRequest($request);
        $comparativeItems = $this->getDoctrine()->getRepository(TenderComparative::class)->comparativeItems($entity->getTenderComparative());
        $comparativeItemAttribites = $this->getDoctrine()->getRepository(TenderComparative::class)->comparativeItemAttribites($entity->getTenderComparative());
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $data = $request->request->all();
            $comment = $form["comment"]->getData();
            $entity->setComment($comment);
            $em->persist($entity);
            $em->flush();
            $message = $translator->trans('data.reject_successfully');
            return $this->redirectToRoute('procure_purchase_memo_bank');
        }
        return $this->render('@TerminalbdProcurement/bank/purchase-memo/revised-memo.html.twig', [
            'entity' => $entity,
            'tender' => $entity->getTenderComparative()->getTender(),
            'comparativeItems' => $comparativeItems,
            'comparativeItemAttribites' => $comparativeItemAttribites,
            'form' => $form->createView(),
        ]);
    }

     /**
     * Show a Setting entity.
     *
     * @Route("/{id}/show-meeting-memo", methods={"GET","POST"}, name="procure_purchase_memo_bank_meeting" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN')")
     */
    public function meetingMemo(Request $request,TenderCommittee $entity): Response
    {
        return $this->render('@TerminalbdProcurement/bank/purchase-memo/meeting-memo.html.twig', [
            'entity' => $entity,
        ]);
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/add-member", methods={"GET","POST"}, name="procure_purchase_memo_bank_add_member" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN')")
     */
    public function addMember(Request $request, TenderMemo $memo): Response
    {
        $data = $_REQUEST;
        $em = $this->getDoctrine()->getManager();
        if($_REQUEST['member']){
            $member = $data['member'];
            $assignTo = $this->getDoctrine()->getRepository(User::class)->find($member);
            $find = $this->getDoctrine()->getRepository(TenderMemoUser::class)->findOneBy(array('assignTo'=>$assignTo,'memo'=>$memo));
            if(empty($find)){
                $entity = new TenderMemoUser();
                $entity->setMemo($memo);
                $entity->setAssignTo($assignTo);
                $entity->setPosition($data['position']);
                $entity->setOrdering($data['ordering']);
                $em->persist($entity);
                $em->flush();
            }
        }
        $html = $this->renderView(
            '@TerminalbdProcurement/bank/purchase-memo/memo-item.html.twig', array(
                'entity' => $memo,
            )
        );
        return new Response($html);

    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/{memo}/memo-delete", methods={"GET"}, name="procure_purchase_memo_bank_memo_delete", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN')")
     */

    public function memoDelete(TenderMemo $entity, $memo): Response
    {
        $em = $this->getDoctrine()->getManager();
        $member = $this->getDoctrine()->getRepository(TenderMemoUser::class)->find($memo);
        $em->remove($member);
        $em->flush();
        $html = $this->renderView(
            '@TerminalbdProcurement/bank/purchase-memo/memo-item.html.twig', array(
                'entity' => $entity,
            )
        );
        return new Response($html);
    }


    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/update-vendor-revised-price", methods={"GET"}, name="procure_purchase_memo_bank_update_vendor_revised", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN')")
     */

    public function updateVendorRevisedPrice(TenderVendor $entity): Response
    {
        $em = $this->getDoctrine()->getManager();
        $price = $_REQUEST['price'];
        $entity->setRevisedTotal($price);
        $em->persist($entity);
        $em->flush();
        return new Response($price);
    }


    /**
     * Show a TenderMemo entity.
     *
     * @Route("/{id}/show", methods={"GET"}, name="procure_purchase_memo_bank_show" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN')")
     */
    public function show(TenderMemo $entity): Response
    {
        $html = $this->renderView('@TerminalbdProcurement/bank/purchase-memo/show.html.twig', array(
                'entity' => $entity,
            )
        );
        return new Response($html);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="procure_purchase_memo_bank_delete")
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN')")
     */

    public function delete(TenderMemo $entity): Response
    {
        $em = $this->getDoctrine()->getManager();
        $response = "invalid";
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Particular entity.');
        }
        try {
            $em->remove($entity);
            $em->flush();
            $response = 'valid';

        } catch (ForeignKeyConstraintViolationException $e) {
            $this->get('session')->getFlashBag()->add(
                'notice',"Data has been relation another Table"
            );
        }catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add(
                'notice', 'Please contact system administrator further notification.'
            );
        }
        return new Response($response);
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/preview", methods={"GET"}, name="procure_purchase_memo_bank_preview" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN')")
     */
    public function printPreview(TenderMemo $entity): Response
    {
        $mode = isset($_REQUEST['mode']) ? $_REQUEST['mode'] :'';
        $approvals = $this->getDoctrine()->getRepository(ProcurementProcess::class)->findBy(array('entityId'=>$entity->getId(),'module'=>$entity->getModule()),array('ordering'=>'ASC'));

        if($mode == 'print'){
            return $this->render(
                '@TerminalbdProcurement/bank/purchase-memo/preview.html.twig', array(
                    'entity' => $entity,
                    'approvals' => $approvals,
                    'mode' => 'print'
                )
            );
        }elseif ($mode == 'pdf'){
             $mpdf = new \Mpdf\Mpdf(['tempDir' => __DIR__ . '/../../../../public/temp']);
            $html = $this->renderView('@TerminalbdProcurement/bank/purchase-memo/pdf.html.twig',array(
                'entity' => $entity,
                'approvals' => $approvals,
                'mode' => "pdf"
            ));
            $mpdf->WriteHTML($html);
            $mpdf->Output();
        }else{
            return $this->render(
                '@TerminalbdProcurement/bank/purchase-memo/preview.html.twig', array(
                    'entity' => $entity,
                    'approvals' => $approvals,
                    'mode' => 'preview'
                )
            );
        }

    }


}
