<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Controller\Bank;

use App\Entity\Application\Procurement;
use App\Entity\Domain\ApprovalUser;
use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\Vendor;
use App\Repository\Application\ProcurementRepository;
use App\Repository\Domain\ApprovalUserRepository;
use App\Service\FormValidationManager;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\InventoryBundle\Entity\StockBook;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementCondition;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\Tender;
use Terminalbd\ProcurementBundle\Entity\TenderComparative;
use Terminalbd\ProcurementBundle\Entity\TenderComparativeItem;
use Terminalbd\ProcurementBundle\Entity\TenderConditionItem;
use Terminalbd\ProcurementBundle\Entity\TenderItemDetails;
use Terminalbd\ProcurementBundle\Entity\TenderMemo;
use Terminalbd\ProcurementBundle\Entity\TenderVendor;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorder;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderItem;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderReceive;
use Terminalbd\ProcurementBundle\Form\ApproveCommentFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFilterFormType;
use Terminalbd\ProcurementBundle\Form\TenderReceiveFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderWorkorderFormType;
use Terminalbd\ProcurementBundle\Repository\ProcurementProcessRepository;
use Terminalbd\ProcurementBundle\Repository\TenderComparativeItemAttributeRepository;
use Terminalbd\ProcurementBundle\Repository\TenderComparativeRepository;
use Terminalbd\ProcurementBundle\Repository\TenderItemRepository;
use Terminalbd\ProcurementBundle\Repository\TenderRepository;
use Terminalbd\ProcurementBundle\Repository\TenderVendorRepository;
use Terminalbd\ProcurementBundle\Repository\TenderWorkorderRepository;


/**
 * @Route("/procure/bssf/work-order")
 * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_APPROVER') or is_granted('ROLE_PROCUREMENT_WORKORDER')")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */

class TenderWorkorderController extends AbstractController
{

    public function paginate(Request $request ,$entities)
    {
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $request->query->get('page', 1)/*page number*/,
            25  /*limit per page*/
        );
        return $pagination;
    }

    /**
     * @Route("/", methods={"GET", "POST"}, name="procure_tender_bank_workorder")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_ADMIN')")
     */
    public function index(Request $request, TranslatorInterface $translator,ProcurementRepository $procurementRepository, TenderWorkorderRepository $repository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $data = $request->request->all();
        $entities = $this->getDoctrine()->getRepository(TenderWorkorder::class)->findBy(array('config' => $config),array('code'=>"DESC"));
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , NULL,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $mode = array('mode'=> $mode);
            $data = array_merge($data,$mode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
        }
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->findBankSearchQuery($config,$this->getUser(),$data);
        } else {
            $search = $repository->findBankSearchQuery($config,$this->getUser(),$data);
        }
        $pagination = $this->paginate($request,$search);
        $data = $_REQUEST;
        return $this->render('@TerminalbdProcurement/bank/workorder/index.html.twig', [
            'pagination' => $pagination,
            'mode' => $mode,
            'searchForm' => $searchForm->createView(),
        ]);
    }

    /**
     * @Route("/branch", methods={"GET", "POST"}, name="procure_tender_bank_branch_workorder")
     * @Security("is_granted('ROLE_PROCUREMENT_INITIATOR') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_MANAGER')")
     */
    public function branchWorkorder(Request $request, TranslatorInterface $translator,ProcurementRepository $procurementRepository, TenderWorkorderRepository $repository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $data = $request->request->all();
        $entities = $this->getDoctrine()->getRepository(TenderWorkorder::class)->findBy(array('config' => $config),array('code'=>"DESC"));
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , NULL,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $mode = array('mode'=> $mode);
            $data = array_merge($data,$mode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
        }
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->findBankBranchRequisitionWorkorderQuery($config,$this->getUser(),$data);
        } else {
            $search = $repository->findBankBranchRequisitionWorkorderQuery($config,$this->getUser(),$data);
        }
        $pagination = $this->paginate($request,$search);
        $data = $_REQUEST;
        return $this->render('@TerminalbdProcurement/bank/workorder/branch-workorder.html.twig', [
            'pagination' => $pagination,
            'mode' => $mode,
            'searchForm' => $searchForm->createView(),
        ]);
    }



    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/new", methods={"GET"}, name="procure_tender_bank_workorder_new" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function new(Request $request, TenderMemo $tenderMemo ,ProcurementRepository $procurementRepository,ApprovalUserRepository $approvalUserRepository ,ProcurementProcessRepository $processRepository): Response
    {

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $comparative = $tenderMemo->getParent()->getTenderComparative();
        $entity = new TenderWorkorder();
        $em = $this->getDoctrine()->getManager();
        $moduleProcess = $this->getDoctrine()->getRepository(ModuleProcess::class)->existModuleProcess($terminal,'work-order');
        if(!empty($moduleProcess)){
            $entity->setModuleProcess($moduleProcess);
        }
        $entity->setConfig($config);
        $entity->setManagementMemo($tenderMemo);
        if($comparative->getTender()->getWorkOrder()){
            $entity->setTenderVendor($comparative->getTender()->getWorkOrder()->getTenderVendor());
        }
        $entity->setTenderComparative($comparative);
        $entity->setModule("work-order");
        $entity->setSubject($tenderMemo->getSubject());
        $entity->setBusinessGroup("bank");
        $entity->setWaitingProcess("New");
        $entity->setProcess("New");
        $date = new \DateTime("now");
        $entity->setWorkorderDate($date);
        $user = $this->getUser();
        $entity->setCreatedBy($user);
        $em->persist($entity);
        $em->flush();
        if($entity->getModuleProcess()->isStatus() == 1){
            $assignUsers = $approvalUserRepository->getApprovalAssignUser($terminal,$entity);
            $processRepository->insertProcurementProcessAssign($entity,$entity->getModule(),$assignUsers);
        }
        $this->getDoctrine()->getRepository(TenderWorkorderItem::class)->insertBankWorkorderDirectItem($entity);
        $this->getDoctrine()->getRepository(TenderWorkorderItem::class)->updateWorkOrderSummary($entity);
        return $this->redirectToRoute('procure_tender_bank_workorder_edit',array('id' => $entity->getId()));
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/{vendor}/multivendor-workorder", methods={"GET"}, name="procure_tender_bank_workorder_multivendor" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function multivendorWorkorder(Request $request, TenderMemo $tenderMemo, $vendor , ProcurementRepository $procurementRepository,ApprovalUserRepository $approvalUserRepository ,ProcurementProcessRepository $processRepository): Response
    {

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $comparative = $tenderMemo->getParent()->getTenderComparative();
        $entity = new TenderWorkorder();
        $em = $this->getDoctrine()->getManager();
        $moduleProcess = $this->getDoctrine()->getRepository(ModuleProcess::class)->existModuleProcess($terminal,'work-order');
        if(!empty($moduleProcess)){
            $entity->setModuleProcess($moduleProcess);
        }
        $tenderVendor = $this->getDoctrine()->getRepository(TenderVendor::class)->find($vendor);
        $entity->setConfig($config);
        $entity->setManagementMemo($tenderMemo);
        $entity->setTenderVendor($tenderVendor);
        $entity->setTenderComparative($comparative);
        $entity->setModule("work-order");
        $entity->setBusinessGroup("bank");
        $entity->setWaitingProcess("New");
        $entity->setProcess("New");
        $date = new \DateTime("now");
        $entity->setWorkorderDate($date);
        $user = $this->getUser();
        $entity->setCreatedBy($user);
        $em->persist($entity);
        $em->flush();
        if($entity->getModuleProcess()->isStatus() == 1){
            $assignUsers = $approvalUserRepository->getApprovalAssignUser($terminal,$entity);
            $processRepository->insertProcurementProcessAssign($entity,$entity->getModule(),$assignUsers);
        }

    //    $this->getDoctrine()->getRepository(TenderWorkorderItem::class)->insertBankWorkorderDirectItem($entity);
    //    $this->getDoctrine()->getRepository(TenderWorkorderItem::class)->updateWorkOrderSummary($entity);
        return $this->redirectToRoute('procure_tender_bank_workorder_edit',array('id' => $entity->getId()));
    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="procure_tender_bank_workorder_edit")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function edit(Request $request, TenderWorkorder $entity ,TranslatorInterface $translator, ProcurementRepository $procurementRepository, ProcurementProcessRepository $processRepository): Response
    {

        $terminal = $this->getUser()->getTerminal();

        /* @var $config  Procurement */

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $tenderComparative = $entity->getTenderComparative();
        $tender = $entity->getTenderComparative()->getTender();
        $requisitions = $this->getDoctrine()->getRepository(TenderItemDetails::class)->getRequisitionList($tender);
        $tenderRepo = $this->getDoctrine()->getRepository(Tender::class);
        $form = $this->createForm(TenderWorkorderFormType::class , $entity,array('config' => $config,'tenderComparative'=>$tenderComparative,'tenderRepo'=>$tenderRepo,'workorder'=> $entity));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $request->request->all();
            $processRepository->approvalAssign($entity);
            $this->getDoctrine()->getManager()->flush();
           // $this->getDoctrine()->getRepository(TenderWorkorder::class)->insertBankWorkOrderItem($entity,$data);
            $this->getDoctrine()->getRepository(TenderConditionItem::class)->insertBankWorkorderConditionItem($entity,$data);
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('procure_tender_bank_workorder');
        }
        $requisitionItems = $this->getDoctrine()->getRepository(TenderItemDetails::class)->getTenderRequisitionItem($entity);
        $vendorItems = "";
        if($entity->getTenderVendor()){
            $vendorItems = $this->getDoctrine()->getRepository(TenderComparativeItem::class)->getBankTenderVendorItems($entity->getTenderVendor());
        }
        $this->getDoctrine()->getRepository(TenderWorkorderItem::class)->insertBankWorkorderRequisitionItem($entity);
        $this->getDoctrine()->getRepository(TenderWorkorderItem::class)->updateWorkOrderSummary($entity);
        return $this->render('@TerminalbdProcurement/bank/workorder/new.html.twig', [
            'entity'            => $entity,
            'requisitionItems'  => $requisitionItems,
            'vendorItems'       => $vendorItems,
            'form'              => $form->createView()
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/process", methods={"GET", "POST"}, name="procure_tender_bank_workorder_process")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function process(Request $request, TenderWorkorder $entity ,TranslatorInterface $translator, ProcurementRepository $procurementRepository, ProcurementProcessRepository $processRepository): Response
    {

        $terminal = $this->getUser()->getTerminal();

        /* @var $config  Procurement */

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $tenderComparative = $entity->getTenderComparative();
        $form = $this->createForm(ApproveCommentFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $data = $request->request->all();
            $comment = $form["comment"]->getData();
            if(isset($data['reject']) and $data['reject'] == "reject"){
                $entity->setProcess('Rejected');
                $entity->setWaitingProcess('Rejected');
                $entity->setReportTo(null);
                $entity->setApproveTo($this->getUser());
                $em->persist($entity);
                $em->flush();
                $message = $translator->trans('data.reject_successfully');
                $this->addFlash('success', $message);
            }elseif(isset($data['re-checked']) and $data['re-checked'] == "re-checked"){
                $processRepository->resetRechecked($entity);
                $entity->setProcess('Re-checked');
                $entity->setWaitingProcess('New');
                $entity->setReportTo(null);
                $entity->setApproveTo($this->getUser());
                $entity->setComment($comment);
                $em->persist($entity);
                $em->flush();
                $message = $translator->trans('data.recheck_successfully');
                $this->addFlash('success', $message);
            }else{
                $processRepository->insertApprovalProcessForGenericComment($this->getUser(),$entity,$comment);
                $count = $processRepository->count(array('entityId'=> $entity->getId(), 'module'=>$entity->getModule(),'close'=>1));
                $ordering = ($count == 0) ? 1 : $count+1;
                $processRepository->approvalAssign($entity,$ordering);
                $message = $translator->trans('data.updated_successfully');
                $this->addFlash('success', $message);

            }
            return $this->redirectToRoute('procure_tender_bank_workorder',['mode'=>'approve']);
        }
        return $this->render('@TerminalbdProcurement/bank/workorder/process.html.twig', [
            'entity'            => $entity,
            'form'              => $form->createView()
        ]);
    }


    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/vendor", methods={"GET","POST"}, name="procure_tender_bank_workorder_vendor" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function vendorUpdate(Request $request, TenderWorkorder $workorder): Response
    {
        $em = $this->getDoctrine()->getManager();
        $v = $_REQUEST['vendor'];
        $vendor = $this->getDoctrine()->getRepository(TenderVendor::class)->find($v);
        $workorder->setTenderVendor($vendor);
        $em->flush();
        return new Response('success');
    }

     /**
     * Show a Setting entity.
     *
     * @Route("/{id}/requisition", methods={"GET","POST"}, name="procure_tender_bank_workorder_requisition" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function requisition(Request $request, TenderWorkorder $workorder): Response
    {
        $em = $this->getDoctrine()->getManager();
        $v = $_REQUEST['requisition'];
        $requisition = $this->getDoctrine()->getRepository(Requisition::class)->find($v);
        if($requisition){
            $workorder->setRequisition($requisition);
        }else{
            $workorder->setRequisition(NULL);
        }
        $em->flush();
        $this->getDoctrine()->getRepository(TenderWorkorderItem::class)->insertBankWorkorderRequisitionItem($workorder);
        $this->getDoctrine()->getRepository(TenderWorkorderItem::class)->updateWorkOrderSummary($workorder);
        return new Response('success');
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/condition-update", methods={"GET","POST"}, name="procure_tender_bank_workorder_condition" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function conditionUpdate(Request $request, TenderWorkorder $tender): Response
    {
        $em = $this->getDoctrine()->getManager();
        $v = $_REQUEST['condition'];
        /* @var $entity ProcurementCondition */
        $entity = $this->getDoctrine()->getRepository(ProcurementCondition::class)->find($v);
        $tender->setCondition($entity);
        $tender->setContent($entity->getBody());
        $tender->setFooterContent($entity->getDescription());
        $em->flush();
        $this->getDoctrine()->getRepository(TenderConditionItem::class)->initialBankWorkorderConditionItem($tender);
        return new Response('success');
    }


    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/item-update", methods={"GET","POST"}, name="procure_tender_bank_workorder_item_update" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function itemUpdate(Request $request, TenderComparativeItem $attribute ): Response
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->request->all();
        $attribute->setUnitPrice($data['unitPrice']);
        $attribute->setQuantity($data['quantity']);
        $attribute->setSubTotal($attribute->getQuantity() * $attribute->getUnitPrice());
        $em->flush();
        return new Response('success');
    }


     /**
     * Show a Setting entity.
     *
     * @Route("/{id}/attribute-update", methods={"GET","POST"}, name="procure_tender_bank_workorder_attribute_update" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function attributeUpdate(Request $request, TenderComparativeItemAttribute $attribute ): Response
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->request->all();
        $attribute->setMetaValue($data['description']);
        $em->flush();
        return new Response('success');
    }


    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET"}, name="procure_tender_bank_workorder_show" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function show(TenderWorkorder $entity): Response
    {
        $html = $this->renderView(
            '@TerminalbdProcurement/bank/workorder/show.html.twig', array(
                'entity' => $entity,
            )
        );
        return new Response($html);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="procure_tender_bank_workorder_delete")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function delete(TenderWorkorder $entity): Response
    {
        $entity = $repository->findOneBy(array('id' => $id));
        $em = $this->getDoctrine()->getManager();
        $response = "invalid";
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Particular entity.');
        }
        try {
            $em->remove($entity);
            $em->flush();
            $response = 'valid';

        } catch (ForeignKeyConstraintViolationException $e) {
            $this->get('session')->getFlashBag()->add(
                'notice',"Data has been relation another Table"
            );
        }catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add(
                'notice', 'Please contact system administrator further notification.'
            );
        }
        return new Response($response);
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/status", methods={"GET"}, name="procure_tender_bank_workorder_status" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function status(TenderWorkorder $entity): Response
    {
        $status = $_REQUEST['status'];
        if($status == "false"){
            $entity->setRecurring(false);
        }else{
            $entity->setRecurring(true);
        }
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/preview", methods={"GET"}, name="procure_tender_bank_workorder_preview" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function printPreview(TenderWorkorder $entity): Response
    {

        $mode = isset($_REQUEST['mode']) ? $_REQUEST['mode'] :'';
        $approvals = $this->getDoctrine()->getRepository(ProcurementProcess::class)->findBy(array('entityId'=>$entity->getId(),'module'=>$entity->getModule()),array('ordering'=>'ASC'));
        return $this->render(
            '@TerminalbdProcurement/bank/workorder/preview.html.twig', array(
                'entity' => $entity,
                'approvals' => $approvals,
                'mode' => 'preview'
            )
        );

    }



}
