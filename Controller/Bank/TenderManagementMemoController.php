<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Controller\Bank;

use App\Entity\Application\Procurement;
use App\Entity\Domain\ApprovalUser;
use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\Vendor;
use App\Entity\User;
use App\Repository\Application\ProcurementRepository;
use App\Repository\Domain\ApprovalUserRepository;
use App\Service\FormValidationManager;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementCondition;
use Terminalbd\ProcurementBundle\Entity\ProcurementConditionItem;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\Tender;
use Terminalbd\ProcurementBundle\Entity\TenderBatch;
use Terminalbd\ProcurementBundle\Entity\TenderBatchItem;
use Terminalbd\ProcurementBundle\Entity\TenderCommittee;
use Terminalbd\ProcurementBundle\Entity\TenderComparative;
use Terminalbd\ProcurementBundle\Entity\TenderCompare;
use Terminalbd\ProcurementBundle\Entity\TenderConditionItem;
use Terminalbd\ProcurementBundle\Entity\TenderItem;
use Terminalbd\ProcurementBundle\Entity\TenderMemo;
use Terminalbd\ProcurementBundle\Entity\TenderMemoItem;
use Terminalbd\ProcurementBundle\Entity\TenderMemoUser;
use Terminalbd\ProcurementBundle\Entity\TenderVendor;
use Terminalbd\ProcurementBundle\Form\ApproveCommentFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderBatchFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderCommitteeFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderManagementMemoFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderManagementQuotationFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderPurchaseMemoFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderQuotationFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFilterFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderCsFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderPreparetionFormType;
use Terminalbd\ProcurementBundle\Form\Bank\TenderVendorFormType;
use Terminalbd\ProcurementBundle\Repository\ProcurementProcessRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionItemRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionRepository;
use Terminalbd\ProcurementBundle\Repository\TenderBatchRepository;
use Terminalbd\ProcurementBundle\Repository\TenderCommitteeRepository;
use Terminalbd\ProcurementBundle\Repository\TenderItemRepository;
use Terminalbd\ProcurementBundle\Repository\TenderMemoRepository;
use Terminalbd\ProcurementBundle\Repository\TenderRepository;
use Terminalbd\ProcurementBundle\Repository\TenderVendorRepository;
use Terminalbd\ProcurementBundle\Entity\ComapnyRequisitionShare;



/**
 * @Route("/procure/bssf/tender-management-approval-memo")
 * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_APPROVER')")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class TenderManagementMemoController extends AbstractController
{

    public function paginate(Request $request ,$entities)
    {
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $request->query->get('page', 1)/*page number*/,
            25  /*limit per page*/
        );
        return $pagination;
    }

    /**
     * @Route("/tender", methods={"GET", "POST"}, name="procure_tender_management_memo_bank")
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_APPROVER')")
     */
    public function index(Request $request, TranslatorInterface $translator,ProcurementRepository $procurementRepository , TenderMemoRepository $repository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $data = $request->request->all();
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , NULL,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $mode = array('mode'=> $mode);
            $data = array_merge($data,$mode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
        }
        $tenderMode = 'tender';
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->findBankManagementMemoQuery($config,$this->getUser(),$tenderMode,$data);
        } else {
            $search = $repository->findBankManagementMemoQuery($config,$this->getUser(),$tenderMode,$data);
        }
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/bank/management-memo/index.html.twig', [
            'config' => $config,
            'pagination' => $pagination,
            'approves' => '',
            'mode' => $mode,
            'searchForm' => $searchForm->createView()
        ]);
    }

    /**
     * @Route("/quotation", methods={"GET", "POST"}, name="procure_tender_management_memo_quotation_bank")
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_APPROVER')")
     */
    public function indexQuotation(Request $request, TranslatorInterface $translator,ProcurementRepository $procurementRepository , TenderMemoRepository $repository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $data = $request->request->all();
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , NULL,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $mode = array('mode'=> $mode);
            $data = array_merge($data,$mode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
        }
        $tenderMode = 'quotation';
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->findBankManagementMemoQuery($config,$this->getUser(),$tenderMode,$data);
        } else {
            $search = $repository->findBankManagementMemoQuery($config,$this->getUser(),$tenderMode,$data);
        }
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/bank/management-memo/index-quotation.html.twig', [
            'config' => $config,
            'pagination' => $pagination,
            'approves' => '',
            'mode' => $mode,
            'searchForm' => $searchForm->createView()
        ]);
    }

    /**
     * @Route("/repeat-order", methods={"GET", "POST"}, name="procure_tender_management_memo_repeatorder_bank")
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_APPROVER')")
     */
    public function indexRepeatOrder(Request $request, TranslatorInterface $translator,ProcurementRepository $procurementRepository , TenderMemoRepository $repository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $data = $request->request->all();
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , NULL,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $mode = array('mode'=> $mode);
            $data = array_merge($data,$mode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
        }
        $tenderMode = 'repeat-order';
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->findBankManagementMemoQuery($config,$this->getUser(),$tenderMode,$data);
        } else {
            $search = $repository->findBankManagementMemoQuery($config,$this->getUser(),$tenderMode,$data);
        }
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/bank/management-memo/index-repeat-order.html.twig', [
            'config' => $config,
            'pagination' => $pagination,
            'approves' => '',
            'mode' => $mode,
            'searchForm' => $searchForm->createView()
        ]);
    }

    /**
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN')")
     * @Route("/new", methods={"GET", "POST"}, name="procure_tender_management_memo_bank_new")
     */
    public function new(Request $request, TranslatorInterface $translator, ProcurementRepository $repository, ProcurementProcessRepository $processRepository, ApprovalUserRepository $approvalUserRepository): Response
    {
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $repository->config($terminal);
        $em = $this->getDoctrine()->getManager();
        $entity = new TenderMemo();
        $entity->setConfig($config);
        $module = "management-memo";
        $user = $this->getUser();
        $entity->setModule($module);
        $entity->setWaitingProcess("New");
        $entity->setProcess("New");
        $entity->setCreatedBy($user);
        $em->persist($entity);
        $em->flush();
        $this->addFlash('success', $translator->trans('data.created_successfully'));
        return $this->redirectToRoute('procure_tender_management_memo_bank_edit', array('id' => $entity->getId()));

    }


    /**
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_APPROVER')")
     * @Route("/{id}/generate", methods={"GET", "POST"}, name="procure_tender_management_memo_bank_generate")
     */
    public function generateMemo(Request $request, TenderMemo $tenderMemo, TranslatorInterface $translator, ProcurementRepository $repository, ProcurementProcessRepository $processRepository, ApprovalUserRepository $approvalUserRepository): Response
    {
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $repository->config($terminal);
        $em = $this->getDoctrine()->getManager();
        $module = "management-memo";
        $user = $this->getUser();
        $moduleProcess = $this->getDoctrine()->getRepository(ModuleProcess::class)->existModuleProcess($terminal,$module);
        if(!empty($moduleProcess) and empty($tenderMemo->getChildren())) {
            $entity = new TenderMemo();
            $entity->setConfig($config);
            $user = $this->getUser();
            $entity->setModuleProcess($moduleProcess);
            $entity->setParent($tenderMemo);
            $entity->setSubject($tenderMemo->getSubject());
            $entity->setModule($module);
            $entity->setWaitingProcess("New");
            $entity->setProcess("New");
            $entity->setCreatedBy($user);
            $em->persist($entity);
            $em->flush();
            $assignUsers = $approvalUserRepository->getApprovalAssignUser($terminal,$entity);
            $processRepository->insertProcurementProcessAssign($entity,$entity->getModule(),$assignUsers);
            $this->addFlash('success', $translator->trans('data.created_successfully'));
            return $this->redirectToRoute('procure_tender_management_memo_bank_edit', array('id' => $entity->getId()));
        }else{
            $this->addFlash('notice', "This cs already generated purchase committe memo");
            $referer = $request->headers->get('referer');
            return new RedirectResponse($referer);
        }

    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="procure_tender_management_memo_bank_edit")
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_APPROVER')")
     */
    public function edit(Request $request,TenderMemo $entity,TranslatorInterface $translator , ProcurementRepository $procurementRepository ,ProcurementProcessRepository $processRepository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $data = $request->request->all();
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $form = $this->createForm(TenderManagementMemoFormType::class, $entity, array('terminal'=>$this->getUser()->getTerminal(),'config'=>$config,'tenderComparative'=>$entity->getParent()->getTenderComparative()));
        $form->handleRequest($request);
        $data = $request->request->all();
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $request->request->all();
            /*if($entity->getApprovedVendor()->getRevisedTotal()){
                $entity->setTenderAmount($entity->getApprovedVendor()->getRevisedTotal());
            }else{
                $entity->setTenderAmount($entity->getApprovedVendor()->getSubTotal());
            }*/
            $this->getDoctrine()->getManager()->flush();
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
            $processRepository->approvalAssign($entity);
            $this->getDoctrine()->getRepository(TenderConditionItem::class)->insertTenderPurchaseMemoConditionItem($entity,$data);
           /* if($entity->getWaitingProcess() == "Approved" and $entity->getTenderAmount() >= $config->getBoardApproveAmount()){
                $this->getDoctrine()->getRepository(TenderMemo::class)->boradMemoGenerate($terminal,$entity);
            }*/
            if($entity->getParent()->getTenderComparative()->getTender()->getProcessMode() == 'tender'){
                return $this->redirectToRoute('procure_tender_management_memo_bank',array('mode' => 'list'));
            }elseif($entity->getParent()->getTenderComparative()->getTender()->getProcessMode() == 'quotation'){
                return $this->redirectToRoute('procure_tender_management_memo_quotation_bank',array('mode' => 'list'));
            }elseif($entity->getParent()->getTenderComparative()->getTender()->getProcessMode() == 'repeat-order'){
                return $this->redirectToRoute('procure_tender_management_memo_repeatorder_bank',array('mode' => 'list'));
            }else{
                return $this->redirect($request->server->get('HTTP_REFERER'));
            }


        }
        return $this->render('@TerminalbdProcurement/bank/management-memo/new.html.twig', [
            'entity' => $entity,
            'committeeMembers' => '',
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/ajax-update", methods={"GET", "POST"}, name="procure_tender_management_memo_bank_ajax_update", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_ADMIN')")
     */
    public function ajaxUpadte(Request $request ,TenderMemo $entity,TranslatorInterface $translator , ProcurementRepository $procurementRepository ,ProcurementProcessRepository $processRepository)
    {

        /* @var $config  Procurement */

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $form = $this->createForm(TenderManagementMemoFormType::class, $entity, array('terminal'=>$this->getUser()->getTerminal(),'config'=>$config,'tenderComparative'=>$entity->getParent()->getTenderComparative()));
        $form->handleRequest($request);
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
        $files = !empty($_FILES['files']) ? $_FILES['files']:'';
        if($files){
            $this->getDoctrine()->getRepository(ProcurementProcess::class)->insertAttachmentFile($entity,$data,$files);
        }
        echo $entity->getFooterContent();
        return new Response('success');
    }

    /**
     * Show a Setting entity.
     * @Route("/{id}/process", methods={"GET","POST"}, name="procure_tender_management_memo_bank_process" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_APPROVER')")
     */
    public function process(Request $request , TranslatorInterface $translator, TenderMemo $entity, ProcurementRepository $procurementRepository, ProcurementProcessRepository $processRepository): Response
    {
        $data = $request->request->all();
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $form = $this->createForm(ApproveCommentFormType::class);
        $form->handleRequest($request);
        $entity->getApprovedVendor();
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $comment = $form["comment"]->getData();

            if(isset($data['reject']) and $data['reject'] == "reject"){
                $entity->setProcess('Rejected');
                $entity->setWaitingProcess('Rejected');
                $entity->setReportTo(null);
                $entity->setApproveTo($this->getUser());
                $em->persist($entity);
                $em->flush();
                $message = $translator->trans('data.reject_successfully');
                $this->addFlash('success', $message);
            }elseif(isset($data['re-checked']) and $data['re-checked'] == "re-checked"){
                $processRepository->resetRechecked($entity);
                $entity->setProcess('Re-checked');
                $entity->setWaitingProcess('New');
                $entity->setReportTo(null);
                $entity->setApproveTo($this->getUser());
                $entity->setComment($comment);
                $em->persist($entity);
                $em->flush();
                $message = $translator->trans('data.recheck_successfully');
                $this->addFlash('success', $message);
            }else{
                $processRepository->insertApprovalProcessForGenericComment($this->getUser(),$entity,$comment);
                $count = $processRepository->count(array('entityId' => $entity->getId(), 'module' => $entity->getModule(), 'close' => 1));
                $ordering = ($count == 0) ? 1 : $count + 1;
                $amount = $this->getDoctrine()->getRepository(TenderVendor::class)->getTenderAverageAmount($entity);
                $entity->setTenderAmount($amount['revisedTotal']);
                $processRepository->approvalAssign($entity, $ordering);
                $message = $translator->trans('data.updated_successfully');
                $this->addFlash('success', $message);
                if($entity->getWaitingProcess() == "Approved" and $entity->getTenderAmount() >= $config->getBoardApproveAmount()) {
                    $this->getDoctrine()->getRepository(TenderMemo::class)->boradMemoGenerate($terminal, $entity);
                }
            }
            if($entity->getParent()->getTenderComparative()->getTender()->getProcessMode() == 'tender'){
                return $this->redirectToRoute('procure_tender_management_memo_bank',array('mode' => 'approve'));
            }elseif($entity->getParent()->getTenderComparative()->getTender()->getProcessMode() == 'quotation'){
                return $this->redirectToRoute('procure_tender_management_memo_quotation_bank',array('mode' => 'approve'));
            }elseif($entity->getParent()->getTenderComparative()->getTender()->getProcessMode() == 'repeat-order'){
                return $this->redirectToRoute('procure_tender_management_memo_repeatorder_bank',array('mode' => 'approve'));
            }else{
//                return $this->redirect($request->server->get('HTTP_REFERER'));
                return $this->redirectToRoute('procure_tender_management_memo_quotation_bank');

            }
            return $this->redirectToRoute('procure_tender_management_memo_quotation_bank');
        }
        return $this->render('@TerminalbdProcurement/bank/management-memo/process.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/committee-process", methods={"GET", "POST"}, name="procure_tender_management_memo_bank_committee_process", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_APPROVER')")
     */
    public function committeeProcess(Request $request,TenderMemo $entity,TranslatorInterface $translator , ProcurementRepository $procurementRepository, ProcurementProcessRepository $processRepository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $data = $request->request->all();
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $form = $this->createForm(ApproveCommentFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $data = $request->request->all();
            $comment = $form["comment"]->getData();
            $processRepository->insertApprovalProcessForGenericComment($this->getUser(),$entity,$comment);
            if(isset($data['reject']) and $data['reject'] == "reject"){
                $entity->setComment($comment);
                $entity->setProcess('Rejected');
                $entity->setWaitingProcess('Rejected');
                $entity->setReportTo(null);
                $em->persist($entity);
                $em->flush();
            }elseif(isset($data['approve']) and $data['approve'] == "approve"){
                $entity->setComment($comment);
                $entity->setProcess('Approved');
                $entity->setWaitingProcess('Approved');
                $entity->setReportTo(null);
                $em->persist($entity);
                $em->flush();
            }
            return $this->redirect($request->server->get('HTTP_REFERER'));
        }
        $approver = $this->getDoctrine()->getRepository(ProcurementProcess::class)->checkApproveProcessUser($entity->getId(),$this->getUser()->getId());
        return $this->render('@TerminalbdProcurement/bank/approval-memo/meeting-process.html.twig', [
            'entity' => $entity,
            'approver' => $approver,
            'form' => $form->createView(),
        ]);
    }



     /**
     * Show a Setting entity.
     *
     * @Route("/{id}/show-meeting-memo", methods={"GET","POST"}, name="procure_tender_management_memo_bank_meeting" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_APPROVER')")
     */
    public function meetingMemo(Request $request,TenderCommittee $entity): Response
    {
        return $this->render('@TerminalbdProcurement/bank/approval-memo/meeting-memo.html.twig', [
            'entity' => $entity,
        ]);
    }


    /**
     * Show a TenderMemo entity.
     *
     * @Route("/{id}/show", methods={"GET"}, name="procure_tender_management_memo_bank_show" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_APPROVER')")
     */
    public function show(TenderMemo $entity): Response
    {
        $html = $this->renderView(
            '@TerminalbdProcurement/bank/management-memo/show.html.twig', array(
                'entity' => $entity,
            )
        );
        return new Response($html);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="procure_tender_management_memo_bank_delete")
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_APPROVER')")
     */

    public function delete(TenderMemo $entity): Response
    {
        $em = $this->getDoctrine()->getManager();
        $response = "invalid";
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Particular entity.');
        }
        try {
            $em->remove($entity);
            $em->flush();
            $response = 'valid';

        } catch (ForeignKeyConstraintViolationException $e) {
            $this->get('session')->getFlashBag()->add(
                'notice',"Data has been relation another Table"
            );
        }catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add(
                'notice', 'Please contact system administrator further notification.'
            );
        }
        return new Response($response);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/vendor-delete", methods={"GET"}, name="procure_tender_management_memo_bank_vendor_delete")
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN')")
     */

    public function deleteVendor(TenderVendor $entity): Response
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();
        return new Response('Success');
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/item-delete", methods={"GET"}, name="procure_tender_management_memo_bank_item_delete")
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN')")
     */

    public function itemDelete(TenderItem $entity): Response
    {
        $em = $this->getDoctrine()->getManager();
        $response = "invalid";
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Particular entity.');
        }
        try {
            $em->remove($entity);
            $em->flush();
            $response = 'valid';

        } catch (ForeignKeyConstraintViolationException $e) {
            $this->get('session')->getFlashBag()->add(
                'notice',"Data has been relation another Table"
            );
        }catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add(
                'notice', 'Please contact system administrator further notification.'
            );
        }
        return new Response($response);
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/preview", methods={"GET"}, name="procure_tender_management_memo_bank_preview" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN')")
     */
    public function printPreview(TenderMemo $entity): Response
    {
        $mode = isset($_REQUEST['mode']) ? $_REQUEST['mode'] :'';
        $approvals = $this->getDoctrine()->getRepository(ProcurementProcess::class)->findBy(array('entityId'=>$entity->getId(),'module'=>$entity->getModule()),array('ordering'=>'ASC'));
        return $this->render(
            '@TerminalbdProcurement/bank/management-memo/preview.html.twig', array(
                'entity' => $entity,
                'approvals' => $approvals,
                'mode' => 'preview'
            )
        );
    }

    /**
     * @Security("is_granted('ROLE_PROCUREMENT_LSSD') or is_granted('ROLE_DOMAIN')")
     * @Route("/{id}/quotation-generate", methods={"GET", "POST"}, name="procure_management_quotation_memo_bank")
     */
    public function generateQuotation(Request $request,TenderMemo $entity,TranslatorInterface $translator , ProcurementRepository $procurementRepository ,ProcurementProcessRepository $processRepository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $data = $request->request->all();
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $form = $this->createForm(TenderManagementQuotationFormType::class, $entity, array('terminal'=>$this->getUser()->getTerminal(),'config'=>$config,'tenderComparative' => $entity->getParent()->getTenderComparative()));
        $form->handleRequest($request);
        $data = $request->request->all();
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $request->request->all();
            $this->getDoctrine()->getManager()->flush();
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
            $files = isset($_FILES['files']) ? $_FILES['files'] : '';
            if(!empty($files)){
                $processRepository->insertAttachmentFile($entity,$data,$files);
            }
            $vendorFiles = !empty($_FILES['vendorFiles']) ?  $_FILES['vendorFiles'] : "";
            if(!empty($vendorFiles)){
                $this->getDoctrine()->getRepository(TenderVendor::class)->vendorAttachmentFile($entity->getParent()->getTenderComparative()->getTender(),$data,$vendorFiles);
            }
            $processRepository->approvalAssign($entity);
            $this->getDoctrine()->getRepository(TenderMemoItem::class)->tenderMemoItem($entity,$data);
            $this->getDoctrine()->getRepository(TenderConditionItem::class)->insertTenderPurchaseMemoConditionItem($entity,$data);
            return $this->redirectToRoute('procure_tender_management_memo_quotation_bank');
        }

        $comparativeItems = $this->getDoctrine()->getRepository(TenderComparative::class)->comparativeItems($entity->getParent()->getTenderComparative());
        $comparativeItemAttribites = $this->getDoctrine()->getRepository(TenderComparative::class)->comparativeItemAttribites($entity->getParent()->getTenderComparative());
        return $this->render('@TerminalbdProcurement/bank/management-memo/quotation.html.twig', [
            'entity' => $entity,
            'tender' => $entity->getParent()->getTenderComparative()->getTender(),
            'tenderComparative' => $entity->getParent()->getTenderComparative(),
            'comparativeItems'          => $comparativeItems,
            'comparativeItemAttribites' => $comparativeItemAttribites,
            'form' => $form->createView(),
        ]);
    }


}
