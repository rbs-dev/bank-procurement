<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Controller\Bank;

use App\Entity\Application\GenericMaster;
use App\Entity\Application\Procurement;
use App\Entity\Core\Setting;
use App\Entity\Core\UserReporting;
use App\Repository\Application\GenericMasterRepository;
use App\Repository\Application\InventoryRepository;
use App\Repository\Application\ProcurementRepository;
use App\Repository\Domain\ModuleProcessItemRepository;
use App\Service\FileUploader;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\GenericBundle\Repository\ParticularRepository;
use Terminalbd\InventoryBundle\Repository\StockRepository;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Form\Bank\RequisitionFormType;
use Terminalbd\ProcurementBundle\Form\ProcurementBankProcessFormType;
use Terminalbd\ProcurementBundle\Form\ProcurementProcessFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionItemFormType;
use Terminalbd\ProcurementBundle\Repository\ProcurementProcessRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionRepository;



/**
 * @Route("/procurement/bssf/purchase-requisition")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class PurchaseRequisitionController extends AbstractController
{
    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="procure_requisition_bank_purchase_edit")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function edit(Request $request, $id,TranslatorInterface $translator, ProcurementRepository $procurementRepository, RequisitionRepository $repository, StockRepository $stockRepository, InventoryRepository $inventoryRepository , ProcurementProcessRepository $processRepository, ModuleProcessItemRepository $processItemRepository,GenericMasterRepository $genericMasterRepository , FileUploader $fileUploader): Response
    {
        $terminal = $this->getUser()->getTerminal();

        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        /* @var $entity Requisition */

        $entity = $repository->findOneBy(array('config' => $config,'id' => $id));
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $count = $this->getDoctrine()->getRepository(RequisitionItem::class)->count(array('requisition' => $id));
        $form = $this->createForm(RequisitionFormType::class , $entity,array('config'=>$config,'particularRepo' => $particularRepository));
        $form->handleRequest($request);
        $data = $request->request->all();
        if ($form->isSubmitted() && $form->isValid() and $count > 0) {
            $this->getDoctrine()->getManager()->flush();
            $files = !empty($_FILES['files']) ? $_FILES['files']:'';
            if($files){
                $this->getDoctrine()->getRepository(ProcurementProcess::class)->insertAttachmentFile($entity,$data,$files);
            }
            $processRepository->approvalRoleAssign($entity);
            if($entity->getProcess() == "In-progress"){
                $repository->requisitionItemHistory($entity);
            }
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('procure_requisition_bank');
        }
        return $this->render('@TerminalbdProcurement/bank/requisition/purchase.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }


}
