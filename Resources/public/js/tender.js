function jqueryTemporaryLoad() {
    $('.amount').change(function(){
        this.value = parseFloat(this.value).toFixed(2);
    });

    function financial(val) {
        return Number.parseFloat(val).toFixed(2);
    }

    $(".number , .amount, .numeric").inputFilter(function(value) {
        return /^-?\d*[.,]?\d*$/.test(value); });

    $('#item').select2('open');
    function jsonResult(response){
        obj = JSON.parse(response);
        $('#invoiceItem').html(obj['invoiceItem']);
        $('#subTotal').html(obj['subTotal']);
        if(obj['low'] > 0 ){location.reload()}
    }

    $('.toggleIcon').toggle(function() {
        $(this).text('[-]');
        var id = $(this).attr('id');
        $("#show-"+id).slideToggle(100);
    }, function() {
        var id = $(this).attr('id');
        $("#show-"+id).slideToggle(100);
        $(this).text('[+]');
    });

    var count = 0;
    var countId = 1;
    var countIdx = 1;
    $('.addFile').click(function(e){
        var $el = $(this);
        var $cloneBlock = $('#clone-attachment');
        var $clone = $cloneBlock.find('.clone:eq(0)').clone();
        $clone.find('.custom-file-input').attr('id', "file-"+(countId++));
        $clone.find('.custom-file-label').attr('for', "file-"+(countIdx++));
        $clone.find(':text,textarea,file' ).val("");
        $clone.find('.custom-file-label' ).html("Choose file");
        $clone.attr('id', "added"+(++count));
        $clone.find('.row-remove').removeClass('hidden');
    //    $clone.find('.custom-file-label').html('');
        $cloneBlock.append($clone);
    });

    $('#clone-attachment').on('click', '.row-remove', function(){
        $(this).closest('.clone').remove();
    });

    $('#sortable').sortable({
        update: function(event, ui) {
          $('#sortable').children().each(function(i) {
            var id = $(this).attr('data-post-id')
                ,order = $(this).index() + 1;
            $.get(Routing.generate('procure_process_ordering'),{'columnId':id,'sorting':order})});

        }
    });

    $(document).on("change", ".input", function() {

        var formData = new FormData($('form#requisitionForm')[0]); // Create an arbitrary FormData instance
        var url = $('form#requisitionForm').attr('data-action'); // Create an arbitrary FormData instance
        $.ajax({
            url:url ,
            type: 'POST',
            processData: false,
            contentType: false,
            data:formData
        });

    });

    $(document).on("click", ".note-editable", function() {
        var formData = new FormData($('form#requisitionForm')[0]); // Create an arbitrary FormData instance
        var url = $('form#requisitionForm').attr('data-action'); // Create an arbitrary FormData instance
        $.ajax({
            url:url ,
            type: 'POST',
            processData: false,
            contentType: false,
            data:formData
        });

    });


    $("#requisitionForm").validate({

        invalidHandler: function(form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                var firstInvalidElement = $(validator.errorList[0].element);
                $('html,body').scrollTop(firstInvalidElement.offset().top);
            }
        }
    });

    $(document).on('change', '.selectVendor', function() {
        var memo = $('#memo').val();
        var url = $('#memo').attr('data-action');
        var id = $(this).val();
        $.get(url,{'memo':memo,'id':id}, function( response ) {
            $('#vendor-list').html(response);
        });
    });

    $(document).on('change', '#workOrder', function() {
        var id = $(this).val();
        var url = $(this).attr('data-action');
        $.get(url,{'id':id}, function( response ) {
            location.reload();
        });
    });

    $('.select2Workorder').select2({
        ajax: {
            url: Routing.generate('procure_tender_bank_select_workorder'),
            dataType: 'json',
            delay: 250,
            type: 'GET',
            data: function (params) {

                return {
                    q: params.term, // search term
                };
            },
            processResults: function (data) {
                var arr = []
                $.each(data, function (index, value) {
                    arr.push({
                        id: index,
                        text: value
                    })
                })
                return {
                    results: arr
                };
            },
            cache: false
        },
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 1,
        allowClear: true,
        placeholder: 'Search by workorder'
    });


    $(document).on('change', '.comment', function() {
        var id = $(this).attr('data-id');
        var comment = $('#comment-'+id).val();
        var url = $(this).attr('data-action');
        $.get(url,{'comment':comment}, function( response ) {
            console.log('success');
        });
    });

    $(document).on('click', '#meetingNotes', function() {
        var comment = $('#notes').val();
        var url = $(this).attr('data-action');
        $.get(url,{'comment':comment}, function( response ) {
            console.log('success');
        });
    });

    $(document).on('click', '.meeting-comment', function() {
        var id = $(this).attr('id');
        var process = $('#process-'+id).val();
        var comment = $('#comment-'+id).val();
        var meeting = $('#meeting-'+id).val();
        var url = $(this).attr('data-action');
        $.get(url,{'comment':comment,'process':process,'meeting':meeting}, function( response ) {
            console.log('success');
        });
    });

    $(".quotationPrice").keyup(function(){

        var sum = 0;
        price = ($(this).val() === "" || $(this).val() === "NaN") ? 0 : $(this).val();
        qty = $(this).attr('data-id');
        id = $(this).attr('id');
        quotationSubTotal = parseFloat(price * qty);
        $('#quotationSubTotal-'+id).html(financial(quotationSubTotal))
        $(".quotationSub").each(function(){
            amount = ($(this).html() === "" || $(this).html() === "NaN") ? 0 : $(this).html();
            sum += +parseFloat(amount);
        });
        if (sum > 0) {
            $('#quotationTotal').html(financial(sum));
        }
    });

    $(".quotationQuantity, .quotationPrice").keyup(function(){

        var sum = 0;
        id = $(this).attr('data-id');
        qty = parseFloat($("#issue-"+id).val() === "" || $("#issue-"+id).val() === "NaN") ? 0 : $("#issue-"+id).val();
        price = parseFloat($("#price-"+id).val() === "" || $("#price-"+id).val() === "NaN") ? 0 : $("#price-"+id).val();
        quotationSubTotal = parseFloat(price * qty);
        $('#quotationSubTotal-'+id).html(financial(quotationSubTotal))
        $(".quotationSub").each(function(){
            amount = ($(this).html() === "" || $(this).html() === "NaN") ? 0 : $(this).html();
            sum += +parseFloat(amount);
        });
        if (sum > 0) {
            $('#quotationTotal').html(financial(sum));
        }
    });

    $(".quotationItem").keyup(function(){

        var sum = 0;
        id = $(this).attr('data-id');
        qty = ($('#quantity-'+id).val() === "" || $(this).val() === "NaN") ? 0 : $("#quantity-"+id).val();
        price = ($('#price-'+id).val() === "" || $(this).val() === "NaN") ? 0 : $('#price-'+id).val();
        quotationSubTotal = (price * qty);
        $('#quotationSubTotal-'+id).html(financial(quotationSubTotal));
        $(".quotationSub").each(function(){
            amount = ($(this).html() === "" || $(this).html() === "NaN") ? 0 : $(this).html();
            sum += +parseFloat(amount);
        });
        if (sum > 0) {
            $('#quotationTotal').html(financial(sum));
        }
    });



    $("#submitBtn").click(function(){

        if($("#requisitionForm").valid() === true) {
            $('#approve-content').confirmModal({
                topOffset: 0,
                top: '25%',
                onOkBut: function(event, el) {
                    $("#requisitionForm").submit()
                }
            });
        }

    });

    $(document).on("change", ".input", function() {

        var formData = new FormData($('form#requisitionForm')[0]); // Create an arbitrary FormData instance
        var url = $('form#requisitionForm').attr('data-action'); // Create an arbitrary FormData instance
        $.ajax({
            url:url ,
            type: 'POST',
            processData: false,
            contentType: false,
            data:formData
        });

    });

    $(document).on('change', '.category2Item', function() {
        var id = $(this).val();
        $.ajax({
            url: Routing.generate('procure_category_item'),
            type: 'POST',
            data:'id='+ id,
            success: function(response) {
                $(".itemAttribute").html(response).select2();
            }
        })
    });

    $(document).on('change', '.itemName , .itemAttribute', function() {

        var id = $(this).val();
        $.ajax({
            url: Routing.generate('procure_category_attribute'),
            type: 'POST',
            data:'id='+ id,
            success: function(response) {
                obj = JSON.parse(response);
                $(".brand").html(obj['brand']);
                $(".size").html(obj['size']);
                $(".color").html(obj['color']);
            }
        })

    });

    /*$(document).on('change', ".shareParcent", function() {
        var sum = 0;
        $(".shareParcent").each(function(){
            amount = ($(this).val() === "" || $(this).val() === "NaN") ? 0 : $(this).val();
            sum += +parseFloat(amount);
        });
        if (sum > 100) {
            alert("Please confirm company share ratio");
        }else{
            var amount = parseFloat($(this).val());
            var url = $(this).attr('data-action');
            $.get(url,{'amount':amount});

        }
    });*/

    /*
    $('form#approveForm').submit(function(e){

        e.preventDefault();
        $.ajax({
            url         : $('form#approveForm').attr( 'action' ),
            type        : $('form#approveForm').attr( 'method' ),
            data        : new FormData($('form#approveForm')[0]),
            processData : false,
            contentType : false,
            success: function(response){
                location.reload();
            }
        });
    });

    */

    $('.courierDelivery').click(function(e){
        e.preventDefault();
        var id = $(this).attr('data-id');
        var url = $(this).attr('data-action');
        var courier = $('#courier-'+id).val();
        var cnNo = $('#cnNo-'+id).val();
        var comment = $('#comment-'+id).val();
        var deliveryMethod = $('#deliveryMethod-'+id).val();
        $.ajax({
            url: url,
            type: 'POST',
            data:'deliveryMethod='+ deliveryMethod+'&courier='+ courier+'&cnNo='+cnNo+'&comment='+comment
        })
    });

    $('.handDelivery').click(function(e){
        e.preventDefault();
        var id = $(this).attr('data-id');
        var url = $(this).attr('data-action');
        var receiverName = $('#receiverName-'+id).val();
        var receiverDesignation = $('#receiverDesignation-'+id).val();
        var receiverAddress = $('#receiverAddress-'+id).val();
        var deliveryMethod = $('#deliveryMethod-'+id).val();
        $.ajax({
            url: url,
            type: 'POST',
            data:'deliveryMethod='+ deliveryMethod+'&receiverName='+ receiverName+'&receiverDesignation='+receiverDesignation+'&receiverAddress='+receiverAddress
        })
    });

    $('form#requisitionItemForm').submit(function(e){

        e.preventDefault();
        var item = $('#item').val();
        var quantity = $('#quantity').val();

        if(item === "NaN" || item ==="" ){
            $('#item').select2('open');
            return false;
        }
        if(quantity === '' || quantity === "NaN"){
            $('#quantity').focus();
            return false;
        }
        $.ajax({
            url         : $('form#itemForm').attr( 'action' ),
            type        : $('form#itemForm').attr( 'method' ),
            data        : new FormData($('form#itemForm')[0]),
            processData : false,
            contentType : false,
            success: function(response){
                location.reload();
            }
        });
    });

    $('form#itemForm').submit(function(e){

        e.preventDefault();
        var item = $('#item').val();
        var quantity = $('#quantity').val();

        if(item === "NaN" || item ==="" ){
            $('#item').select2('open');
            return false;
        }
        if(quantity === '' || quantity === "NaN"){
            $('#quantity').focus();
            return false;
        }
        $.ajax({
            url         : $('form#itemForm').attr( 'action' ),
            type        : $('form#itemForm').attr( 'method' ),
            data        : new FormData($('form#itemForm')[0]),
            processData : false,
            contentType : false,
            success: function(response){
                setTimeout(jsonResult(response),100);
             //   afterSelect2Submit();
                $(".reset-select").select2("val", "");
                $('form#itemForm')[0].reset();

            }
        });
    });

    $('form#assignForm').submit(function(e){

        e.preventDefault();
        $.ajax({
            url         : $('form#assignForm').attr( 'action' ),
            type        : $('form#assignForm').attr( 'method' ),
            data        : new FormData($('form#assignForm')[0]),
            processData : false,
            contentType : false,
            success: function(response){
                $('form#assignForm')[0].reset();
                $('.approvalUser').html(response);
                $('.status').bootstrapToggle();
            }
        });
    });

    $('form#additionalItemForm').submit(function(e){

        e.preventDefault();
        var item = $('#job_requisition_additional_item_form_name').val();
        var quantity = $('#job_requisition_additional_item_form_quantity').val();

        if(item === "NaN" || item ==="" ){
            $('#name').focus();
            return false;
        }
        if(quantity === '' || quantity === "NaN"){
            $('#quantity').focus();
            return false;
        }

        $.ajax({
            url         : $('form#additionalItemForm').attr( 'action' ),
            type        : $('form#additionalItemForm').attr( 'method' ),
            data        : new FormData($('form#additionalItemForm')[0]),
            processData : false,
            contentType : false,
            success: function(response){
                obj = JSON.parse(response);
                $('#invoiceAdditionalItem').html(obj['invoiceItem']);
                $('form#additionalItemForm')[0].reset();
            }
        });
    });


    $('form#addApproval').submit(function(e){

        e.preventDefault();
        var item = $('#assignUser').val();
        if(item === "NaN" || item ==="" ){
            $('#assignUser').select2('open');
            return false;
        }
        $.ajax({
            url         : $('form#addApproval').attr( 'action' ),
            type        : $('form#addApproval').attr( 'method' ),
            data        : new FormData($('form#addApproval')[0]),
            processData : false,
            contentType : false,
            success: function(response){
                $('.status').bootstrapToggle();
                $('form#addApproval')[0].reset();
                $(".user-select").select2('open').prop('selectedIndex',0);
                $('.approvalUser').html(response);
            }
        });
    });



    $(document).on('change', '.inline-quantity', function() {

        var id = $(this).attr('data-id');
        var url = $(this).attr('data-action');
        var quantity = parseFloat($('#quantity-'+id).val());
        var price = parseFloat($('#price-'+id).val());
        var returnQuantity = parseFloat($('#returnQuantity-'+id).val());
        var subTotal = (quantity*price);
        $('#subTotal-'+id).html(subTotal);
        $.ajax({
            url: url,
            type: 'POST',
            data:'quantity='+ quantity+'&returnQuantity='+ returnQuantity+'&price='+ price,
            success: function(response) {
                obj = JSON.parse(response);
                var total = parseFloat(obj['total']).toFixed(2)
                var subTotal = parseFloat(obj['subTotal']).toFixed(2)
                var commission = parseFloat(obj['commission']).toFixed(2)
                $('#commission').html(commission);
                $('.subTotal').html(subTotal);
                $('.total').html(total);
            }
        })
    });

    $(document).on('change', '.receive-quantity', function() {

        var id = $(this).attr('data-id');
        var url = $(this).attr('data-action');
        var quantity = parseFloat($('#quantity-'+id).val());
        var maxQuantity = parseFloat($('#quantity-'+id).attr('max'));
        var price = parseFloat($('#price-'+id).val());
        var returnQuantity = parseFloat($('#returnQuantity-'+id).val());
        if (maxQuantity < quantity) {
            alert("Invalid quantity, less then equal to "+maxQuantity);
            $('#quantity-'+id).val(maxQuantity).focus();
            var subTotal = (price * maxQuantity);
            var sub = parseFloat(subTotal).toFixed(2)
            $("#woSubTotal-"+id).html(sub);
            return false;
        }

        var subTotal = (quantity*price);
        $('#subTotal-'+id).html(subTotal);
        $.ajax({
            url: url,
            type: 'POST',
            data:'quantity='+ quantity+'&returnQuantity='+ returnQuantity+'&price='+ price,
            success: function(response) {
                obj = JSON.parse(response);
                var total = parseFloat(obj['total']).toFixed(2)
                var subTotal = parseFloat(obj['subTotal']).toFixed(2)
                var commission = parseFloat(obj['commission']).toFixed(2)
                $('#commission').html(commission);
                $('.subTotal').html(subTotal);
                $('.total').html(total);
            }
        })
    });

    $(document).on('change', '.remainItem', function() {
        var id = $(this).attr('data-id');
        var quantity = parseFloat($('#quantity-'+id).val());
        var issue = parseFloat($('#issue-'+id).val());
        var remain = (quantity - issue);
        $("#remaining-"+id).html(remain);
    });

    $(document).on('change', '.adjustAmount', function() {
        var id = $(this).attr('data-id');
        var quantity = parseFloat($('#currentAmount-'+id).val());
        var issue = parseFloat($('#adjustAmount-'+id).val());
        var remain = (quantity - issue);
        $("#remaining-"+id).html(remain);
        setTimeout(totalAmount, 1000);
    });

    /*$(document).on('form#ajaxForm').submit(function(e){
        e.preventDefault();
        $.ajax({
            url         : $('form#ajaxForm').attr( 'action' ),
            type        : $('form#ajaxForm').attr( 'method' ),
            data        : new FormData($('form#ajaxForm')[0]),
            processData : false,
            contentType : false,
            success: function(response){
                alert(response);
            }
        });
    });*/

    function totalAmount() {
        var sum = 0;
        $(".adjustAmount").each(function(){
            sum += +parseFloat($(this).val());
        });
        $("#total").html(sum);
    }


    $(document).on('click', '.ajax-reset', function() {

        var url = $(this).attr('data-action');
        $.get(url, function(data){
            location.reload();
        });
    });

    $(document).on('change', '.updatePriceQuantity', function() {

        var id = $(this).attr('data-id');
        var url = $(this).attr('data-action');
        var unitPrice = parseFloat($('#unitPrice-'+id).val());
        var quantity = parseFloat($('#quantity-'+id).val());
        var subTotal = (unitPrice * quantity);
        $("#subTotal-"+id).html(subTotal);
        $.ajax({
            url: url,
            type: 'POST',
            data:'quantity='+ quantity+"&unitPrice="+unitPrice
        })
    });

     $(document).on('change', '.updateCSItem', function() {

        var id = $(this).attr('data-id');
        var vendorId = $(this).attr('data-vendor-id');
        var url = $(this).attr('data-action');
        var unitPrice = parseFloat($('#unitPrice-'+id).val());
        var quantity = parseFloat($('#quantity-'+id).val());
        var subTotal = (unitPrice * quantity);
        $("#subTotal-"+id).html(financial(subTotal));
        var sum = 0;
        $(".vendorTotal-"+vendorId).each(function(){
             amount = ($(this).html() === "" || $(this).html() === "NaN") ? 0 : $(this).html();
             sum += +parseFloat(amount);
        });
        $("#quotationSubTotal-"+vendorId).html(financial(sum));
        $.ajax({
            url: url,
            type: 'POST',
            data:'quantity='+ quantity+"&unitPrice="+unitPrice
        })
    });

    $(document).on('change', '.updateReviseCSItem', function() {

        var id = $(this).attr('data-id');
        var url = $(this).attr('data-action');
        var unitPrice = parseFloat($('#unitPrice-'+id).val());
        var quantity = parseFloat($('#quantity-'+id).val());
        var subTotal = (unitPrice * quantity);
        $("#revisedSubTotal-"+id).html(subTotal);
        $.ajax({
            url: url,
            type: 'POST',
            data:'unitPrice='+ unitPrice
        })
    });

     $(document).on('change', '.updateCSItemAttribute', function() {

        var id = $(this).attr('data-id');
        var url = $(this).attr('data-action');
        var description = $('#attribute-'+id).val();
        $.ajax({
            url: url,
            type: 'POST',
            data:'description='+ description
        })
    });

    $(document).on('change', '.quantity', '.price', function() {

        var url = $(this).attr('data-action');
        var id = $(this).attr('data-id');
        var quantity = parseFloat($('#quantity-'+id).val());
        var price = parseFloat($('#price-'+id).val());
        var subTotal  = (quantity * price);
        $("#subTotal-"+id).html(subTotal);
        $.ajax({
            url: url,
            type: 'POST',
            data:'quantity='+ quantity,
            success: function(response) {
                if(response !== "success"){
                    setTimeout(jsonResult(response),100);
                }
            }
        })
    });

    $(document).on('change', '.processQuantity', function() {

        var url = $(this).attr('data-action');
        var id = $(this).attr('data-id');
        var quantity = parseFloat($('#quantity-'+id).val());
        var price = parseFloat($('#price-'+id).val());
        var subTotal  = (quantity * price);
        $("#subTotal-"+id).html(financial(subTotal));
        $.ajax({
            url: url,
            type: 'POST',
            data:'quantity='+ quantity+'&price='+ price,
            success: function(response) {
                var sum = 0;
                $(".subTotal").each(function(){
                    sum += +parseFloat($(this).text());
                });
                $("#approveTotal").html(financial(sum));
            }
        })
    });

    $(document).on('change', '.approveQuantity', function() {

        var id = $(this).attr('data-id');
        var url = $(this).attr('data-action');
        var quantity = parseFloat($('#approveQuantity-'+id).val());
        $.ajax({
            url: url,
            type: 'POST',
            data:'quantity='+ quantity
        })
    });

    $(document).on('keypress keyup blur', '.storeRequisitionQuantity', function() {
        var id = $(this).attr('data-id');
        var quantity = parseFloat($('#quantity-'+id).val());
        var requisitionItemPrice = parseFloat($('#requisitionItemPrice-'+id).val());
        var remainingQuantity = parseFloat($('#remainingQuantity-'+id).val());
        var remin  = (remainingQuantity - quantity);
        var subTotal = quantity*requisitionItemPrice;
        $("#remainQnt-"+id).html(remin);
        $("#subTotal-"+id).html(subTotal);

        var sum = 0;
        $(".subTotal").each(function(){
            sum += +parseFloat($(this).text());
        });
        $(".grandTotal").html(sum);
    });


    $(document).on('click',".itemRemove", function (event) {
        var id = $(this).attr('data-id');
        var url = $(this).attr('data-action');
        $('#confirm-content').confirmModal({
            topOffset: 0,
            top: '25%',
            onOkBut: function(el) {
                $.get(url, function( response ) {
                    $(event.target).closest('tr').hide();
                    setTimeout(jsonResult(response),100);
                });
            }
        });
    });


    $(document).on('click',".itemApprovalRemove", function (event) {
        var id = $(this).attr('data-id');
        var url = $(this).attr('data-action');
        $('#confirm-content').confirmModal({
            topOffset: 0,
            top: '25%',
            onOkBut: function(el) {
                $.get(url, function( response ) {
                    $(event.target).closest('tr').hide();
                });
            }
        });
    });

    $(document).on('change', '.updateWorkorderItem', function() {
        var id = $(this).attr('data-id');
        var url = $(this).attr('data-action');
        var quantity = parseFloat($('#quantity-'+id).val());
        var actualQuantity = parseFloat($('#actualQuantity-'+id).val());
        var price = parseFloat($('#price-'+id).val());
        var wearhouse = parseFloat($('#wearhouse-'+id).val());
        var discount = 0;
        var vat = 0;
        var shippingCharge = 0;
        var commission = 0;
        if (actualQuantity < quantity) {
            alert("Invalid quantity, less then equal to "+actualQuantity);
            $('#quantity-'+id).val(actualQuantity).focus();
            var subTotal = (price * actualQuantity);
            var sub = parseFloat(subTotal).toFixed(2)
            $("#woSubTotal-"+id).html(sub);
            return false;
        }
        var subTotal = (price * quantity);
        var sub = parseFloat(subTotal).toFixed(2)
        $("#woSubTotal-"+id).html(sub);
        $.ajax({
            url: url,
            type: 'POST',
            data: 'quantity='+ quantity+"&price="+price+"&wearhouse="+wearhouse+"&discount="+discount+"&vat="+vat+"&shippingCharge="+shippingCharge+"&commission="+commission,
            success: function(response) {
                $('#invoice').html(response);
            }
        })
    });

    $(document).on('change', '.updatePo2WorkorderItem', function() {

        var id = $(this).attr('data-id');
        var url = $(this).attr('data-action');
        var quantity = parseFloat($('#quantity-'+id).val());
        var actualQuantity = parseFloat($('#actualQuantity-'+id).val());
        var price = parseFloat($('#price-'+id).val());
        var wearhouse = 0;
        var discount = parseFloat($('#discount-'+id).val());
        var vat = parseFloat($('#vat-'+id).val());
        var description = $('#description-'+id).val();
        var shippingCharge = parseFloat($('#shippingCharge-'+id).val());
        var commission = parseFloat($('#vendorCommission-'+id).val());
        if (actualQuantity < quantity) {
            alert("Invalid quantity, less then equal to "+actualQuantity);
            $('#quantity-'+id).val(actualQuantity).focus();
            var subTotal = (price * actualQuantity);
            var sub = parseFloat(subTotal).toFixed(2)
            $("#woSubTotal-"+id).html(sub);
            return false;
        }
        var subTotal = (price * quantity);
        var sub = parseFloat(subTotal).toFixed(2)
        $("#woSubTotal-"+id).html(sub);
        $.ajax({
            url: url,
            type: 'POST',
            data: 'quantity='+ quantity+"&price="+price+"&wearhouse="+wearhouse+"&discount="+discount+"&vat="+vat+"&shippingCharge="+shippingCharge+"&commission="+commission+"&description="+description,
            success: function(response) {
                $('#workorder-po').html(response);
            }
        })
    });

    $(document).on('change',".ajaxUpdate", function (event) {
        $.ajax({
            url         : $('form#requisitionForm').attr( 'data-action' ),
            type        : $('form#requisitionForm').attr( 'method' ),
            data        : new FormData($('form#requisitionForm')[0]),
            processData : false,
            contentType : false,
            success: function(response){
                obj = JSON.parse(response);
                var subTotal = parseFloat(obj['subTotal']).toFixed(2)
                var total = parseFloat(obj['total']).toFixed(2)
                $('.subTotal').html(subTotal);
                $('.total').html(total);
            }
        });

    });

   /*

   $(".select2StockItem").select2({
        ajax: {
            url: Routing.generate('inv_stock_item_autocomplete'),
            data: function (params, page) {
                return {
                    q: params,
                    page_limit: 100
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            }
        },
        placeholder: 'Search for a add stock item',
        minimumInputLength: 1,
        allowClear: true
    });

    function afterSelect2Submit(){

        $('.itemAttribute').prepend('<option selected></option>').select2({
            ajax: {
                url: Routing.generate('inv_stock_item_autocomplete'),
                data: function (params, page) {
                    return {
                        q: params,
                        page_limit: 100
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                }
            },
            placeholder: 'Search for a add stock item',
            minimumInputLength: 1,
            allowClear: true
        });

    }
*/
    $(document).on('change', '.memoItem', function() {
        var process = $('#process').val();
        var id = $('#memo').val();
        $.ajax({
            url: Routing.generate('procure_tender_committee_bank_show_memo'),
            type: 'POST',
            data:'id='+id+"&process="+process,
            success: function(response) {
                obj = JSON.parse(response);
                $("#memoDate").html(obj['created']);
                $("#subject").html(obj['subject']);
            }
        })
    });

    $('form#memoForm').submit(function(e){
        e.preventDefault();
        $.ajax({
            url         : $('form#memoForm').attr( 'action' ),
            type        : $('form#memoForm').attr( 'method' ),
            data        : new FormData($('form#memoForm')[0]),
            processData : false,
            contentType : false,
            success: function(response){
                $('#invoiceItem').html(response);
            }
        })
    });

    $(document).on('click', '#meeting-reload', function(e) {
                location.reload();
    });

    $(document).on('click',"#addMember", function (event) {

        var url = $(this).attr('data-action');
        var member = $('#member').val();
        if (member === "") {
            alert("Please add purchase committee member");
            return false;
        }
        var position = $('#position').val();
        var ordering = $('#ordering').val();
        $.get(url,{'member':member,'position':position,'ordering':ordering}, function( response ) {
            $("#invoiceItem").html(response);
        });
    });

}






