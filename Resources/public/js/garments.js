function jqueryTemporaryLoad() {
    $('.amount').change(function(){
        this.value = parseFloat(this.value).toFixed(2);
    });

    function financial(val) {
        return Number.parseFloat(val).toFixed(2);
    }

    $(".number , .amount, .numeric").inputFilter(function(value) {
        return /^-?\d*[.,]?\d*$/.test(value); });

    $('#item').select2('open');
    function jsonResult(response){
        obj = JSON.parse(response);
        $('#invoiceItem').html(obj['invoiceItem']);
        $('#subTotal').html(obj['subTotal']);
       // if(obj['low'] > 0 ){location.reload()}
    }


    $('.toggleIcon').toggle(function() {
        $(this).text('[-]');
        var id = $(this).attr('id');
        $("#show-"+id).slideToggle(100);
    }, function() {
        var id = $(this).attr('id');
        $("#show-"+id).slideToggle(100);
        $(this).text('[+]');
    });

    var count = 0;
    var countId = 1;
    var countIdx = 1;
    $('.addFile').click(function(e){
        var $el = $(this);
        var $cloneBlock = $('#clone-attachment');
        var $clone = $cloneBlock.find('.clone:eq(0)').clone();
        $clone.find('.custom-file-input').attr('id', "file-"+(countId++));
        $clone.find('.custom-file-label').attr('for', "file-"+(countIdx++));
        $clone.find(':text,textarea,file' ).val("");
        $clone.find('.custom-file-label' ).html("Choose file");
        $clone.attr('id', "added"+(++count));
        $clone.find('.row-remove').removeClass('hidden');
        //$clone.find('.custom-file-label').html('');
        $cloneBlock.append($clone);
    });

    $('#clone-attachment').on('click', '.row-remove', function(){
        $(this).closest('.clone').remove();
    });

    $('#sortable').sortable({
        update: function(event, ui) {
          $('#sortable').children().each(function(i) {
            var id = $(this).attr('data-post-id')
                ,order = $(this).index() + 1;
            $.get(Routing.generate('procure_process_ordering'),{'columnId':id,'sorting':order})});

        }
    });

    $("#requisitionForm").validate({

        invalidHandler: function(form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                var firstInvalidElement = $(validator.errorList[0].element);
                $('html,body').scrollTop(firstInvalidElement.offset().top);
            }
        }
    });

    $("#submitRequisitionBtn").click(function(){
        var sum = 0;
        if($("#requisitionForm").valid() === true) {
            $('#approve-content').confirmModal({
                topOffset: 0,
                top: '25%',
                onOkBut: function(event, el) {
                    $("#requisitionForm").submit()
                }
            });
        }
    });

    $("#submitBtn").click(function(){

        if($("#requisitionForm").valid() === true) {
            $('#approve-content').confirmModal({
                topOffset: 0,
                top: '25%',
                onOkBut: function (event, el) {
                    $("#requisitionForm").submit()
                }
            });
        }
    });

    $(document).on("change", ".input", function() {

        var formData = new FormData($('form#requisitionForm')[0]); // Create an arbitrary FormData instance
        var url = $('form#requisitionForm').attr('data-action'); // Create an arbitrary FormData instance
        $.ajax({
            url:url ,
            type: 'POST',
            processData: false,
            contentType: false,
            data:formData
        });

    });

    $('.select2Item').select2({
        ajax: {
            url: Routing.generate('search_select2_stock_item'),
            dataType: 'json',
            delay: 250,
            type: 'GET',
            data: function (params) {

                return {
                    q: params.term, // search term
                };
            },
            processResults: function (data) {
                var arr = []
                $.each(data, function (index, value) {
                    arr.push({
                        id: index,
                        text: value
                    })
                })
                return {
                    results: arr
                };
            },
            cache: false
        },
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 1,
        allowClear: true,
        placeholder: 'Search for a add stock item'
    });

    $('.selectCapexItem').select2({
        ajax: {
            url: Routing.generate('search_select2_capex_stock_item'),
            dataType: 'json',
            delay: 250,
            type: 'GET',
            data: function (params) {

                return {
                    q: params.term, // search term
                };
            },
            processResults: function (data) {
                var arr = []
                $.each(data, function (index, value) {
                    arr.push({
                        id: index,
                        text: value
                    })
                })
                return {
                    results: arr
                };
            },
            cache: false
        },
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 1,
        allowClear: true,
        placeholder: 'Search for a add stock item'
    });

    $('.selectOpexItem').select2({
        ajax: {
            url: Routing.generate('search_select2_opex_stock_item'),
            dataType: 'json',
            delay: 250,
            type: 'GET',
            data: function (params) {

                return {
                    q: params.term, // search term
                };
            },
            processResults: function (data) {
                var arr = []
                $.each(data, function (index, value) {
                    arr.push({
                        id: index,
                        text: value
                    })
                })
                return {
                    results: arr
                };
            },
            cache: false
        },
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 1,
        allowClear: true,
        placeholder: 'Search for a add stock item'
    });

    $(document).on('change', '.category2RequisitionItem', function() {
        var id = $(this).val();
        var requisitionMode = $('#requisitionMode').val();
        $.ajax({
            url: Routing.generate('procure_category_requisition_item'),
            type: 'POST',
            data:'id='+id+"&itemMode="+requisitionMode,
            success: function(response) {
                $(".itemAttribute").html(response).select2();
            }
        })
    });

    $(document).on('change', '.category2Item', function() {
        var id = $(this).val();
        $.ajax({
            url: Routing.generate('procure_category_item'),
            type: 'POST',
            data:'id='+id,
            success: function(response) {
                $(".itemAttribute").html(response).select2();
            }
        })
    });

    $(document).on('change', '.itemName , .itemAttribute', function() {

        var id = $(this).val();
        $.ajax({
            url: Routing.generate('procure_category_attribute'),
            type: 'POST',
            data:'id='+ id,
            success: function(response) {
                obj = JSON.parse(response);
                $(".brand").html(obj['brand']).select2();
                $(".size").html(obj['size']).select2();
                $(".color").html(obj['color']).select2();
            }
        })

    });

    /*$(document).on('change', ".shareParcent", function() {
        var sum = 0;
        $(".shareParcent").each(function(){
            amount = ($(this).val() === "" || $(this).val() === "NaN") ? 0 : $(this).val();
            sum += +parseFloat(amount);
        });
        if (sum > 100) {
            alert("Please confirm company share ratio");
        }else{
            var amount = parseFloat($(this).val());
            var url = $(this).attr('data-action');
            $.get(url,{'amount':amount});

        }
    });*/

    /*$('form#approveForm').submit(function(e){

        e.preventDefault();
        $.ajax({
            url         : $('form#approveForm').attr( 'action' ),
            type        : $('form#approveForm').attr( 'method' ),
            data        : new FormData($('form#approveForm')[0]),
            processData : false,
            contentType : false,
            success: function(response){
                location.reload();
            }
        });
    });*/

    $('.courierDelivery').click(function(e){
        e.preventDefault();
        var id = $(this).attr('data-id');
        var url = $(this).attr('data-action');
        var courier = $('#courier-'+id).val();
        var cnNo = $('#cnNo-'+id).val();
        var comment = $('#comment-'+id).val();
        var deliveryMethod = $('#deliveryMethod-'+id).val();
        $.ajax({
            url: url,
            type: 'POST',
            data:'deliveryMethod='+ deliveryMethod+'&courier='+ courier+'&cnNo='+cnNo+'&comment='+comment
        })
    });

    $('.handDelivery').click(function(e){
        e.preventDefault();
        var id = $(this).attr('data-id');
        var url = $(this).attr('data-action');
        var receiverName = $('#receiverName-'+id).val();
        var receiverDesignation = $('#receiverDesignation-'+id).val();
        var receiverAddress = $('#receiverAddress-'+id).val();
        var deliveryMethod = $('#deliveryMethod-'+id).val();
        $.ajax({
            url: url,
            type: 'POST',
            data:'deliveryMethod='+ deliveryMethod+'&receiverName='+ receiverName+'&receiverDesignation='+receiverDesignation+'&receiverAddress='+receiverAddress
        })
    });

    $('form#requisitionItemForm').submit(function(e){

        e.preventDefault();
        var item = $('#item').val();
        var quantity = $('#quantity').val();

        if(item === "NaN" || item ==="" ){
            $('#item').select2('open');
            return false;
        }
        if(quantity === '' || quantity === "NaN"){
            $('#quantity').focus();
            return false;
        }
        $.ajax({
            url         : $('form#itemForm').attr( 'action' ),
            type        : $('form#itemForm').attr( 'method' ),
            data        : new FormData($('form#itemForm')[0]),
            processData : false,
            contentType : false,
            success: function(response){
                location.reload();
            }
        });
    });

    $('form#itemForm').submit(function(e){

        e.preventDefault();
        var item = $('.itemAttribute').val();
        var quantity = $('#quantity').val();
        if(item === null || item ==="" ){
            $('.itemAttribute').select2('open');
            return false;
        }
        if(quantity === '' || quantity === "NaN"){
            $('#quantity').focus();
            return false;
        }
        $.ajax({
            url         : $('form#itemForm').attr( 'action' ),
            type        : $('form#itemForm').attr( 'method' ),
            data        : new FormData($('form#itemForm')[0]),
            processData : false,
            contentType : false,
            success: function(response){
                $('form#itemForm')[0].reset();
                setTimeout(jsonResult(response),100);
                $('.itemAttribute').val('').trigger("change");
               // $('.itemAttribute').select2('val', $('.itemAttribute').find(':selected').val());
                $('.size').select2();
                $('.color').select2();
                $('.brand').select2();
            }
        });
    });


    $('form#assignForm').submit(function(e){

        e.preventDefault();
        $.ajax({
            url         : $('form#assignForm').attr( 'action' ),
            type        : $('form#assignForm').attr( 'method' ),
            data        : new FormData($('form#assignForm')[0]),
            processData : false,
            contentType : false,
            success: function(response){
                $('form#assignForm')[0].reset();
                $('.approvalUser').html(response);
                $('.status').bootstrapToggle();
            }
        });
    });

    $('form#additionalItemForm').submit(function(e){

        e.preventDefault();
        var item = $('#job_requisition_additional_item_form_name').val();
        var quantity = $('#job_requisition_additional_item_form_quantity').val();

        if(item === "NaN" || item ==="" ){
            $('#name').focus();
            return false;
        }
        if(quantity === '' || quantity === "NaN"){
            $('#quantity').focus();
            return false;
        }

        $.ajax({
            url         : $('form#additionalItemForm').attr( 'action' ),
            type        : $('form#additionalItemForm').attr( 'method' ),
            data        : new FormData($('form#additionalItemForm')[0]),
            processData : false,
            contentType : false,
            success: function(response){
                obj = JSON.parse(response);
                $('#invoiceAdditionalItem').html(obj['invoiceItem']);
                $('form#additionalItemForm')[0].reset();
            }
        });
    });


    $('form#addApproval').submit(function(e){

        e.preventDefault();
        var item = $('#assignUser').val();
        if(item === "NaN" || item ==="" ){
            $('#assignUser').select2('open');
            return false;
        }
        $.ajax({
            url         : $('form#addApproval').attr( 'action' ),
            type        : $('form#addApproval').attr( 'method' ),
            data        : new FormData($('form#addApproval')[0]),
            processData : false,
            contentType : false,
            success: function(response){
                $('.status').bootstrapToggle();
                $('form#addApproval')[0].reset();
                $(".user-select").select2('open').prop('selectedIndex',0);
                $('.approvalUser').html(response);
            }
        });
    });



    $(document).on('change', '.inline-quantity', function() {

        var id = $(this).attr('data-id');
        var url = $(this).attr('data-action');
        var quantity = parseFloat($('#quantity-'+id).val());
        var price = parseFloat($('#price-'+id).val());
        var returnQuantity = parseFloat($('#returnQuantity-'+id).val());
        var subTotal = (quantity*price);
        $('#subTotal-'+id).html(subTotal);
        $.ajax({
            url: url,
            type: 'POST',
            data:'quantity='+ quantity+'&returnQuantity='+ returnQuantity+'&price='+ price,
            success: function(response) {
                obj = JSON.parse(response);
                var total = parseFloat(obj['total']).toFixed(2)
                var subTotal = parseFloat(obj['subTotal']).toFixed(2)
                var commission = parseFloat(obj['commission']).toFixed(2)
                $('#commission').html(commission);
                $('.subTotal').html(subTotal);
                $('.total').html(total);
            }
        })
    });

    $(document).on('change', '.remainItem', function() {
        var id = $(this).attr('data-id');
        var quantity = parseFloat($('#quantity-'+id).val());
        var issue = parseFloat($('#issue-'+id).val());
        var price = parseFloat($('#price-'+id).val());
        var remain = (quantity - issue);
        var subTotal = (issue * price);
        $("#remaining-"+id).html(remain);
        $("#subTotal-"+id).html(financial(subTotal));
        var sum = 0;
        $(".subTotal").each(function(){
            amount = ($(this).html() === "" || $(this).html() === "NaN") ? 0 : $(this).html();
            sum += +parseFloat(amount);
        });
        $("#total").html(financial(sum));
        $(".totalAmount").val(sum);
    });

    $(document).on('change', '.adjustAmount', function() {
        var id = $(this).attr('data-id');
        var quantity = parseFloat($('#currentAmount-'+id).val());
        var issue = parseFloat($('#adjustAmount-'+id).val());
        var remain = (quantity - issue);
        $("#remaining-"+id).html(remain);
        setTimeout(totalAmount, 1000);
    });

    /*$(document).on('form#ajaxForm').submit(function(e){
        e.preventDefault();
        $.ajax({
            url         : $('form#ajaxForm').attr( 'action' ),
            type        : $('form#ajaxForm').attr( 'method' ),
            data        : new FormData($('form#ajaxForm')[0]),
            processData : false,
            contentType : false,
            success: function(response){
                alert(response);
            }
        });
    });*/

    function totalAmount() {
        var sum = 0;
        $(".adjustAmount").each(function(){
            sum += +parseFloat($(this).val());
        });
        $("#total").html(sum);
    }


    $(document).on('click', '.ajax-reset', function() {

        var url = $(this).attr('data-action');
        $.get(url, function(data){
            location.reload();
        });
    });

    $(document).on('change', '.updatePriceQuantity', function() {

        var id = $(this).attr('data-id');
        var url = $(this).attr('data-action');
        var unitPrice = parseFloat($('#unitPrice-'+id).val());
        var quantity = parseFloat($('#quantity-'+id).val());
        var subTotal = (unitPrice * quantity);
        $("#subTotal-"+id).html(subTotal);
        $.ajax({
            url: url,
            type: 'POST',
            data:'quantity='+ quantity+"&unitPrice="+unitPrice
        })
    });

     $(document).on('change', '.updateCSItem', function() {

        var id = $(this).attr('data-id');
        var url = $(this).attr('data-action');
        var unitPrice = parseFloat($('#unitPrice-'+id).val());
        var quantity = parseFloat($('#quantity-'+id).val());
        var subTotal = (unitPrice * quantity);
        $("#subTotal-"+id).html(subTotal);
        $.ajax({
            url: url,
            type: 'POST',
            data:'quantity='+ quantity+"&unitPrice="+unitPrice
        })
    });

     $(document).on('change', '.updateCSItemAttribute', function() {

        var id = $(this).attr('data-id');
        var url = $(this).attr('data-action');
        var description = $('#attribute-'+id).val();
        $.ajax({
            url: url,
            type: 'POST',
            data:'description='+ description
        })
    });

    $(document).on('change', '.quantity', '.price', function() {

        var url = $(this).attr('data-action');
        var id = $(this).attr('data-id');
        var quantity = parseFloat($('#quantity-'+id).val());
        var price = parseFloat($('#price-'+id).val());
        var subTotal  = (quantity * price);
        $("#subTotal-"+id).html(financial(subTotal));
        $.ajax({
            url: url,
            type: 'POST',
            data:'quantity='+ quantity+'&price='+ price,
            success: function(response) {
                if(response !== "success"){
                    setTimeout(jsonResult(response),100);
                }
            }
        })
    });

    $(document).on('change', '.processQuantity', function() {

        var url = $(this).attr('data-action');
        var id = $(this).attr('data-id');
        var quantity = parseFloat($('#quantity-'+id).val());
        var price = parseFloat($('#price-'+id).val());
        var subTotal  = (quantity * price);
        $("#subTotal-"+id).html(financial(subTotal));
        $.ajax({
            url: url,
            type: 'POST',
            data:'quantity='+ quantity+'&price='+ price,
            success: function(response) {
                var sum = 0;
                $(".subTotal").each(function(){
                    sum += +parseFloat($(this).text());
                });
                $("#approveTotal").html(financial(sum));
            }
        })
    });

    $(document).on('change', '.approveQuantity', function() {

        var id = $(this).attr('data-id');
        var url = $(this).attr('data-action');
        var quantity = parseFloat($('#approveQuantity-'+id).val());
        $.ajax({
            url: url,
            type: 'POST',
            data:'quantity='+ quantity
        })
    });


    $(document).on('keypress keyup blur', '.storeRequisitionQuantity', function() {
        var id = $(this).attr('data-id');
        var quantity = parseFloat($('#quantity-'+id).val());
        var requisitionItemPrice = parseFloat($('#requisitionItemPrice-'+id).val());
        var remainingQuantity = parseFloat($('#remainingQuantity-'+id).val());
        var remin  = (remainingQuantity - quantity);
        var subTotal = quantity*requisitionItemPrice;
        $("#remainQnt-"+id).html(remin);
        $("#subTotal-"+id).html(subTotal);

        var sum = 0;
        $(".subTotal").each(function(){
            sum += +parseFloat($(this).text());
        });
        $(".grandTotal").html(sum);
    });


    $(document).on('click',".itemRemove, .itemApprovalRemove", function (event) {
        var id = $(this).attr('data-id');
        var url = $(this).attr('data-action');
        $('#confirm-content').confirmModal({
            topOffset: 0,
            top: '25%',
            onOkBut: function(el) {
                $.get(url, function( response ) {
                    $(event.target).closest('tr').hide();
                    setTimeout(jsonResult(response),100);
                });
            }
        });
    });


    $(document).on('change', '.updateWorkorderItem', function() {
        var id = $(this).attr('data-id');
        var url = $(this).attr('data-action');
        var quantity = parseFloat($('#quantity-'+id).val());
        var unitPrice = parseFloat($('#price-'+id).val());
        var subTotal = (unitPrice * quantity);
        $("#woSubTotal-"+id).html(subTotal);
        $.ajax({
            url: url,
            type: 'POST',
            data:'quantity='+ quantity+"&unitPrice="+unitPrice,
            success: function(response) {
                obj = JSON.parse(response);
                var subTotal = parseFloat(obj['subTotal']).toFixed(2)
                var total = parseFloat(obj['total']).toFixed(2)
                $('.subTotal').html(subTotal);
                $('.total').html(total);
            }
        })
    });

    $(document).on('change',".ajaxUpdate", function (event) {
        $.ajax({
            url         : $('form#requisitionForm').attr( 'data-action' ),
            type        : $('form#requisitionForm').attr( 'method' ),
            data        : new FormData($('form#requisitionForm')[0]),
            processData : false,
            contentType : false,
            success: function(response){
                obj = JSON.parse(response);
                var subTotal = parseFloat(obj['subTotal']).toFixed(2)
                var total = parseFloat(obj['total']).toFixed(2)
                $('.subTotal').html(subTotal);
                $('.total').html(total);
            }
        });

    });

    $(".select2StockItem").select2({
        ajax: {
            url: Routing.generate('inv_stock_item_autocomplete'),
            data: function (params, page) {
                return {
                    q: params,
                    page_limit: 100
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            }
        },
        placeholder: 'Search for a add stock item',
        minimumInputLength: 1,
        allowClear: true
    });

    function afterSelect2Submit(){

        $('.itemAttribute').prepend('<option selected></option>').select2({
            ajax: {
                url: Routing.generate('inv_stock_item_autocomplete'),
                data: function (params, page) {
                    return {
                        q: params,
                        page_limit: 100
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                }
            },
            placeholder: 'Search for a add stock item',
            minimumInputLength: 1,
            allowClear: true
        });

    }

    $(document).on('keydown keyup change', '.maxQuantity', function() {
        var id = $(this).attr('data-id');
        var char = parseFloat($(this).val());
        var maxLength = parseFloat($(this).attr('max'));
        if(char > maxLength){
            $(this).val(0).focus();
            return false;
        }
    });

}






