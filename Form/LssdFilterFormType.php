<?php

namespace Terminalbd\ProcurementBundle\Form;

use App\Entity\Admin\Terminal;
use App\Entity\Application\Procurement;
use App\Entity\Core\Setting;
use App\Entity\Domain\Branch;
use App\Repository\Core\SettingRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToStringTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\ProcurementBundle\Entity\OrderDelivery;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Repository\ParticularRepository;

/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class LssdFilterFormType extends AbstractType
{


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $terminal =  $options['terminal']->getId();
        $config =  $options['config']->getId();
        $builder

            ->add('startDate', DateType::class, [
                'widget' => 'single_text',
                'html5' => false,
                'required' => false,
                'mapped' => false,
                'attr' => ['class' => 'datePicker','placeholder'=>"Enter Start Date"],
            ])

            ->add('endDate', DateType::class, [
                'widget' => 'single_text',
                'html5' => false,
                'required' => false,
                'mapped' => false,
                'attr' => ['class' => 'datePicker','placeholder'=>"Enter End Date"],
            ])
            ->add('department', EntityType::class, [
                'class' => Setting::class,
                'required' => false,
                'mapped' => false,
                'query_builder' => function (EntityRepository $er) use($terminal) {
                    return $er->createQueryBuilder('e')
                        ->join('e.settingType','type')
                        ->where('e.status =1')
                        ->andWhere("type.slug ='department'")
                        ->andWhere("e.terminal ='{$terminal}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2'],
                'choice_label' => 'name',
                'placeholder' => 'Department',
            ])
            ->add('branch', EntityType::class, [
                'class' => Branch::class,
                'required' => false,
                'mapped' => false,
                'query_builder' => function (EntityRepository $er) use($terminal) {
                    return $er->createQueryBuilder('e')
                        ->where('e.status =1')
                        ->andWhere("e.terminal ='{$terminal}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2'],
                'choice_label' => 'name',
                'placeholder' => 'Branch name',
            ])
            /*->add('priority', EntityType::class, [
                'class' => Particular::class,
                'required' => false,
                'mapped' => false,
                'query_builder' => function (EntityRepository $er) use($config) {
                    return $er->createQueryBuilder('e')
                        ->join('e.particularType','type')
                        ->where('e.status =1')
                        ->andWhere("type.slug ='priority'")
                        ->andWhere("e.config ='{$config}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2'],
                'choice_label' => 'name',
                'placeholder' => 'Priority',
            ])*/

            ->add('requisitionType', EntityType::class, [
                'class' => Particular::class,
                'required' => false,
                'mapped' => false,
                'query_builder' => function (EntityRepository $er) use($config) {
                    return $er->createQueryBuilder('e')
                        ->join('e.particularType','type')
                        ->where('e.status =1')
                        ->andWhere("type.slug ='requisition-type'")
                        ->andWhere("e.config ='{$config}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2'],
                'choice_label' => 'name',
                'placeholder' => 'Requisition type',
            ])
            ->add('deliveryMethod', EntityType::class, [
                'class' => Particular::class,
                'required' => false,
                'mapped' => false,
                'query_builder' => function (EntityRepository $er) use($config) {
                    return $er->createQueryBuilder('e')
                        ->join('e.particularType','type')
                        ->where('e.status =1')
                        ->andWhere("type.slug ='delivery-mode'")
                        ->andWhere("e.config ='{$config}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2'],
                'choice_label' => 'name',
                'placeholder' => 'Delivery Mode',
            ])

            ->add('courier', EntityType::class, [
                'class' => Particular::class,
                'required' => false,
                'mapped' => false,
                'query_builder' => function (EntityRepository $er) use($config) {
                    return $er->createQueryBuilder('e')
                        ->join('e.particularType','type')
                        ->where('e.status =1')
                        ->andWhere("type.slug ='courier-service'")
                        ->andWhere("e.config ='{$config}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2'],
                'choice_label' => 'name',
                'placeholder' => 'Courier service',
            ])


            /*->add('department', EntityType::class, [
                'class' => Setting::class,
                'required' => true,
                'query_builder' => function (EntityRepository $er) use($terminal) {
                    return $er->createQueryBuilder('e')
                        ->join('e.settingType','type')
                        ->where('e.status =1')
                        ->andWhere("type.slug ='department'")
                        ->andWhere("e.terminal ='{$terminal}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a requisition priority',
            ])
            ->add('branch', EntityType::class, [
                'class' => Branch::class,
                'required' => true,
                'query_builder' => function (EntityRepository $er) use($terminal) {
                    return $er->createQueryBuilder('e')
                        ->where('e.status =1')
                        ->andWhere("e.terminal ='{$terminal}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a requisition priority',
            ])*/

            ->add('requisitionNo', TextType::class, [
                'attr' => ['autofocus' => false,'placeholder'=>"Enter requisition no"],
                'required' => false,
                'mapped' => false,
                'label' => false
            ])

            ->add('requisitionOrderNo', TextType::class, [
                'attr' => ['autofocus' => false,'placeholder'=>"Enter requisition order no"],
                'required' => false,
                'mapped' => false,
                'label' => false
            ])
            ->add('filter', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-sm  purple-bg white-font'
                ]
            ])
            ->setMethod('GET')
            ->getForm();
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class'        => OrderDelivery::class,
            'terminal'          => Terminal::class,
            'config'            => Procurement::class,
            'particularRepo'    => ParticularRepository::class,
        ]);
    }
}
