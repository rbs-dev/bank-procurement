<?php

namespace Terminalbd\ProcurementBundle\Form;

use App\Entity\Admin\Terminal;
use App\Entity\Application\Procurement;
use App\Entity\Core\Setting;
use App\Entity\Domain\Branch;
use App\Entity\Domain\Vendor;
use App\Entity\User;
use App\Repository\Core\SettingRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToStringTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\ProcurementBundle\Entity\EnlistedVendor;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\TenderVendor;
use Terminalbd\ProcurementBundle\Repository\ParticularRepository;

/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class RequisitionFilterFormType extends AbstractType
{


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $terminal =  $options['terminal']->getId();
        $config =  $options['config']->getId();
        $builder

            ->add('startDate', DateType::class, [
                'widget' => 'single_text',
                'html5' => false,
                'required' => false,
                'mapped' => false,
                'attr' => ['class' => 'datePicker','placeholder'=>"Start Date"],
            ])

            ->add('endDate', DateType::class, [
                'widget' => 'single_text',
                'html5' => false,
                'required' => false,
                'mapped' => false,
                'attr' => ['class' => 'datePicker','placeholder'=>"End Date"],
            ])

            ->add('priority', EntityType::class, [
                'class' => Particular::class,
                'required' => false,
                'query_builder' => function (EntityRepository $er) use($config) {
                    return $er->createQueryBuilder('e')
                        ->join('e.particularType','type')
                        ->where('e.status =1')
                        ->andWhere("type.slug ='priority'")
                        ->andWhere("e.config ='{$config}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2'],
                'choice_label' => 'name',
                'mapped' => false,
                'placeholder' => 'Choose a priority',
            ])

            ->add('requisitionType', EntityType::class, [
                'class' => Particular::class,
                'mapped' => false,
                'required' => false,
                'query_builder' => function (EntityRepository $er) use($config) {
                    return $er->createQueryBuilder('e')
                        ->join('e.particularType','type')
                        ->where('e.status =1')
                        ->andWhere("type.slug ='requisition-type'")
                        ->andWhere("e.config ='{$config}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a requisition type',
            ])

            ->add('department', EntityType::class, [
                'class' => Setting::class,
                'required' => false,
                'mapped' => false,
                'query_builder' => function (EntityRepository $er) use($terminal) {
                    return $er->createQueryBuilder('e')
                        ->join('e.settingType','type')
                        ->where('e.status =1')
                        ->andWhere("type.slug ='department'")
                        ->andWhere("e.terminal ='{$terminal}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a department',
            ])
            ->add('section', EntityType::class, [
                'class' => Setting::class,
                'required' => false,
                'mapped' => false,
                'query_builder' => function (EntityRepository $er) use($terminal) {
                    return $er->createQueryBuilder('e')
                        ->join('e.settingType','type')
                        ->where('e.status =1')
                        ->andWhere("type.slug ='section'")
                        ->andWhere("e.terminal ='{$terminal}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a section',
            ])
            ->add('branch', EntityType::class, [
                'class' => Branch::class,
                'required' => false,
                'mapped' => false,
                'query_builder' => function (EntityRepository $er) use($terminal) {
                    return $er->createQueryBuilder('e')
                        ->where('e.status =1')
                        ->andWhere("e.branchType ='branch'")
                        ->andWhere('e.isDelete IS NULL')
                        ->andWhere("e.terminal ='{$terminal}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a company name',
            ])
            ->add('unit', EntityType::class, [
                'class' => Branch::class,
                'required' => false,
                'group_by'  => 'parent.name',
                'choice_translation_domain' => true,
                'query_builder' => function (EntityRepository $er) use($terminal){
                    return $er->createQueryBuilder('e')
                        ->join('e.parent','p')
                        ->where("e.branchType ='branch-unit'")
                        ->andWhere('e.isDelete IS NULL')
                        ->andWhere('p.status=1')
                        ->andWhere('e.status=1')
                        ->andWhere("e.terminal ={$terminal}")
                        ->orderBy('e.code', 'ASC');
                },
                'attr'=>['class'=>'select2 input'],
                'choice_label' => 'code',
                'placeholder' => 'Company unit',
            ])
            ->add('vendor', EntityType::class, [
                'class' => Vendor::class,
                'required' => false,
                'query_builder' => function (EntityRepository $er) use($terminal) {
                    return $er->createQueryBuilder('e')
                        ->where('e.status =1')
                        ->andWhere("e.terminal ={$terminal}")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>''],
                'choice_label' => 'companyName',
                'placeholder' => 'Choose vendor for work order ',
            ])
            ->add('enlistedVendor', EntityType::class, [
                'class' => EnlistedVendor::class,
                'required' => false,
                'query_builder' => function (EntityRepository $er) use($config) {
                    return $er->createQueryBuilder('e')
                        ->where('e.status =1')
                        ->andWhere("e.config ='{$config}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>''],
                'choice_label' => 'name',
                'placeholder' => 'Choose enlisted vendor',
            ])
            ->add('createdBy', EntityType::class, [
                'class' => User::class,
                'required' => false,
                'mapped' => false,
                'query_builder' => function (EntityRepository $er) use($terminal) {
                    return $er->createQueryBuilder('e')
                        ->where('e.enabled =1')
                        ->andWhere("e.userGroup ='employee'")
                        ->andWhere("e.terminal ='{$terminal}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2 col-md-12','data-trigger' => "focus"],
                'choice_label' => 'nameWithEmployeeId',
                'placeholder' => 'Choose a created by',
            ])

            ->add('invoice', TextType::class, [
                'attr' => ['autofocus' => false,'placeholder'=>"Enter ref no"],
                'required' => false,
                'mapped' => false,
                'label' => false
            ])
            ->add('requisitionNo', TextType::class, [
                'attr' => ['autofocus' => false,'placeholder'=>"Enter requisition no"],
                'required' => false,
                'mapped' => false,
                'label' => false
            ])
             ->add('jobRequisition', TextType::class, [
                'attr' => ['autofocus' => false,'placeholder'=>"Enter job approval"],
                'required' => false,
                'mapped' => false,
                'label' => false
            ])
            ->add('orderNo', TextType::class, [
                'attr' => ['autofocus' => false,'placeholder'=>"Enter order no"],
                'required' => false,
                'mapped' => false,
                'label' => false
            ])
            ->add('buyerName', TextType::class, [
                'attr' => ['autofocus' => false,'placeholder'=>"Enter buyer name"],
                'required' => false,
                'mapped' => false,
                'label' => false
            ])
            ->add('itemName', TextType::class, [
                'attr' => ['autofocus' => false,'placeholder'=>"Enter item name"],
                'required' => false,
                'mapped' => false,
                'label' => false
            ])
            ->add('keyword', TextType::class, [
                'attr' => ['autofocus' => true,'class'=>'','placeholder'=>"Enter any keyword"],
                'required' => false,
                'mapped' => false,
            ])
            ->add('filter', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-sm  purple-bg white-font'
                ]
            ])
            ->add('process', TextType::class, [
                'attr' => ['autofocus' => true,'class'=>''],
                'required' => false,
                'mapped' => false,
            ])
            ->setMethod('GET')
            ->getForm();
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'terminal'          => Terminal::class,
            'config'            => Procurement::class,
            'particularRepo'    => ParticularRepository::class,
        ]);
    }
}
