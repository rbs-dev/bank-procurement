<?php

namespace Terminalbd\ProcurementBundle\Form;

use App\Entity\Application\Procurement;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToStringTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Repository\ParticularRepository;

/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ProcurementProcessFormType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $terminal =  $options['config']->getTerminal();
        $users =  $options['users'];
        $builder

            ->add('process', TextType::class, [
                'attr' => ['autofocus' => true,'class'=>''],
                 'required' => true,
            ])
            ->add('assignTo', EntityType::class, [
                'class' => User::class,
                'required' => true,
                'query_builder' => function (EntityRepository $er) use($terminal,$users) {
                    $qb = $er->createQueryBuilder('e');
                    $qb->leftJoin('e.profile','p');
                    $qb->where('e.enabled =1');
                    $qb->andWhere("e.userGroup ='employee'");
                    $qb->andWhere('e.approveUser =1');
                    $qb->andWhere("e.terminal ='{$terminal}'");
                    if($users){
                        $qb->andWhere($qb->expr()->notIn('e.id', ':uids'))->setParameter('uids', $users);
                    }
                    $qb->orderBy('e.name', 'ASC');
                    return $qb;
                },
                'attr'=>['class'=>'select2 col-md-9 assign-select','data-trigger' => "focus"],
                'choice_label' => 'nameWithEmployeeId',
                'placeholder' => 'Choose a assign to',
            ])
            ->add('isRejected',CheckboxType::class,[
                'required' => false,
                'attr' => [
                    'class' => 'checkboxToggle btn-sm ',
                    'data-toggle' => "toggle",
                    'data-style' => "slow",
                    'data-size' => "sm",
                    'data-offstyle' => "warning",
                    'data-onstyle'=> "success",
                    'data-on' => "Yes",
                    'data-off'=> "No"
                ],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ProcurementProcess::class,
            'config' => Procurement::class,
            'users' => 'users'
        ]);
    }
}
