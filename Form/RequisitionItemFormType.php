<?php

namespace Terminalbd\ProcurementBundle\Form;

use App\Entity\Application\GenericMaster;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\GenericBundle\Entity\Category;
use Terminalbd\GenericBundle\Entity\Item;
use Terminalbd\GenericBundle\Repository\ItemRepository;
use Terminalbd\InventoryBundle\Entity\PurchaseItem;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\SecurityBillingBundle\Entity\Particular;
use Terminalbd\SecurityBillingBundle\Entity\ParticularType;

/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class RequisitionItemFormType extends AbstractType
{


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $config =  $options['config']->getId();
        $builder

            ->add('category', EntityType::class, [
                'class' => Category::class,
                'multiple' => false,
                'mapped' => false,
                'attr'=>['class'=>'select2'],
                'placeholder' => 'Choose a category name',
                'query_builder' => function(EntityRepository $er)  use($config){
                    return $er->createQueryBuilder('e')
                        ->join('e.config','b')
                        ->where('b.id = :config')->setParameter('config', $config)
                        ->orderBy('e.name', 'ASC');
                },
            ])

            ->add('item', EntityType::class, [
                'class' => Item::class,
                'multiple' => false,
                'group_by'  => 'category.name',
                'choice_label'  => 'skuName',
                'attr'=>['class'=>'select2 item-select'],
                'placeholder' => 'Choose a item name',
                'choice_translation_domain' => true,
                'query_builder' => function(EntityRepository $er)  use($config){
                    return $er->createQueryBuilder('e')
                        ->join('e.config','b')
                        ->join('e.itemType','it')
                        ->where('b.id = :config')->setParameter('config', $config)
                       /* ->andWhere('it.slug IN (:slugs)')->setParameter('slugs', array('purchase-item'))*/
                        ->orderBy('e.name', 'ASC');
                },
            ])
            ->add('description', TextType::class, [
                'attr' => ['autofocus' => true,'class' => '','placeholder'=>"Enter Description"],
                'required' => false,
            ])
            ->add('quantity', NumberType::class, [
                'attr' => ['autofocus' => true,'class' => 'number-input','placeholder'=>"Enter quantity"],
                'required' => true,
            ])
            ->add('stockIn', NumberType::class, [
                'attr' => ['autofocus' => true,'class' => 'number-input','placeholder'=>"Enter stock in"],
                'required' => false,
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => RequisitionItem::class,
            'config' => GenericMaster::class,
        ]);
    }
}
