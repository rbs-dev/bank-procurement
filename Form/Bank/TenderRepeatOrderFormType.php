<?php

namespace Terminalbd\ProcurementBundle\Form\Bank;

use App\Entity\Admin\Terminal;
use App\Entity\Application\Procurement;
use App\Entity\Core\Setting;
use App\Entity\Domain\Vendor;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToStringTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\ProcurementBundle\Entity\JobRequisition;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementCondition;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionOrder;
use Terminalbd\ProcurementBundle\Entity\Tender;
use Terminalbd\ProcurementBundle\Entity\TenderMemo;
use Terminalbd\ProcurementBundle\Repository\ParticularRepository;

/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class TenderRepeatOrderFormType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $terminal =  $options['terminal']->getId();
        $config =  $options['config']->getId();
        $builder

            ->add('subject', TextareaType::class, [
                'attr' => ['autofocus' => true,'class'=>'textarea','rows' => 3],
                'required' => true,
                'help' => "",
            ])

            ->add('email', TextareaType::class, [
                'attr' => ['autofocus' => true, 'placeholder' => 'example@gmail.com','class'=>'textarea','rows' => 3],
                'required' => true,
                'help' => "Added email via comma ie: example@gmail.com,google@gmail.com",
            ])

            ->add('content', TextareaType::class, [
                'attr' => ['autofocus' => true,'class'=>'textarea editor','rows' => 8],
                'required' => true,
                'help' => "",
            ])
            ->add('conclusion', TextareaType::class, [
                'attr' => ['autofocus' => true,'class'=>'textarea editor','rows' => 8],
                'required' => true,
                'help' => "",
            ])
        ;


    }

    /**
     * {@inheritdoc}
     */

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TenderMemo::class,
            'terminal' => Terminal::class,
            'config' => Procurement::class,
        ]);
    }
}
