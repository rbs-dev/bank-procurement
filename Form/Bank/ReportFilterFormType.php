<?php

namespace Terminalbd\ProcurementBundle\Form\Bank;

use App\Entity\Admin\Terminal;
use App\Entity\Application\GenericMaster;
use App\Entity\Core\Setting;
use App\Entity\Domain\Branch;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Terminalbd\BudgetBundle\Entity\Head;
use Terminalbd\GenericBundle\Entity\Category;
use Terminalbd\GenericBundle\Entity\Item;
use Terminalbd\GenericBundle\Repository\CategoryRepository;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;

/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ReportFilterFormType extends AbstractType
{


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $terminal =  $options['terminal']->getId();
        $config =  $options['config']->getId();
        $builder
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'attr'=>['class'=>'select2 category2RequisitionItem'],
                'required' => false,
                'mapped' => false,
                'placeholder' => 'Choose a name',
                'choice_label' => 'nestedLabel',
                'choices'   => $options['categoryRepo']->getFlatCategoryTree($options['generic'])
            ])

            ->add('item', EntityType::class, [
                'class' => Item::class,
                'multiple' => false,
                'required' => false,
                'mapped' => false,
                'choice_label'  => 'name',
                'attr'=>['class'=>'select2 itemAttribute'],
                'placeholder' => 'Choose a item name',
                'query_builder' => function(EntityRepository $er)  use($config){
                    return $er->createQueryBuilder('e')
                        ->join('e.config','b')
                        ->where("b.status =1");
                },
            ])
            ->add('itemMode',ChoiceType::class,
                array(
                    'attr'=>array('class'=>''),
                    'required'    => false,
                    'mapped' => false,
                    'choices'  => [
                        'OPEX' => 'OPEX',
                        'CAPEX' => 'CAPEX',
                        'Others' => 'Others',
                    ],
                    'placeholder' => 'Choose a  item mode',
                )
            )
            ->add('department', EntityType::class, [
                'class' => Setting::class,
                'required' => false,
                'query_builder' => function (EntityRepository $er) use($terminal) {
                    return $er->createQueryBuilder('e')
                        ->join('e.settingType','type')
                        ->where('e.status =1')
                        ->andWhere("type.slug ='department'")
                        ->andWhere("e.terminal ='{$terminal}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2 input'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a department name',
            ])
            ->add('section', EntityType::class, [
                'class' => Setting::class,
                'required' => false,
                'mapped' => false,
                'query_builder' => function (EntityRepository $er) use($terminal) {
                    return $er->createQueryBuilder('e')
                        ->join('e.settingType','type')
                        ->where('e.status =1')
                        ->andWhere("type.slug ='section'")
                        ->andWhere("e.terminal ='{$terminal}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2 col-md-12 input'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a requisition section',
            ])
            ->add('companyUnit', EntityType::class, [
                'class' => Branch::class,
                'required' => false,
                'mapped' => false,
                'group_by'  => 'customer.name',
                'choice_translation_domain' => true,
                'query_builder' => function (EntityRepository $er)  use($terminal){
                    return $er->createQueryBuilder('e')
                        ->where("e.branchType ='branch-unit'")
                        ->andWhere('e.isDelete IS NULL')
                        ->andWhere("e.terminal ={$terminal}")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'input'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a company unit',
            ])
            ->add('company', EntityType::class, [
                'class' => Branch::class,
                'required' => false,
                'query_builder' => function (EntityRepository $er) use($terminal) {
                    return $er->createQueryBuilder('e')
                        ->where('e.status =1')
                        ->andWhere("e.branchType ='branch'")
                        ->andWhere("e.terminal ='{$terminal}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>''],
                'choice_label' => 'name',
                'placeholder' => 'Choose a company name',
            ])
            ->add('wearhouse', EntityType::class, [
                'class' => Branch::class,
                'required' => false,
                'group_by'  => 'parent.name',
                'choice_translation_domain' => true,
                'query_builder' => function (EntityRepository $er) use($terminal) {
                    return $er->createQueryBuilder('e')
                        ->where('e.status =1')
                        ->andWhere("e.branchType ='sub-branch'")
                        ->andWhere("e.terminal ='{$terminal}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'ajaxUpdate'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a wear house name',
            ])

            ->add('startDate', DateType::class, [
                'widget' => 'single_text',
                'html5' => false,
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'class' => 'datePicker',
                    'placeholder'=>'From Date',
                    'autocomplete' => 'off'
                ],
            ])

            ->add('endDate', DateType::class, [
                'widget' => 'single_text',
                'html5' => false,
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'class' => 'datePicker',
                    'placeholder'=>'To Date',
                    'autocomplete' => 'off'
                ],
            ])
            ->add('requisitionType', EntityType::class, [
                'class' => Particular::class,
                'required' => false,
                'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('e')
                        ->where('e.status = 1')
                        ->orderBy('e.name', 'ASC');
                },
                'choice_label' => 'name',
                'attr' => [
                    'class' => 'select2',
                ],
                'placeholder' => 'Select Type',
            ])
            ->add('filter', SubmitType::class, [
                'attr' => [
                    'class' => 'btn purple-bg white-font'
                ]
            ])
            ->setMethod('GET')
        ;

        $builder->get('category')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event){
                $category = $event->getForm()->getData();
                if(!empty($category)){
                    $category = $event->getForm()->getData()->getId();
                    $this->addItemField($event->getForm()->getParent(), $category);
                }
            }

        );
    }

    public  function addItemField(FormInterface $form, $category) {

        $form->add('item', EntityType::class, [
            'class' => Item::class,
            'required' => false,
            'mapped' => false,
            'query_builder' => function (EntityRepository $er)  use($category) {
                return $er->createQueryBuilder('e')
                    ->join('e.category','c')
                    ->where("c.id ={$category}")
                    ->orderBy('e.name', 'ASC');
            },
            'attr'=>['class'=>'select2'],
            'choice_label' => 'name',
            'placeholder' => 'Choose a item name',
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'terminal'          => Terminal::class,
            'config'            => Procurement::class,
            'categoryRepo'      => CategoryRepository::class,
            'generic'           => GenericMaster::class,
        ]);
    }
}
