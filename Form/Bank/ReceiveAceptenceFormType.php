<?php

namespace Terminalbd\ProcurementBundle\Form\Bank;

use App\Entity\Admin\Terminal;
use App\Entity\Application\Procurement;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToStringTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\InventoryBundle\Entity\Purchase;
use Terminalbd\ProcurementBundle\Entity\JobRequisition;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderReceive;
use Terminalbd\ProcurementBundle\Repository\ParticularRepository;
use Terminalbd\ProcurementBundle\Repository\TenderWorkorderReceiveRepository;

/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ReceiveAceptenceFormType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder

            ->add('acceptence', TextareaType::class, [
                'attr' => ['autofocus' => true,'class'=>'textarea','rows' => 4],
                 'required' => true,
                 'mapped' => true,
            ])
            ->add('file', FileType::class, [
                'required' => false,
                'attr'=>['class'=>'custom-file-input'],
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'application/pdf',
                            'application/x-pdf',
                            'image/png',
                            'image/jpeg',
                            'image/jpg',
                            'image/gif'
                        ],
                        'mimeTypesMessage' => 'Please upload a valid PDF document',
                    ])
                ],
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TenderWorkorderReceive::class
        ]);
    }
}
