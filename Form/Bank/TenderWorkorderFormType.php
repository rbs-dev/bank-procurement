<?php

namespace Terminalbd\ProcurementBundle\Form\Bank;

use App\Entity\Application\Procurement;
use App\Entity\Core\Setting;
use App\Entity\Domain\Branch;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToStringTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\ProcurementBundle\Entity\EnlistedVendor;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementCondition;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\TenderComparative;
use Terminalbd\ProcurementBundle\Entity\TenderMemo;
use Terminalbd\ProcurementBundle\Entity\TenderVendor;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorder;
use Terminalbd\ProcurementBundle\Repository\ParticularRepository;
use Terminalbd\ProcurementBundle\Repository\TenderRepository;

/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class TenderWorkorderFormType extends AbstractType
{


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $workorder =  $options['workorder'];
        $memo = $workorder->getManagementMemo();
        $requisition = '';
        if($workorder->getRequisition()){
            $requisition = $workorder->getRequisition()->getId();
        }
        $vendor = '';
        if($workorder->getTenderVendor()){
            $vendor = $workorder->getTenderVendor()->getId();
        }
        $config =  $options['config']->getId();
        $terminal =  $options['config']->getTerminal();
        $tender =  $options['tenderComparative']->getTender()->getId();
        $builder

             ->add('attendTo', TextType::class, [
                'attr' => ['autofocus' => true,'class'=>''],
                 'required' => false,
            ])
            ->add('subject', TextType::class, [
                'attr' => ['autofocus' => true,'class'=>''],
                 'required' => true,
            ])
            ->add('content', TextareaType::class, [
                'attr' => ['autofocus' => true,'class'=>'editor'],
                 'required' => false,
            ])
            ->add('footerContent', TextareaType::class, [
                'attr' => ['autofocus' => true,'class'=>'editor'],
                 'required' => false,
            ])
            ->add('discount', TextType::class, [
                'attr' => ['autofocus' => true,'class'=>'number'],
                'required' => false,
            ])
            ->add('vat', TextType::class, [
                'attr' => ['autofocus' => true,'class'=>'number'],
                'required' => false,
            ])
            ->add('workorderDate', DateType::class, [
                'widget' => 'single_text',
                'html5' => true,
                'required' => true,
                'attr' => ['class' => '','min' => date('Y-m-d'),'placeholder'=>"Workorder Date"],
            ])
            ->add('validateDate', DateType::class, [
                'widget' => 'single_text',
                'html5' => true,
                'required' => false,
                'attr' => ['class' => '','min' => date('Y-m-d'),'placeholder'=>"Validate Date"],
            ])
            ->add('tenderVendor', ChoiceType::class, [
                'multiple' => false,
                'required' => true,
                'data' => "{$vendor}",
                'attr'=>['class'=>'vendor'],
                'mapped' => false,
                'placeholder' => 'Choose vendor for work order',
                'choices'   => $this->getApprovedVendorList($memo)
            ])
            ->add('requisition', ChoiceType::class, [
                'multiple' => false,
                'required' => true,
                'data' => "{$requisition}",
                'attr'=>['class'=>'requisition'],
                'mapped' => false,
                'placeholder' => 'Choose a requisition',
                'choices'   => $options['tenderRepo']->getTenderRequisitions($tender,$workorder)
            ])
            ->add('paymentMode', EntityType::class, [
                'class' => Particular::class,
                'required' => true,
                'query_builder' => function (EntityRepository $er) use($config) {
                    return $er->createQueryBuilder('e')
                        ->join('e.particularType','type')
                        ->where('e.status =1')
                        ->andWhere("type.slug ='payment-mode'")
                        ->andWhere("e.config ='{$config}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a payment mode',
            ])
            ->add('condition', EntityType::class, [
                'class' => ProcurementCondition::class,
                'required' => false,
                'query_builder' => function (EntityRepository $er) use($config) {
                    return $er->createQueryBuilder('e')
                        ->where('e.status =1')
                        ->andWhere("e.config ='{$config}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'condition col-md-12'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a template name',
            ])
            ->add('deliveryMode', EntityType::class, [
                'class' => Particular::class,
                'required' => true,
                'query_builder' => function (EntityRepository $er) use($config) {
                    return $er->createQueryBuilder('e')
                        ->join('e.particularType','type')
                        ->where('e.status =1')
                        ->andWhere("type.slug ='delivery-mode'")
                        ->andWhere("e.config ='{$config}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a delivery mode',
            ])
            ->add('partialDelivery',CheckboxType::class,[
                'required' => false,
                'label' => "Partial Delivery",
                'attr' => [
                    'class' => 'checkboxToggle',
                    'data-toggle' => "toggle",
                    'data-style' => "slow",
                    'data-offstyle' => "warning",
                    'data-onstyle'=> "info",
                    'data-on' => "Enabled",
                    'data-off'=> "Disabled"
                ],
            ])
            ->add('partialPayment',CheckboxType::class,[
                'required' => false,
                'label' => "Partial Payment",
                'attr' => [
                    'class' => 'checkboxToggle',
                    'data-toggle' => "toggle",
                    'data-style' => "slow",
                    'data-offstyle' => "warning",
                    'data-onstyle'=> "info",
                    'data-on' => "Enabled",
                    'data-off'=> "Disabled"
                ],
            ])

        ;
    }
    
    private function getApprovedVendorList(TenderMemo $memo)
    {
        $array = array();
        foreach ($memo->getApprovedVendor() as $memo){
            $array[$memo->getName()] =  $memo->getId();
        }
        return $array;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TenderWorkorder::class,
            'config' => Procurement::class,
            'tenderComparative' => TenderComparative::class,
            'workorder' => TenderWorkorder::class,
            'tenderRepo' => TenderRepository::class,
        ]);

    }
}
