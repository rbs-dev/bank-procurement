<?php

namespace Terminalbd\ProcurementBundle\Form\Bank;

use App\Entity\Admin\Terminal;
use App\Entity\Application\Procurement;
use App\Entity\Domain\Vendor;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToStringTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\ProcurementBundle\Entity\JobRequisition;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementCondition;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionOrder;
use Terminalbd\ProcurementBundle\Entity\Tender;
use Terminalbd\ProcurementBundle\Entity\TenderCommittee;
use Terminalbd\ProcurementBundle\Entity\TenderMemo;
use Terminalbd\ProcurementBundle\Repository\ParticularRepository;

/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class TenderCommitteeFormType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $terminal =  $options['terminal']->getId();
        $config =  $options['config']->getId();
        $process =  $options['process'];
        $builder
            ->add('subject', TextareaType::class, [
                'attr' => ['autofocus' => true,'class'=>'input textarea','rows' => 2],
                'required' => true,
                'help' => "",
            ])
            ->add('email', TextareaType::class, [
                'attr' => ['autofocus' => true, 'placeholder' => 'example@gmail.com','class'=>'input textarea','rows' => 3],
                'required' => false,
                'help' => "Added email via comma ie: example@gmail.com,google@gmail.com",
            ])
            ->add('expectedDate', DateType::class, [
                'widget' => 'single_text',
                'html5' => true,
                'required' => true,
                'attr' => ['class' => 'input', 'min' => date('Y-m-d'),'placeholder'=>"Expected Date",'data-trigger' => "focus"],
            ])
            ->add('expectedTime', TextType::class, [
                'required' => true,
                'attr' => ['autofocus' => true, 'placeholder' => '','class'=>'input timePicker'],

            ])
            ->add('content', TextareaType::class, [
                'attr' => ['autofocus' => true,'class'=>'input textarea editor','rows' => 8],
                'required' => false,
                'help' => "",
            ])
            ->add('mettingModaretor', EntityType::class, [
                'class' => User::class,
                'required' => true,
               // 'group_by'  => 'department.name',
               // 'choice_translation_domain' => true,
                'query_builder' => function (EntityRepository $er) use($terminal) {
                    return $er->createQueryBuilder('e')
                        ->join('e.profile','p')
                        ->join('p.department','d')
                        ->where('e.enabled =1')
                        ->andWhere("e.terminal ='{$terminal}'")
                        ->orderBy('e.name', 'ASC');
                },
                'choice_label' => 'nameWithEmployeeId',
                'attr'=>['class'=>'select2 input'],
                'placeholder' => 'Choose a approve user',
            ])
            ->add('parent', EntityType::class, [
                'class' => TenderCommittee::class,
                'required' => false,
                'query_builder' => function (EntityRepository $er) use($config,$process)  {
                    return $er->createQueryBuilder('e')
                        ->where('e.status =1')
                        ->andWhere("e.process ='Closed'")
                        ->andWhere("e.module ='{$process}'")
                        ->andWhere("e.config ='{$config}'")
                        ->orderBy('e.subject', 'ASC');
                },
                'attr'=>['class'=>'input col-md-12'],
                'choice_label' => 'oldMeeting',
                'placeholder' => 'Choose a parent tender meeting',
            ])
            ->add('condition', EntityType::class, [
                'class' => ProcurementCondition::class,
                'required' => false,
                'query_builder' => function (EntityRepository $er) use($config) {
                    return $er->createQueryBuilder('e')
                        ->where('e.status =1')
                        ->andWhere("e.config ='{$config}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'condition col-md-12'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a template name',
            ])


        ;
    }

    /**
     * {@inheritdoc}
     */

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TenderCommittee::class,
            'terminal' => Terminal::class,
            'config' => Procurement::class,
            'process' => '',
        ]);
    }
}
