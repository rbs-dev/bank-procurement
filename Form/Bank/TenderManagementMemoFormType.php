<?php

namespace Terminalbd\ProcurementBundle\Form\Bank;

use App\Entity\Admin\Terminal;
use App\Entity\Application\Procurement;
use App\Entity\Domain\Vendor;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToStringTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\ProcurementBundle\Entity\JobRequisition;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementCondition;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionOrder;
use Terminalbd\ProcurementBundle\Entity\Tender;
use Terminalbd\ProcurementBundle\Entity\TenderCommittee;
use Terminalbd\ProcurementBundle\Entity\TenderComparative;
use Terminalbd\ProcurementBundle\Entity\TenderMemo;
use Terminalbd\ProcurementBundle\Entity\TenderVendor;
use Terminalbd\ProcurementBundle\Repository\ParticularRepository;

/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class TenderManagementMemoFormType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $terminal =  $options['terminal']->getId();
        $config =  $options['config']->getId();
        $tender =   $options['tenderComparative']->getTender()->getId();
        $builder
            ->add('subject', TextareaType::class, [
                'attr' => ['autofocus' => true,'class'=>'textarea input','rows' => 2],
                'required' => true,
            ])
             ->add('recommendationt', TextareaType::class, [
                'attr' => ['autofocus' => true,'class'=>'textarea input','rows' => 2],
                'required' => false,
            ])

            ->add('condition', EntityType::class, [
                'class' => ProcurementCondition::class,
                'required' => false,
                'query_builder' => function (EntityRepository $er) use($config) {
                    return $er->createQueryBuilder('e')
                        ->where('e.status =1')
                        ->andWhere("e.config ='{$config}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'condition input col-md-12'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a template name',
            ])

            ->add('approvedVendor', EntityType::class, [
                'class' => TenderVendor::class,
                'multiple' => true,
                'choice_label'  => 'name',
                'attr'=>['class'=>'select2 input'],
                'placeholder' => 'Choose a item name',
                'query_builder' => function(EntityRepository $er)  use($tender){
                    return $er->createQueryBuilder('e')
                        ->where('e.tender = :tender')->setParameter('tender', $tender)
                        ->andWhere('e.subTotal > 0')
                        ->orderBy('e.name', 'ASC');
                },
            ])

            ->add('footerContent', TextareaType::class, [
                'attr' => ['autofocus' => true,'class'=>'textarea editor input','rows' => 8],
                'required' => false,
            ])
            
            ->add('content', TextareaType::class, [
                'attr' => ['autofocus' => true,'class'=>'textarea editor input','rows' => 8],
                'required' => false,
                'help' => "",
            ]);
    }

    /**
     * {@inheritdoc}
     */

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TenderMemo::class,
            'terminal' => Terminal::class,
            'config' => Procurement::class,
            'tenderComparative' => TenderComparative::class,
        ]);
    }
}
