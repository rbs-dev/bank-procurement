<?php

namespace Terminalbd\ProcurementBundle\Form\Bank;

use App\Entity\Application\GenericMaster;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\GenericBundle\Entity\Item;
use Terminalbd\GenericBundle\Repository\ItemRepository;
use Terminalbd\InventoryBundle\Entity\PurchaseItem;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\SecurityBillingBundle\Entity\Particular;
use Terminalbd\SecurityBillingBundle\Entity\ParticularType;

/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class RequisitionItemFormType extends AbstractType
{


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $config =  $options['config']->getId();
        $builder

            ->add('item', EntityType::class, [
                'class' => Item::class,
                'multiple' => false,
                'group_by'  => 'category.name',
                'choice_label'  => 'skuName',
                'attr'=>['class'=>'select2 item-select'],
                'placeholder' => 'Choose a item name',
                'choice_translation_domain' => true,
                'query_builder' => function(EntityRepository $er)  use($config){
                    return $er->createQueryBuilder('e')
                        ->join('e.config','b')
                       // ->join('e.itemType','it')
                        ->where('b.id = :config')->setParameter('config', $config)
                       /* ->andWhere('it.slug IN (:slugs)')->setParameter('slugs', array('purchase-item'))*/
                        ->orderBy('e.name', 'ASC');
                },
            ])
            ->add('description', TextType::class, [
                'attr' => ['autofocus' => true,'class' => '','placeholder'=>"Enter item description"],
                'required' => false,
            ])
            ->add('warranty', TextType::class, [
                'attr' => ['autofocus' => true,'class' => '','placeholder'=>"Enter item warranty"],
                'required' => false,
            ])
            ->add('quantity', NumberType::class, [
                'attr' => ['autofocus' => true,'class' => 'number numeric number-input','data-toggle' => "tooltip",'title' => "Enter quantity",'placeholder'=>"Quantity"],
                'required' => true,
            ])
            ->add('stockIn', NumberType::class, [
                'attr' => ['autofocus' => true,'class' => 'number numeric number-input','data-toggle' => "tooltip",'title' => "Enter present stock",'placeholder'=>"Present stock"],
                'required' => false,
            ])
            ->add('repairCost', TextType::class, [
                'attr' => ['autofocus' => true,'class' => 'number numeric','data-toggle' => "tooltip",'title' => "Enter repair cost",'placeholder'=>"Enter repair cost"],
                'required' => false,
            ])
            ->add('presentBookValue', TextType::class, [
                'attr' => ['autofocus' => true,'class' => 'number numeric','data-toggle' => "tooltip",'title' => "Enter present book value",'placeholder'=>"Enter present book value"],
                'required' => false,
            ])
            ->add('previousPurchasePrice', TextType::class, [
                'attr' => ['autofocus' => true,'class' => 'number numeric','data-toggle' => "tooltip",'title' => "Enter Previous purchase price",'placeholder'=>"Previous purchase price"],
                'required' => false,
            ])

            ->add('previousPurchaseDate', DateType::class, [
                'widget' => 'single_text',
                'html5' => true,
                'required' => false,
                'attr' => ['class' => '','data-toggle' => "tooltip",'title' => "Enter Previous purchase date",'placeholder'=>"Purchase Date",'data-trigger' => "focus"],
            ])

            ->add('lastServiceDate', DateType::class, [
                'widget' => 'single_text',
                'html5' => true,
                'required' => false,
                'attr' => ['class' => '','placeholder'=>"Service Date",'data-toggle' => "tooltip",'title' => "Enter service date",'data-trigger' => "focus"],
            ])

            ->add('file', FileType::class, [
                'required' => false,
                'attr'=>['class'=>'custom-file-input'],
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'application/pdf',
                            'application/x-pdf',
                            'image/png',
                            'image/jpeg',
                            'image/jpg',
                            'image/gif'
                        ],
                        'mimeTypesMessage' => 'Please upload a valid PDF document',
                    ])
                ],
            ])

        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => RequisitionItem::class,
            'config' => GenericMaster::class,
        ]);
    }
}
