<?php

namespace Terminalbd\ProcurementBundle\Form;

use App\Entity\Application\Procurement;
use App\Entity\Core\Setting;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\ProcurementBundle\Entity\EnlistedVendor;
use Terminalbd\ProcurementBundle\Entity\JobRequisition;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ParticularType;
use Terminalbd\ProcurementBundle\Entity\ProcurementCondition;
use Terminalbd\ProcurementBundle\Repository\ParticularRepository;


/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ConditionFormType extends AbstractType
{


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $terminal =  $options['config']->getTerminal();
        $config =  $options['config']->getId();
        $builder
            /*->add('conditionType', EntityType::class, [
                'class' => Particular::class,
                'required' => true,
                'query_builder' => function (EntityRepository $er) use($config) {
                    return $er->createQueryBuilder('e')
                        ->join('e.particularType','type')
                        ->where('e.status =1')
                        ->andWhere("type.slug ='condition'")
                        ->andWhere("e.config ='{$config}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a condition type',
            ])*/
            /*->add('department', EntityType::class, [
                'class' => Setting::class,
                'required' => true,
                'query_builder' => function (EntityRepository $er) use($terminal) {
                    return $er->createQueryBuilder('e')
                        ->join('e.settingType','type')
                        ->where('e.status =1')
                        ->andWhere("type.slug ='department'")
                        ->andWhere("e.terminal ='{$terminal}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a department name',
            ])*/
                
            ->add('name', TextType::class, [
                'attr' => ['autofocus' => true,'class'=>''],
                 'required' => true,

            ])
            ->add('subject', TextType::class, [
                'attr' => ['autofocus' => true,'class'=>''],
                 'required' => false,

            ])
            ->add('description', TextareaType::class, [
                'attr' => ['autofocus' => true,'class'=>'textarea editor','rows'=>8],
                 'required' => false,

            ])
            ->add('body', TextareaType::class, [
                'attr' => ['autofocus' => true,'class'=>'textarea editor','rows'=>8],
                 'required' => false,

            ])
            ->add('ccEmail', TextareaType::class, [
                'attr' => ['autofocus' => true,'class'=>''],
                 'required' => false,

            ])
            ->add('status',CheckboxType::class,[
                'required' => false,
                'attr' => [
                    'class' => 'checkboxToggle',
                    'data-toggle' => "toggle",
                    'data-style' => "slow",
                    'data-offstyle' => "warning",
                    'data-onstyle'=> "info",
                    'data-on' => "Enabled",
                    'data-off'=> "Disabled"
                ],
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ProcurementCondition::class,
            'config' => Procurement::class,
        ]);
    }

}
