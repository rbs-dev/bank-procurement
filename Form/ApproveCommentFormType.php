<?php

namespace Terminalbd\ProcurementBundle\Form;

use App\Entity\Application\Procurement;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToStringTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\ProcurementBundle\Entity\JobRequisition;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Repository\ParticularRepository;

/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ApproveCommentFormType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            /*->add('reject', ChoiceType::class, [
                'choices' => [
                    'Approve' => 'approve',
                    'Reject' => 'reject',
                ],
                'expanded' => true,
                'mapped' => false,
                'data' => 'approve',
                'attr' => [
                    'class' => 'form-check-inline'
                ]

            ])*/
            ->add('comment', TextareaType::class, [
                'attr' => ['autofocus' => true,'class'=>'textarea','rows' => 4],
                 'required' => true,
                 'mapped' => false,
            ]);
    }

    /**
     * {@inheritdoc}
     */

    public function configureOptions(OptionsResolver $resolver): void
    {

    }
}
